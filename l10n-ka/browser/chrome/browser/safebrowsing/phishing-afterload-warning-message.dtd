<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "უკან დაბრუნება">
<!ENTITY safeb.palm.seedetails.label "ვრცლად">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "ეს თაღლითური საიტი არაა…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "ე">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "ინფორმაციის მომწოდებელი: <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "საიტზე გადასვლამ, შესაძლოა ავნოს თქვენს კომპიუტერს">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName;-მა შეზღუდა ეს გვერდი, რადგან ცდილობდა მავნე პროგრამის დაყენებას, რომელსაც შეეძლო მოეპარა ან გაენადგურებინა პირადი მონაცემები თქვენს კომპიუტერში.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> მიჩნეულია <a id='error_desc_link'>მავნე პროგრამების გამავრცელებლად</a>. თუ ფიქრობთ, რომ არასწორია ეს ინფორმაცია, შეგიძლიათ <a id='report_detection'>მოგვახსენოთ ამის შესახებ</a>, ან <a id='ignore_warning_link'>უგულებელყოთ ეს გაფრთხილება</a> და მაინც გადახვიდეთ ამ სახიფათო საიტზე.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> მიჩნეულია <a id='error_desc_link'>მავნე პროგრამების გამავრცელებლად</a>. თუ ფიქრობთ, რომ არასწორია ეს ინფორმაცია, შეგიძლიათ <a id='report_detection'>მოგვახსენოთ ამის შესახებ</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "შეიტყვეთ მეტი სახიფათო ვებშიგთავსის შესახებ, როგორიცაა ვირუსები, მავნე კოდი და გაეცანით მისგან თავდაცვის გზებს ბმულზე <a id='learn_more_link'>StopBadware.org</a>. დამატებით იხილეთ &brandShortName;-ის თაღლითური და მავნე გვერდებისგან დაცვის შესახებ ინფორმაცია საიტზე <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "ეს საიტი შესაძლოა მავნე პროგრამებს შეიცავდეს">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName;-მა შეზღუდა ეს გვერდი, რადგან შესაძლოა მოტყუებით დაგაყენებინოთ პროგრამა, რომელიც ხელყოფს თქვენს ბრაუზერს (მაგალითად, შეგიცვლით საწყის გვერდს, ამოგიგდებთ დამატებით რეკლამებს საიტებზე და ა.შ.).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> მიჩნეულია <a id='error_desc_link'>საზიანო პროგრამების გამავრცელებლად</a>. შეგიძლიათ <a id='ignore_warning_link'>უგულებელყოთ ეს გაფრთხილება</a> და მაინც გადახვიდეთ ამ სახიფათო საიტზე.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> მიჩნეულია <a id='error_desc_link'>მავნე პროგრამების გამავრცელებლად</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "გაეცანით საზიანო და არასასურველ პროგრამებზე ინფორმაციას <a id='learn_more_link'>არასასურველი პროგრამების დებულებაში</a>. იხილეთ ვრცლად, &brandShortName;-ის ყალბი და მავნე საიტებისგან დაცვის შესახებ ბმულზე <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "თაღლითური საიტი">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName;-მა შეზღუდა ეს გვერდი, რადგან შესაძლოა მოტყუებით დაგაყენებინოთ პროგრამა ან ხელყოს თქვენი პირადი მონაცემები, როგორიცაა პაროლები ან საკრედიტო ბარათების ნომრები.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> მიჩნეულია <a id='error_desc_link'>თაღლითურ საიტად</a>. თუ ფიქრობთ, რომ ეს არასწორი ინფორმაციაა, შეგიძლიათ <a id='report_detection'>მოგვახსენოთ ამის შესახებ</a>, ან <a id='ignore_warning_link'>უგულებელყოთ</a> ეს გაფრთხილება და მაინც გადახვიდეთ სახიფათო საიტზე.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> მიჩნეულია <a id='error_desc_link'>თაღლითურ საიტად</a>. თუ ფიქრობთ, რომ ეს ინფორმაცია არასწორია, შეგიძლიათ <a id='report_detection'>მოგვახსენოთ ამის შესახებ</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "იხილეთ ვრცლად, თაღლითური და ყალბი საიტების შესახებ ბმულზე <a id='learn_more_link'>www.antiphishing.org</a>. გაეცანით ინფორმაციას &brandShortName;-ის ყალბი და სახიფათო გვერდებისგან დაცვის შესახებ საიტზე <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "ეს საიტი შესაძლოა მავნე კოდს შეიცავდეს">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName;-მა შეზღუდა ეს გვერდი, რადგან შესაძლოა სახიფათო პროგრამას აყენებდეს და იპარავდეს ან აზიანებდეს თქვენს პირად მონაცემებს (როგორიცაა ფოტოები, პაროლები, შეტყობინებები და საკრედიტო ბარათები).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> მიჩნეულია <a id='error_desc_link'>საზიანო პროგრამების გამავრცელებლად</a>. შეგიძლიათ <a id='ignore_warning_link'>უგულებელყოთ ეს გაფრთხილება</a> და მაინც გადახვიდეთ ამ სახიფათო საიტზე.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> მიჩნეულია <a id='error_desc_link'>საზიანო პროგრამების გამავრცელებლად</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "იხილეთ ვრცლად, &brandShortName;-ის ყალბი და სახიფათო გვერდებისგან დაცვის შესახებ საიტზე <a id='firefox_support'>support.mozilla.org</a>.">

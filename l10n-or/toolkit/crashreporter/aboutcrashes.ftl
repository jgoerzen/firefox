# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

clear-all-reports-label = ସମସ୍ତ ବିବରଣୀଗୁଡ଼ିକୁ କାଢ଼ିଦିଅନ୍ତୁ
delete-confirm-title = ଆପଣ ନିଶ୍ଚିତ କି?
delete-confirm-description = ଏହା ସମସ୍ତ ବିବରଣୀକୁ ଅପସାରଣ କରିଦେବ ଏବଂ ପଦକ୍ଷେପ ବାତିଲ କରିହେବ ନାହିଁ.
id-heading = ବିବରଣୀ ପରିଚୟ
crashes-submitted-label = ଦାଖଲ କରାଯାଇଥିବା ଅଚଳ ବିବରଣୀ
date-submitted-heading = ଦାଖଲ କରାଯାଇଥିବା ତାରିଖ
no-reports-label = କୌଣସି ଅଚଳ ବିବରଣୀ ଦାଖଲ ହୋଇନାହିଁ.
no-config-label = ଏହି ପ୍ରୟୋଗଟି  ଅଚଳ ବିବରଣୀ ଦାଖଲ କରିବାକୁ ବିନ୍ୟାସିତ ହୋଇନାହିଁ. <code>breakpad.reportURL</code> ପସନ୍ଦକୁ ସେଟକରିବା ଉଚିତ.

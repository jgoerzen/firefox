### This file contains the entities needed for the 'edit' menu
### It's currently only used for the Browser Console

editmenu-undo =
    .label = Rückgängig
    .accesskey = R

editmenu-redo =
    .label = Wiederherstellen
    .accesskey = W

editmenu-cut =
    .label = Ausschneiden
    .accesskey = A

editmenu-copy =
    .label = Kopieren
    .accesskey = K

editmenu-paste =
    .label = Einfügen
    .accesskey = E

editmenu-delete =
    .label = Löschen
    .accesskey = L

editmenu-select-all =
    .label = Alles markieren
    .accesskey = m

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Symud i Fyny
    .accesskey = F
languages-customize-movedown =
    .label = Symud i Lawr
    .accesskey = L
languages-customize-remove =
    .label = Tynnu
    .accesskey = T
languages-customize-select-language =
    .placeholder = Dewis iaith i'w hychwanegu…
languages-customize-add =
    .label = Ychwanegu
    .accesskey = Y
messenger-languages-window =
    .title = Gosodiadau Iaith { -brand-short-name }
    .style = width: 40em
messenger-languages-description = Bydd { -brand-short-name } yn dangos yr iaith gyntaf fel eich rhagosodedig a bydd yn danogs ieithoedd eraill yn y drefn fyddan nhw'n ymddangos.

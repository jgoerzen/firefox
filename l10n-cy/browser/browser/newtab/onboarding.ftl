# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Rhowch brawf arno nawr
onboarding-welcome-header = Croeso i { -brand-short-name }
onboarding-start-browsing-button-label = Cychwyn Pori

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Pori Preifat
onboarding-private-browsing-text = Pori ar eich pen eich hun. Mae Pori Preifat gyda Rhwystro Cynnwys yn rhwystro tracwyr ar-lein sy'n eich dilyn o amgylch y we.
onboarding-screenshots-title = Screenshots
onboarding-screenshots-text = Cymrwch luniau sgrin, eu cadw a'u rhannu - heb adael { -brand-short-name }. Cipiwch adran neu dudalen gyfan wrth i chi bori, Yna cadwch nhw i'r we ar gyfer mynediad a rhannu hawdd.
onboarding-addons-title = Ychwanegion
onboarding-addons-text = Ychwnaegwch ragor o nodweddion sy'n gwneud i { -brand-short-name } weithio'n galetach ar eich cyfer. Cymharu prisiau, gwybod am y tywydd neu mynegu eich hun gyda thema gyfaddas.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Porwch yn gynt, yn glyfrach neu yn fwy diogel gydag estyniadau fel Ghostery, cyn gadael i chi rwystro hysbysebion trafferthus.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Rhestrau Rhwystro
    .style = width: 55em
blocklist-desc = Gallwch ddewis pa restr fydd { -brand-short-name } yn ei ddefnyddio i rwystro elfennau Gwe gall fod yn tracio eich gweithgaredd pori.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Rhestr
blocklist-button-cancel =
    .label = Diddymu
    .accesskey = D
blocklist-button-ok =
    .label = Cadw Newidiadau
    .accesskey = N
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Diogelwch sylfaenol Disconnect.me (Argymhellir).
blocklist-item-moz-std-desc = Caniatáu rhai tracwyr fel bod gwefannau'n gweithio'n iawn.
blocklist-item-moz-full-name = Diogelwch caeth Disconnect.me.
blocklist-item-moz-full-desc = Rhwystro tracwyr hysbys. Efallai na fydd rhai gwefannau'n gweithio'n iawn.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Responsive Design Mode,
# available from the Web Developer sub-menu -> 'Responsive Design Mode'.
#
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (responsive.editDeviceList): option displayed in the device
# selector
responsive.editDeviceList=Golygu'r rhestr…

# LOCALIZATION NOTE (responsive.editDeviceList2): Context menu item displayed in the
# device selector.
responsive.editDeviceList2=Golygu Rhestr…

# LOCALIZATION NOTE (responsive.exit): Tooltip text of the exit button.
responsive.exit=Cau'r Modd Cynllunio Ymatebol

# LOCALIZATION NOTE (responsive.rotate): Tooltip text of the rotate button.
responsive.rotate=Cylchdroi'r port golwg

# LOCALIZATION NOTE (responsive.deviceListLoading): placeholder text for
# device selector when it's still fetching devices
responsive.deviceListLoading=Llwytho…

# LOCALIZATION NOTE (responsive.deviceListError): placeholder text for
# device selector when an error occurred
responsive.deviceListError=Dim rhestr ar gael

# LOCALIZATION NOTE (responsive.done): Button text in the device list modal
responsive.done=Gorffen

# LOCALIZATION NOTE (responsive.noDeviceSelected): placeholder text for the
# device selector
responsive.noDeviceSelected=heb ddewis dyfais

# LOCALIZATION NOTE  (responsive.title): the title displayed in the global
# toolbar
responsive.title=Modd Cynllunio Ymatebol

# LOCALIZATION NOTE (responsive.responsiveMode): Placeholder text for the
# device selector.
responsive.responsiveMode=Ymatebol

# LOCALIZATION NOTE (responsive.enableTouch): Tooltip text for the touch
# simulation button when it's disabled.
responsive.enableTouch=Galluogi efelychiad cyffwrdd

# LOCALIZATION NOTE (responsive.disableTouch): Tooltip text for the touch
# simulation button when it's enabled.
responsive.disableTouch=Analluogi efelychiad cyffwrdd

# LOCALIZATION NOTE  (responsive.screenshot): Tooltip of the screenshot button.
responsive.screenshot=Cymryd llun sgrin o'r porth gweld

# LOCALIZATION NOTE (responsive.screenshotGeneratedFilename): The auto generated
# filename.
# The first argument (%1$S) is the date string in yyyy-mm-dd format and the
# second argument (%2$S) is the time string in HH.MM.SS format.
responsive.screenshotGeneratedFilename=Llun Sgrin %1$S yn %2$S

# LOCALIZATION NOTE (responsive.remoteOnly): Message displayed in the tab's
# notification box if a user tries to open Responsive Design Mode in a
# non-remote tab.
responsive.remoteOnly=Mae'r Modd Cynllun Ymatebol ar gael yn unig ar gyfer tabiau porwyr pell, megis rhai sy'n cael ei ddefnyddio ar gyfer cynnwys gwe yn Firefox amlbroses.

# LOCALIZATION NOTE (responsive.changeDevicePixelRatio): Tooltip for the
# device pixel ratio dropdown when is enabled.
responsive.changeDevicePixelRatio=Newid cyfradd picsel dyfais o'r portholwg

# LOCALIZATION NOTE (responsive.devicePixelRatio.auto): Tooltip for the device pixel ratio
# dropdown when it is disabled because a device is selected.
# The argument (%1$S) is the selected device (e.g. iPhone 6) that set
# automatically the device pixel ratio value.
responsive.devicePixelRatio.auto=Cyfradd picsel dyfais yn cael ei osod yn awtomatig gan %1$S

# LOCALIZATION NOTE (responsive.customDeviceName): Default value in a form to
# add a custom device based on an arbitrary size (no association to an existing
# device).
responsive.customDeviceName=Dyfais Gyfaddas

# LOCALIZATION NOTE (responsive.customDeviceNameFromBase): Default value in a
# form to add a custom device based on the properties of another.  %1$S is the
# name of the device we're staring from, such as "Apple iPhone 6".
responsive.customDeviceNameFromBase=%1$S (Cyfaddas)

# LOCALIZATION NOTE (responsive.addDevice): Button text that reveals a form to
# be used for adding custom devices.
responsive.addDevice=Ychwanegu Dyfais

# LOCALIZATION NOTE (responsive.deviceAdderName): Label of form field for the
# name of a new device.  The available width is very low, so you might see
# overlapping text if the length is much longer than 5 or so characters.
responsive.deviceAdderName=Enw

# LOCALIZATION NOTE (responsive.deviceAdderSize): Label of form field for the
# size of a new device.  The available width is very low, so you might see
# overlapping text if the length is much longer than 5 or so characters.
responsive.deviceAdderSize=Maint

# LOCALIZATION NOTE (responsive.deviceAdderPixelRatio): Label of form field for
# the device pixel ratio of a new device.  The available width is very low, so you
# might see overlapping text if the length is much longer than 5 or so
# characters.
responsive.deviceAdderPixelRatio=DPR

# LOCALIZATION NOTE (responsive.deviceAdderUserAgent): Label of form field for
# the user agent of a new device.  The available width is very low, so you might
# see overlapping text if the length is much longer than 5 or so characters.
responsive.deviceAdderUserAgent=UA

# LOCALIZATION NOTE (responsive.deviceAdderTouch): Label of form field for the
# touch input support of a new device.  The available width is very low, so you
# might see overlapping text if the length is much longer than 5 or so
# characters.
responsive.deviceAdderTouch=Cyffwrdd

# LOCALIZATION NOTE (responsive.deviceAdderSave): Button text that submits a
# form to add a new device.
responsive.deviceAdderSave=Cyffwrdd

# LOCALIZATION NOTE (responsive.deviceDetails): Tooltip that appears when
# hovering on a device in the device modal.  %1$S is the width of the device.
# %2$S is the height of the device.  %3$S is the device pixel ratio value of the
# device.  %4$S is the user agent of the device.  %5$S is a boolean value
# noting whether touch input is supported.
responsive.deviceDetails=Maint: %1$S x %2$S\nDPR: %3$S\nUA: %4$S\nCyffwrdd: %5$S

# LOCALIZATION NOTE (responsive.devicePixelRatioOption): UI option in a menu to configure
# the device pixel ratio. %1$S is the devicePixelRatio value of the device.
responsive.devicePixelRatioOption=DPR: %1$S

# LOCALIZATION NOTE (responsive.reloadConditions.label): Label on button to open a menu
# used to choose whether to reload the page automatically when certain actions occur.
responsive.reloadConditions.label=Ail-lwytho'r pan…

# LOCALIZATION NOTE (responsive.reloadConditions.title): Title on button to open a menu
# used to choose whether to reload the page automatically when certain actions occur.
responsive.reloadConditions.title=Dewis p'un ai i ail-lwytho'r dudalen yn awtomatig pan fydd camau penodol yn digwydd

# LOCALIZATION NOTE (responsive.reloadConditions.touchSimulation): Label on checkbox used
# to select whether to reload when touch simulation is toggled.
responsive.reloadConditions.touchSimulation=Ail-lwytho pan mae efelychiad cyffwrdd wedi ei doglo

# LOCALIZATION NOTE (responsive.reloadConditions.userAgent): Label on checkbox used
# to select whether to reload when user agent is changed.
responsive.reloadConditions.userAgent=Ail-lwytho pan mae asiant defnyddiwr wedi ei newid

# LOCALIZATION NOTE (responsive.reloadNotification.description): Text in notification bar
# shown on first open to clarify that some features need a reload to apply.  %1$S is the
# label on the reload conditions menu (responsive.reloadConditions.label).
responsive.reloadNotification.description=Mae'r  newidiadau efelychiad dyfais yn gofyn am eu hail-lwytho i weithio.  Mae ail-lwytho awtomatig wedi'i analluogi drwy ragosodiad er mwyn osgoi colli unrhyw newidiadau yn y DevTools.  Gallwch alluogi ail-lwytho drwy'r ddewislen “%1$S”.

# LOCALIZATION NOTE (responsive.reloadNotification.description2): Text in notification bar
# shown on first open to clarify that some features need a reload to apply.
responsive.reloadNotification.description2=Mae newidiadau i efelychiad dyfais angen ail-lwytho i weithio'n iawn. Mae ail-lwytho awtomatig wedi eu hanalluogi drwy ragosodiad i osgoi colli unrhyw newidiadau yn DevTools. Gallwch alluogi ail-lwytho drwy'r ddewislen Gosodiadau.

# LOCALIZATION NOTE (responsive.leftAlignViewport): Label on checkbox used in the settings
# menu.
responsive.leftAlignViewport=Alinio Viweport i'r chwith

# LOCALIZATION NOTE (responsive.settingOnboarding.content): This is the content shown in
# the setting onboarding tooltip that is displayed below the settings menu button in
# Responsive Design Mode.
responsive.settingOnboarding.content=Newydd: Newid I alinio i'r chwith neu olygu ail-lwytho'r ymddygiad yma.

# LOCALIZATION NOTE (responsive.customUserAgent): This is the placeholder for the user
# agent input in the responsive design mode toolbar.
responsive.customUserAgent=Asiant Defnyddiwr Cyfaddas

responsive.showUserAgentInput=Dangos asiant defnyddiwr

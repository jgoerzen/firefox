<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Gå tilbake">
<!ENTITY safeb.palm.seedetails.label "Se detaljer">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Dette er ikke et villedende nettsted …">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Rådgiving levert av <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Å besøke dette nettstedet kan skade datamaskinen din">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; blokkert denne siden fordi den kan forsøke å installere skadelig programvare som kan stjele eller slette personlig informasjon på datamaskinen din.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> er <a id='error_desc_link'>rapportert å inneholde skadelig programvare</a>. Du kan <a id='report_detection'>rapportere et deteksjonsproblem</a> eller <a id='ignore_warning_link'>ignorere risikoen</a> og gå til dette usikre nettstedet.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> er <a id='error_desc_link'>rapportert å inneholde skadelig programvare</a>. Du kan <a id='report_detection'>rapportere et deteksjonsproblem</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Les mer om skadelig nettinnhold, inkludert virus og annen skadelig programvare, og hvordan du beskytter datamaskinen din på <a id='learn_more_link'>StopBadware.org</a>. Les mer om &brandShortName; sin beskyttelse mot nettfisking og skadelig programvare på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Nettstedet forut kan inneholde skadelig programvare">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; blokkerte denne nettsiden fordi den kan prøve å lure deg til å installere programmer som skader nettleseropplevelsen din (for eksempel ved å endre startsiden din eller ved å vise ekstra annonser på nettsteder du besøker).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> er <a id='error_desc_link'>rapportert å inneholde skadelig programvare</a>. Du kan <a id='ignore_warning_link'>ignorere risikoen</a> og gå til dette usikre nettstedet.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> er <a id='error_desc_link'>rapportert å inneholde skadelig programvare</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Les mer om skadelig og uønsket programvare på <a id='learn_more_link'>Uønsket programvarepolicy</a>. Les mer om &brandShortName; sin beskyttelse mot nettfisking og skadelig programvare på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Villedende nettsted">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; blokkert denne siden fordi det kan få deg til å gjøre noe farlig som å installere programvare eller avsløre personlig informasjon som passord eller kredittkort.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> er <a id='error_desc_link'>rapportert som et villedende nettsted</a>. Du kan <a id='report_detection'>rapportere et deteksjonsproblem</a> eller <a id='ignore_warning_link'>ignorere risikoen</a> og gå til dette usikre nettstedet.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> er <a id='error_desc_link'>rapportert som et villedende nettsted</a>. Du kan <a id='report_detection'>rapportere et deteksjonsproblem</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Les mer om villedende nettsteder og nettfisking på <a id='learn_more_link'>www.antiphishing.org</a>. Les mer om &brandShortName; sin beskyttelse mot nettfisking og skadelig programvare på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Nettstedet forut kan inneholde skadelig programvare">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; blokkerte denne siden fordi den kan prøve å installere farlige apper som stjeler eller sletter din informasjon (for eksempel bilder, passord, meldinger og kredittkort).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> er <a id='error_desc_link'>rapportert å inneholde potensielt skadelig programvare</a>. Du kan <a id='ignore_warning_link'>ignorere risikoen</a> og gå til dette usikre nettstedet.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> er <a id='error_desc_link'>rapportert å inneholde potensielt skadelig programvare</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Les mer om &brandShortName; sin beskyttelse mot nettfisking og skadelig programvare på <a id='firefox_support'>support.mozilla.org</a>.">

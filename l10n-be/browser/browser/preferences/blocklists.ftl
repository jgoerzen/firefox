# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Спісы блакавання
    .style = width: 55em
blocklist-desc = Вы можаце выбраць, які спіс { -brand-short-name } будзе выкарыстоўваць для блакавання элементаў Сеціва, якія могуць асочваць вашу актыўнасць.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Спіс
blocklist-button-cancel =
    .label = Скасаваць
    .accesskey = С
blocklist-button-ok =
    .label = Захаваць змены
    .accesskey = З
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me, базавая ахова (рэкамендуецца).
blocklist-item-moz-std-desc = Дазваляе некаторыя трэкеры для карэктнай працы вэб-сайтаў.
blocklist-item-moz-full-name = Disconnect.me, строгая ахова.
blocklist-item-moz-full-desc = Блакуе вядомыя трэкеры. Некаторыя вэб-сайты могуць некарэктна працаваць.

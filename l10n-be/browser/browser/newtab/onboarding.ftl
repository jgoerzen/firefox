# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Паспрабуйце зараз
onboarding-welcome-header = Вітаем у { -brand-short-name }
onboarding-start-browsing-button-label = Пачаць агляданне

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Прыватнае агляданне
onboarding-private-browsing-text = Аглядайце самі па сабе. Прыватнае агляданне з блакаваннем змесціва блакіруе сеціўныя трэкеры, якія сочаць за вамі ў інтэрнэце.
onboarding-screenshots-title = Здымкі экрана
onboarding-screenshots-text = Рабіце, захоўвайце і дзяліцеся здымкамі экрана — не выходзячы з { -brand-short-name }. Пры агляданні захапіце ўчастак або ўсю старонку. Потым захавайце ў інтэрнэце, каб мець лёгкі доступ і хутка дзяліцца.
onboarding-addons-title = Дадаткі
onboarding-addons-text = Дадавайце іншыя функцыі, каб { -brand-short-name } працаваў лепш для вас. Параўноўвайце цэны, глядзіце надвор'е, або падкрэслівайце індывідуальнасць уласнай тэмай афармлення.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Зрабіце агляд хутчэйшым, бяспечнейшым і зручнейшым з дадаткамі накшталт Ghostery, які дазваляе блакіраваць рэкламу, што раздражняе.

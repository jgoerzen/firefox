<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/.  -->
<!ENTITY aboutDialog.title          "Пра &brandFullName;">

<!-- LOCALIZATION NOTE (update.*):
# These strings are also used in the update pane of preferences.
# See about:preferences#general.
-->
<!-- LOCALIZATION NOTE (update.checkForUpdatesButton.*, update.updateButton.*):
# Only one button is present at a time.
# The button when displayed is located directly under the Firefox version in
# the about dialog (see bug 596813 for screenshots).
-->
<!ENTITY update.checkForUpdatesButton.label       "Праверыць наяўнасць абнаўленняў">
<!ENTITY update.checkForUpdatesButton.accesskey   "П">
<!ENTITY update.updateButton.label3               "Перазапуск для абнаўлення &brandShorterName;">
<!ENTITY update.updateButton.accesskey            "П">


<!-- LOCALIZATION NOTE (warningDesc.version): This is a warning about the experimental nature of Nightly and Aurora builds. It is only shown in those versions. -->
<!ENTITY warningDesc.version        "&brandShortName; з'яўляецца эксперыментальным і можа быць няўстойлівым.">

<!-- LOCALIZATION NOTE (community.exp.*) This paragraph is shown in "experimental" builds, i.e. Nightly and Aurora builds, instead of the other "community.*" strings below. -->
<!ENTITY community.exp.start        "">
<!-- LOCALIZATION NOTE (community.exp.mozillaLink): This is a link title that links to http://www.mozilla.org/. -->
<!ENTITY community.exp.mozillaLink  "&vendorShortName;">
<!ENTITY community.exp.middle       " — ">
<!-- LOCALIZATION NOTE (community.exp.creditsLink): This is a link title that links to about:credits. -->
<!ENTITY community.exp.creditsLink  "сусветнае згуртаванне">
<!ENTITY community.exp.end          ", якое працуе разам, каб захаваць Сеціва адкрытым, публічным і даступным кожнаму.">

<!ENTITY community.start2           "&brandShortName; распрацаваны ">
<!-- LOCALIZATION NOTE (community.mozillaLink): This is a link title that links to http://www.mozilla.org/. -->
<!ENTITY community.mozillaLink      "&vendorShortName;">
<!ENTITY community.middle2          ", ">
<!-- LOCALIZATION NOTE (community.creditsLink): This is a link title that links to about:credits. -->
<!ENTITY community.creditsLink      "сусветным згуртаваннем">
<!ENTITY community.end3             ", якое працуе разам, каб захаваць Сеціва адкрытым, публічным і даступным кожнаму.">

<!ENTITY helpus.start               "Хочаце дапамагчы? ">
<!-- LOCALIZATION NOTE (helpus.donateLink): This is a link title that links to https://donate.mozilla.org/?utm_source=firefox&utm_medium=referral&utm_campaign=firefox_about&utm_content=firefox_about. -->
<!ENTITY helpus.donateLink          "Зрабіце ахвяраванне">
<!ENTITY helpus.middle              " або ">
<!-- LOCALIZATION NOTE (helpus.getInvolvedLink): This is a link title that links to http://www.mozilla.org/contribute/. -->
<!ENTITY helpus.getInvolvedLink     "далучайцеся да нас!">
<!ENTITY helpus.end                 "">

<!ENTITY releaseNotes.link          "Што новага">

<!-- LOCALIZATION NOTE (bottomLinks.license): This is a link title that links to about:license. -->
<!ENTITY bottomLinks.license        "Звесткі пра ліцэнзію">

<!-- LOCALIZATION NOTE (bottomLinks.rights): This is a link title that links to about:rights. -->
<!ENTITY bottomLinks.rights         "Правы канчатковага карыстальніка">

<!-- LOCALIZATION NOTE (bottomLinks.privacy): This is a link title that links to https://www.mozilla.org/legal/privacy/. -->
<!ENTITY bottomLinks.privacy        "Палітыка прыватнасці">

<!-- LOCALIZATION NOTE (update.checkingForUpdates): try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.checkingForUpdates  "Праверка існавання абнаўленняў…">
<!-- LOCALIZATION NOTE (update.noUpdatesFound): try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.noUpdatesFound      "&brandShortName; абноўлены">
<!-- LOCALIZATION NOTE (update.adminDisabled): try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.adminDisabled       "Абнаўленні забаронены вашым сістэмным адміністратарам">
<!-- LOCALIZATION NOTE (update.otherInstanceHandlingUpdates): try to make the localized text short -->
<!ENTITY update.otherInstanceHandlingUpdates "&brandShortName; абнаўляецца іншым асобнікам">
<!ENTITY update.restarting          "Перазапуск...">

<!-- LOCALIZATION NOTE (update.failed.start,update.failed.linkText,update.failed.end):
     update.failed.start, update.failed.linkText, and update.failed.end all go into
     one line with linkText being wrapped in an anchor that links to a site to download
     the latest version of Firefox (e.g. http://www.firefox.com). As this is all in
     one line, try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.failed.start        "Няўдача абнаўлення. ">
<!ENTITY update.failed.linkText     "Сцягнуць апошнюю версію">
<!ENTITY update.failed.end          "">

<!-- LOCALIZATION NOTE (update.manual.start,update.manual.end): update.manual.start and update.manual.end
     all go into one line and have an anchor in between with text that is the same as the link to a site
     to download the latest version of Firefox (e.g. http://www.firefox.com). As this is all in one line,
     try to make the localized text short (see bug 596813 for screenshots). -->
<!ENTITY update.manual.start        "Абнаўленні даступныя на ">
<!ENTITY update.manual.end          "">

<!-- LOCALIZATION NOTE (update.unsupported.start,update.unsupported.linkText,update.unsupported.end):
     update.unsupported.start, update.unsupported.linkText, and
     update.unsupported.end all go into one line with linkText being wrapped in
     an anchor that links to a site to provide additional information regarding
     why the system is no longer supported. As this is all in one line, try to
     make the localized text short (see bug 843497 for screenshots). -->
<!ENTITY update.unsupported.start    "Вы не можаце працягнуць абнаўленне на гэтай сістэме. ">
<!ENTITY update.unsupported.linkText "Падрабязней">
<!ENTITY update.unsupported.end      "">

<!-- LOCALIZATION NOTE (update.downloading.start,update.downloading.end): update.downloading.start and
     update.downloading.end all go into one line, with the amount downloaded inserted in between. As this
     is all in one line, try to make the localized text short (see bug 596813 for screenshots). The — is
     the "em dash" (long dash).
     example: Downloading update — 111 KB of 13 MB -->
<!ENTITY update.downloading.start   "Сцягванне абнаўлення — ">
<!ENTITY update.downloading.end     "">

<!ENTITY update.applying            "Прымяняецца абнаўленне…">

<!-- LOCALIZATION NOTE (channel.description.start,channel.description.end): channel.description.start and
     channel.description.end create one sentence, with the current channel label inserted in between.
     example: You are currently on the _Stable_ update channel. -->
<!ENTITY channel.description.start  "Зараз вы на ">
<!ENTITY channel.description.end    " канале абнаўленняў. ">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Jeziki
    .style = width: 30em
webpage-languages-window =
    .title = Nastavitve jezikov spletnih strani
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = Spletne strani so včasih na voljo v več jezikih. Izberite prednostni vrstni red jezikov za prikaz strani
languages-customize-spoof-english =
    .label = Zahtevaj angleške različice spletnih strani za večjo zasebnost
languages-customize-moveup =
    .label = Premakni gor
    .accesskey = G
languages-customize-movedown =
    .label = Premakni dol
    .accesskey = D
languages-customize-remove =
    .label = Odstrani
    .accesskey = R
languages-customize-select-language =
    .placeholder = Izberite dodaten jezik …
languages-customize-add =
    .label = Dodaj
    .accesskey = D
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = Nastavitve jezikov { -brand-short-name }a
    .style = width: 40em
browser-languages-description = { -brand-short-name } bo privzeto prikazal prvi jezik, po potrebi pa pomožne jezike v navedenem vrstnem redu.

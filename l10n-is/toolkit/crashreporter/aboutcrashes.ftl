# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Hrunskýrslur
clear-all-reports-label = Fjarlægja allar skýrslur
delete-confirm-title = Ertu viss?
delete-confirm-description = Þetta mun óafturkvæmt eyða öllum skýrslum.
crashes-unsubmitted-label = Ótilkynntar hrun skýrslur
id-heading = Skýrslu auðkenni
date-crashed-heading = Hrun dagsetning
crashes-submitted-label = Tilkynntar hrun skýrslur
date-submitted-heading = Sendingardagur
no-reports-label = Engar hrun skýrslur hafa verið sendar.
no-config-label = Þetta forrit hefur ekki verið stillt til að sýna hrun skýrslur. Stillingin <code>breakpad.reportURL</code> verður að vera stillt.

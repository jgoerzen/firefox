# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S vistar núna heimilisföng og þannig geturðu á fljótlegan hátt fyllt út eyðublöð.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Stillingar fyrir sjálfvirkar útfyllingar
autofillOptionsLinkOSX = Stillingar fyrir sjálfvirkar útfyllingar
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Stillingar fyrir sjálfvirkar útfyllingar & öryggi
autofillSecurityOptionsLinkOSX = Stillingar fyrir sjálfvirkar útfyllingar & öryggi
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Breyta stillingum fyrir sjálfvirkar útfyllingar
changeAutofillOptionsOSX = Breyta stillingum fyrir sjálfvirkar útfyllingar
changeAutofillOptionsAccessKey = C
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Deila heimilisföngum með samstilltum tækjum
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Deila greiðslukortum með samstilltum tækjum
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Viltu uppfæra heimilisfangið með þessum nýju upplýsingum?
updateAddressDescriptionLabel = Heimilisfang sem á að uppfæra:
createAddressLabel = Búa til nýtt heimilisfang
createAddressAccessKey = C
updateAddressLabel = Uppfæra heimilisfang
updateAddressAccessKey = U
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Viltu leyfa %S að vista þetta greiðslukort? (Öryggiskóði verður ekki vistaður)
saveCreditCardDescriptionLabel = Greiðslukort sem á að vista:
saveCreditCardLabel = Vista greiðslukort
saveCreditCardAccessKey = S
cancelCreditCardLabel = Ekki vista
cancelCreditCardAccessKey = D
neverSaveCreditCardLabel = Aldrei vista greiðslukort
neverSaveCreditCardAccessKey = N
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Viltu uppfæra greiðslukortið með þessum nýju upplýsingum?
updateCreditCardDescriptionLabel = Greiðslukort sem á að uppfæra:
createCreditCardLabel = Búa til nýtt greiðslukort
createCreditCardAccessKey = K
updateCreditCardLabel = Uppfæra greiðslukort
updateCreditCardAccessKey = U
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Opna skilaboðaflipa fyrir sjálfvirka útfyllingu

# LOCALIZATION NOTE (autocompleteFooterOption, autocompleteFooterOptionOSX): Used as a label for the button,
# displayed at the bottom of the drop down suggestion, to open Form Autofill browser preferences.
autocompleteFooterOption = Stillingar fyrir sjálfvirkar útfyllingar
autocompleteFooterOptionOSX = Stillingar fyrir sjálfvirkar útfyllingar

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Fleiri stillingar
autocompleteFooterOptionOSXShort = Stillingar
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = heimilisfang
category.name = nafn
category.organization2 = fyrirtæki
category.tel = sími
category.email = tölvupóstfang
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Setja einnig inn fyrir %S
phishingWarningMessage2 = Sjálfvirk útfylling fyrir %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S uppgötvaði óöruggt vefsvæði. Slökkt er á sjálfvirkri útfyllingu tímabundið.

# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Hreinsa sjálfvirkar útfyllingar á eyðublaði

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Sjálfvirk útfylling á heimilisfangi
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Fræðast meira
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Vistað heimilisfang…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Fylla sjálfvirkt út kortanúmer
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Vistuð greiðslukort…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Vistuð heimilisföng
manageCreditCardsTitle = Vistuð greiðslukort
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Heimilisföng
creditCardsListHeader = Greiðslukort
showCreditCardsBtnLabel = Sýna greiðslukort
hideCreditCardsBtnLabel = Fela greiðslukort
removeBtnLabel = Fjarlægja
addBtnLabel = Bæta við…
editBtnLabel = Breyta…

# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Bæta við nýju heimilisfangi
editAddressTitle = Breyta heimilisfangi
givenName = Fornafn
additionalName = Miðnafn
familyName = Eftirnafn
organization2 = Fyrirtæki
streetAddress = Heimilisfang
city = Borg
province = Hérað
state = Ríki
postalCode = Póstnúmer
zip = Póstnúmer
country = Land eða hérað
tel = Sími
email = Tölvupóstfang
cancelBtnLabel = Hætta við
saveBtnLabel = Vista
countryWarningMessage = Sjálfvirk útfylling er bara virk fyrir US heimilisföng eins og er

countryWarningMessage2 = Sjálfvirkar útfyllingar er bara tiltækar fyrir sum lönd.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Bæta við nýju kortanúmeri
editCreditCardTitle = Breyta kortanúmeri
cardNumber = Kortanúmer
nameOnCard = Nafn á korti
cardExpires = Rennur út

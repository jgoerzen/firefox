<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Til baka">
<!ENTITY safeb.palm.seedetails.label "Sjá nánar">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Þetta er ekki svindlsvæði…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Ráðleggingar frá <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Það gæti verið hættulegt að heimsækja þetta vefsvæði">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; lokaði á þessa síðu þar sem hún gæti reynt að setja inn hættulegan hugbúnað sem gæti stolið eða eytt persónulegum gögnum á tölvunni.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að innihalda spilliforrit</a>. Þú getur <a id='report_detection'>sent inn tilkynningu á rangri flokkun</a> eða <a id='ignore_warning_link'>hunsað áhættuna</a> og haldið áfram á þessa óöruggu síðu.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að innihalda spilliforrit</a>. Þú getur <a id='report_detection'>sent inn tilkynningu á rangri flokkun</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Lærðu meira um hættuleg vefforrit eins og vírusa og önnur spilliforrit og hvernig þú getur verndað tölvuna þína á <a id='learn_more_link'>StopBadware.org</a>. Lærðu meira um &brandShortName;’s vörninni gegn svikum og spilliforritum á <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Vefsvæðið gæti innihaldið spilliforrit">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; lokaði á þessa síðu þar sem hún gæti reynt að plata þig í að setja inn forrit sem gætu valdið skaða á upplifun í vafra (til dæms, með því að breyta heimasíðu eða birta auka auglýsingar á vefsvæðum sem þú heimsækir).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að innihalda spilliforrit</a>. Þú getur <a id='ignore_warning_link'>hunsað áhættuna</a> ef þú vilt og haldið áfram á þetta óörugga vefsvæði.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að innihalda spilliforrit</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Lærðu meira um hættuleg vefforrit og óæskilegan hugbúnað á <a id='learn_more_link'>Unwanted Software Policy</a>. Lærðu meira um &brandShortName;’s vörninni gegn svikum og spilliforritum á <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Svindlsvæði framundan">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; lokaði á þessa síðu þar sem hún gæti reynt að plata þig í að gera eitthvað hættulegt eins og að setja inn hugbúnað eða birta persónulegar upplýsingar eins og lykilorð eða kortanúmer.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að sé svika vefsvæði</a>. Þú getur <a id='report_detection'>tilkynnt ranga flokkun</a> eða <a id='ignore_warning_link'>hunsað áhættuna</a> og haldið áfram á þetta óörugga vefsvæði.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að sé svika vefsvæði</a>. Þú getur <a id='report_detection'>tilkynnt ranga flokkun</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Lærðu meira um svika vefsvæði og netveiðar <a id='learn_more_link'>www.antiphishing.org</a>. Lærðu meira um &brandShortName;’s vörninni gegn netveiðum og spilliforritum á <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Vefsvæði gæti innihaldið spilliforrit">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; lokaði á þessa síðu þar sem hún gæti reynt að setja inn hættuleg forrit sem stela eða eyða upplýsingum (til dæmis, myndum, lykilorðum, skilaboðum og kreditkortum).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt um að innihalda hugsanlegt hættulegt forrit</a>. Þú getur <a id='ignore_warning_link'>hunsað áhættuna</a> ef þú vilt og haldið áfram á þetta óörugga vefsvæði.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> hefur verið <a id='error_desc_link'>tilkynnt að innihaldi hættulegt forrit</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Lærðu meira um &brandShortName;’s vörninni gegn netveiðum og spilliforritum á <a id='firefox_support'>support.mozilla.org</a>.">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Tungumál
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Vefsíður eru oft í boði á mörgum tungumálum. Veldu forgangsröð þeirra tungumála sem vefsíður eiga að birtast á
languages-customize-spoof-english =
    .label = Biðja um enskar útgáfur af vefsíðum til að auka friðhelgi
languages-customize-moveup =
    .label = Færa upp
    .accesskey = u
languages-customize-movedown =
    .label = Færa niður
    .accesskey = n
languages-customize-remove =
    .label = Fjarlægja
    .accesskey = r
languages-customize-select-language =
    .placeholder = Veldu tungumál til að bæta við…
languages-customize-add =
    .label = Bæta við
    .accesskey = B
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

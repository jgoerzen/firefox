# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Jezici
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Web stranice su ponekad dostupne na više jezika. Odaberite jezike za prikaz tih web stranica prema preferiranom redoslijedu
languages-customize-moveup =
    .label = Pomakni gore
    .accesskey = g
languages-customize-movedown =
    .label = Pomakni dolje
    .accesskey = d
languages-customize-remove =
    .label = Ukloni
    .accesskey = U
languages-customize-select-language =
    .placeholder = Odaberite jezik kojeg želite dodati...
languages-customize-add =
    .label = Dodaj
    .accesskey = a
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

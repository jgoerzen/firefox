# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Pomakni gore
    .accesskey = g
languages-customize-movedown =
    .label = Pomakni dolje
    .accesskey = d
languages-customize-remove =
    .label = Ukloni
    .accesskey = U
languages-customize-select-language =
    .placeholder = Odaberite jezik kojeg želite dodati...
languages-customize-add =
    .label = Dodaj
    .accesskey = D
messenger-languages-window =
    .title = { -brand-short-name } jezične postavke
    .style = width: 40em
messenger-languages-description = { -brand-short-name } će prikazati prvi jezik kao zadani, dok će alternativne jezike prikazati ukoliko je potrebno prema redosljedu pojavljivanja.

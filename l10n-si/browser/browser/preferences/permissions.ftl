# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = හැරදැමීම්
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-block =
    .label = වලකන්න
    .accesskey = B
permissions-session =
    .label = සැසියට ඉඩදෙන්න
    .accesskey = S
permissions-allow =
    .label = ඉඩ දෙන්න
    .accesskey = A
permissions-status =
    .label = තත්ත්වය
permissions-button-cancel =
    .label = අවලංගු කරන්න
    .accesskey = C
permissions-button-ok =
    .label = වෙනස්කම් සුරකින්න
    .accesskey = S
permissions-capabilities-allow =
    .label = අනුමත කරන්න
permissions-capabilities-block =
    .label = අවහිර කරන්න

## Invalid Hostname Dialog

permissions-invalid-uri-title = සාවද්‍ය නමක් ඇතුළත් කරන ලඳි
permissions-invalid-uri-label = නිවැරදි ධාරක නමක් ඇතුළත් කරන්න

## Exceptions - Tracking Protection


## Exceptions - Cookies


## Exceptions - Pop-ups

permissions-exceptions-popup-desc = උපත්තන කවුළු විවෘත කිරීම අවසර ඇති වෙබ් අඩවි ඔබට නිර්ණය කල හැක. ඔබට කළමනාකරණය කිරීමට අවශ්‍ය ඒවායේ නිවැරදි ලිපිනය ඇතුළත් කර අවහිර කරන්න හෝ  අනුමත කරන්න ක්ලික් කරන්න.

## Exceptions - Saved Logins


## Exceptions - Add-ons

permissions-exceptions-addons-desc = ඇඩෝන ස්ථාපනයට අවසර ඇති වෙබ් අඩවි ඔබට නිර්ණය කල හැක. ඔබට කළමනාකරණය කිරීමට අවශ්‍ය ඒවායේ නිවැරදි ලිපිනය ඇතුළත් කර අවහිර කරන්න හෝ  අනුමත කරන්න ක්ලික් කරන්න.

## Site Permissions - Notifications


## Site Permissions - Location


## Site Permissions - Camera


## Site Permissions - Microphone


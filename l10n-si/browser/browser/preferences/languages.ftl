# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = භාෂා
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = ජාල පිටු ඇතැම් විට එක් භාශාවකට වඩා ඉදිරිපත් කරයි. අභිප්‍රේතය පරිදි මෙම ජාල අඩවි පෙන්වීම සඳහා භාශාවන් තෝරන්න
languages-customize-moveup =
    .label = ඉහළට ගෙනයන්න
    .accesskey = U
languages-customize-movedown =
    .label = පහලට ගෙනයන්න
    .accesskey = D
languages-customize-remove =
    .label = ඉවත් කරන්න
    .accesskey = R
languages-customize-select-language =
    .placeholder = භාෂාවක් එක්කිරීමට තෝරන්න...
languages-customize-add =
    .label = එක් කරන්න
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

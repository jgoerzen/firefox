# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

translation-window =
    .title = විකල්පයන් - පරිවර්තනය
    .style = width: 36em
translation-close-key =
    .key = w
translation-languages-column =
    .label = භාෂා
translation-languages-button-remove =
    .label = භාශාව ඉවත් කරන්න
    .accesskey = R
translation-languages-button-remove-all =
    .label = සියළු භාශා ඉවත් කරන්න
    .accesskey = e
translation-sites-button-remove =
    .label = අඩවිය ඉවත් කරන්න
    .accesskey = S
translation-sites-button-remove-all =
    .label = සියළු අඩවි ඉවත් කරන්න
    .accesskey = i
translation-button-close =
    .label = වසන්න
    .accesskey = C

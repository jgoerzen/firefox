# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = زبان‌ها
    .style = width: 30em
webpage-languages-window =
    .title = تنظیمات زبان صفحه وب
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = گاهی صفحات وب به زبان‌های دیگر هم ارائه می‌شوند. زبان‌هایی که مایلید این صفحات را ببینید را به ترتیب ترجیحات خود انتخاب کنید
languages-customize-spoof-english =
    .label = درخواست نسخه انگلیسی وب سایت برای بهینه سازی حریم شخصی
languages-customize-moveup =
    .label = انتقال به بالا
    .accesskey = ب
languages-customize-movedown =
    .label = انتقال به پایین
    .accesskey = پ
languages-customize-remove =
    .label = حذف
    .accesskey = ح
languages-customize-select-language =
    .placeholder = انتخاب زبان برای اضافه کردن…
languages-customize-add =
    .label = افزودن
    .accesskey = ا
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = تنظیمات زبان { -brand-short-name }
    .style = width: 40em

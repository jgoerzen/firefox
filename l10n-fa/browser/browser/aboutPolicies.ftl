# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = خط مشی‌های سازمانی
# 'Active' is used to describe the policies that are currently active
active-policies-tab = فعال
errors-tab = خطاها
documentation-tab = مستندات
policy-name = نام سیاست
policy-value = ارزش سیاست
policy-errors = خطاهای سیاست

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = همین حالا امتحان کنید
onboarding-welcome-header = به { -brand-short-name } خوش آمدید
onboarding-start-browsing-button-label = شروع وب‌گردی

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = مرور ناشناس
onboarding-screenshots-title = تصاویر صفحه
onboarding-screenshots-text = تصاویر صفحه را بگیرید،‌ ذخیره و هم‌رسان کنید - بدون ترک { -brand-short-name }. هنگام مرور یک بخش یا کل صفحه را ثبت کنید. سپس آن را برای دسترسی آسان و هم‌رسانی روی وب ذخیره کنید.
onboarding-addons-title = افزونه‌ها

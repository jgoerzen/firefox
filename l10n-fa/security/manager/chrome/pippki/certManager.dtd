<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY certmgr.title                       "مدیر گواهی">

<!ENTITY certmgr.tab.mine                     "گواهی‌های شما">
<!ENTITY certmgr.tab.others2                  "افراد">
<!ENTITY certmgr.tab.websites3                "پایگاه‌ها">
<!ENTITY certmgr.tab.ca                       "مراجع صدور">
<!ENTITY certmgr.tab.orphan2                  "دیگران">

<!ENTITY certmgr.mine2                        "شما گواهینامه‌ هایی از شرکت هایی دارید که شما هویت شما را میشناسند">
<!ENTITY certmgr.others2                      "شما گواهینامه ای روی این پرونده دارید که این افراد را می شناسد">
<!ENTITY certmgr.websites3                    "شما گواهینامه هایی روی این پرونده دارید که سرو‌رها را میشناسد">
<!ENTITY certmgr.cas2                         "شما گواهینامه هایی روی این پرونده دارید که مسئولین این گواهینامه‌ها را میشناسد">
<!ENTITY certmgr.orphans2                     "شما گواهینامه هایی روی این پرونده دارید که در هیچ یک از دسته بندی های دیگر جای نمی‌گیرد">

<!ENTITY certmgr.detail.general_tab.title     "عمومی">
<!ENTITY certmgr.detail.general_tab.accesskey "ع">
<!ENTITY certmgr.detail.prettyprint_tab.title "جزئیات">
<!ENTITY certmgr.detail.prettyprint_tab.accesskey "ج">

<!ENTITY certmgr.pending.label                "در حال تأیید گواهی‌ها…">
<!ENTITY certmgr.subjectinfo.label            "صادر شده برای">
<!ENTITY certmgr.issuerinfo.label             "صادر شده توسط">
<!ENTITY certmgr.periodofvalidity.label       "مدت اعتبار" >
<!ENTITY certmgr.fingerprints.label           "اثر انگشت">
<!ENTITY certmgr.certdetail.title             "جزئیات گواهی">
<!ENTITY certmgr.certdetail.cn                "نام معمول (CN)">
<!ENTITY certmgr.certdetail.o                 "سازمان (O)">
<!ENTITY certmgr.certdetail.ou                "واحد سازمانی (OU)">
<!ENTITY certmgr.certdetail.serialnumber      "شمارهٔ سریال">
<!ENTITY certmgr.certdetail.sha256fingerprint "اثر انگشت SHA-256">
<!ENTITY certmgr.certdetail.sha1fingerprint   "اثر انگشت SHA1">

<!ENTITY certmgr.editcacert.title             "ویرایش تنظیمات اعتماد به مرجع صدور گواهی">
<!ENTITY certmgr.editcert.edittrust           "ویرایش تنظیمات اعتماد:">
<!ENTITY certmgr.editcert.trustssl            "این گواهی می‌تواند هویت وب‌گاه‌ها را تأیید کند.">
<!ENTITY certmgr.editcert.trustemail          "این گواهی می‌تواند هویت فرستندگان نامه را تأیید کند.">
<!ENTITY certmgr.editcert.trustobjsign        "این گواهی می‌تواند هویت سازندگان نرم‌افزار را تأیید کند.">

<!ENTITY certmgr.deletecert.title             "حذف گواهی">

<!ENTITY certmgr.certname                     "نام گواهی">
<!ENTITY certmgr.certserver                   "کارگزار">
<!ENTITY certmgr.override_lifetime            "مدت زمان">
<!ENTITY certmgr.tokenname                    "دستگاه امنیتی">
<!ENTITY certmgr.begins                       "آغاز می شود در">
<!ENTITY certmgr.expires                      "تاریخ انقضا">
<!ENTITY certmgr.email                        "نشانی پست الکترونیکی">
<!ENTITY certmgr.serial                       "شمارهٔ سریال">

<!ENTITY certmgr.close.label                  "بستن">
<!ENTITY certmgr.close.accesskey              "س">
<!ENTITY certmgr.view2.label                  "مشاهده…">
<!ENTITY certmgr.view2.accesskey              "م">
<!ENTITY certmgr.edit3.label                  "ویرایش اعتماد…">
<!ENTITY certmgr.edit3.accesskey              "و">
<!ENTITY certmgr.export.label                 "صادر کردن…">
<!ENTITY certmgr.export.accesskey             "ص">
<!ENTITY certmgr.delete2.label                "حذف…">
<!ENTITY certmgr.delete2.accesskey            "ح">
<!ENTITY certmgr.delete_builtin.label         "حذف یا عدم اعتماد…">
<!ENTITY certmgr.delete_builtin.accesskey     "ح">
<!ENTITY certmgr.backup2.label                "پشتیبان‌گیری…">
<!ENTITY certmgr.backup2.accesskey            "پ">
<!ENTITY certmgr.backupall2.label             "پشتیبان‌گیری از همه…">
<!ENTITY certmgr.backupall2.accesskey         "ه">
<!ENTITY certmgr.restore2.label               "وارد کردن…">
<!ENTITY certmgr.restore2.accesskey           "و">
<!ENTITY certmgr.details.label                "فیلدهای گواهی">
<!ENTITY certmgr.details.accesskey            "ف">
<!ENTITY certmgr.fields.label                 "مقدار فیلد">
<!ENTITY certmgr.fields.accesskey             "م">
<!ENTITY certmgr.hierarchy.label              "سلسه‌مراتب گواهی‌ها">
<!ENTITY certmgr.hierarchy.accesskey2         "H">
<!ENTITY certmgr.addException.label           "افزودن استثنا…">
<!ENTITY certmgr.addException.accesskey       "ا">

<!ENTITY exceptionMgr.title                   "افزودن استثناء امنیتی">
<!ENTITY exceptionMgr.exceptionButton.label   "تأیید استثناء امنیتی">
<!ENTITY exceptionMgr.exceptionButton.accesskey "ت">
<!ENTITY exceptionMgr.supplementalWarning     "بانک‌ها و فروشگاه‌های قانونی و بسیاری از پایگاه‌های عمومی این کار را از شما نخواهند خواست.">
<!ENTITY exceptionMgr.certlocation.caption2   "کارگزار">
<!ENTITY exceptionMgr.certlocation.url        "مکان:">
<!ENTITY exceptionMgr.certlocation.download   "دریافت گواهی">
<!ENTITY exceptionMgr.certlocation.accesskey  "د">
<!ENTITY exceptionMgr.certstatus.caption      "وضعیت گواهی">
<!ENTITY exceptionMgr.certstatus.viewCert     "مشاهده…">
<!ENTITY exceptionMgr.certstatus.accesskey    "م">
<!ENTITY exceptionMgr.permanent.label         "ذخیرهٔ دائمی این استثنا">
<!ENTITY exceptionMgr.permanent.accesskey     "ذ">

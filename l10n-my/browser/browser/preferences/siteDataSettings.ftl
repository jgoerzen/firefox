# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-search-textbox =
    .placeholder = ဝဘ်ဆိုက်များကို ရှာဖွေရန်
    .accesskey = S
site-data-column-host =
    .label = ဆိုက်
site-data-column-storage =
    .label = သိမ်းဆည်းပါ
site-data-remove-selected =
    .label = ရွေးထားသည့်အရာများကို ဖယ်ရှားပါ
    .accesskey = r
site-data-button-cancel =
    .label = မလုပ်ဆောင်တော့ပါ
    .accesskey = C
site-data-button-save =
    .label = ပြောင်းလဲမှုများကို သိမ်းပါ
    .accesskey = a
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = အားလုံးကို ဖယ်ရှားရန်
    .accesskey = e
site-data-remove-shown =
    .label = ပြသထားသည့် အရာအားလုံးကို ဖယ်ရှားပါ
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
site-data-removing-dialog =
    .title = { site-data-removing-header }
    .buttonlabelaccept = ဖယ်ရှားပါ

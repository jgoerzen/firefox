# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Caktoni URL vetjake përditësimi aplikacioni.
policy-Authentication = Formësoni mirëfilltësim të integruar për sajte që e mbulojnë atë.
policy-BlockAboutAddons = Blloko hyrje te Përgjegjësi i Shtesave (about:addons).
policy-BlockAboutConfig = Blloko hyrje te faqja about:config.
policy-BlockAboutProfiles = Blloko hyrje te faqja about:profiles.
policy-BlockAboutSupport = Blloko hyrje te faqja about:support.
policy-Bookmarks = Krijoni faqerojtës te paneli Faqerojtës, menuja Faqerojtës, ose te një dosje e caktuar brenda tyre.
policy-Certificates = Të përdoren apo jo dëshmi të brendshme. Deri sot ky rregull vlen vetëm nën Windows.
policy-Cookies = Lejoni ose jo që sajte të depozitojnë <em>cookies</em>.
policy-DisableAppUpdate = Pengoje shfletuesin të përditësohet.
policy-DisableBuiltinPDFViewer = Çaktivizo PDF.js, parësin e brendshëm të PDF-ve në { -brand-short-name }.
policy-DisableDeveloperTools = Blloko hyrje te mjetet për zhvillues.
policy-DisableFirefoxAccounts = Çaktivizo shërbime me bazë { -fxaccount-brand-name }, përfshi Sync-un.
policy-DisableFirefoxStudies = Pengoja { -brand-short-name }-it xhirimin e studimeve.
policy-DisableForgetButton = Pengo përdorimin e butoni Harroje.
policy-DisableFormHistory = Mos mba mend historik kërkimesh dhe plotësimi formularësh.
policy-DisableMasterPasswordCreation = Nëse zgjidhet <em>true</e>, s’mund të krijohet fjalëkalim i përgjithshëm.
policy-DisablePocket = Çaktivizo veçorinë e ruajtjes së faqeve web në Pocket.
policy-DisablePrivateBrowsing = Çaktivizo Shfletimin Privat.
policy-DisableProfileImport = Çaktivizo te menu urdhrin për Importim të dhënash nga një tjetër shfletues.
policy-DisableProfileRefresh = Çaktivizo te faqja about:support butonin Rifreskoni { -brand-short-name }.
policy-DisableSafeMode = Çaktivizo veçorinë e rinisjes nën Mënyrën e Parrezik. Shënim: tasti Shift për hyrje nën Mnëyrën e Parrezik mund të çaktivizohet vetëm në Windows duke përdorur Rregulla Grupi.
policy-DisableTelemetry = Çaktivizo Telemetry-në.

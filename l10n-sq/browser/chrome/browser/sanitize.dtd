<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY sanitizePrefs2.title          "Rregullime për Pastrim Historiku">
<!-- LOCALIZATION NOTE (sanitizePrefs2.modal.width): width of the Clear History on Shutdown dialog.
     Should be large enough to contain the item* strings on a single line.
     The column width should be set at half of the dialog width. -->
<!ENTITY sanitizePrefs2.modal.width    "34em">
<!ENTITY sanitizePrefs2.column.width   "17em">

<!ENTITY sanitizeDialog2.title         "Pastro Historikun Së Fundi">
<!-- LOCALIZATION NOTE (sanitizeDialog2.width): width of the Clear Recent History dialog -->
<!ENTITY sanitizeDialog2.width         "34em">

<!ENTITY clearDataSettings3.label     "Kur e mbyll &brandShortName;-in, duhet të pastrojë vetvetiu krejt">

<!ENTITY clearDataSettings4.label     "Kur mbyllet, &brandShortName;-i duhet t&apos;i pastrojë të tëra automatikisht">

<!-- XXX rearrange entities to match physical layout when l10n isn't an issue -->
<!-- LOCALIZATION NOTE (clearTimeDuration.*): "Time range to clear" dropdown.
     See UI mockup at bug 480169 -->
<!ENTITY clearTimeDuration.label          "Interval kohor për t&apos;u pastruar: ">
<!ENTITY clearTimeDuration.accesskey      "I">
<!ENTITY clearTimeDuration.lastHour       "Ora e Fundit">
<!ENTITY clearTimeDuration.last2Hours     "Dy Orët e Fundit">
<!ENTITY clearTimeDuration.last4Hours     "Katër Orët e Fundit">
<!ENTITY clearTimeDuration.today          "Sot">
<!ENTITY clearTimeDuration.everything     "Gjithçka">
<!-- Localization note (clearTimeDuration.suffix) - trailing entity for languages
that require it.  -->
<!ENTITY clearTimeDuration.suffix         "">

<!-- LOCALIZATION NOTE (detailsProgressiveDisclosure.*): Labels and accesskeys
     of the "Details" progressive disclosure button.  See UI mockup at bug
     480169 -->
<!ENTITY detailsProgressiveDisclosure.label     "Hollësi">
<!ENTITY detailsProgressiveDisclosure.accesskey "h">

<!ENTITY historySection.label         "Historik">
<!ENTITY dataSection.label            "Të dhëna">

<!ENTITY itemHistoryAndDownloads.label     "Historik Shfletimesh dhe Shkarkimesh">
<!ENTITY itemHistoryAndDownloads.accesskey "H">
<!ENTITY itemFormSearchHistory.label       "Historik Formularësh &amp; Kërkimesh">
<!ENTITY itemFormSearchHistory.accesskey   "F">
<!ENTITY itemCookies.label                 "Cookie">
<!ENTITY itemCookies.accesskey             "C">
<!ENTITY itemCache.label                   "Fshehtinë">
<!ENTITY itemCache.accesskey               "e">
<!ENTITY itemOfflineApps.label             "Të dhëna Sajti Jashtë Linje">
<!ENTITY itemOfflineApps.accesskey         "T">
<!ENTITY itemActiveLogins.label            "Hyrje Aktive">
<!ENTITY itemActiveLogins.accesskey        "A">
<!ENTITY itemSitePreferences.label         "Parapëlqime Për Sajtin">
<!ENTITY itemSitePreferences.accesskey     "P">

<!-- LOCALIZATION NOTE (sanitizeEverythingUndoWarning): Second warning paragraph
     that appears when "Time range to clear" is set to "Everything".  See UI
     mockup at bug 480169 -->
<!ENTITY sanitizeEverythingUndoWarning     "Ky veprim nuk mund të zhbëhet.">

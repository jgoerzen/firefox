# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fonts-window =
    .title = ফন্ট
fonts-window-close =
    .key = w

## Font groups by language

fonts-langgroup-arabic =
    .label = আৰবী
fonts-langgroup-armenian =
    .label = আৰ্মেনীয়
fonts-langgroup-bengali =
    .label = বঙালী
fonts-langgroup-simpl-chinese =
    .label = সাধাৰণ চীনা
fonts-langgroup-trad-chinese-hk =
    .label = পাৰম্পৰিক চীনা (হং কং)
fonts-langgroup-trad-chinese =
    .label = পাৰম্পৰিক চীনা (টাইৱান)
fonts-langgroup-cyrillic =
    .label = চিৰিলিক
fonts-langgroup-devanagari =
    .label = দেৱনগৰি
fonts-langgroup-ethiopic =
    .label = ঈথিওপিক
fonts-langgroup-georgian =
    .label = জৰ্জীয়ান
fonts-langgroup-el =
    .label = গ্ৰীক
fonts-langgroup-gujarati =
    .label = গুজৰাটী
fonts-langgroup-gurmukhi =
    .label = গুৰ্মুখী
fonts-langgroup-japanese =
    .label = জাপানী
fonts-langgroup-hebrew =
    .label = হিব্ৰু
fonts-langgroup-kannada =
    .label = কন্নড়
fonts-langgroup-khmer =
    .label = খ্‌মেৰ
fonts-langgroup-korean =
    .label = কোৰিয়ান
# Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language.
fonts-langgroup-latin =
    .label = লেটিন
fonts-langgroup-malayalam =
    .label = মলয়ালম
fonts-langgroup-sinhala =
    .label = সিংহালা
fonts-langgroup-tamil =
    .label = তামিল
fonts-langgroup-telugu =
    .label = টেলুগু
fonts-langgroup-thai =
    .label = থাই
fonts-langgroup-tibetan =
    .label = তিব্বতীয়
fonts-langgroup-canadian =
    .label = ইউনিফাইড কেনেডীয় ছিলাবাৰী
fonts-langgroup-other =
    .label = অন্য লিখাৰ চিস্টেমসমূহ

## Default fonts and their sizes

fonts-default-serif =
    .label = Serif
fonts-default-sans-serif =
    .label = Sans Serif
fonts-minsize-none =
    .label = একো নাই

## Text Encodings
##
## Translate the encoding names as adjectives for an encoding, not as the name
## of the language.

fonts-languages-fallback-name-auto =
    .label = বৰ্তমান স্থানীয়ৰ বাবে অবিকল্পিত
fonts-languages-fallback-name-arabic =
    .label = আৰবী
fonts-languages-fallback-name-baltic =
    .label = বল্টিক
fonts-languages-fallback-name-ceiso =
    .label = মধ্য ইউৰোপীয়, ISO
fonts-languages-fallback-name-cewindows =
    .label = মধ্য ইউৰোপীয়, Microsoft
fonts-languages-fallback-name-simplified =
    .label = চীনা, সৰলিকৃত
fonts-languages-fallback-name-traditional =
    .label = চীনা, পাৰম্পৰিক
fonts-languages-fallback-name-cyrillic =
    .label = চিৰিলিক
fonts-languages-fallback-name-greek =
    .label = গ্ৰীক
fonts-languages-fallback-name-hebrew =
    .label = হিব্ৰু
fonts-languages-fallback-name-japanese =
    .label = জাপানী
fonts-languages-fallback-name-korean =
    .label = কোৰিয়ান
fonts-languages-fallback-name-thai =
    .label = থাই
fonts-languages-fallback-name-turkish =
    .label = তুৰ্কী
fonts-languages-fallback-name-vietnamese =
    .label = ভিয়েতনামি
fonts-languages-fallback-name-other =
    .label = অন্য (পশ্চিম ইউৰোপীয় অন্তৰ্ভুক্ত)
# Variables:
#   $name {string, "Arial"} - Name of the default font
fonts-label-default =
    .label = অবিকল্পিত ({ $name })

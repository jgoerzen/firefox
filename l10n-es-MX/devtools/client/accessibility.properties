# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Accessibility panel
# which is available from the Web Developer sub-menu -> 'Accessibility'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (accessibility.role): A title text used for Accessibility
# tree header column that represents accessible element role.
accessibility.role=Rol

# LOCALIZATION NOTE (accessibility.name): A title text used for Accessibility
# tree header column that represents accessible element name.
accessibility.name=Nombre

# LOCALIZATION NOTE (accessibility.logo): A title text used for Accessibility
# logo used on the accessibility panel landing page.
accessibility.logo=Logo de accesibilidad

# LOCALIZATION NOTE (accessibility.properties): A title text used for header
# for Accessibility details sidebar.
accessibility.properties=Propiedades

# LOCALIZATION NOTE (accessibility.treeName): A title text used for
# Accessibility tree (that represents accessible element name) container.
accessibility.treeName=Árbol de accesibilidad

# LOCALIZATION NOTE (accessibility.accessible.notAvailable): A title text
# displayed when accessible sidebar panel does not have an accessible object to
# display.
accessibility.accessible.notAvailable=Información de accesibilidad no disponible

# LOCALIZATION NOTE (accessibility.enable): A title text for Enable
# accessibility button used to enable accessibility service.
accessibility.enable=Habilitar características de accesibilidad

# LOCALIZATION NOTE (accessibility.enabling): A title text for Enable
# accessibility button used when accessibility service is being enabled.
accessibility.enabling=Habilitando características de accesibilidad…

# LOCALIZATION NOTE (accessibility.disable): A title text for Disable
# accessibility button used to disable accessibility service.
accessibility.disable=Deshabilitar características de accesibilidad

# LOCALIZATION NOTE (accessibility.disabling): A title text for Disable
# accessibility button used when accessibility service is being
# disabled.
accessibility.disabling=Deshabilitando características de accesibilidad…

# LOCALIZATION NOTE (accessibility.pick): A title text for Picker button
# button used to pick accessible objects from the page.
accessibility.pick=Seleccionar un objeto accesible de la página

# LOCALIZATION NOTE (accessibility.disable.disabledTitle): A title text used for
# a tooltip for Disable accessibility button when accessibility service can not
# be disabled. It is the case when a user is using a 3rd party accessibility
# tool such as screen reader.
accessibility.disable.disabledTitle=El servicio de accesibilidad no puede deshabilitarse. Es usado fuera de las Herramientas de Desarrollador.

# LOCALIZATION NOTE (accessibility.disable.enabledTitle): A title text used for
# a tooltip for Disable accessibility button when accessibility service can be
# disabled.
accessibility.disable.enabledTitle=El servicio de accesibilidad se deshabilitará para todas las pestañas y ventanas.

# LOCALIZATION NOTE (accessibility.enable.disabledTitle): A title text used for
# a tooltip for Enabled accessibility button when accessibility service can not
# be enabled.
accessibility.enable.disabledTitle=El servicio de accesibilidad no puede habilitarse. Se habilitará a través de las preferencias de privacidad de servicios de accesibilidad.

# LOCALIZATION NOTE (accessibility.enable.enabledTitle): A title text used for
# a tooltip for Enabled accessibility button when accessibility service can be
# enabled.
accessibility.enable.enabledTitle=El servicio de accesibilidad se habilitará para todas las pestañas y ventanas.

# LOCALIZATION NOTE (accessibility.learnMore): A text that is used as is or as textual
# description in places that link to accessibility inspector documentation.
accessibility.learnMore=Aprender más

# LOCALIZATION NOTE (accessibility.description.general): A title text used when
# accessibility service description is provided before accessibility inspector
# is enabled.
accessibility.description.general=La funciones de accesibilidad están deshabilitadas de manera predeterminada debido a un impacto negativo en el rendimiento. Considera deshabilitar las funciones de accesibilidad antes de utilizar otros paneles de Herramientas para desarrolladores.

# LOCALIZATION NOTE (accessibility.description.general.p1): A title text for the first
# paragraph, used when accessibility service description is provided before accessibility
# inspector is enabled. %S in the content will be replaced by a link at run time
# with the accessibility.learnMore string.
accessibility.description.general.p1=El inspector de accesibilidad te permite examinar el árbol de accesibilidad de la página actual, que es usada por lectores de pantalla y otras tecnologías de asistencia. %S

# LOCALIZATION NOTE (accessibility.description.general.p2): A title text for the second
# paragraph, used when accessibility service description is provided before accessibility
# inspector is enabled.
accessibility.description.general.p2=Las características de accesibilidad pueden afectar el desempeño de otros paneles de herramientas de desarrollo y deben desactivarse cuando no se usen.

# LOCALIZATION NOTE (accessibility.description.oldVersion): A title text used
# when accessibility service description is provided when a client is connected
# to an older version of accessibility actor.
accessibility.description.oldVersion=Está conectado a un servidor de depuración que es muy antiguo. Para usar el panel de Accesibilidad, por favor conéctate a la última versión del servidor de depuración.

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Acerca de herramientas de desarrollador">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Habilitar herramientas de Firefox Developer">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Habilitar las herramientas de desarrollador de Firefox para usar Inspeccionar elemento">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Examinar y editar HTML y CSS para el inspector de herramientas de desarrollador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Desarrollar y depurar WebExtensions, web workers, service workers y más con las herramientas de desarrollador de Firefox.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Activaste un atajo de las herramientas de desarrollador. Si eso fue un error, puedes cerrar esta pestaña.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Examinar, editar y depurar HTML, CSS, y JavaScript con herramientas como Inspector y Depurador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Perfecciona el HTML, CSS y JavaScript de tu sitio web con herramientas como el Inspector y el Depurador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Las herramientas de desarrollador están deshabilitadas por defecto para darte más control sobre tu navegador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Conoce más acerca de las herramientas de desarrollador">

<!ENTITY  aboutDevtools.enable.enableButton "Habilitar herramientas de desarrollador">
<!ENTITY  aboutDevtools.enable.closeButton "Cerrar esta página">

<!ENTITY  aboutDevtools.enable.closeButton2 "Cerrar esta pestaña">

<!ENTITY  aboutDevtools.welcome.title "¡Bienvenido a las herramientas de desarrollador de Firefox!">

<!ENTITY  aboutDevtools.newsletter.title "Boletín de Mozilla Developer">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Obtén noticias de desarrollo, trucos y recursos enviados directamente a tu bandeja de entrada.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "Correo electrónico">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Estoy de acuerdo con que Mozilla maneje mi información para explican en su <a class='external' href='https://www.mozilla.org/privacy/'>Política de Privacidad</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Suscribir">
<!ENTITY  aboutDevtools.newsletter.thanks.title "¡Gracias!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Si no has confirmado previamente una suscripción a un boletín relacionado a Mozilla, puede que tengas que hacerlo. Por favor, revisa tu bandeja de entrada o filtro de spam en busca de un email de nosotros.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "¿Buscas más que unas simples herramientas de desarrollador? Revisa el navegador Firefox que está creado específicamente por desarrolladores y flujos de trabajo modernos.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Aprender más">


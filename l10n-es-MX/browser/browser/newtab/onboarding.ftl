# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Pruébalo ahora
onboarding-welcome-header = Bienvenido a { -brand-short-name }
onboarding-start-browsing-button-label = Empieza a navegar

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Navegación Privada
onboarding-private-browsing-text = Navegue en solitatio. Navegación Privada con Bloqueo de Contenido bloquea los rastreadores en línea que te siguen por la web.
onboarding-screenshots-title = Screenshots
onboarding-screenshots-text = Toma, guarda y comparte capturas de pantalla sin dejar { -brand-short-name }. Captura una parte o la página completa mientras navegas. Luego guarda en la web para compartir y acceder fácilmente.
onboarding-addons-title = Complementos
onboarding-addons-text = Agrega aun más funcionalidades para hacer que { -brand-short-name } trabaje mejor para ti. Compara precios, mira el clima o expresa tu personalidad con un tema personalizado.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Navega más rápido, de manera más inteligente o segura con complementos como Ghostery que te permite bloquear los molestos anuncios.

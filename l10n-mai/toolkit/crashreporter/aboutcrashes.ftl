# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

clear-all-reports-label = Remove All Reports
delete-confirm-title = की अहाँ निश्चित छी?
delete-confirm-description = This will delete all reports and cannot be undone.
id-heading = Report ID
crashes-submitted-label = Submitted Crash Reports
date-submitted-heading = Date Submitted
no-reports-label = No crash reports have been submitted.
no-config-label = This application has not been configured to display crash reports. The preference <code>breakpad.reportURL</code> must be set.

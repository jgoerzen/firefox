# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-desc = अहाँ चुनि सकैत छी जे फाॅयरफ़ाॅक्स कोन सूची प्रयोग करत { -brand-short-name } वेब तत्व केँ ब्लाॅक करए कलेल जे कि अहाँ ब्राउजिंग गतिविधि केँ ट्रैक कए सकैत अछि.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = सूची
blocklist-button-cancel =
    .label = रद्द करू
    .accesskey = र
blocklist-button-ok =
    .label = परिवर्तन सहेजू
    .accesskey = प
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = डिस्कनेक्ट.मी बेसिक सुरक्षा(अनुशंसित)
blocklist-item-moz-std-desc = कुछ ट्रैकर्स केँ अनुमति दिअ ताकि वेबसाइट सही तरह सँ काज क सकै.
blocklist-item-moz-full-name = डिस्कनेक्ट.मी कठोर सुरक्षा.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-search-textbox =
    .placeholder = ওয়েবসাইটের অনুসন্ধান করুন
    .accesskey = S
site-data-column-host =
    .label = সাইট
site-data-column-storage =
    .label = সংগ্রহশালা
site-data-remove-selected =
    .label = নির্বাচন করা অপসরণ করুন
    .accesskey = r
site-data-button-cancel =
    .label = বাতিল
    .accesskey = C
site-data-button-save =
    .label = পরিবর্তন সংরক্ষণ করুন
    .accesskey = a
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = সব অপসারণ করুন
    .accesskey = e
site-data-remove-shown =
    .label = প্রদর্শিত সব কিছু অপসারণ করুন
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
site-data-removing-dialog =
    .title = { site-data-removing-header }
    .buttonlabelaccept = অপসারণ করুন

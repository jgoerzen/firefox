# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = ব্যতিক্রম
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-address = ওয়েবসাইটের ঠিকানা
    .accesskey = d
permissions-block =
    .label = প্রতিরোধ করা হবে
    .accesskey = B
permissions-session =
    .label = সেশনের জন্য প্রবেশাধিকার প্রদান করা হবে
    .accesskey = S
permissions-allow =
    .label = প্রবেশাধিকার প্রদান করুন
    .accesskey = A
permissions-site-name =
    .label = ওয়েবসাইট
permissions-status =
    .label = অবস্থা
permissions-remove =
    .label = ওয়েবসাইট অপসারণ করুন
    .accesskey = R
permissions-remove-all =
    .label = সমস্ত ওয়েবসাইটগুলি মুছে ফেলুন
    .accesskey = e
permissions-button-cancel =
    .label = বাতিল
    .accesskey = C
permissions-button-ok =
    .label = পরিবর্তন সংরক্ষণ করুন
    .accesskey = S
permissions-searchbox =
    .placeholder = ওয়েবসাইটের অনুসন্ধান করুন
permissions-capabilities-allow =
    .label = অনুমতি প্রদান করা হবে
permissions-capabilities-block =
    .label = অবরুদ্ধ করুন
permissions-capabilities-prompt =
    .label = সর্বদা জিজ্ঞাসা করা হবে

## Invalid Hostname Dialog

permissions-invalid-uri-title = অবৈধ হোস্ট-নেম উল্লিখিত হয়েছে
permissions-invalid-uri-label = অনুগ্রহ করে বৈধ হোস্ট-নেম উল্লেখ করুন

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = ব্যতিক্রমসমূহ - ট্র্যাকিং প্রোটেকশন
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = আপনি এই ওয়েবসাইটগুলিতে ট্র্যাকিং প্রোটেকশন বন্ধ করেছেন।

## Exceptions - Cookies


## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = অনুমতিপ্রাপ্ত ওয়েবসাইট - পপ-আপ
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = কোন ওয়েব-সাইটগুলিকে পপ-আপ উইন্ডো প্রদর্শন করার অনুমতি প্রদান করা হবে আপনি তা উল্লেখ করতে পারেন। সাইটের সঠিক ঠিকানা লিখে অনুমতি প্রদান করুন ক্লিক করুন।

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = ব্যতিক্রম - সংরক্ষিত লগইন
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = নিম্নলিখিত ওয়েবসাইটগুলির জন্য লগইন সংক্রান্ত তথ্য সংরক্ষণ করা হবে না

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = অনুমতিপ্রাপ্ত ওয়েবসাইট - অ্যাড-অন ইনস্টলেশন
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = কোন ওয়েব-সাইটগুলিকে সফ্টওয়্যার ইনস্টল করার অনুমতি প্রদান করা হবে আপনি তা উল্লেখ করতে পারেন। সাইটের সঠিক ঠিকানা লিখে অনুমতি প্রদান করুন ক্লিক করুন।

## Exceptions - Autoplay Media


## Site Permissions - Notifications

permissions-site-notification-window =
    .title = সেটিংস - বিজ্ঞপ্তির অনুমতি
    .style = { permissions-window.style }

## Site Permissions - Location

permissions-site-location-window =
    .title = সেটিংস - অবস্থান অনুমতি
    .style = { permissions-window.style }
permissions-site-location-disable-desc = এটি আপনার অবস্থান অ্যাক্সেস করার জন্য অনুমতি অনুরোধ থেকে উপরে তালিকাভুক্ত নয়  যেকোন ওয়েবসাইটকে প্রতিরোধ করবে। আপনার অবস্থান অ্যাক্সেস ব্লক কিছু ওয়েবসাইট বৈশিষ্ট্য বিরত হতে পারে।

## Site Permissions - Camera

permissions-site-camera-window =
    .title = সেটিংস - ক্যামেরা অনুমতি
    .style = { permissions-window.style }

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = সেটিংস - মাইক্রোফোন অনুমতি
    .style = { permissions-window.style }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = ভাষা
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = অনেক ক্ষেত্রে ওয়েব-পেজগুলি একাধিক ভাষায় প্রদর্শিত হয়। ওয়েব-পেজ প্রদর্শনের জন্য ব্যবহৃত ভাষা আপনার পছন্দ অনুসারে নির্বাচন করুন
languages-customize-moveup =
    .label = উপরে স্থানান্তর
    .accesskey = U
languages-customize-movedown =
    .label = নীচে স্থানান্তর
    .accesskey = D
languages-customize-remove =
    .label = মুছে ফেলুন
    .accesskey = R
languages-customize-select-language =
    .placeholder = যোগ করার উদ্দেশ্যে একটি ভাষা নির্বাচন করুন...
languages-customize-add =
    .label = যোগ করুন
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

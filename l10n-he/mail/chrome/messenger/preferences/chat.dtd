<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  generalTab.label            "כללי">

<!ENTITY  startupAction.label         "כאשר &brandShortName; מופעל:">
<!ENTITY  startupAction.accesskey     "פ">
<!ENTITY  startupOffline.label        "להשאיר את חשבון הצ׳אט שלי לא מקוון">
<!ENTITY  startupConnectAuto.label    "התחברות לחשבונות הצ׳אט שלי אוטומטית">

<!-- LOCALIZATION NOTE: reportIdleAfter.label is displayed first, then
there's a field where the user can enter a number, and itemTime is
displayed at the end of the line. The translations of the
reportIdleAfter.label and idleTime parts don't have to mean the exact
same thing as in English; please try instead to translate the whole
sentence. -->
<!ENTITY  reportIdleAfter.label         "לאפשר לאנשי הקשר שלי לדעת על חוסר פעילות מצדי לאחר">
<!ENTITY  reportIdleAfter.accesskey     "פ">
<!ENTITY  idleTime                      "דקות של חוסר פעילות">

<!ENTITY  andSetStatusToAway.label      "ולהגדיר את המצב למרוחק עם הודעה מצב זו:">
<!ENTITY  andSetStatusToAway.accesskey  "ר">

<!ENTITY  sendTyping.label              "שליחת התרעת הקלדה בדיונים">
<!ENTITY  sendTyping.accesskey          "ק">

<!ENTITY  chatNotifications.label             "כשמגיעות הודעות שמיועדות אליך:">
<!ENTITY  desktopChatNotifications.label      "הצגת התרעה:">
<!ENTITY  desktopChatNotifications.accesskey  "ר">
<!ENTITY  completeNotification.label          "עם שם השולח ותצוגה מקדימה של הודעה">
<!ENTITY  buddyInfoOnly.label                 "עם שם השולח בלבד">
<!ENTITY  dummyNotification.label             "ללא כל מידע">
<!ENTITY  getAttention.label                  "הבהוב הפריט בשורת המשימות">
<!ENTITY  getAttention.accesskey              "ב">
<!ENTITY  getAttentionMac.label               "הנפשת הסמל במעגן">
<!ENTITY  getAttentionMac.accesskey           "ע">
<!ENTITY  chatSound.accesskey                 "ס">
<!ENTITY  chatSound.label                     "השמעת צליל">
<!ENTITY  play.label                          "השמעה">
<!ENTITY  play.accesskey                      "ש">
<!ENTITY  systemSound.label                   "צליל בררת המחדל של המערכת להודעות דוא״ל חדשות">
<!ENTITY  systemSound.accesskey               "ב">
<!ENTITY  customsound.label                   "שימוש בקובץ השמע הבא">
<!ENTITY  customsound.accesskey               "ש">
<!ENTITY  browse.label                        "עיון…">
<!ENTITY  browse.accesskey                    "ע">

<!ENTITY messageStyleTab.title             "סגנונות הודעות">
<!ENTITY messageStylePreview.label         "תצוגה מקדימה:">
<!ENTITY messageStyleTheme.label           "ערכת נושא:">
<!ENTITY messageStyleTheme.accesskey       "ע">
<!ENTITY messageStyleThunderbirdTheme.label "Thunderbird">
<!ENTITY messageStyleBubblesTheme.label    "בועות">
<!ENTITY messageStyleDarkTheme.label       "כהה">
<!ENTITY messageStylePaperSheetsTheme.label "גיליונות נייר">
<!ENTITY messageStyleSimpleTheme.label     "פשוט">
<!ENTITY messageStyleDefaultTheme.label    "בררת מחדל">
<!ENTITY messageStyleVariant.label         "הגוון:">
<!ENTITY messageStyleVariant.accesskey     "ג">
<!ENTITY messageStyleShowHeader.label      "הצגת כותרת">
<!ENTITY messageStyleShowHeader.accesskey  "כ">
<!ENTITY messageStyleNoPreview.title       "אין תצוגה מקדימה זמינה">

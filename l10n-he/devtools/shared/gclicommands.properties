# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside Web Console commands.
# The Web Console command line is available from the Web Developer sub-menu
# -> 'Web Console'.
#
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (helpDesc) A very short string used to describe the
# function of the help command.
helpDesc=קבלת עזרה על הפקודות הזמינות

# LOCALIZATION NOTE (helpAvailable) Used in the output of the help command to
# explain the contents of the command help table.

# LOCALIZATION NOTE (notAvailableInE10S) Used in the output of any command that
# is not compatible with multiprocess mode (E10S).

# LOCALIZATION NOTE (consoleDesc) A very short string used to describe the
# function of the console command.
consoleDesc=פקודות לשליטה במסוף

# LOCALIZATION NOTE (consoleManual) A longer description describing the
# set of commands that control the console.
consoleManual=סינון, מחיקה וסגירת פלט המסוף

# LOCALIZATION NOTE (consoleclearDesc) A very short string used to describe the
# function of the 'console clear' command.
consoleclearDesc=מחיקת פלט המסוף

# LOCALIZATION NOTE (screenshotDesc) A very short description of the
# 'screenshot' command. See screenshotManual for a fuller description of what
# it does. This string is designed to be shown in a menu alongside the
# command name, which is why it should be as short as possible.
screenshotDesc=שמירת תמונה של הדף

# LOCALIZATION NOTE (screenshotManual) A fuller description of the 'screenshot'
# command, displayed when the user asks for help on what it does.
screenshotManual=שמירת תמונת PNG של כל החלון החשוף (אפשר גם לאחר השהייה)

# LOCALIZATION NOTE (screenshotFilenameDesc) A very short string to describe
# the 'filename' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotFilenameDesc=קובץ יעד

# LOCALIZATION NOTE (screenshotFilenameManual) A fuller description of the
# 'filename' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotFilenameManual=שם הקובץ (צריך להיות עם סיומת '.png') אליו ישמר תצלום המסך.

# LOCALIZATION NOTE (screenshotClipboardDesc) A very short string to describe
# the 'clipboard' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotClipboardDesc=העתקת צילום המסך ללוח? (true/false)

# LOCALIZATION NOTE (screenshotClipboardManual) A fuller description of the
# 'clipboard' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotClipboardManual=True אם ברצונך להעתיק את צילום המסך במקום לשמור אותו לקובץ.

# LOCALIZATION NOTE (screenshotGroupOptions) A label for the optional options of
# the screenshot command.
screenshotGroupOptions=אפשרויות

# LOCALIZATION NOTE (screenshotDelayDesc) A very short string to describe
# the 'delay' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotDelayDesc=השהייה (שניות)

# LOCALIZATION NOTE (screenshotDelayManual) A fuller description of the
# 'delay' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotDelayManual=זמן להמתין (בשניות) לפני שיילקח תצלום המסך

# LOCALIZATION NOTE (screenshotDPRDesc) A very short string to describe
# the 'dpr' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotDPRDesc=יחס הפיקסלים של המכשיר

# LOCALIZATION NOTE (screenshotDPRManual) A fuller description of the
# 'dpr' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotDPRManual=יחס הפיקסלים של ההתקן שיהיה בשימוש בזמן ביצוע הצילום

# LOCALIZATION NOTE (screenshotFullPageDesc) A very short string to describe
# the 'fullpage' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotFullPageDesc=כול הדף? (true/false)

# LOCALIZATION NOTE (screenshotFullPageManual) A fuller description of the
# 'fullpage' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotFullPageManual=‏True אם צילום המסך אמור לכלול חלקים מהעמוד הנמצאים מחוץ לגבולות הגלילה.

# LOCALIZATION NOTE (screenshotFileDesc) A very short string to describe
# the 'file' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotFileDesc=האם לשמור לקובץ? (true/false)

# LOCALIZATION NOTE (screenshotFileManual) A fuller description of the
# 'file' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotFileManual=‏True אם צילום המסך אמור לשמור את הקובץ אפילו כאשר אפשרויות אחרות פעילות (למשל: לוח עריכה).

# LOCALIZATION NOTE (screenshotGeneratedFilename) The auto generated filename
# when no file name is provided. The first argument (%1$S) is the date string
# in yyyy-mm-dd format and the second argument (%2$S) is the time string
# in HH.MM.SS format. Please don't add the extension here.
screenshotGeneratedFilename=צילום מסך %1$S בשעה %2$S

# LOCALIZATION NOTE (screenshotErrorSavingToFile) Text displayed to user upon
# encountering error while saving the screenshot to the file specified.
screenshotErrorSavingToFile=אירעה שגיאה בשמירה אל

# LOCALIZATION NOTE (screenshotSavedToFile) Text displayed to user when the
# screenshot is successfully saved to the file specified.
screenshotSavedToFile=נשמר אל

# LOCALIZATION NOTE (screenshotErrorCopying) Text displayed to user upon
# encountering error while copying the screenshot to clipboard.
screenshotErrorCopying=אירעה שגיאה בהעתקה ללוח.

# LOCALIZATION NOTE (screenshotCopied) Text displayed to user when the
# screenshot is successfully copied to the clipboard.
screenshotCopied=הועתק ללוח הגזירים.

# LOCALIZATION NOTE (screenshotTooltipPage) Text displayed as tooltip for screenshot button in devtools ToolBox.
screenshotTooltipPage=צילום העמוד כולו

# LOCALIZATION NOTE (screenshotImgurDesc) A very short string to describe
# the 'imgur' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotImgurDesc=העלאה אל imgur.com

# LOCALIZATION NOTE (screenshotImgurManual) A fuller description of the
# 'imgur' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotImgurManual=השתמש אם ברצונך להעלות את התמונה אל imgur.com במקום לשמור לדיסק

# LOCALIZATION NOTE (screenshotImgurError) Text displayed to user upon
# encountering error while uploading the screenshot to imgur.com.
screenshotImgurError=לא ניתן להגיע ל־API של imgur

# LOCALIZATION NOTE (screenshotImgurUploaded) Text displayed to user when the
# screenshot is successfully sent to Imgur but the program is waiting on a response.
# The argument (%1$S) is a new image URL at Imgur.
screenshotImgurUploaded=הועלה אל %1$S

# LOCALIZATION NOTE (inspectDesc) A very short description of the 'inspect'
# command. See inspectManual for a fuller description of what it does. This
# string is designed to be shown in a menu alongside the command name, which
# is why it should be as short as possible.
inspectDesc=חקירת צומת

# LOCALIZATION NOTE (inspectManual) A fuller description of the 'inspect'
# command, displayed when the user asks for help on what it does.
inspectManual=חקירת הממדים והמאפיינים של רכיב באמצעות בוחר CSS לפתיחה במדגיש ה־DOM

# LOCALIZATION NOTE (inspectNodeDesc) A very short string to describe the
# 'node' parameter to the 'inspect' command, which is displayed in a dialog
# when the user is using this command.
inspectNodeDesc=בוחר CSS

# LOCALIZATION NOTE (inspectNodeManual) A fuller description of the 'node'
# parameter to the 'inspect' command, displayed when the user asks for help
# on what it does.
inspectNodeManual=בוחר CSS לשימוש עם Document.querySelector שמסייע בזיהוי רכיב יחיד

# LOCALIZATION NOTE (eyedropperDesc) A very short description of the 'eyedropper'
# command. See eyedropperManual for a fuller description of what it does. This
# string is designed to be shown in a menu alongside the command name, which
# is why it should be as short as possible.
eyedropperDesc=דגימת צבע מהדף

# LOCALIZATION NOTE (eyedropperManual) A fuller description of the 'eyedropper'
# command, displayed when the user asks for help on what it does.
eyedropperManual=פתיחת חלונית המגדילה אזור בדף כדי לדגום פיקסלים ולהעתיק ערכי צבע

# LOCALIZATION NOTE (debuggerClosed) Used in the output of several commands
# to explain that the debugger must be opened first.
debuggerClosed=יש לפתוח את מנפה השגיאות טרם השימוש בפקודה זו

# LOCALIZATION NOTE (debuggerStopped) Used in the output of several commands
# to explain that the debugger must be opened first before setting breakpoints.
debuggerStopped=יש לפתוח את מנפה השגיאות לפני הגדרת נקודות עצירה

# LOCALIZATION NOTE (breakDesc) A very short string used to describe the
# function of the break command.
breakDesc=ניהול נקודות עצירה

# LOCALIZATION NOTE (breakManual) A longer description describing the
# set of commands that control breakpoints.
breakManual=פקודות להצגה, הוספה והסרה של נקודות עצירה

# LOCALIZATION NOTE (breaklistDesc) A very short string used to describe the
# function of the 'break list' command.
breaklistDesc=הצגת נקודות עצירה ידועות

# LOCALIZATION NOTE (breaklistNone) Used in the output of the 'break list'
# command to explain that the list is empty.
breaklistNone=לא הוגדרו נקודות עצירה

# LOCALIZATION NOTE (breaklistOutRemove) A title used in the output from the
# 'break list' command on a button which can be used to remove breakpoints
breaklistOutRemove=הסרה

# LOCALIZATION NOTE (breakaddAdded) Used in the output of the 'break add'
# command to explain that a breakpoint was added.
breakaddAdded=נקודת עצירה נוספה

# LOCALIZATION NOTE (breakaddFailed) Used in the output of the 'break add'
# command to explain that a breakpoint could not be added.
breakaddFailed=לא ניתן להגדיר נקודת עצירה: %S

# LOCALIZATION NOTE (breakaddDesc) A very short string used to describe the
# function of the 'break add' command.
breakaddDesc=הוספת נקודת עצירה

# LOCALIZATION NOTE (breakaddManual) A longer description describing the
# set of commands that are responsible for adding breakpoints.
breakaddManual=סוגי נקודת עצירה נתמכים: שורה

# LOCALIZATION NOTE (breakaddlineDesc) A very short string used to describe the
# function of the 'break add line' command.
breakaddlineDesc=הוספת שורת עצירה

# LOCALIZATION NOTE (breakaddlineFileDesc) A very short string used to describe
# the function of the file parameter in the 'break add line' command.
breakaddlineFileDesc=כתובת קובץ ה־JS

# LOCALIZATION NOTE (breakaddlineLineDesc) A very short string used to describe
# the function of the line parameter in the 'break add line' command.
breakaddlineLineDesc=מספר שורה

# LOCALIZATION NOTE (breakdelDesc) A very short string used to describe the
# function of the 'break del' command.
breakdelDesc=הסרת נקודת עצירה

# LOCALIZATION NOTE (breakdelBreakidDesc) A very short string used to describe
# the function of the index parameter in the 'break del' command.
breakdelBreakidDesc=אינדקס נקודת העצירה

# LOCALIZATION NOTE (breakdelRemoved) Used in the output of the 'break del'
# command to explain that a breakpoint was removed.
breakdelRemoved=נקודת עצירה הוסרה

# LOCALIZATION NOTE (dbgDesc) A very short string used to describe the
# function of the dbg command.
dbgDesc=ניהול מנפה שגיאות

# LOCALIZATION NOTE (dbgManual) A longer description describing the
# set of commands that control the debugger.
dbgManual=Commands to interrupt or resume the main thread, step in, out and over lines of code

# LOCALIZATION NOTE (dbgOpen) A very short string used to describe the function
# of the dbg open command.
dbgOpen=פתיחת מנפה השגיאות

# LOCALIZATION NOTE (dbgClose) A very short string used to describe the function
# of the dbg close command.
dbgClose=סגירת מנפה השגיאות

# LOCALIZATION NOTE (dbgInterrupt) A very short string used to describe the
# function of the dbg interrupt command.
dbgInterrupt=Pauses the main thread

# LOCALIZATION NOTE (dbgContinue) A very short string used to describe the
# function of the dbg continue command.
dbgContinue=Resumes the main thread, and continues execution following a breakpoint, until the next breakpoint or the termination of the script.

# LOCALIZATION NOTE (dbgStepDesc) A very short string used to describe the
# function of the dbg step command.
dbgStepDesc=ניהול צעדים

# LOCALIZATION NOTE (dbgStepManual) A longer description describing the
# set of commands that control stepping.
dbgStepManual=פקודות לכניסה, יציאה ודילוג מעל שורות של קוד

# LOCALIZATION NOTE (dbgStepOverDesc) A very short string used to describe the
# function of the dbg step over command.
dbgStepOverDesc=Executes the current statement and then stops at the next statement. If the current statement is a function call then the debugger executes the whole function, and it stops at the next statement after the function call

# LOCALIZATION NOTE (dbgStepInDesc) A very short string used to describe the
# function of the dbg step in command.
dbgStepInDesc=Executes the current statement and then stops at the next statement. If the current statement is a function call, then the debugger steps into that function, otherwise it stops at the next statement

# LOCALIZATION NOTE (dbgStepOutDesc) A very short string used to describe the
# function of the dbg step out command.
dbgStepOutDesc=Steps out of the current function and up one level if the function is nested. If in the main body, the script is executed to the end, or to the next breakpoint. The skipped statements are executed, but not stepped through

# LOCALIZATION NOTE (dbgListSourcesDesc) A very short string used to describe the
# function of the dbg list command.
dbgListSourcesDesc=הצגת כתובות המקור שנטענו במנפה השגיאות

# LOCALIZATION NOTE (dbgBlackBoxDesc) A very short string used to describe the
# function of the 'dbg blackbox' command.

# LOCALIZATION NOTE (dbgBlackBoxSourceDesc) A very short string used to describe the
# 'source' parameter to the 'dbg blackbox' command.

# LOCALIZATION NOTE (dbgBlackBoxGlobDesc) A very short string used to describe the
# 'glob' parameter to the 'dbg blackbox' command.

# LOCALIZATION NOTE (dbgBlackBoxInvertDesc) A very short string used to describe the
# 'invert' parameter to the 'dbg blackbox' command.

# LOCALIZATION NOTE (dbgBlackBoxEmptyDesc) A very short string used to let the
# user know that no sources were black boxed.

# LOCALIZATION NOTE (dbgBlackBoxNonEmptyDesc) A very short string used to let the
# user know which sources were black boxed.

# LOCALIZATION NOTE (dbgBlackBoxErrorDesc) A very short string used to let the
# user know there was an error black boxing a source (whose url follows this
# text).

# LOCALIZATION NOTE (dbgUnBlackBoxDesc) A very short string used to describe the
# function of the 'dbg unblackbox' command.

# LOCALIZATION NOTE (dbgUnBlackBoxSourceDesc) A very short string used to describe the
# 'source' parameter to the 'dbg unblackbox' command.

# LOCALIZATION NOTE (dbgUnBlackBoxGlobDesc) A very short string used to describe the
# 'glob' parameter to the 'dbg blackbox' command.

# LOCALIZATION NOTE (dbgUnBlackBoxEmptyDesc) A very short string used to let the
# user know that we did not stop black boxing any sources.

# LOCALIZATION NOTE (dbgUnBlackBoxNonEmptyDesc) A very short string used to let the
# user know which sources we stopped black boxing.

# LOCALIZATION NOTE (dbgUnBlackBoxErrorDesc) A very short string used to let the
# user know there was an error black boxing a source (whose url follows this
# text).

# LOCALIZATION NOTE (dbgUnBlackBoxInvertDesc) A very short string used to describe the
# 'invert' parameter to the 'dbg unblackbox' command.

# LOCALIZATION NOTE (consolecloseDesc) A very short description of the
# 'console close' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
consolecloseDesc=סגירת המסוף

# LOCALIZATION NOTE (consoleopenDesc) A very short description of the
# 'console open' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
consoleopenDesc=פתח את המסוף

# LOCALIZATION NOTE (editDesc) A very short description of the 'edit'
# command. See editManual2 for a fuller description of what it does. This
# string is designed to be shown in a menu alongside the command name, which
# is why it should be as short as possible.
editDesc=שיפור משאב עמוד

# LOCALIZATION NOTE (editManual2) A fuller description of the 'edit' command,
# displayed when the user asks for help on what it does.
editManual2=עריכת אחד המשאבים שמרכיב את העמוד הזה

# LOCALIZATION NOTE (editResourceDesc) A very short string to describe the
# 'resource' parameter to the 'edit' command, which is displayed in a dialog
# when the user is using this command.
editResourceDesc=כתובת לעריכה

# LOCALIZATION NOTE (editLineToJumpToDesc) A very short string to describe the
# 'line' parameter to the 'edit' command, which is displayed in a dialog
# when the user is using this command.
editLineToJumpToDesc=מספר שורה לקפיצה

# LOCALIZATION NOTE (resizePageArgWidthDesc) A very short string to describe the
# 'width' parameter to the 'resizepage' command, which is displayed in a dialog
# when the user is using this command.
resizePageArgWidthDesc=רוחב בפיקסלים

# LOCALIZATION NOTE (resizePageArgHeightDesc) A very short string to describe the
# 'height' parameter to the 'resizepage' command, which is displayed in a dialog
# when the user is using this command.
resizePageArgHeightDesc=גובה בפיקסלית

# LOCALIZATION NOTE (resizeModeOnDesc) A very short string to describe the
# 'resizeon ' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeOnDesc=כניסה למצב עיצוב מסתגל

# LOCALIZATION NOTE (resizeModeOffDesc) A very short string to describe the
# 'resize off' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeOffDesc=יציאה ממצב עיצוב מסתגל

# LOCALIZATION NOTE (resizeModeToggleDesc) A very short string to describe the
# 'resize toggle' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeToggleDesc=שינוי מצב עיצוב מסתגל

# LOCALIZATION NOTE (resizeModeToggleTooltip2) A string displayed as the
# tooltip of button in devtools toolbox which toggles Responsive Design Mode.
# Keyboard shortcut will be shown inside brackets.
resizeModeToggleTooltip2=מצב עיצוב מסתגל (%S)

# LOCALIZATION NOTE (resizeModeToDesc) A very short string to describe the
# 'resize to' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeToDesc=שינוי גודל דף

# LOCALIZATION NOTE (resizeModeDesc) A very short string to describe the
# 'resize' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeDesc=שליטה במצב עיצוב מסתגל

# LOCALIZATION NOTE (resizeModeManual2) A fuller description of the 'resize'
# command, displayed when the user asks for help on what it does.
# The argument (%1$S) is the browser name.
resizeModeManual2=אתרים מסתגלים מגיבים לסביבה שלהם כדי שייראו טוב בתצוגה של מכשירים ניידים, מסך קולנוע וכל מה שביניהם. מצב עיצוב מסתגל מאפשר לך לבדוק בקלות מגוון גדלים של תצוגות בתוך %1$S ללא צורך בשינוי גודל חלון הדפדפן.

# LOCALIZATION NOTE (scratchpadOpenTooltip) A string displayed as the
# tooltip of button in devtools toolbox which opens Scratchpad.
scratchpadOpenTooltip=דף טיוטה

# LOCALIZATION NOTE (paintflashingDesc) A very short string used to describe the
# function of the "paintflashing" command
paintflashingDesc=הדגשת האזור הצבוע

# LOCALIZATION NOTE (paintflashingOnDesc) A very short string used to describe the
# function of the "paintflashing on" command.
paintflashingOnDesc=הפעלת הבהוב צבע

# LOCALIZATION NOTE (paintflashingOffDesc) A very short string used to describe the
# function of the "paintflashing off" command.
paintflashingOffDesc=כיבוי הבהוב צבע

# LOCALIZATION NOTE (paintflashingChromeDesc) A very short string used to describe the
# function of the "paintflashing on/off chrome" command.

# LOCALIZATION NOTE (paintflashingManual) A longer description describing the
# set of commands that control paint flashing.
paintflashingManual=ציור אזורים שנצבעו מחדש בצבעים שונים

# LOCALIZATION NOTE (paintflashingTooltip) A string displayed as the
# tooltip of button in devtools toolbox which toggles paint flashing.
paintflashingTooltip=הדגשת האזורים הצבועים

# LOCALIZATION NOTE (paintflashingToggleDesc) A very short string used to describe the
# function of the "paintflashing toggle" command.
paintflashingToggleDesc=החלפת מצב להבהוב צבע

# LOCALIZATION NOTE (splitconsoleTooltip2) A string displayed as the
# tooltip of button in devtools toolbox which toggles the split webconsole.
# Keyboard shortcut will be shown inside brackets.
splitconsoleTooltip2=החלפת מצב תצוגה של מסוף מפוצל (%S)

# LOCALIZATION NOTE (rulersDesc) A very short description of the
# 'rulers' command. See rulersManual for a fuller description of what
# it does. This string is designed to be shown in a menu alongside the
# command name, which is why it should be as short as possible.
rulersDesc=הפעלה/כיבוי סרגלים לעמוד

# LOCALIZATION NOTE (rulersManual) A fuller description of the 'rulers'
# command, displayed when the user asks for help on what it does.
rulersManual=הפעלה/כיבוי של סרגלים אופקיים ואנכיים עבור הדף הנוכחי

# LOCALIZATION NOTE (rulersTooltip) A string displayed as the
# tooltip of button in devtools toolbox which toggles the rulers.
rulersTooltip=הפעלה/כיבוי של סרגלים לעמוד

# LOCALIZATION NOTE (measureDesc) A very short description of the
# 'measure' command. See measureManual for a fuller description of what
# it does. This string is designed to be shown in a menu alongside the
# command name, which is why it should be as short as possible.
measureDesc=מדידת חלק מהדף

# LOCALIZATION NOTE (measureManual) A fuller description of the 'measure'
# command, displayed when the user asks for help on what it does.
measureManual=הפעלת כלי המדידה כדי למדוד אזור שרירותי בעמוד

# LOCALIZATION NOTE (measureTooltip) A string displayed as the
# tooltip of button in devtools toolbox which toggles the measuring tool.
measureTooltip=מדידת חלק מהדף

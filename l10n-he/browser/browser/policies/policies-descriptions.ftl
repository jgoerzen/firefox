# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = הגדרת כתובת מותאמת אישית לעדכון היישום.
policy-Authentication = הגדרת אימות משולב לאתרים שתומכים בזה.
policy-BlockAboutAddons = חסימת הגישה למנהל התוספות (about:addons).
policy-BlockAboutConfig = חסימת הגישה לעמוד about:config.
policy-BlockAboutProfiles = חסימת הגישה לעמוד about:profiles.
policy-BlockAboutSupport = חסימת הגישה לעמוד about:support.
policy-Bookmarks = יצירת סימניות בסרגל הסימניות, תפריט הסימניות או בתיקייה מסוימת בתוכם.
policy-Certificates = האם להשתמש באישורים המובנים. מדיניות זו תקפה ל־Windows בלבד נכון לעכשיו.
policy-Cookies = לאשר או לסרב להגדרת עוגיות מאתרים.
policy-DisableAppUpdate = מניעת עדכון הדפדפן.
policy-DisableBuiltinPDFViewer = נטרול PDF.js, מציג ה־PDF המובנה ב־{ -brand-short-name }.
policy-DisableDeveloperTools = חסימת גישה לכלי הפיתוח.
policy-DisableFeedbackCommands = השבתת פקודות לשליחת משוב מתפריט העזרה (שליחת משוב ודיווח על אתר מטעה).
policy-DisableFirefoxAccounts = נטרול שירותים מבוססי { -fxaccount-brand-name }, לרבות Sync.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = נטרול התכונה Firefox Screenshots.
policy-DisableFirefoxStudies = למנוע מ־{ -brand-short-name } להריץ מחקרים.
policy-DisableForgetButton = מניעת גישה לכפתור 'לשכוח'
policy-DisableFormHistory = לא לזכור היסטוריית חיפוש וטפסים.
policy-DisableMasterPasswordCreation = אם true, לא ניתן ליצור ססמה ראשית.
policy-DisablePocket = נטרול התכונה לשמירת דפי אינטרנט ב־Pocket.
policy-DisablePrivateBrowsing = נטרול גלישה פרטית.
policy-DisableProfileImport = נטרול פקודת התפריט לייבוא נתונים מדפדפן אחר.
policy-DisableProfileRefresh = נטרול הכפתור 'רענון { -brand-short-name }' בעמוד about:support.
policy-DisableSecurityBypass = למנוע מהמשתמש לעקוף אזהרות אבטחה מסוימות.
policy-DisableSetDesktopBackground = להשבית את הפקודה לקביעת רקע כרקע שולחן העבודה בתפריט.
policy-DisableSetAsDesktopBackground = להשבית את הפקודה לקביעה כרקע שולחן העבודה בתפריט עבור תמונות.
policy-DisableSystemAddonUpdate = למנוע מהדפדפן להתקין ולעדכן תוספות מערכת.
policy-DisableTelemetry = כיבוי Telemetry.
policy-DisplayBookmarksToolbar = הצגת סרגל הכלים של הסימניות כברירת מחדל.
policy-DisplayMenuBar = הצגת סרגל התפריטים כברירת מחדל.
policy-DontCheckDefaultBrowser = נטרול בדיקת דפדפן ברירת המחדל עם ההפעלה.
# “lock” means that the user won’t be able to change this setting
policy-EnableTrackingProtection = הפעלה או נטרול של חסימת תוכן עם אפשרות לנעול את הבחירה.
policy-FlashPlugin = לאפשר או לדחות את השימוש בתוסף החיצוני Flash.
# “lock” means that the user won’t be able to change this setting
policy-Homepage = הגדרה ונעילה כרשות של דף הבית.
policy-InstallAddonsPermission = לאפשר לאתרים מסוימים להתקין תוספות.
policy-Permissions = קביעת תצורה של הרשאות עבור מצלמה, מיקרופון, מיקום והתרעות.
policy-PopupBlocking = לאפשר לאתרים מסוימים להקפיץ חלונות כברירת מחדל.
policy-Proxy = קביעת תצורה של הגדרות שרת מתווך.
policy-SanitizeOnShutdown = ניקוי כל נתוני הניווט עם הכיבוי.
policy-SearchBar = הגדרת מיקום ברירת המחדל של סרגל החיפוש. המשתמש עדיין מורשה להתאים זאת אישית.
policy-SearchEngines = הגדרת תצורת מנועי החיפוש. מדיניות זו זמינה רק בגרסה עם תמיכה מורחבת (ESR).
# “format” refers to the format used for the value of this policy. See also:
# https://github.com/mozilla/policy-templates/blob/master/README.md#websitefilter-machine-only
policy-WebsiteFilter = חסימת ביקור באתרים. יש לעיין בתיעוד לקבלת פרטים נוספים על התבנית.

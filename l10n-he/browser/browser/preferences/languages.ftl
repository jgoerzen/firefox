# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = שפות
    .style = width: 30em
webpage-languages-window =
    .title = הגדרות שפת עמוד אינטרנט
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = דפים מסויימים לעתים מוצעים ביותר משפה אחת. נא לציין כאן שפות להצגה לפי סדר חשיבות
languages-customize-spoof-english =
    .label = בקשת מהדורות באנגלית של אתרים להגברת הפרטיות
languages-customize-moveup =
    .label = הזז למעלה
    .accesskey = ל
languages-customize-movedown =
    .label = הזז למטה
    .accesskey = מ
languages-customize-remove =
    .label = הסרה
    .accesskey = ה
languages-customize-select-language =
    .placeholder = בחירת שפה להוספה…
languages-customize-add =
    .label = הוספה
    .accesskey = ס
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = הגדרות שפה של { -brand-short-name }
    .style = width: 40em
browser-languages-description = ‏{ -brand-short-name } יציג את השפה הראשונה כברירת המחדל שלך ויציג שפות חלופיות במידת הצורך לפי סדר הופעתם.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S tagad saglabā adreses, lai jūs varētu ātrāk aizpildīt formas.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Formu automātiskās aizpildes iestatījumi
autofillOptionsLinkOSX = Formu automātiskās aizpildes iestatījumi
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Formu aizpildes un drošības iestatījumi
autofillSecurityOptionsLinkOSX = Formu aizpildes un drošības iestatījumi
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Mainīt automātiskās aizpildes iestatījumus
changeAutofillOptionsOSX = Mainīt automātiskās aizpildes iestatījumus
changeAutofillOptionsAccessKey = C
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Koplietot adreses sinhronizētajās ierīcēs
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Koplietot kredītkartes sinhronizētajās ierīcēs
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Vai vēlaties atjaunināt savu adresi ar šo jauno informāciju?
updateAddressDescriptionLabel = Atjaunināmā adrese:
createAddressLabel = Izveidot jaunu adresi
createAddressAccessKey = A
updateAddressLabel = Atjaunināt adresi
updateAddressAccessKey = T
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Vai vēlaties, lai %S saglabā kredītakrtei? (drošības kods netiks saglabāts)
saveCreditCardDescriptionLabel = Saglabājamā kredītkarte:
saveCreditCardLabel = Saglabāt kredītkarti
saveCreditCardAccessKey = S
cancelCreditCardLabel = Nesaglabāt
cancelCreditCardAccessKey = D
neverSaveCreditCardLabel = Nekad nesaglabāt kredītkartes
neverSaveCreditCardAccessKey = N
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Vai vēlaties atjaunināt saglabāto kredītkarti ar jauno informāciju?
updateCreditCardDescriptionLabel = Atjaunināmā kredītkarte:
createCreditCardLabel = Izveidot jaunu kredītkarti
createCreditCardAccessKey = I
updateCreditCardLabel = Atjaunināt kredītkarti
updateCreditCardAccessKey = U
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Atvērt automātiskās aizpildes ziņojumu paneli

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Vairāk iestatījumu
autocompleteFooterOptionOSXShort = Iestatījumi
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = adrese
category.name = vārds
category.organization2 = uzņēmums
category.tel = telefons
category.email = e-pasts
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Aizpilda arī %S
phishingWarningMessage2 = Aizpilda %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S ir atklājis nedrošu lapu. Formu automātiskā aizpilde ir īslaicīgi deaktivēta.
# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Notīrīt automātiski saglabāto formu

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Automātiski aizpildīt adreses
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Uzzināt vairāk
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Saglabātās adreses…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Automātiski aizpildīt kredītkartes
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Saglabātās kredītkartes…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Saglabātās adreses
manageCreditCardsTitle = Saglabātās kredītkartes
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Adreses
creditCardsListHeader = Kredītkartes
showCreditCardsBtnLabel = Rādīt kredītkartes
hideCreditCardsBtnLabel = Paslēpt kredītkartes
removeBtnLabel = Aizvākt
addBtnLabel = Pievienot…
editBtnLabel = Rediģēt…
# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Pievienot adresi
editAddressTitle = Rediģēt adresi
givenName = Vārds
additionalName = Otrs vārds
familyName = Uzvārds
organization2 = Uzņēmums
streetAddress = Adrese
city = Pilsēta
province = Rajons
state = Štats
postalCode = Pasta indekss
zip = Pasta indekss
country = Štats vai reģions
tel = Telefons
email = Epasts
cancelBtnLabel = Atcelt
saveBtnLabel = Saglabāt
countryWarningMessage2 = Formu automātiskās aizpilde šobrīd ir pieejama tikai dažās valstīs.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Paslēpt kredītkarti
editCreditCardTitle = Rediģēt kredītkarti
cardNumber = Kartes numurs
nameOnCard = Kartes īpašnieks
cardExpires = Derīguma termiņš
billingAddress = Rēķina adrese

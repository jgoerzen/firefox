<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "डेवलपर उपकरणों के बारे में">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Firefox डेवलपर उपकरणों को सक्षम करें">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "इंस्पेक्ट एलिमेंट का उपयोग करने हेतु Firefox डेवलपर उपकरणों को सक्षम करें">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "HTML और CSS को डेवलपर उपकरणों के साथ जांचें तथा सम्पादित करें’ इंस्पेक्टर.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Firefox डेवलपर उपकरणों के साथ WebExtensions, वेब कर्ता , सेवा कर्ता  तथा अधिक चीजों को विकसित करें तथा पुनर्निरक्षण करें.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "आपने डेवलपर उपकरणों के एक शार्टकट को सक्रीय किया है. यदि यह एक गलती थी, तो आप इस टैब को बंद कर सकते हैं.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "इंस्पेक्टर तथा डीबगर जैसे उपकरणों के साथ HTML, CSS, और JavaScript कि जाँच, संपादन एवं पुनर्निरक्षण करें.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "इंस्पेक्टर तथा डीबगर जैसे उपकरणों के साथ अपने वेबसाइट के HTML, CSS, और JavaScript की सुधार करें.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "आपके ब्राउज़र के ऊपर आपको अधिक नियंत्रण देने के लिए Firefox डेवलपर उपकरण डिफ़ॉल्ट रूप से अक्षम है.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "डेवलपर उपकरणों के बारे में और जानें">

<!ENTITY  aboutDevtools.enable.enableButton "डेवलपर उपकरण सक्षम करें">
<!ENTITY  aboutDevtools.enable.closeButton "यह पृष्ठ बंद करें">

<!ENTITY  aboutDevtools.enable.closeButton2 "इस टैब को बन्द करें">

<!ENTITY  aboutDevtools.welcome.title "Firefox डेवलपर उपकरण में आपका स्वागत है!">

<!ENTITY  aboutDevtools.newsletter.title "Mozilla डेवलपर संवादपत्र">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "डेवलपर समाचार, तरकीबें और संसाधन सीधे अपने इनबॉक्स में पहुँचा हुआ पाएँ.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "ईमेल">
<!ENTITY  aboutDevtools.newsletter.privacy.label "मैं Mozilla द्वारा मेरी जानकारियों को संभालने से राजी हूँ जैसा कि इस <a class='external' href='https://www.mozilla.org/privacy/'>गोपनीयता निति</a>में बताया गया है.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "सदस्यता लें">
<!ENTITY  aboutDevtools.newsletter.thanks.title "धन्यवाद!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "अगर आपने पिछले किसी Mozilla संबंधित समाचार की सदस्यता पुष्टि नहीं की है, तो आपको करनी पड़ सकती है. कृपया अपने इनबॉक्स या अवांछनीय ई-मेल फ़िल्टर में हमारे द्वारा भेजी गयी ईमेल की जांच करें.">

<!ENTITY  aboutDevtools.footer.title "Firefox डेवलपर संस्करण">
<!ENTITY  aboutDevtools.footer.message "सिर्फ डेवलपर उपकरण से ज्यादा की तलाश में हैं? Firefox ब्राउज़र को जाँचें जो कि विशेष रूप से डेवलपरों और आधुनिक कार्यप्रवाहों के लिए बनाया गया है.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "अधिक जानें">


<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Wróćo">
<!ENTITY safeb.palm.seedetails.label "Hlejće podrobnosće">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "To wobšudne sydło njeje…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Rada data wot <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Wopyt tutoho websydła móže waš ličak poškodźić">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; je tutu stronu zablokował, dokelž móhła pospytować, złóstnu softwaru instalować, kotraž móhła wosobinske daty na wašim ličaku kradnyć abo zhašeć.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Je so zdźěliło, zo sydło <span id='malware_sitename'/> <a id='error_desc_link'>złóstnu softwaru wobsahuje</a>. Móžeće <a id='report_detection'>problem z wuslědźenjom zdźělić</a> abo<a id='ignore_warning_link'>riziko ignorować</a>, zo byšće tute njewěste sydło wopytał.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Je so zdźěliło, zo sydło <span id='malware_sitename'/> <a id='error_desc_link'>złóstnu softwaru wobsahuje</a>. Móžeće <a id='report_detection'>problem z wuslědźenjom zdźělić</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Zhońće <a id='learn_more_link'>StopBadware.org</a>wjace wo škódnym webwobsahu inkluziwnje wirusy a druhu škódnu softwaru a kak móžeće swój ličak škitać. Zhońće wjace wo škiće přećiwo kradnjenju datow a škódnej softwarje přez &brandShortName; na <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Tute sydło móže škódne programy wobsahować">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; je tutu stronu zablokował, dokelž móhła pospytować, was zawjedować, programy instalować, kotrež wašemu přehladowanskemu dožiwjenju škodźeć (na přikład přez to, zo wašu startowu stronu měnjeja abo přidatne wabjenje na sydłach, kotrež wopytujeće, pokazuja).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Je so zdźěliło, zo sydło <span id='unwanted_sitename'/><a id='error_desc_link'>škódnu softwaru wobsahuje</a>. Móžeće <a id='ignore_warning_link'>riziko ignorować</a> a tute njewěste sydło wopytał.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Je so zdźěliło, zo sydło <span id='unwanted_sitename'/><a id='error_desc_link'>škódnu softwaru wobsahuje</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Zhońće wjace wo škódnej a njewitanej softwarje w <a id='learn_more_link'>prawidłach wo njewitanej softwarje</a>. Zhońće na <a id='firefox_support'>support.mozilla.org</a> wjace wo škiće &brandShortName; přećiwo kradnjenju datow a škódnym programam.">


<!ENTITY safeb.blocked.phishingPage.title3 "Wobšudne sydło">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; je tutu stronu zablokował, dokelž móhła was zawjedować, něšto strašne činił, kaž na přikład softwaru instalować abo wosobinske daty kaž hesła abo kreditne karty přeradzić.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Je so zdźěliło, zo sydło <span id='phishing_sitename'/> je <a id='error_desc_link'>wobšudne sydło</a>. Móžeće <a id='report_detection'>problem ze spóznaćom zdźělić</a> abo <a id='ignore_warning_link'>riziko ignorować</a> a tute njewěste sydło wopytać.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Je so zdźěliło, zo sydło <span id='phishing_sitename'/> je <a id='error_desc_link'>wobšudne sydło</a>. Móžeće <a id='report_detection'>problem ze spóznaćom zdźělić</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Zhońće na <a id='learn_more_link'>www.antiphishing.org</a>wjace wo wobšudnych sydłach a kradnjenju datow. Zhońće na <a id='firefox_support'>support.mozilla.org</a>wjace wo škiće &brandShortName; přećiwo kradnjenju datow a škódnej softwarje.">


<!ENTITY safeb.blocked.harmfulPage.title "Sydło prědku móže škódnu softwaru wobsahować">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; je tutu stronu zablokował, dokelž móhła strašne nałoženja instalować, kotrež waše daty kradnu abo hašeja (na přikład fota, hesła, powěsće a kreditne karty).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Je so zdźěliło, zo sydło <span id='harmful_sitename'/><a id='error_desc_link'>potencielnje škódne nałoženje wobsahuje</a>. Móžeće <a id='ignore_warning_link'>riziko ignorować</a> tute njewěste sydło wopytać.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Je so zdźěliło, zo sydło <span id='harmful_sitename'/><a id='error_desc_link'>potencielnje škódne nałoženje wobsahuje</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Zhońće na <a id='firefox_support'>support.mozilla.org</a>wjace wo škiće &brandShortName; přećiwo kradnjenju datow a škódnej softwarje.">

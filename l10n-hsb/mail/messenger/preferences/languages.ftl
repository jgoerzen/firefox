# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Horje
    .accesskey = H
languages-customize-movedown =
    .label = Dele
    .accesskey = D
languages-customize-remove =
    .label = Wotstronić
    .accesskey = W
languages-customize-select-language =
    .placeholder = Wubjerće rěč, zo byšće ju přidał…
languages-customize-add =
    .label = Přidać
    .accesskey = P
messenger-languages-window =
    .title = Rěčne nastajenja { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } pokaza prěnju rěč jako waš standard a dalše rěče, jeli trjeba, w porjedźe, w kotrymž so jewja.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = ਪਾਬੰਦੀਸ਼ੁਦਾ ਸੂਚੀ
    .style = width: 50em
blocklist-desc = ਤੁਸੀਂ ਚੁਣ ਸਕਦੇ ਹੋ ਕਿ, ਜਿਹੜੇ ਤੱਤ ਤੁਹਾਡੀ ਵੈੱਬ ਤੇ ਨਜ਼ਰਸਾਨੀ ਦੀ ਸਰਗਰਮੀ ਦੀ ਪੈੜ੍ਹ ਦੱਬ ਸਕਦੇ ਹਨ, ਤੇ ਪਾਬੰਦੀ ਲਾਉਣ ਲਈ { -brand-short-name } ਕਿਹੜੀ ਸੂਚੀ ਨੂੰ ਵਰਤੇਗੀ।
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = ਸੂਚੀ
blocklist-button-cancel =
    .label = ਰੱਦ ਕਰੋ
    .accesskey = C
blocklist-button-ok =
    .label = ਬਦਲਾਅ ਸੰਭਾਲੋ
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me ਮੂਲ ਸੁਰੱਖਿਆ (ਸਿਫਾਰਸ਼ੀ)।
blocklist-item-moz-std-desc = ਕੁੱਝ ਪੈੜ੍ਹ ਦੱਬਣ ਵਾਲਿਆਂ ਨੂੰ ਇਜਾਜ਼ਤ ਦਿੰਦਾ ਹੈ ਤਾਂ ਕਿ ਵੈੱਬਸਾਈਟਾਂ ਠੀਕ ਤਰ੍ਹਾਂ ਚੱਲ ਸਕਣ।
blocklist-item-moz-full-name = Disconnect.me ਸਖ਼ਤ ਸੁਰੱਖਿਆ।
blocklist-item-moz-full-desc = ਜਾਣ-ਪਛਾਣੇ ਪੈੜ੍ਹ ਦੱਬਣ ਵਾਲਿਆਂ 'ਤੇ ਪਾਬੰਦੀ ਲਗਾਉਂਦੀ ਹੈ। ਕੁਝ ਵੈੱਬਸਾਈਟਾਂ ਠੀਕ ਤਰ੍ਹਾਂ ਚੱਲ ਨਹੀਂ ਸਕਣਗੀਆਂ।

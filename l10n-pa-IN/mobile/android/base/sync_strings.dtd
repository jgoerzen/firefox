<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "ਫਾਇਰਫਾਕਸ ਸਿੰਕ">
<!ENTITY syncBrand.shortName.label "ਸਿੰਕ">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label; ਨਾਲ ਕਨੈਕਟ ਕਰੋ'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'ਆਪਣੇ ਨਵੇਂ ਡਿਵਾਈਸ ਨੂੰ ਸਰਗਰਮ ਕਰਨ ਲਈ, ਡਿਵਾਈਸ ਉੱਤੇ “&syncBrand.shortName.label; ਨੂੰ ਸੈਟਅੱਪ ਕਰੋ” ਨੂੰ ਚੁਣੋ।'>
<!ENTITY sync.subtitle.pair.label 'ਸਰਗਰਮ ਕਰਨ ਲਈ ਆਪਣੇ ਹੋਰ ਡਿਵਾਈਸ ਉੱਤੇ “ਡਿਵਾਈਸ ਨੂੰ ਪੇਅਰ ਕਰੋ” ਨੂੰ ਚੁਣੋ'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'ਮੇਰੇ ਕੋਲ ਡਿਵਾਈਸ ਨਹੀਂ ਹੈ…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'ਲਾਗਇਨ'>
<!ENTITY sync.configure.engines.title.history 'ਅਤੀਤ'>
<!ENTITY sync.configure.engines.title.tabs 'ਟੈਬਾਂ'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS2; ਉੱਤੇ &formatS1;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'ਬੁੱਕਮਾਰਕ ਮੇਨੂ'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'ਟੈਗ'>
<!ENTITY bookmarks.folder.toolbar.label 'ਬੁੱਕਮਾਰਕ ਟੂਲਬਾਰ'>
<!ENTITY bookmarks.folder.other.label 'ਹੋਰ ਬੁੱਕਮਾਰਕ'>
<!ENTITY bookmarks.folder.desktop.label 'ਡੈਸਕਟਾਪ ਬੁੱਕਮਾਰਕ'>
<!ENTITY bookmarks.folder.mobile.label 'ਮੋਬਾਈਲ ਬੁੱਕਮਾਰਕ'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'ਪਿੰਨ ਕੀਤੇ'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'ਬਰਾਊਜ਼ਿੰਗ ਉੱਤੇ ਵਾਪਸ ਜਾਓ'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label; ਲਈ ਜੀ ਆਇਆਂ ਨੂੰ'>
<!ENTITY fxaccount_getting_started_description2 'ਆਪਣੀਆਂ ਟੈਬਾਂ, ਬੁੱਕਮਾਰਕਾਂ, ਲਾਗਇਨ ਤੇ ਹੋਰਾਂ ਨੂੰ ਸਿੰਕ ਕਰਨ ਲਈ ਲਾਗਇਨ ਕਰੋ।'>
<!ENTITY fxaccount_getting_started_get_started 'ਸ਼ੁਰੂ ਕਰੋ'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label; ਦੇ ਪੁਰਾਣੇ ਵਰਜ਼ਨ ਨੂੰ ਵਰਤਣਾ ਹੈ?'>

<!ENTITY fxaccount_status_auth_server 'ਖਾਤੇ ਦਾ ਸਰਵਰ'>
<!ENTITY fxaccount_status_sync_now 'ਹੁਣੇ ਸਿੰਕ ਕਰੋ'>
<!ENTITY fxaccount_status_syncing2 '…ਸਿੰਕ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ'>
<!ENTITY fxaccount_status_device_name 'ਡਿਵਾਈਸ ਦਾ ਨਾਂ'>
<!ENTITY fxaccount_status_sync_server 'ਸਿੰਕ ਲਈ ਸਰਵਰ'>
<!ENTITY fxaccount_status_needs_verification2 'ਤੁਹਾਡੇ ਖਾਤੇ ਨੂੰ ਤਸਦੀਕ ਕਰਨ ਦੀ ਲੋੜ ਹੈ। ਤਸਦੀਕ ਕਰਨ ਵਾਲੀ ਈਮੇਲ ਮੁੜ-ਭੇਜਣ ਲਈ ਛੂਹੋ।'>
<!ENTITY fxaccount_status_needs_credentials 'ਕਨੈਕਟ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਦਾ। ਸਾਈਨ ਕਰਨ ਲਈ ਛੂਹੋ।'>
<!ENTITY fxaccount_status_needs_upgrade 'ਸਾਈਨ ਇਨ ਕਰਨ ਲਈ ਤੁਹਾਨੂੰ &brandShortName; ਨੂੰ ਅੱਪਗਰੇਡ ਕਰਨ ਦੀ ਲੋੜ ਹੈ।'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; ਨੂੰ ਸੈਟਅੱਪ ਕੀਤਾ ਹੈ, ਪਰ ਆਪਣੇ-ਆਪ ਸਿੰਕ ਨਹੀਂ ਕਰਦਾ ਹੈ। ਐਂਡਰਾਈਡ ਸੈਟਿੰਗਾਂ &gt; ਡਾਟਾ ਵਰਤੋ ਵਿੱਚ “ਡਾਟੇ ਨੂੰ ਆਪਣੇ-ਆਪ ਸਿੰਕ ਕਰੋ” ਨੂੰ ਬਦਲੋ'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; ਨੂੰ ਸੈਟਅੱਪ ਕੀਤਾ ਹੈ, ਪਰ ਆਪਣੇ-ਆਪ ਸਿੰਕ ਨਹੀਂ ਕਰਦਾ ਹੈ। ਐਂਡਰਾਈਡ ਸੈਟਿੰਗਾਂ &gt; ਖਾਤੇ ਮੇਨੂ ਵਿੱਚ “ਡਾਟੇ ਨੂੰ ਆਪਣੇ-ਆਪ ਸਿੰਕ ਕਰੋ” ਨੂੰ ਬਦਲੋ'>
<!ENTITY fxaccount_status_needs_finish_migrating 'ਆਪਣੇ ਨਵੇਂ ਫਾਇਰਫਾਕਸ ਖਾਤੇ ਵਿੱਚ ਸਾਈਨ ਇਨ ਕਰਨ ਲਈ ਛੂਹੋ।'>
<!ENTITY fxaccount_status_choose_what 'ਚੁਣੋ ਕਿ ਕੀ ਸਿੰਕ ਕਰਨਾ ਹੈ'>
<!ENTITY fxaccount_status_bookmarks 'ਬੁੱਕਮਾਰਕ'>
<!ENTITY fxaccount_status_history 'ਅਤੀਤ'>
<!ENTITY fxaccount_status_passwords2 'ਲਾਗਇਨ'>
<!ENTITY fxaccount_status_tabs 'ਟੈਬਾਂ ਨੂੰ ਖੋਲ੍ਹੋ'>
<!ENTITY fxaccount_status_additional_settings 'ਵਾਧੂ ਸੈਟਿੰਗਾਂ'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Wi-Fi ਉੱਤੇ ਹੀ ਸਿੰਕ ਕਰੋ'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'ਸੈਲੂਲਰ ਤੇ ਖ਼ਰਚੇ ਵਾਲੇ ਨੈੱਟਵਰਕ \&apos;ਤੇ &brandShortName; ਨੂੰ ਸਿੰਕ ਕਰਨ ਤੋਂ ਰੋਕੋ'>
<!ENTITY fxaccount_status_legal 'ਕਨੂੰਨੀ' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'ਸੇਵਾ ਦੀ ਸ਼ਰਤਾਂ'>
<!ENTITY fxaccount_status_linkprivacy2 'ਪਰਦੇਦਾਰੀ ਦੀ ਸੂਚਨਾ'>
<!ENTITY fxaccount_remove_account 'ਡਿਸ-ਕਨੈਕਟ ਕਰੋ&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'ਸਿੰਕ ਤੋਂ ਡਿਸ-ਕਨੈਕਟ ਕਰਨਾ ਹੈ?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'ਤੁਹਾਡਾ ਬਰਾਊਜ਼ਿੰਗ ਡਾਟਾ ਇਸ ਡਿਵਾਈਸ \&apos;ਤੇ ਰਹੇਗਾ, ਪਰ ਇਸ ਤੁਹਾਡੇ ਖਾਤੇ ਨਾਲ ਲਿੰਕ ਨਹੀਂ ਹੋਵੇਗਾ।'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'ਫਾਇਰਫਾਕਸ ਖਾਤਾ &formatS; ਡਿਸਕਨੈਕਟ ਹੈ।'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'ਡਿਸ-ਕਨੈਕਟ ਕਰੋ'>

<!ENTITY fxaccount_enable_debug_mode 'ਡੀਬੱਗ ਮੋਡ ਨੂੰ ਸਮਰੱਥ ਕਰੋ'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; ਚੋਣਾਂ'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; ਦੀ ਸੰਰਚਨਾ'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; ਕਨੈਕਟ ਨਹੀਂ ਹੈ'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; ਵਜੋਂ ਸਾਈਨ ਇਨ ਕਰਨ ਲਈ ਛੂਹੋ'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label; ਦੇ ਅੱਪਗਰੇਡ ਨੂੰ ਪੂਰਾ ਕਰਨਾ ਹੈ?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; ਵਜੋਂ ਸਾਈਨ ਇਨ ਕਰਨ ਲਈ ਛੂਹੋ'>

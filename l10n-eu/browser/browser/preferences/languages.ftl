# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Hizkuntzak
    .style = width: 38em
webpage-languages-window =
    .title = Webgunearen hizkuntza-ezarpenak
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = Web orriak hainbat hizkuntzatan eskaintzen dira batzuetan. Aukeratu orri hauek bistaratzeko hizkuntzak, hobespenaren arabera ordenatuta
languages-customize-spoof-english =
    .label = Eskatu web orrien ingelesezko bertsioak pribatutasuna areagotzeko
languages-customize-moveup =
    .label = Eraman gora
    .accesskey = o
languages-customize-movedown =
    .label = Eraman behera
    .accesskey = b
languages-customize-remove =
    .label = Kendu
    .accesskey = K
languages-customize-select-language =
    .placeholder = Hautatu gehitu nahi duzun hizkuntza…
languages-customize-add =
    .label = Gehitu
    .accesskey = G
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = { -brand-short-name } hizkuntza-ezarpenak
    .style = width: 40em
browser-languages-description = { -brand-short-name }(e)k lehen hizkuntza erakutsiko dizu lehenetsita eta ordezkoak zerrendako ordenan erakutsiko dizkizu behar izanez gero.

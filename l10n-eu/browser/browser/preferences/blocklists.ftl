# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Blokeo-zerrendak
    .style = width: 55em
blocklist-desc = Zure nabigazio-jardueraren jarraipena egiten ari litezkeen web elementuak blokeatzeko { -brand-short-name }ek erabiliko duen zerrenda aukera dezakezu.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Zerrenda
blocklist-button-cancel =
    .label = Utzi
    .accesskey = U
blocklist-button-ok =
    .label = Gorde aldaketak
    .accesskey = G
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me oinarrizko babesa (gomendatua).
blocklist-item-moz-std-desc = Baimendu neurri baterainoko jarraipena webguneak ondo ibil daitezen.
blocklist-item-moz-full-name = Disconnect.me babes zorrotza.
blocklist-item-moz-full-desc = Blokeatu jarraipena toki ezagunetatik. Zenbait webgune agian ez dira ondo ibiliko.

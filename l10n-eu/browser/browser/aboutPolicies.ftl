# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Enpresa-politikak
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Aktibo
errors-tab = Erroreak
documentation-tab = Dokumentazioa
policy-name = Politikaren izena
policy-value = Politikaren balioa
policy-errors = Politika-erroreak
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = Talde-gidalerroak erabiltzean, ordenagailuaren mailan bakarrik ezar daiteke gidalerro hau.

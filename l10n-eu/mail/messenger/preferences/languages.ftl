# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Eraman gora
    .accesskey = g
languages-customize-movedown =
    .label = Eraman behera
    .accesskey = b
languages-customize-remove =
    .label = Kendu
    .accesskey = K
languages-customize-select-language =
    .placeholder = Hautatu gehitu nahi duzun hizkuntza…
languages-customize-add =
    .label = Gehitu
    .accesskey = G
messenger-languages-window =
    .title = { -brand-short-name } hizkuntza ezarpenak
    .style = width: 40em
messenger-languages-description = { -brand-short-name } lehen hizkuntza erakutsiko dizu lehenetsita eta ordezkoak zerrendako ordenan erakutsiko dizkizu behar izanez gero.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Ɗemɗe
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Kelle geese ena kolliree sahaaji e ɗemɗe keewɗe. Suɓo ɗemɗe ngam hollirde ɗee kelle, e degginol cuɓorol
languages-customize-spoof-english =
    .label = Ɗaɓɓit jame Engelee kelle geese ɗee ngam suturo ɓeydiiɗo
languages-customize-moveup =
    .label = Dirtin Dow
    .accesskey = D
languages-customize-movedown =
    .label = Dirtin Les
    .accesskey = L
languages-customize-remove =
    .label = Ittu
    .accesskey = I
languages-customize-select-language =
    .placeholder = Labo ɗemgal ngam ɓeydude…
languages-customize-add =
    .label = Ɓeydu
    .accesskey = Ɓ
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-heading = Jokkel basiyangel
cfr-doorhanger-extension-sumo-link =
    .tooltiptext = Hol ko waɗi mi yiyde ɗumɗoo
cfr-doorhanger-extension-cancel-button = Wonaa jooni
    .accesskey = N
cfr-doorhanger-extension-ok-button = Ɓeydu jooni
    .accesskey = A
cfr-doorhanger-extension-learn-more-link = Jokku taro
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = baɗɗo { $name }
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = Wasiya

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.


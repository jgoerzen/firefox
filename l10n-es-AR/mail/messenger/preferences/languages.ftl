# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Subir
    .accesskey = u
languages-customize-movedown =
    .label = Bajar
    .accesskey = B
languages-customize-remove =
    .label = Eliminar
    .accesskey = R
languages-customize-select-language =
    .placeholder = Seleccione un idioma para agregar...
languages-customize-add =
    .label = Agregar
    .accesskey = A
messenger-languages-window =
    .title = Configuración de idioma de { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } mostrará el primer idioma como el predeterminado e irá mostrando idiomas alternativos si es necesario en el orden en que aparecen.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = مرضی کے مطابق ایپ اپڈیٹ URL سیٹ کریں۔
policy-Authentication = اس ویب سائٹ کے لئے مربوط تصدیق کو ترتیب دیں جو اس کی حمایت کرتی ہے۔
policy-BlockAboutAddons = ایڈ اون مینیجر(about:addons) تک رسائی کو روکیں۔
policy-BlockAboutConfig = about:config صفحے پر رسائی کو روکیں۔
policy-BlockAboutProfiles = about:profiles صفحے پر رسائی کو روکیں۔
policy-BlockAboutSupport = about:support صفحے پر رسائی کو روکیں۔
policy-DisableDeveloperTools = تخلیق کار ٹول تک رسائی کو روکیں۔
policy-DisableForgetButton = فارگیٹ بٹن تک رسائی کو روکیں۔
policy-DisablePrivateBrowsing = نجی براؤزنگ غیر فعال کریں
policy-DisableTelemetry = ٹیلی میٹری بند کریں۔
policy-Proxy = پراکسی سيٹنگيں ترتیب کریں۔
policy-SanitizeOnShutdown = بند کرنے پر تمام نیویگیشن ڈیٹا صاف کریں۔

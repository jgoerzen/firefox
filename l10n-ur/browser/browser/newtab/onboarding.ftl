# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ابھی استعمال کریں
onboarding-welcome-header = { -brand-short-name } میں خوش آمدید
onboarding-start-browsing-button-label = براؤزنگ شروع کریں

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = نجی براؤزنگ
onboarding-screenshots-title = اسکرین شاٹس
onboarding-addons-title = ایڑ آن
onboarding-ghostery-title = گھوسٹری

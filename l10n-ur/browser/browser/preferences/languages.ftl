# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = زبانیں
    .style = width: 30em
webpage-languages-window =
    .title = ویب صفحہ زبان سيٹنگيں
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = بعض اوقات ویب صفہ ایک سے زیادہ زبانوں میں دستیاب ہوتے ہیں۔ ویب صفحوں نمائش کرنے کے لیے، زبانوں کی ترجیحی ترتیب انتخاب کریں۔
languages-customize-spoof-english =
    .label = درجہ افزوں رازداری کے لیے ویب صفحات کے انگریزی ورژن کی درخواست کریں
languages-customize-moveup =
    .label = اوپر کریں
    .accesskey = ا
languages-customize-movedown =
    .label = نیچے کریں
    .accesskey = ن
languages-customize-remove =
    .label = ہٹائیں
    .accesskey = ہ
languages-customize-select-language =
    .placeholder = اضافہ کرنے کے لیے زبان منتخب کریں…
languages-customize-add =
    .label = اضافہ کریں
    .accesskey = ا
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = { -brand-short-name } زبان سيٹنگيں
    .style = width: 40em
browser-languages-description = { -brand-short-name } آپ کی پہلی زبان طے شدہ طور پر ظاہر کرے گا اور اگر ضروری ہو تو وہ ظاہر ترتیب میں متبادل زبانوں کو ظاہر کرے گا۔

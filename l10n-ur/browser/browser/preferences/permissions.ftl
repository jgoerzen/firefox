# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = استثنیات
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-address = ویب سائٹ کا پتہ
    .accesskey = d
permissions-block =
    .label = بلاک کریں
    .accesskey = ب
permissions-session =
    .label = سیشن کے لیے اجازت دیں
    .accesskey = س
permissions-allow =
    .label = ہونے دیں
    .accesskey = ہ
permissions-site-name =
    .label = ويب سائٹ
permissions-status =
    .label = حالت
permissions-remove =
    .label = ویب سائٹ ہٹائیں
    .accesskey = R
permissions-remove-all =
    .label = تمام ویب سائٹیں ہٹائیں
    .accesskey = e
permissions-button-cancel =
    .label = منسوخ کریں
    .accesskey = م
permissions-button-ok =
    .label = تبدیلیاں محفوظ کریں
    .accesskey = م
permissions-searchbox =
    .placeholder = ویبسائٹ تلاش کریں
permissions-capabilities-allow =
    .label = ہونے دیں
permissions-capabilities-block =
    .label = بلاک کریں
permissions-capabilities-prompt =
    .label = ہمیشہ پوچھیں

## Invalid Hostname Dialog

permissions-invalid-uri-title = ناجائز نام میزبان داخل
permissions-invalid-uri-label = کوئی جائز نام میزبان داخل کریں

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = استثنیات - سراغ کاری سے حفاظت
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = آپ نے ان ویب سائٹس پر ٹریکنگ کے تحفظ کو غیر فعال کردیا ہے۔

## Exceptions - Cookies

permissions-exceptions-cookie-window =
    .title = استثنیات - کوکیز اور سائٹ کا ڈیٹا
    .style = { permissions-window.style }
permissions-exceptions-cookie-desc = آپ اختصاص کر سکتے ہیں کی کس ویب سائٹس کو ہمیشہ یا کبھی کوکیز اور سائٹ کا ڈیٹا کو استعمال کرنے کی اجازت ہے۔ اس سائٹ کا پرا نام ٹائپ کرے جسکا آپ انتظام کرنا چاھتے ہے اور پھر کلک کریں بلاک، سیشن کے لئے اجازت دے، یا اجازت دے۔

## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = اجازت دی ویب سائٹ - پاپ اپ
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = آپ اختصاص کر سکتے ہیں کہ کونسی سائٹیں پاپ اپ کھولنے کے لیے اجازت دادہ ہیں۔ جس سائٹ کو اجازت دینا چاہتے ہیں اس کا ٹھیک ٹھیک پتہ ٹائپ کریں اور پھر اجازت دیں پر کلک کریں۔

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = استثنیات ۔ محفوظ شدہ لاگ ان
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = درج ذیل ویب سائٹس کے لئے لاگ ان کو محفوظ نہیں کیا جائے گا

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = اجازت دار ویب سائٹیں - ایڈ اون انسٹالیشن
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = آپ اختصاص کر سکتے ہیں کہ کونسی سائٹیں وابستہ دریچے کھولنے کےلیے اجازت دادہ ہیں۔ جس سائٹ کو اجازت دینا چاہتے ہیں اس کا ٹھیک ٹھیک پتہ ٹائپ کریں اور پھر 'ہونے دیں' پر کلک کریں۔

## Exceptions - Autoplay Media

permissions-exceptions-autoplay-media-window =
    .title = اجازت داد ویب سائٹیں - اوٹوپلے
    .style = { permissions-window.style }
permissions-exceptions-autoplay-media-window2 =
    .title = استثنیات - اوٹوپلے
    .style = { permissions-window.style }

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = سیٹنگیں - نوٹیفکیشن اجازات
    .style = { permissions-window.style }

## Site Permissions - Location

permissions-site-location-window =
    .title = سیٹنگیں- محل وقوع اجازت
    .style = { permissions-window.style }
permissions-site-location-disable-label =
    .label = آپ کے مقام تک رسائی حاصل کرنے سے متعلق نئی درخواستیں بند کریں

## Site Permissions - Camera

permissions-site-camera-window =
    .title = سیٹنگیں- کیمرہ اجازتیں
    .style = { permissions-window.style }
permissions-site-camera-disable-label =
    .label = اپنے کیمرے تک رسائی حاصل کرنے سے متعلق نئی درخواستیں بند کریں

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = سيٹنگيں - مائکروفون کی اجازت
    .style = { permissions-window.style }
permissions-site-microphone-disable-label =
    .label = اپنے مائیکروفون تک رسائی حاصل کرنے سے متعلق نئی درخواستیں بند کریں

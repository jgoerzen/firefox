# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

url-classifier-provider-title = مہیا کار
url-classifier-provider = مہیا کار
url-classifier-provider-update-btn = تازہ کاری
url-classifier-cache-title = کیسہ
url-classifier-cache-refresh-btn = تازہ کریں
url-classifier-cache-clear-btn = صاف کریں
url-classifier-cache-table-name = نام جدول
url-classifier-cache-prefix = سابقہ
url-classifier-debug-title = ٹھیک کریں
url-classifier-debug-module-btn = لاگ ماڈیول سیٹ کریں
url-classifier-debug-file-btn = لاگ مسل سیٹ کریں
url-classifier-debug-js-log-chk = JS لاگ سیٹ کریں
url-classifier-debug-sb-modules = محفوظ براؤزانگ لاگ ماڈیول
url-classifier-debug-modules = موجودہ لاگ ماڈیول
url-classifier-debug-sbjs-modules = محفوظ براوزنگ JS لاگ
url-classifier-debug-file = موجودہ لاگ مسل
url-classifier-not-available = N/A
url-classifier-enabled = اہل بنایا گیا
url-classifier-disabled = نااہل
url-classifier-updating = تازہ کاری کر رہا ہے
url-classifier-cannot-update = تازہ کاری نہیں کر سکتا
url-classifier-success = کاميابى

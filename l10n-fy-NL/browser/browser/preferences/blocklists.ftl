# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Blokkearlisten
    .style = width: 55em
blocklist-desc = Jo kinne kieze hokker list { -brand-short-name } brûke sil foar it blokkearjen fan web-eleminten dy't jo sneupaktiviteit folgje kinne.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = List
blocklist-button-cancel =
    .label = Annulearje
    .accesskey = A
blocklist-button-ok =
    .label = Wizigingen bewarje
    .accesskey = W
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me - basisbeskerming (Oanrekommandearre).
blocklist-item-moz-std-desc = Stiet inkelde folgers ta, sadat websites goed funksjonearje.
blocklist-item-moz-full-name = Disconnect.me - strange beskerming.
blocklist-item-moz-full-desc = Blokkearret bekende folgers. Guon websites wurkje mooglik net goed.

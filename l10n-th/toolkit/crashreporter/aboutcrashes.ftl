# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = รายงานข้อขัดข้อง
clear-all-reports-label = เอารายงานทั้งหมดออก
delete-confirm-title = คุณแน่ใจหรือไม่?
delete-confirm-description = สิ่งนี้จะลบรายงานทั้งหมดและไม่สามารถเลิกทำได้
crashes-unsubmitted-label = รายงานข้อขัดข้องที่ยังไม่ได้ส่ง
id-heading = ID รายงาน
date-crashed-heading = วันที่ขัดข้อง
crashes-submitted-label = รายงานข้อขัดข้องที่ส่งแล้ว
date-submitted-heading = วันที่ส่ง
no-reports-label = ยังไม่เคยมีการรายงานข้อขัดข้อง
no-config-label = แอปพลิเคชันนี้ไม่ได้ถูกกำหนดค่าให้แสดงผลรายงานข้อขัดข้อง ค่ากำหนด <code>breakpad.reportURL</code> ต้องถูกตั้ง

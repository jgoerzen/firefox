# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

url-classifier-provider-title = ผู้ให้บริการ
url-classifier-provider = ผู้ให้บริการ
url-classifier-provider-last-update-time = เวลาที่อัปเดตล่าสุด
url-classifier-provider-next-update-time = เวลาที่อัปเดตถัดไป
url-classifier-provider-last-update-status = สถานะการอัปเดตล่าสุด
url-classifier-provider-update-btn = อัปเดต
url-classifier-cache-title = แคช
url-classifier-cache-refresh-btn = เรียกใหม่
url-classifier-cache-clear-btn = ล้าง
url-classifier-cache-table-name = ชื่อตาราง
url-classifier-debug-title = ดีบั๊ก
url-classifier-enabled = ถูกเปิดใช้งาน
url-classifier-disabled = ถูกปิดใช้งาน
url-classifier-updating = กำลังอัปเดต
url-classifier-cannot-update = ไม่สามารถอัปเดต
url-classifier-success = สำเร็จ
url-classifier-update-error = ข้อผิดพลาดการอัปเดต ({ $error })
url-classifier-download-error = ข้อผิดพลาดการดาวน์โหลด ({ $error })

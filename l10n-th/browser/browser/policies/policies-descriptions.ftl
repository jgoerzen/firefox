# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = ตั้ง URL อัปเดตแอปที่กำหนดเอง
policy-BlockAboutAddons = ปิดกั้นการเข้าถึงตัวจัดการส่วนเสริม (about:addons)
policy-BlockAboutConfig = ปิดกั้นการเข้าถึงหน้า about:config
policy-BlockAboutProfiles = ปิดกั้นการเข้าถึงหน้า about:profiles
policy-BlockAboutSupport = ปิดกั้นการเข้าถึงหน้า about:support
policy-Cookies = ยอมรับหรือปฏิเสธเว็บไซต์ให้ตั้งค่าคุกกี้
policy-DisableAppUpdate = ป้องกันเบราว์เซอร์จากการอัปเดต
policy-DisableDeveloperTools = ปิดกั้นการเข้าถึงเครื่องมือนักพัฒนา
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = ปิดใช้งานคุณลักษณะ Firefox Screenshots
policy-DisablePrivateBrowsing = ปิดใช้งานการท่องเว็บแบบส่วนตัว
policy-DisableTelemetry = ปิดการวัดและส่งข้อมูลทางไกล
policy-InstallAddonsPermission = อนุญาตบางเว็บไซต์ในการติดตั้งส่วนเสริม
policy-Permissions = ปรับแต่งการอนุญาตสำหรับกล้อง, ไมโครโฟน, ตำแหน่งที่ตั้ง และการแจ้งเตือน
policy-Proxy = กำหนดค่าการตั้งค่าพร็อกซี

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "In sci Strumenti do svilupatô">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Abilita Strumenti do svilupatô">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Abilita Strumenti do svilupatô pe deuviâ Ispeçionn-a elemento">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Ezamina e cangia HTML e CSS con l'ispetô di Strumenti do svilupatô.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Sviluppa e fanni o debug co-e WebExtensions, worker web, worker de serviçioe atro ancon graçie a-i Strumenti do svilupatô de Firefox.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Ti æ ativou o scorsaieu di Strumenti do svilupatô. Se l'é stæto un erô, ti peu serâ sto feuggio.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Crea o teu scito perfetto con HTML, CSS e JavaScript graçie a atressi comme l'Ispetô e-o Debugger.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "I Strumenti do svilupatô en dizabilitæ comme preinpostaçion pe date ciù contròllo in sciô teu navegatô.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Atre informaçioin in sci Strumenti do svilupatô">

<!ENTITY  aboutDevtools.enable.enableButton "Abilita Strumenti do svilupatô">
<!ENTITY  aboutDevtools.enable.closeButton2 "Særa sto feuggio">

<!ENTITY  aboutDevtools.welcome.title "Benvegnuo inti Strumenti do svilupatô de Firefox!">

<!ENTITY  aboutDevtools.newsletter.title "Newsletter di Svilupatoî Mozilla">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Piggite e notiçie di svilupatoî e risorse mandæ pròprio inta teu cazella de pòsta.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "Email">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Son d'acòrdio che Mozilla o manezze e mæ informaçioin comme spiegou inta <a class='external' href='https://www.mozilla.org/privacy/'>Politica da Privacy</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Abon-ite">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Graçie!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Se primma no ti æ confermou l'abonamento a-a newsletter in sce Mozilla ti doviesci fâlo. Amîa ben inta teu pòsta in intrâ ò into spam se no ti gh'æ ina email da niâtri.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Ti çerchi quarcösa de ciù che di Strumenti do svilupatô? Ma aloa amia o navegatô Firefox fæto pe-i svilopatoî e 'n moderno flusso de travaggio.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Atre informaçioin">


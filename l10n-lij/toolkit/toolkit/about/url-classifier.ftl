# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

url-classifier-title = Informaçioin do Clasificatô URL
url-classifier-provider-title = Fornitô
url-classifier-provider = Fornitô
url-classifier-provider-last-update-time = Data urtimo agiornamento
url-classifier-provider-next-update-time = Data pròscimo agiornamento
url-classifier-provider-back-off-time = Tenpo de interuçion
url-classifier-provider-last-update-status = Stato urtimo agiornamento
url-classifier-provider-update-btn = Agiornamento
url-classifier-cache-title = Cache
url-classifier-cache-refresh-btn = Agiorna
url-classifier-cache-clear-btn = Netezza
url-classifier-cache-table-name = Nomme tabella
url-classifier-cache-ncache-entries = Numero de ingresci negativi inta cache
url-classifier-cache-pcache-entries = Numero de ingresci poxitivi inta cache
url-classifier-cache-show-entries = Fanni vedde ingresci
url-classifier-cache-entries = Ingresci inta cache
url-classifier-cache-prefix = Prefisso
url-classifier-cache-ncache-expiry = Scadensa da cache negativa
url-classifier-cache-fullhash = Hash conpleto
url-classifier-cache-pcache-expiry = Scadensa da cache poxitiva
url-classifier-debug-title = Debug
url-classifier-debug-module-btn = Inpòsta mòdoli do diaio
url-classifier-debug-file-btn = Inpòsta file do diaio
url-classifier-debug-js-log-chk = Inpòsta diaio JS
url-classifier-debug-sb-modules = Mòdolo do diaio da navegaçion segua
url-classifier-debug-modules = Mòdoli do diaio corenti
url-classifier-debug-sbjs-modules = Diao JS da navegaçion segua
url-classifier-debug-file = File do diaio corente
url-classifier-trigger-update = Agiornamento do Trigger
url-classifier-not-available = N/D
url-classifier-disable-sbjs-log = Dizabilita diao JS da navegaçion segua
url-classifier-enable-sbjs-log = Abilita diao JS da navegaçion segua
url-classifier-enabled = Ativo
url-classifier-disabled = Dizativou
url-classifier-updating = agiorno
url-classifier-cannot-update = no pòsso agiornâ
url-classifier-success = ariescio
url-classifier-update-error = erô d'agiornamento ({ $error })
url-classifier-download-error = erô de descaregamento ({ $error })

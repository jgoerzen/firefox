# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-sumo-link =
    .tooltiptext = Pi ngo atye kaneno man
cfr-doorhanger-extension-cancel-button = Pe kombedi
    .accesskey = P
cfr-doorhanger-extension-ok-button = Med kombedi
    .accesskey = M
cfr-doorhanger-extension-learn-more-link = Nong ngec mapol

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.


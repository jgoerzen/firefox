# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Leb
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Potbuk me kakube kare mogo kikelo i leb makato acel. Yer leb me yaro potbuk magi, kit ma imito
languages-customize-spoof-english =
    .label = Peny pi potbuk me kakube ma ii leb muno pi mung me lamal
languages-customize-moveup =
    .label = Kob Malo
    .accesskey = M
languages-customize-movedown =
    .label = Kob Piny
    .accesskey = P
languages-customize-remove =
    .label = Kwany
    .accesskey = K
languages-customize-select-language =
    .placeholder = Yer leb me ameda…
languages-customize-add =
    .label = Med
    .accesskey = M
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

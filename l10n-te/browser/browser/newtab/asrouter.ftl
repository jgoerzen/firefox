# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-sumo-link =
    .tooltiptext = ఇది నేను ఎందుకు చూస్తున్నాను
cfr-doorhanger-extension-cancel-button = ఇప్పుడు వద్దు
    .accesskey = N
cfr-doorhanger-extension-ok-button = ఇప్పుడే చేర్చు
    .accesskey = A
cfr-doorhanger-extension-learn-more-link = ఇంకా తెలుసుకోండి

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

# Variables:
#   $total (Number) - The total number of users using the add-on
cfr-doorhanger-extension-total-users =
    { $total ->
        [one] { $total } వాడుకరి
       *[other] { $total } వాడుకరులు
    }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = మినహాయింపులు
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-address = వెబ్‌సైటు చిరునామా
    .accesskey = d
permissions-block =
    .label = నిరోధించు
    .accesskey = B
permissions-session =
    .label = భాగము కొరకు అనుమతించు
    .accesskey = S
permissions-allow =
    .label = అనుమతించు
    .accesskey = A
permissions-site-name =
    .label = వెబ్‌సైటు
permissions-status =
    .label = స్థితి
permissions-remove =
    .label = వెబ్‌సైటును తీసివేయి
    .accesskey = R
permissions-remove-all =
    .label = అన్ని వెబ్‌సైట్లను తీసివేయి
    .accesskey = e
permissions-button-cancel =
    .label = రద్దుచేయి
    .accesskey = C
permissions-button-ok =
    .label = మార్పులను భద్రపరచు
    .accesskey = S
permissions-searchbox =
    .placeholder = వెబ్‌సైట్లను వెతకండి
permissions-capabilities-allow =
    .label = అనుమతించు
permissions-capabilities-block =
    .label = నిరోధించు
permissions-capabilities-prompt =
    .label = ఎల్లప్పుడూ అడుగు

## Invalid Hostname Dialog

permissions-invalid-uri-title = ప్రామాణికం కాని హోస్టునామము ప్రవేశపెట్టబడింది
permissions-invalid-uri-label = దయచేసి ప్రామాణికమైన హోస్టు నామము ప్రవేశపెట్టండి

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = మినహాయింపులు - ట్రాకింగ్ సంరక్షణ
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = ఈ వెబ్‌సైట్లలో మీరు ట్రాకింగ్ సంరక్షణను అచేతనించారు.

## Exceptions - Cookies

permissions-exceptions-cookie-window =
    .title = మినహాయింపులు - కుకీలు, సైటు డేటా
    .style = { permissions-window.style }

## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = అనుమతించిన వెబ్‌సైట్లు - పాప్అప్‌లు
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = పాప్‌అప్ విండోలను తెరుచుటకు ఏ వెబ్ సైటులను అనుమతించాలో మీరు వివరించవచ్చు. మీరు అనుమతించాలనుకున్న వెబ్ సైటు ఖచ్చితమైన చిరునామా తెలిపి అప్పుడు అనుమతించుని నొక్కండి.

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = మినహాయింపులు - భద్రపరిచన ప్రవేశాలు
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = ఈ క్రింది వెబ్‌సైట్ల ప్రవేశాలు భద్రపరచబడవు

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = అనుమతించబడిన సైట్లు - పొడిగింతల స్థాపన
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = పొడిగింతలను స్థాపించుటకు ఏయే వెబ్ సైట్లను అనుమతించాలో మీరు పేర్కొనవచ్చు. మీరు అనుమతించాలనుకున్న వెబ్ సైటు ఖచ్చితమైన చిరునామాను ఇచ్చి అనుమతించు బొత్తాన్ని నొక్కండి.

## Exceptions - Autoplay Media

permissions-exceptions-autoplay-media-window =
    .title = అనుమతించిన వెబ్‌సైట్లు - ఆటోప్లే
    .style = { permissions-window.style }

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = అమరికలు - గమనింపుల అనుమతులు
    .style = { permissions-window.style }
permissions-site-notification-disable-label =
    .label = గమనింపులను అనుమతించమని అడిగే అభ్యర్థనలను నిరోధించు

## Site Permissions - Location

permissions-site-location-window =
    .title = అమరికలు - స్థాన అనుమతులు
    .style = { permissions-window.style }
permissions-site-location-disable-label =
    .label = మీ స్థాన సమాచారం గురించి వచ్చే కొత్త అభ్యర్థనలను తిరస్కరించు

## Site Permissions - Camera

permissions-site-camera-window =
    .title = అమరికలు - కెమెరా అనుమతులు
    .style = { permissions-window.style }
permissions-site-camera-disable-label =
    .label = మీ కెమేరాను వాడుటకై వచ్చే కొత్త అభ్యర్థనలను తిరస్కరించు

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = అమరికలు - మైక్రోఫోన్ అనుమతులు
    .style = { permissions-window.style }
permissions-site-microphone-disable-label =
    .label = మీ మైక్రోఫోనును వాడుటకై వచ్చే కొత్త అభ్యర్థనలను తిరస్కరించు

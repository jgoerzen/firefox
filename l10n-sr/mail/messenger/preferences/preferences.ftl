# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Изаберите језике који ће се користити за приказ менија, порука и обавештења у програму { -brand-short-name }.
manage-messenger-languages-button =
    .label = Подеси заменске…
    .accesskey = з
confirm-messenger-language-change-description = Поново покрени { -brand-short-name } за примену ових измена
confirm-messenger-language-change-button = Примени и поново покрени

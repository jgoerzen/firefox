# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Localizar a próxima ocorrência do texto
findbar-previous =
    .tooltiptext = Localizar a ocorrência anterior do texto
findbar-find-button-close =
    .tooltiptext = Fechar a barra de localizar
findbar-highlight-all =
    .label = Realçar tudo
    .accesskey = a
    .tooltiptext = Realçar todas as ocorrências do texto
findbar-case-sensitive =
    .label = Diferenciar maiúsculas/minúsculas
    .accesskey = c
    .tooltiptext = Pesquisar ignorando maiúsculas de minúsculas
findbar-entire-word =
    .label = Palavras completas
    .accesskey = w
    .tooltiptext = Só pesquisar palavras inteiras

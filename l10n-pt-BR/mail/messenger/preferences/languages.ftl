# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Mover para cima
    .accesskey = c
languages-customize-movedown =
    .label = Mover para baixo
    .accesskey = b
languages-customize-remove =
    .label = Remover
    .accesskey = R
languages-customize-select-language =
    .placeholder = Selecione um idioma a ser adicionado…
languages-customize-add =
    .label = Adicionar
    .accesskey = A
messenger-languages-window =
    .title = Configurações de idioma do { -brand-short-name }
    .style = width: 40em
messenger-languages-description = O { -brand-short-name } mostrará o primeiro idioma como padrão e exibirá idiomas alternativos, se necessário, na ordem que aparecerem.

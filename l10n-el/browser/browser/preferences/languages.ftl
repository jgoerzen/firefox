# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Γλώσσες
    .style = width: 32em
webpage-languages-window =
    .title = Ρυθμίσεις γλώσσας ιστοσελίδων
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = Μερικές φορές, οι ιστοσελίδες προσφέρονται σε περισσότερες από μια γλώσσες. Επιλέξτε τις γλώσσες εμφάνισης αυτών των ιστοσελίδων, σε σειρά προτίμησης
languages-customize-spoof-english =
    .label = Απαίτηση αγγλικών εκδόσεων ιστοσελίδων για ενισχυμένο απόρρητο
languages-customize-moveup =
    .label = Μετακίνηση πάνω
    .accesskey = π
languages-customize-movedown =
    .label = Μετακίνηση κάτω
    .accesskey = κ
languages-customize-remove =
    .label = Αφαίρεση
    .accesskey = Α
languages-customize-select-language =
    .placeholder = Επιλέξτε γλώσσα προς προσθήκη…
languages-customize-add =
    .label = Προσθήκη
    .accesskey = θ
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = Ρυθμίσεις γλώσσας του { -brand-short-name }
    .style = width: 40em
browser-languages-description = Το { -brand-short-name } θα εμφανίζει την πρώτη γλώσσα ως την προεπιλεγμένη και θα προβάλει εναλλακτικές γλώσσες με τη σειρά που φαίνονται, αν είναι απαραίτητο.

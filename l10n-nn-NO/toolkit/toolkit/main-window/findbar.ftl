# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Finn neste førekomst av frasen
findbar-previous =
    .tooltiptext = Finn førre førekomsten av frasen
findbar-find-button-close =
    .tooltiptext = Lat att søkjelinja
findbar-highlight-all =
    .label = Marker alt
    .accesskey = M
    .tooltiptext = Marker alle funn av teksten
findbar-case-sensitive =
    .label = Skil mellom store og små bokstavar
    .accesskey = k
    .tooltiptext = Skil mellom store og små bokstavar i søket
findbar-entire-word =
    .label = Heile ord
    .accesskey = H
    .tooltiptext = Søk berre etter heile ord

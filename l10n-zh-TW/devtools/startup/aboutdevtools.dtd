<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "關於開發者工具">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "啟用 Firefox 開發者工具">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "啟用 Firefox 開發者工具來檢測頁面元素">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "使用開發者工具的檢測器來檢測和編輯 HTML 和 CSS。">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "使用 Firefox 開發者工具開發，並對 WebExtensions、Web Worker、Service Worker 除錯。">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "您按下了開發者工具的快捷鍵。若不是您要的，可以直接關閉此分頁。">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "使用檢測器、除錯器等工具，讓網站上的 HTML、CSS 及 JavaScript 變得更棒。">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "為了讓您能對瀏覽器更有控制，已預設停用 Firefox 開發者工具。">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "了解開發者工具的更多資訊">

<!ENTITY  aboutDevtools.enable.enableButton "啟用開發者工具">
<!ENTITY  aboutDevtools.enable.closeButton2 "關閉此分頁">

<!ENTITY  aboutDevtools.welcome.title "歡迎使用 Firefox 開發者工具！">

<!ENTITY  aboutDevtools.newsletter.title "Mozilla 開發者電子報">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "讓開發者消息、秘訣與資源直接寄到信箱裡。">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "電子郵件">
<!ENTITY  aboutDevtools.newsletter.privacy.label "我同意 Mozilla 依照<a class='external' href='https://www.mozilla.org/privacy/'>隱私權保護政策</a>當中描述的方式處理這些資訊。">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "訂閱">
<!ENTITY  aboutDevtools.newsletter.thanks.title "謝啦！">
<!ENTITY  aboutDevtools.newsletter.thanks.message "若您先前沒有確認訂閱過 Mozilla 電子報，現在可能需要確認。請到信箱收信，或到垃圾信件匣中看看有沒有來自我們的訊息。">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "想要的不只是開發者工具嗎？快試試特地為了開發者與現代工作流程所打造的 Firefox 瀏覽器。">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "了解更多">


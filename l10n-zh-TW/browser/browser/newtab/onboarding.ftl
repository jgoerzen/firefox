# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = 立刻試試
onboarding-welcome-header = 歡迎使用 { -brand-short-name }
onboarding-start-browsing-button-label = 開始瀏覽

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = 隱私瀏覽
onboarding-private-browsing-text = 上網不露足跡。含有內容封鎖機制的隱私瀏覽功能會封鎖在網路上追蹤您的線上追蹤器。
onboarding-screenshots-title = 拍攝擷圖
onboarding-screenshots-text = 不用離開 { -brand-short-name } 即可拍攝、儲存、分享畫面擷圖。可直接拍攝整個畫面或頁面中的一部份，然後自動上傳到網路上，方便使用分享。
onboarding-addons-title = 附加元件
onboarding-addons-text = 擴充 { -brand-short-name } 內建的功能，讓瀏覽器更好用。您可以用附加元件來比價、查天氣，或是自訂佈景主題展現自我。
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = 使用 Ghostery 封鎖討人厭的廣告，讓您上網更快、更聰明、更安全。

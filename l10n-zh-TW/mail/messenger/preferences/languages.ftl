# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = 上移
    .accesskey = U
languages-customize-movedown =
    .label = 下移
    .accesskey = D
languages-customize-remove =
    .label = 移除
    .accesskey = R
languages-customize-select-language =
    .placeholder = 選取要加入的語言…
languages-customize-add =
    .label = 新增
    .accesskey = A
messenger-languages-window =
    .title = { -brand-short-name } 語言設定
    .style = width: 40em
messenger-languages-description = { -brand-short-name } 將會以第一種語言作為您的預設語言，並根據所選的順序在需要時顯示其他語言。

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-remove =
    .label = Dilemel
    .accesskey = D
languages-customize-add =
    .label = Ouzhpennañ
    .accesskey = O
messenger-languages-window =
    .title = { -brand-short-name } Arventennoù Yezh
    .style = width: 40em

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Danevelloù sac'hadenn
clear-all-reports-label = Dilemel an holl zanevelloù
delete-confirm-title = Sur oc'h?
delete-confirm-description = An holl zanevelloù a vo dilamet da viken.
crashes-unsubmitted-label = Danevelloù sac'hadenn n'int ket treuzkaset
id-heading = Naoudi an danevell
date-crashed-heading = Deiziad ar sac'hadenn
crashes-submitted-label = Danevelloù sac'hadenn treuzkaset
date-submitted-heading = Deiziad an danevell
no-reports-label = Danevell sac'hadenn ebet bet treuzkaset.
no-config-label = N'eo ket kefluniet an arload-mañ evit diskouez an danevelloù sac'hadenn. Ret eo d'an arventenn <code>breakpad.reportURL</code> bezañ spisaet.

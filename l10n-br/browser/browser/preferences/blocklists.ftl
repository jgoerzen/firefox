# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Rolloù stankañ
    .style = width: 50em
blocklist-desc = Gallout a rit dibab peseurt roll a vo arveret gant { -brand-short-name } evit stankañ an elfennoù Web a c'hall heuliañ ho oberiantiz merdeiñ.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Roll
blocklist-button-cancel =
    .label = Nullañ
    .accesskey = N
blocklist-button-ok =
    .label = Enrollañ ar c'hemmoù
    .accesskey = E
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Gwarez diazez Disconnect.me (erbedet).
blocklist-item-moz-std-desc = Aotren heulierien evit ma 'z afe al lec'hiennoù en-dro.
blocklist-item-moz-full-name = Gwarez Disconnect.me strizh.
blocklist-item-moz-full-desc = Stankañ an heulierien anavezet. Gallout a raio ul lodenn eus al lec'hiennoù chom hep mont en-dro.

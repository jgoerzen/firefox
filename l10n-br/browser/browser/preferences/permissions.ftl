# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Nemedennoù
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-address = Chomlec'h al lec'hienn
    .accesskey = C
permissions-block =
    .label = Herzel
    .accesskey = H
permissions-session =
    .label = Aotren evit an estez
    .accesskey = A
permissions-allow =
    .label = Aotren
    .accesskey = A
permissions-site-name =
    .label = Lec'hienn
permissions-status =
    .label = Stad
permissions-remove =
    .label = Lemel kuit al lec'hienn
    .accesskey = L
permissions-remove-all =
    .label = Lemel kuit an holl lec'hiennoù
    .accesskey = h
permissions-button-cancel =
    .label = Nullañ
    .accesskey = N
permissions-button-ok =
    .label = Enrollañ ar c'hemmoù
    .accesskey = E
permissions-searchbox =
    .placeholder = Klask lec'hiennoù
permissions-capabilities-allow =
    .label = Aotren
permissions-capabilities-block =
    .label = Herzel
permissions-capabilities-prompt =
    .label = Goulenn bewech

## Invalid Hostname Dialog

permissions-invalid-uri-title = Anv ostiz bet enanket didalvoudek
permissions-invalid-uri-label = Roit un anv ostiz talvoudek, mar plij

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = Nemedennoù - Gwarez heuliañ
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = Diweredekaet ho peus ar gwarez heuliañ war al lec'hiennoù-mañ.

## Exceptions - Cookies

permissions-exceptions-cookie-window =
    .title = Nemedennoù - Toupinoù ha roadennoù lec'hienn
    .style = { permissions-window.style }
permissions-exceptions-cookie-desc = Gallout a ri despizañ pe lec'hiennoù a vo aotreet pe difennet da arverañ toupinoù ha roadennoù lec'hienn. Biziatait chomlec'h al lec'hienn a fell deoc'h merañ ha klikit war "Stankañ", "Aotren evit an estez" pe "Aotren".

## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = Lec'hiennoù aotreet - diflugelloù
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = Gallout a rit erspizañ pe lec'hiennoù zo aotreet da zigeriñ diflugelloù. Skrivit chomlec'h resis al lec'hienn a roit an aotre dezhi ha klikit war Aotren.

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = Nemedennoù - Titouroù kennaskañ enrollet
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = Ne vo ket enrollet titouroù kennaskañ al lec'hiennoù da-heul

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = Lec'hiennoù aotreet - Staliadur askouezhioù
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = Posupl eo deoc'h erspizañ pe lec'hiennoù a vo aotreet da staliañ askouezhioù. Roit chomlec'h spis al lec'hienn da vezañ aotreet ha klikit war Aotren.

## Exceptions - Autoplay Media

permissions-exceptions-autoplay-media-window =
    .title = Lec'hiennoù internet aotreet - Lenn emgefreek
    .style = { permissions-window.style }
permissions-exceptions-autoplay-media-desc = Gellout a rit despizañ peseurt lec'hiennoù a zo aotreet evit lenn elfennoù media ent emgefreek. Biziatait chomlec'h rik al lec'hienn ho peus c'hoant da aotren ha klikit war Aotren.
permissions-exceptions-autoplay-media-window2 =
    .title = Nemedennoù - Lenn emgefreek
    .style = { permissions-window.style }
permissions-exceptions-autoplay-media-desc2 = Gallout a rit despizañ pe lec'hiennoù a zo bepred aotreet da lenn mediaoù ent emgefreek gant ar son, hag ar re n'int morse aotreet. Biziatait chomlec'h al lec'hienn a fell deoc'h merañ ha klikit war Stankañ pe Aotren.

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = Arventennoù - Aotreoù ar rebuzadurioù
    .style = { permissions-window.style }
permissions-site-notification-desc = Goulennet eo bet gant al lec'hiennoù da-heul kas rebuzadurioù deoc'h. Gallout a rit despizañ peseurt lec'hienn a zo aotreet da gas rebuzadurioù deoc'h. Gallout a rit ivez stankañ goulennoù aotren ar rebuzadurioù nevez.
permissions-site-notification-disable-label =
    .label = Stankañ ar goulennoù aotren ar rebuzadurioù nevez
permissions-site-notification-disable-desc = Herzel a raio al lec'hiennoù ha n'int ket er roll a-us da c'houlenn diganeoc'h an aotre da gas rebuzadurioù. Stankañ ar rebuzadurioù a c'hall terriñ keweriusterioù 'zo el lec'hiennoù.

## Site Permissions - Location

permissions-site-location-window =
    .title = Arventennoù - Aotreoù al lec'hiadur
    .style = { permissions-window.style }
permissions-site-location-desc = Goulennet eo bet gant al lec'hiennoù da-heul gouzout ho lec'hiadur. Gallout a rit despizañ peseurt lec'hienn a zo aotreet da c'houzout ho lec'hiadur. Gallout a rit ivez stankañ goulennoù gouzout ho lec'hiadur nevez.
permissions-site-location-disable-label =
    .label = Stankañ ar goulennoù gouzout ho lec'hiadur nevez
permissions-site-location-disable-desc = Herzel a raio al lec'hiennoù ha n'int ket er roll a-us da c'houlenn diganeoc'h gouzout ho lec'hiadur. Kement-se a c'hall terriñ keweriusterioù 'zo el lec'hiennoù.

## Site Permissions - Camera

permissions-site-camera-window =
    .title = Arventennoù - Aotreoù ar c'hamera
    .style = { permissions-window.style }
permissions-site-camera-desc = Goulennet eo bet gant al lec'hiennoù da-heul haeziñ ho kamera. Gallout a rit despizañ peseurt lec'hienn a zo aotreet da haeziñ ho kamera. Gallout a rit ivez stankañ goulennoù haeziñ ho kamera nevez.
permissions-site-camera-disable-label =
    .label = Stankañ ar goulennoù haeziñ ho kamera nevez
permissions-site-camera-disable-desc = Herzel a raio al lec'hiennoù ha n'int ket er roll a-us da c'houlenn diganeoc'h haezi ho kamera. Kement-se a c'hall terriñ keweriusterioù 'zo el lec'hiennoù.

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = Arventennoù - Aotreoù ar mikrofon
    .style = { permissions-window.style }
permissions-site-microphone-desc = Al lec'hiennoù da heul o deus goulennet haeziñ ho mikro. Gallout a rit despizañ pe lec'hienn a zo aotreet da haeziñ ho mikro. Gallout a rit ivez stankañ goulennoù nevez a c'houlenn haeziñ ho mikro.
permissions-site-microphone-disable-label =
    .label = Stankañ ar goulennoù nevez haeziñ ho mikro
permissions-site-microphone-disable-desc = Herzel a raio al lec'hiennoù ha n'int ket er roll a-us da c'houlenn diganeoc'h haeziñ ho mikro. Kement-se a c'hall terriñ keweriusterioù 'zo el lec'hiennoù.

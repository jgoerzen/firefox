# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Yezhoù
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = A-wechoù e vez kinniget pajennadoù Web e meur a yezh. Diuzit e peseurt yezh e vo skrammet ar pajennadoù web-mañ, dre urzh ho tibab
languages-customize-spoof-english =
    .label = Goulenn ar pajennoù web e Saozneg evit gwellaat ar prevezded
languages-customize-moveup =
    .label = Davit ar c'hrec'h
    .accesskey = D
languages-customize-movedown =
    .label = Davit an traoñ
    .accesskey = a
languages-customize-remove =
    .label = Dilemel
    .accesskey = i
languages-customize-select-language =
    .placeholder = Diuzañ ur yezh da ouzhpennañ…
languages-customize-add =
    .label = Ouzhpennañ
    .accesskey = O
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }

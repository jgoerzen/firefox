# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Amprouit bremañ
onboarding-welcome-header = Degemer mat war { -brand-short-name }
onboarding-start-browsing-button-label = Stagañ da verdeiñ

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Merdeiñ prevez
onboarding-private-browsing-text = Merdeit drezoc'h hoc'h unan. Ar merdeiñ prevez gant ar stanker endalc'had a stank heulierien enlinenn a heuilh ac'hanoc'h war ar web.
onboarding-screenshots-title = Tapadennoù skramm
onboarding-screenshots-text = Kemerit, enrollit ha rannit tapadennoù skramm - hep kuitaat { -brand-short-name }. Tapit ul lodenn pe ur bajenn a-bezh en ur verdeiñ. Enrollit war ar web evit gallout he haeziñ hag he rannañ aesoc'h.
onboarding-addons-title = Askouezhioù
onboarding-addons-text = Ouzhpennit keweriusterioù a lako { -brand-short-name } da labourat muioc'h evidoc'h. Keñveriit prizioù, sellit ouzh liv an amzer pe eztaolit ho personelezh gant un neuz personelaet.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Merdeit herrekoc'h, speredekoc'h, pe diogeloc'h gant askouezhioù evel Ghostery hag a ro tro deoc'h da stankañ ar bruderezhioù aloubus.

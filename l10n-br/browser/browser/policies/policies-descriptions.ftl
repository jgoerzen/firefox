# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Lakaat un URL hizivaat personelaet evit an arload.
policy-Authentication = Kefluniañ an dilesa ebarzhet evit al lec'hiennoù skoret.
policy-BlockAboutAddons = Stankañ an ardoer askouezhioù (about:addons).
policy-BlockAboutConfig = Stankañ ar bajenn about:config.
policy-BlockAboutProfiles = Stankañ ar bajenn about:profiles.
policy-BlockAboutSupport = Stankañ ar bajenn about:support.
policy-Bookmarks = Krouiñ sinedoù e barrenn ostilhoù ar sinedoù, lañser ar sinedoù, pe un teuliad resis eno.
policy-Certificates = Arverañ pe get testenioù enkorfet. Ar reolenn-mañ n'eo nemet war Windows evit ar mare.
policy-Cookies = Aotren pe difenn al lec'hiennoù da zespizañ toupinoù.
policy-DisableAppUpdate = Mirout ar merdeer da hizivaat.
policy-DisableBuiltinPDFViewer = Diweredekaat PDF.js, gweler PDF enkorfet { -brand-short-name }.
policy-DisableDeveloperTools = Stankañ an ostilhoù diorroer.
policy-DisableFeedbackCommands = Diweredekaat arc'hadoù evit kas evezhiadennoù adalek al lañser skoazell (Reiñ ho meno ha Danevelliñ ul lec'hienn dagus).
policy-DisableFirefoxAccounts = Diweredekaat gwazerezhioù diazezet war { -fxaccount-brand-name }, Sync en o zouez.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Diweredekaat ar c'heweriuster Tapadennoù skramm.
policy-DisableFirefoxStudies = Mirout { -brand-short-name } da lañsañ studiadennoù.
policy-DisableForgetButton = Diweredekaat an afell evit dilemel ar roadennoù.
policy-DisableFormHistory = Chom hep derc'hel soñj eus ar roll istor klask ha furmskridoù.
policy-DisableMasterPasswordCreation = M'eo gwir e vo tu krouiñ ur ger-tremen mestr.
policy-DisablePocket = Diweredekaat ar c'heweriuster evit enrollañ pajennoù e Pocket.
policy-DisablePrivateBrowsing = Diweredekaat ar merdeiñ prevez.
policy-DisableProfileImport = Diweredekaat arc'had al lañser evit enporzhiañ roadennoù adalek ur merdeer all.
policy-DisableProfileRefresh = Diweredekaat an afell Azgrenaat { -brand-short-name } er bajenn about:support.

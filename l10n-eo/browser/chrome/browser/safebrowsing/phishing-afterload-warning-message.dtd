<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Iri reen">
<!ENTITY safeb.palm.seedetails.label "Montri detalojn">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Tiu ĉi ne estas trompa retejo…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "t">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Informo provizita de <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Vizito de tiu ĉi retejo povas difekti vian komputilon">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; blokis tiun ĉi paĝon ĉar ĝi povus klopodi instali malican programaron, kiu povus ŝteli aŭ forigi personajn informojn en via komputilo.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> estis <a id='error_desc_link'>denuncita kiel retejo, kiu enhavas malican programaron</a>. Vi povas <a id='report_detection'>raporti eltrovan problemon</a> aŭ <a id='ignore_warning_link'>ignori la riskon</a> kaj tamen iri al tiu ĉi nesekura retejo.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> estis <a id='error_desc_link'>denuncita kiel retejon kiu enhavas malican programaron</a>. Vi povas <a id='report_detection'>raporti eltrovan problemon</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Vi trovos pli da informo pri difektaj teksaĵaj enhavoj, kiuj inkluzivas virusojn kaj aliajn malicajn programon, kaj pri la maniero protekti vian komputiloj ĉe <a id='learn_more_link'>StopBadware.org</a>. Pli da informo pri la protekto de &brandShortName; kontraŭ trompoj kaj malicaj programoj troveblas ĉe <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "La vizitota retejo povus enhavi difektajn programojn">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; blokis tiun ĉi paĝon ĉar ĝi trompe povus igi vin instali programojn, kiuj malutilos al via retuma sperto (ekzemple per ŝanĝo de via eka paĝo aŭ per aldono de reklamoj en vizitataj retejoj).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> estis <a id='error_desc_link'>denuncita pro tio ke ĝi enhavas malutilan programaron</a>. Vi povas <a id='ignore_warning_link'>ignori la riskon</a> kaj tamen iri al tiu nesekura retejo.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> estis <a id='error_desc_link'>denuncita pro tio ke ĝi enhavas malutilan programaron</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Pli da informo pri malutila kaj nedezirata programaro ĉe <a id='learn_more_link'>politiko pri nedezirata programaro</a>. Pli da informo pri la protekto de &brandShortName; kontraŭ rettrompoj kaj malutilaj programoj ĉe <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Trompa retejo">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; blokis tiun ĉi paĝon ĉar ĝi povas trompe igi vin fari ion danĝeran kiel instali programaron aŭ malkaŝi personajn informojn, kiel pasvortojn aŭ kreditkartojn.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> estis <a id='error_desc_link'>denuncita kiel trompan retejon</a>. Vi povas <a id='report_detection'>raporti problemon pri misdetekto</a> aŭ <a id='ignore_warning_link'>ignori la riskon</a> kaj tamen iri al tiu ĉi nesekura retejo.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> estis <a id='error_desc_link'>denuncita kiel trompan retejon</a>. Vi povas <a id='report_detection'>raporti problemon pri misdetekto</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Legu pli pri trompaj retejoj kaj rettrompoj ĉe <a id='learn_more_link'>www.antiphishing.org</a>. Pli da informo pri la protekto de &brandShortName; kontraŭ rettrompoj kaj malutilaj programoj ĉe <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "La vizitota retejo povus enhavi fiprogramojn">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; blokis tiun ĉi paĝon, ĉar ĝi povus klopodi instali danĝerajn programojn, kiu ŝtelas aŭ forigas viajn datumojn (ekzemple: fotojn, pasvortojn, mesaĝojn kaj kreditkartojn).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> estis <a id='error_desc_link'>denuncita pro tio ke ĝi enhavas eble malutilan programon</a>. Vi povas <a id='ignore_warning_link'>ignori la riskon</a> kaj tamen iri al tiu ĉi nesekura retejo.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> estis <a id='error_desc_link'>denuncita pro tio ke ĝi havas programon. kiu povus malutili</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Pli da informo pri la protekto de &brandShortName; kontraŭ rettrompoj kaj malicaj programoj ĉe <a id='firefox_support'>support.mozilla.org</a>.">

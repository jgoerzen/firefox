# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# LOCALIZATION NOTE
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.
# LOCALIZATION NOTE (browserConsole.title): shown as the
# title when opening the browser console popup
browserConsole.title=Retumila konzolo
# LOCALIZATION NOTE (timestampFormat): %1$02S = hours (24-hour clock),
# %2$02S = minutes, %3$02S = seconds, %4$03S = milliseconds.
timestampFormat=%02S:%02S:%02S.%03S
helperFuncUnsupportedTypeError=pprint ne povas esti vokita por tiu ĉi tipo de objekto.

ConsoleAPIDisabled=The Web Console logging API (console.log, console.info, console.warn, console.error) has been disabled by a script on this page.

# LOCALIZATION NOTE (webConsoleXhrIndicator): the indicator displayed before
# a URL in the Web Console that was requested using an XMLHttpRequest.
webConsoleXhrIndicator=XHR

# LOCALIZATION NOTE (webConsoleMoreInfoLabel): the more info tag displayed
# after security related web console messages.
webConsoleMoreInfoLabel=Pli da informo

# LOCALIZATION NOTE (stacktrace.anonymousFunction): this string is used to
# display JavaScript functions that have no given name - they are said to be
# anonymous. Test console.trace() in the webconsole.
stacktrace.anonymousFunction=<anonima>

# LOCALIZATION NOTE (stacktrace.asyncStack): this string is used to
# indicate that a given stack frame has an async parent.
# %S is the "Async Cause" of the frame.
stacktrace.asyncStack=(Nesamtempa: %S)

# LOCALIZATION NOTE (timeLog): this string is used to display the result of
# the console.timeLog() call. Parameters: %1$S is the name of the timer, %2$S
# is the number of milliseconds.
timeLog=%1$S: %2$Sms

# LOCALIZATION NOTE (timeEnd): this string is used to display the result of
# the console.timeEnd() call. Parameters: %1$S is the name of the timer, %2$S
# is the number of milliseconds.
timeEnd=%1$S: %2$Sms

# LOCALIZATION NOTE (consoleCleared): this string is displayed when receiving a
# call to console.clear() to let the user know the previous messages of the
# console have been removed programmatically.
consoleCleared=Konzolo viŝita.

# LOCALIZATION NOTE (noCounterLabel): this string is used to display
# count-messages with no label provided.
noCounterLabel=<neniu etikedo>

# LOCALIZATION NOTE (counterDoesntExist): this string is displayed when
# console.countReset() is called with a counter that doesn't exist.
counterDoesntExist=Nombrilo “%S” ne ekzistas.

# LOCALIZATION NOTE (noGroupLabel): this string is used to display
# console.group messages with no label provided.
noGroupLabel=<neniu etikedo de grupo>

# LOCALIZATION NOTE (Autocomplete.blank): this string is used when inputnode
# string containing anchor doesn't matches to any property in the content.
Autocomplete.blank=  <- sen rezulto

maxTimersExceeded=La maksimuma permesata nombro de tempomezuriloj estis superita en tiu ĉi paĝo.
timerAlreadyExists=La tempumilo “%S” jam ekzistas.
timerDoesntExist=La tempumilo “%S” ne ekzistas.
timerJSError=Ne eblis pritrakti la nomon de tempumilo.

# LOCALIZATION NOTE (connectionTimeout): message displayed when the Remote Web
# Console fails to connect to the server due to a timeout.
connectionTimeout=Elĉerpiĝis la tempo por la konekto. Kontrolu la konzolon de eraroj ĉe ambaŭ flankoj por vidi ĉu estas erarmesaĝoj. Remalfermu la teksaĵan konzolon por klopodi denove.

# LOCALIZATION NOTE (propertiesFilterPlaceholder): this is the text that
# appears in the filter text box for the properties view container.
propertiesFilterPlaceholder=Filtri atributojn

# LOCALIZATION NOTE (emptyPropertiesList): the text that is displayed in the
# properties pane when there are no properties to display.
emptyPropertiesList=Neniu atributo montrebla

# LOCALIZATION NOTE (messageRepeats.tooltip2): the tooltip text that is displayed
# when you hover the red bubble that shows how many times a message is repeated
# in the web console output.
# This is a semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of message repeats
# example: 3 repeats
messageRepeats.tooltip2=#1 fojo;#1 fojoj

# LOCALIZATION NOTE (openNodeInInspector): the text that is displayed in a
# tooltip when hovering over the inspector icon next to a DOM Node in the console
# output
openNodeInInspector=Klaku por elekti nodon en la inspektilo

# LOCALIZATION NOTE (cdFunctionInvalidArgument): the text that is displayed when
# cd() is invoked with an invalid argument.
cdFunctionInvalidArgument=Ne eblas cd() al la donita fenestro. Nevalida parametro.

# LOCALIZATION NOTE (selfxss.msg): the text that is displayed when
# a new user of the developer tools pastes code into the console
# %1 is the text of selfxss.okstring
selfxss.msg=Averto pri trompo: estu singarda kiam vi algluas aĵojn kiujn vi ne komprenas. Tio povus permesi al atakantoj ŝteli vian identon aŭ regi vian komputilon. Bonvolu tajpi '%S' malsupre (enigklavo ne bezonata) por permesi la algluon.

# LOCALIZATION NOTE (selfxss.okstring): the string to be typed
# in by a new user of the developer tools when they receive the sefxss.msg prompt.
# Please avoid using non-keyboard characters here
selfxss.okstring=permesi algluon

# LOCALIZATION NOTE (messageToggleDetails): the text that is displayed when
# you hover the arrow for expanding/collapsing the message details. For
# console.error() and other messages we show the stacktrace.
messageToggleDetails=Montri/kaŝi detalojn de mesaĝo.

# LOCALIZATION NOTE (groupToggle): the text that is displayed when
# you hover the arrow for expanding/collapsing the messages of a group.
groupToggle=Montri/kaŝi grupon.

# LOCALIZATION NOTE (table.index, table.iterationIndex, table.key, table.value):
# the column header displayed in the console table widget.
table.index=(indekso)
table.iterationIndex=(indekso de ripeto)
table.key=Ŝlosilo
table.value=Valoroj

# LOCALIZATION NOTE (level.error, level.warn, level.info, level.log, level.debug):
# tooltip for icons next to console output
level.error=Eraro
level.warn=Averto
level.info=Informo
level.log=Registro
level.debug=Senerarigo

# LOCALIZATION NOTE (webconsole.find.key)
# Key shortcut used to focus the search box on upper right of the console
webconsole.find.key=CmdOrCtrl+F

# LOCALIZATION NOTE (webconsole.close.key)
# Key shortcut used to close the Browser console (doesn't work in regular web console)
webconsole.close.key=CmdOrCtrl+W

# LOCALIZATION NOTE (webconsole.clear.key*)
# Key shortcut used to clear the console output
webconsole.clear.key=Ctrl+Shift+L
webconsole.clear.keyOSX=Ctrl+L

# LOCALIZATION NOTE (webconsole.menu.copyURL.label)
# Label used for a context-menu item displayed for network message logs. Clicking on it
# copies the URL displayed in the message to the clipboard.
webconsole.menu.copyURL.label=Kopii adreson de ligilo
webconsole.menu.copyURL.accesskey=a

# LOCALIZATION NOTE (webconsole.menu.openURL.label)
# Label used for a context-menu item displayed for network message logs. Clicking on it
# opens the URL displayed in a new browser tab.
webconsole.menu.openURL.label=Malfermi retadreson en nova langeto
webconsole.menu.openURL.accesskey=n

# LOCALIZATION NOTE (webconsole.menu.openInNetworkPanel.label)
# Label used for a context-menu item displayed for network message logs. Clicking on it
# opens the network message in the Network panel
webconsole.menu.openInNetworkPanel.label=Malfermi en reta panelo
webconsole.menu.openInNetworkPanel.accesskey=M

# LOCALIZATION NOTE (webconsole.menu.storeAsGlobalVar.label)
# Label used for a context-menu item displayed for object/variable logs. Clicking on it
# creates a new global variable pointing to the logged variable.
webconsole.menu.storeAsGlobalVar.label=Konservi kiel ĉiean varianton
webconsole.menu.storeAsGlobalVar.accesskey=K

# LOCALIZATION NOTE (webconsole.menu.copyMessage.label)
# Label used for a context-menu item displayed for any log. Clicking on it will copy the
# content of the log (or the user selection, if any).
webconsole.menu.copyMessage.label=Kopii mesaĝon
webconsole.menu.copyMessage.accesskey=K

# LOCALIZATION NOTE (webconsole.menu.copyObject.label)
# Label used for a context-menu item displayed for object/variable log. Clicking on it
# will copy the object/variable.
webconsole.menu.copyObject.label=Kopii objekton
webconsole.menu.copyObject.accesskey=o

# LOCALIZATION NOTE (webconsole.menu.selectAll.label)
# Label used for a context-menu item that will select all the content of the webconsole
# output.
webconsole.menu.selectAll.label=Elekti ĉion
webconsole.menu.selectAll.accesskey=E

# LOCALIZATION NOTE (webconsole.menu.openInSidebar.label)
# Label used for a context-menu item displayed for object/variable logs. Clicking on it
# opens the webconsole sidebar for the logged variable.
webconsole.menu.openInSidebar.label=Malfermi en flanka strio
webconsole.menu.openInSidebar.accesskey=f

# LOCALIZATION NOTE (webconsole.menu.timeWarp.label)
# Label used for a context-menu item displayed for any log. Clicking on it will
# jump to the execution point where the log item was generated.
webconsole.menu.timeWarp.label=Salti ĉi tien

# LOCALIZATION NOTE (webconsole.clearButton.tooltip)
# Label used for the tooltip on the clear logs button in the console top toolbar bar.
# Clicking on it will clear the content of the console.
webconsole.clearButton.tooltip=Viŝi la eligon de la tekŝaĵa konzolo

# LOCALIZATION NOTE (webconsole.toggleFilterButton.tooltip)
# Label used for the tooltip on the toggle filter bar button in the console top
# toolbar bar. Clicking on it will toggle the visibility of an additional bar which
# contains filter buttons.
webconsole.toggleFilterButton.tooltip=Ŝalti/malŝalti la filtran strion

# LOCALIZATION NOTE (webconsole.filterInput.placeholder)
# Label used for for the placeholder on the filter input, in the console top toolbar.
webconsole.filterInput.placeholder=Filtri eligon

# LOCALIZATION NOTE (webconsole.errorsFilterButton.label)
# Label used as the text of the "Errors" button in the additional filter toolbar.
# It shows or hides error messages, either inserted in the page using
# console.error() or as a result of a javascript error..
webconsole.errorsFilterButton.label=Eraroj

# LOCALIZATION NOTE (webconsole.warningsFilterButton.label)
# Label used as the text of the "Warnings" button in the additional filter toolbar.
# It shows or hides warning messages, inserted in the page using console.warn().
webconsole.warningsFilterButton.label=Avertoj

# LOCALIZATION NOTE (webconsole.logsFilterButton.label)
# Label used as the text of the "Logs" button in the additional filter toolbar.
# It shows or hides log messages, inserted in the page using console.log().
webconsole.logsFilterButton.label=Registroj

# LOCALIZATION NOTE (webconsole.infoFilterButton.label)
# Label used as the text of the "Info" button in the additional filter toolbar.
# It shows or hides info messages, inserted in the page using console.info().
webconsole.infoFilterButton.label=Informoj

# LOCALIZATION NOTE (webconsole.debugFilterButton.label)
# Label used as the text of the "Debug" button in the additional filter toolbar.
# It shows or hides debug messages, inserted in the page using console.debug().
webconsole.debugFilterButton.label=Senerarigi

# LOCALIZATION NOTE (webconsole.cssFilterButton.label)
# Label used as the text of the "CSS" button in the additional filter toolbar.
# It shows or hides CSS warning messages, inserted in the page by the browser
# when there are CSS errors in the page.
webconsole.cssFilterButton.label=CSS

# LOCALIZATION NOTE (webconsole.xhrFilterButton.label)
# Label used as the text of the "XHR" button in the additional filter toolbar.
# It shows or hides messages displayed when the page makes an XMLHttpRequest or
# a fetch call.
webconsole.xhrFilterButton.label=XHR

# LOCALIZATION NOTE (webconsole.requestsFilterButton.label)
# Label used as the text of the "Requests" button in the additional filter toolbar.
# It shows or hides messages displayed when the page makes a network call, for example
# when an image or a scripts is requested.
webconsole.requestsFilterButton.label=Petoj

# LOCALIZATION NOTE (webconsole.filteredMessages.label)
# Text of the "filtered messages" bar, shown when console messages are hidden
# because the user has set non-default filters in the filter bar.
# This is a semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# example: 345 items hidden by filters.
webconsole.filteredMessages.label=#1 elemento kaŝita de filtriloj;#1 elementoj kaŝitaj de filtriloj

# Label used as the text of the "Reset filters" button in the "filtered messages" bar.
# It resets the default filters of the console to their original values.
webconsole.resetFiltersButton.label=Remeti filtrilojn

# LOCALIZATION NOTE (webconsole.enablePersistentLogs.label)
webconsole.enablePersistentLogs.label=Persistaj registroj
# LOCALIZATION NOTE (webconsole.enablePersistentLogs.tooltip)
webconsole.enablePersistentLogs.tooltip=Se vi aktivigas tiun ĉi elekteblon, la eligo ne estos viŝita kiam vi iras al nova paĝo

# LOCALIZATION NOTE (webconsole.navigated): this string is used in the console when the
# current inspected page is navigated to a new location.
# Parameters: %S is the new URL.
webconsole.navigated=Ŝanĝita al %S

# LOCALIZATION NOTE (webconsole.closeSplitConsoleButton.tooltip): This is the tooltip for
# the close button of the split console.
webconsole.closeSplitConsoleButton.tooltip=Fermi apartigitan konzolon (Esk)

# LOCALIZATION NOTE (webconsole.closeSidebarButton.tooltip): This is the tooltip for
# the close button of the sidebar.
webconsole.closeSidebarButton.tooltip=Fermi flankan strion

# LOCALIZATION NOTE (webconsole.reverseSearch.input.placeHolder):
# This string is displayed in the placeholder of the reverse search input in the console.
webconsole.reverseSearch.input.placeHolder=Serĉi en historio

# LOCALIZATION NOTE (webconsole.reverseSearch.result.closeButton.tooltip):
# This string is displayed in the tooltip of the close button in the reverse search toolbar.
# A keyboard shortcut will be shown inside the latter pair of brackets.
webconsole.reverseSearch.closeButton.tooltip=Fermi (%S)

# LOCALIZATION NOTE (webconsole.reverseSearch.results):
# This string is displayed in the reverse search UI when there are at least one result
# to the search.
# This is a semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 index of current search result displayed.
# #2 total number of search results.
webconsole.reverseSearch.results=1 rezulto;#1 el #2 rezultoj

# LOCALIZATION NOTE (webconsole.reverseSearch.noResult):
# This string is displayed in the reverse search UI when there is no results to the search.
webconsole.reverseSearch.noResult=Neniu rezulto

# LOCALIZATION NOTE (webconsole.reverseSearch.result.previousButton.tooltip):
# This string is displayed in the tooltip of the "previous result" button in the reverse search toolbar.
# A keyboard shortcut will be shown inside the latter pair of brackets.
webconsole.reverseSearch.result.previousButton.tooltip=Antaŭa rezulto (%S)

# LOCALIZATION NOTE (webconsole.reverseSearch.result.nextButton.tooltip):
# This string is displayed in the tooltip of the "next result" button in the reverse search toolbar.
# A keyboard shortcut will be shown inside the latter pair of brackets.
webconsole.reverseSearch.result.nextButton.tooltip=Post rezulto (%S)

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Tìm cụm từ ở phần sau
findbar-previous =
    .tooltiptext = Tìm cụm từ ở phần trước
findbar-find-button-close =
    .tooltiptext = Đóng thanh Tìm kiếm
findbar-highlight-all =
    .label = Đánh dấu tất cả
    .accesskey = l
    .tooltiptext = Tô sáng tất cả các cụm từ tìm thấy
findbar-case-sensitive =
    .label = Phân biệt HOA-thường
    .accesskey = A
    .tooltiptext = Tìm kiếm có phân biệt chữ hoa và chữ thường
findbar-entire-word =
    .label = Toàn bộ từ
    .accesskey = T
    .tooltiptext = Chỉ tìm toàn bộ từ

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-BlockAboutConfig = Chặn truy cập vào trang about:config.
policy-BlockAboutProfiles = Chặn truy cập vào trang about:profiles.
policy-BlockAboutSupport = Chặn truy cập vào trang about:support.
policy-DisableDeveloperTools = Chặn truy cập đến công cụ nhà phát triển.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Vô hiệu hóa Firefox Screenshots.
policy-DisablePrivateBrowsing = Vô hiệu hóa Duyệt web riêng tư.

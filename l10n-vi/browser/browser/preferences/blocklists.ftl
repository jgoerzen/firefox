# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Danh sách chặn
    .style = width: 55em
blocklist-desc = Bạn có thể chọn danh sách mà { -brand-short-name } sẽ sử dụng để chặn các phần tử của trang có thể theo dõi hoạt động duyệt web của bạn.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Danh sách
blocklist-button-cancel =
    .label = Hủy bỏ
    .accesskey = H
blocklist-button-ok =
    .label = Lưu thay đổi
    .accesskey = L
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Bảo vệ cơ bản của Disconnect.me (khuyên dùng).
blocklist-item-moz-std-desc = Cho phép một vài hệ thống theo dõi để trang hoạt động đúng.
blocklist-item-moz-full-name = Bảo vệ nghiêm ngặt của Disconnect.me.
blocklist-item-moz-full-desc = Chặn các trình theo dõi mà đã được nhận biết. Một số trang web có thể hoạt động không đúng.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Ngôn ngữ
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Thỉnh thoảng các trang web cung cấp nhiều hơn một ngôn ngữ. Chọn ngôn ngữ để hiển thị các trang web này, theo thứ tự ưu tiên
languages-customize-spoof-english =
    .label = Yêu cầu phiên bản tiếng Anh của trang web để nâng cao tính riêng tư
languages-customize-moveup =
    .label = Chuyển Lên
    .accesskey = L
languages-customize-movedown =
    .label = Chuyển Xuống
    .accesskey = g
languages-customize-remove =
    .label = Xóa
    .accesskey = X
languages-customize-select-language =
    .placeholder = Chọn một ngôn ngữ để thêm…
languages-customize-add =
    .label = Thêm
    .accesskey = T
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = { -brand-short-name } Cài đặt ngôn ngữ
    .style = width: 40em
browser-languages-description = { -brand-short-name } sẽ hiển thị ngôn ngữ mà bạn đã chọn làm mặc định và sẽ hiển thị ngôn ngữ khác nếu cần thiết được sắp xếp theo thứ tự mà chúng xuất hiện.

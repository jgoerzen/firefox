# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Mozgatás felfelé
    .accesskey = f
languages-customize-movedown =
    .label = Mozgatás lefelé
    .accesskey = l
languages-customize-remove =
    .label = Eltávolítás
    .accesskey = E
languages-customize-select-language =
    .placeholder = Hozzáadandó nyelv kiválasztása…
languages-customize-add =
    .label = Hozzáadás
    .accesskey = H
messenger-languages-window =
    .title = { -brand-short-name } nyelvi beállítások
    .style = width: 40em
messenger-languages-description = A { -brand-short-name } az első nyelvet alapértelmezettként fogja megjeleníteni, majd ha szükséges, akkor az alternatív nyelveket a megjelenésük sorrendjében jeleníti meg.

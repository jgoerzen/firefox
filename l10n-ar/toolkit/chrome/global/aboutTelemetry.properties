# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = تظهر هذه الصفحة معلومات عن الأداء، و العتاد، و الاستخدام، و التخصيصات التي جمعتها أداة تليمتري. تُرسل هذه المعلومات إلى %1$S للمساعدة في تحسين %2$S.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = تيليمتري %1$S و تيليمتري الممتد %2$S.
telemetryEnabled = مفعّل
telemetryDisabled = معطّل
extendedTelemetryEnabled = مفعّل
extendedTelemetryDisabled = معطّل

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = يجمع تيليمتري %1$S و الرفع %2$S.
releaseData = بيانات الإصدار
prereleaseData = بيانات الإصدار الأولي
telemetryUploadEnabled = مفعّل
telemetryUploadDisabled = معطّل

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = كل معلومة ترسل مدمجة مع ”%1$S“. تنظر حاليًا إلى طَرْقَة %2$S.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = ‏%1$S، ‏%2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = كل معلومة ترسل مدمجة مع ”%1$S“. تنظر حاليًا إلى طَرْقَة %2$S.
pingExplanationLink = طَرَقَات
currentPing = الحالي

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = ابحث في %1$S
filterAllPlaceholder = ابحث في كل الأقسام

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = نتائج ”%1$S“
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = للأسف لا نتائج في %1$S عن ”%2$S“
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = للأسف لا نتائج في أي من أقسام ”%S“
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = للأسف لا بيانات متاحة حاليا في ”%S“
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = الطرْق الحالي
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = الكل

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = العيّنات
histogramAverage = المتوسط
histogramSum = المجموع
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = انسخ

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = سجل تليمتري
telemetryLogHeadingId = المعرف
telemetryLogHeadingTimestamp = ختم الوقت
telemetryLogHeadingData = البيانات

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = أظهر بيانات SQL في الخيط الرئيسي
slowSqlOther = بيانات SQL بطيئة في الخيوط المساعدة
slowSqlHits = الإصابات
slowSqlAverage = متوسط الوقت (ms)
slowSqlStatement = الاستعلام

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = معرّف الإضافة
addonTableDetails = التفاصيل
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = مزوّد %1$S

keysHeader = خاصية
namesHeader = الاسم
valuesHeader = القيمة

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = تقرير التعليق #%1$S ‏(%2$S ثوان)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = ‏%1$S (عدد الالتقاطات: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = كتابة متأخرة #%1$S

stackTitle = المكدّس:
memoryMapTitle = خريطة الذاكرة:

errorFetchingSymbols = حدث خطأ أثناء جَلّب الرموز. تأكد من أنك متصل بالإنترنت وحاول ثانيةً.

parentPayload = حمولة الأصل
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = حمولة الفرع %1$S
timestampHeader = الختم الزمني
categoryHeader = الفئة
methodHeader = الطريقة
objectHeader = الكائن
extraHeader = إضافي

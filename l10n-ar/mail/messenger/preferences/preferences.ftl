# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = اختر اللغات المستخدمة لعرض القوائم والرسائل والتنبيهات من { -brand-short-name }.
confirm-messenger-language-change-description = أعِد تشغيل { -brand-short-name } لتطبيق هذه التغييرات
confirm-messenger-language-change-button = طبِّق وأعِد التشغيل

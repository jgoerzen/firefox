# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

clear-site-data-window =
    .title = Llimpiar datos
    .style = width: 35em
clear-site-data-description = Llimpiar toles cookies y datos de sitios atroxaos por { -brand-short-name } podríen zarrate la sesión en sitios web y desaniciar conteníu web fuera de llinia. Llimpiar los datos de la caché nun va afeutar a los anicios de sesión.

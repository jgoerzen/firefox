# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Llingües
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = A vegaes les páxines web úfrense en más d'una llingua. Escueyi les llingües p'amosar neses páxines, n'orde de preferencia
languages-customize-moveup =
    .label = Xubir
    .accesskey = u
languages-customize-movedown =
    .label = Baxar
    .accesskey = B
languages-customize-remove =
    .label = Desaniciar
    .accesskey = r
languages-customize-select-language =
    .placeholder = Escueyi una llingua p'amestar…
languages-customize-add =
    .label = Amestar
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]

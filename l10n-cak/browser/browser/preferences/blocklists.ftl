# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Kicholajem taq q'atoj
    .style = width: 55em
blocklist-desc = Yatikïr nacha' achike rucholajem xtrokisaj ri { -brand-short-name } richin yeruq'ät taq ch'akulal k'amaya'l, ri yetikïr nikïl ri nasamajij pan okik'amaya'l.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Cholajem
blocklist-button-cancel =
    .label = Tiq'at
    .accesskey = T
blocklist-button-ok =
    .label = Keyak taq jaloj
    .accesskey = K
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Kinel pa ri ruxe'el chajinïk (Chilab'en).
blocklist-item-moz-std-desc = Tiya' q'ij chi ke jujun taq kanokela' richin chi kesamäj ütz ri taq ruxaq k'amaya'l.
blocklist-item-moz-full-name = Kinelesäx el pa rub'eyal ruchajinik.
blocklist-item-moz-full-desc = Ninq'ät taq kanonel ri etaman kiwäch. K'o Jujun taqruxaq ajk'amaya'l man kan ta ütz yesamäj.

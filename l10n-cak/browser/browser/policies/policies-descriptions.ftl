# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Tiya' jun ichinan URL richin nik'ex ri chokoy.
policy-Authentication = Tib'an runuk'ulem ri chijun jikib'an pa taq ajk'amaya'l ruxaq nikiya' q'ij.
policy-BlockAboutAddons = Tiq'at rokem ri Runuk'samajel Chokoy (about:addons).
policy-BlockAboutConfig = Tiq'at okem pa ri about:config ruxaq.
policy-BlockAboutProfiles = Tiq'at okem pa ri about:profiles ruxaq.
policy-BlockAboutSupport = Tiq'at okem pa ri about:support ruxaq.
policy-Bookmarks = Ketz'uk taq yaketal pa ri Rukatz'ik yaketal. pa ri Ruk'utsamaj yaketal o pa jun kiyakwuj.
policy-Certificates = Ye'okisäx o man ye'okisäx ta iqitzijib'äl etz'aqatisan pa cholajil. Re na'ojil re' xa xe pa ri Windows wakami.
policy-Cookies = Niya' o man niya' ta q'ij chi ke ri ajk'amaya'l taq ruxaq richin nikijikib'a' taq kaxlanwäy.
policy-DisableAppUpdate = Man tiya' q'ij richin nuk'ëx ri' ri okik'amaya'l.
policy-DisableBuiltinPDFViewer = Tichup PDF.js, ri tz'etöy PDF pa { -brand-short-name }.
policy-DisableDeveloperTools = Tiq'at okem pa ri kisamajib'al b'anonela'.
policy-DisableFeedbackCommands = Kechup taq nuk'uj richin yetaq taq tzijol pa ri ruk'utsamaj Tob'äl (Titaq Tzijol chuqa' Kesujüx Q'olonel taq Ruxaq).
policy-DisableFirefoxAccounts = Tichup { -fxaccount-brand-name } samaj ruxe'el, chuqa' Sync.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Tichup ri rusamaj Firefox Screenshots.
policy-DisableFirefoxStudies = Man tiya' q'ij chi ri { -brand-short-name } tib'än tijonïk.
policy-DisableForgetButton = Tiq'at okem pa ri rupitz'b'al Timestäx.
policy-DisableFormHistory = Man tinatäx ri runatab'al kanob'äl chuqa' taq nojwuj
policy-DisableMasterPasswordCreation = We nitzij, man nitikïr ta nitz'uk jun ajtij ewan tzij.
policy-DisablePocket = Tichup ri samaj richin yeyak taq ajk'amaya'l ruxaq pa Pocket.
policy-DisablePrivateBrowsing = Tichup  ri Ichinan Okem pa K'amaya'l.
policy-DisableProfileImport = Tichup ri runuk'uj k'utsamaj richin yejik' taq tzij pa ch'aqa' chik taq okik'amaya'l.
policy-DisableProfileRefresh = Tichup ri Titzolïx { -brand-short-name } pitz'b'äl pa ri ruxaq about:support.
policy-DisableSafeMode = Tichup ri rub'anikil richin nitikirisäx chik pa ri Ütz Rub'anikil. Ch'utitzijol: Ri Shift pitz'b'äl richin yatok pa ri Ütz Rub'anikil xa xe tikirel nichup pa Windows rik'in rokisaxik Runa'ojil Rumolaj Q'inoj.
policy-DisableSecurityBypass = Tichajïx chi ri okisanel tik'o pa kiwi' jujun kitzijol k'ayewal.
policy-DisableSetDesktopBackground = Tichup runuk'uj k'utsamaj richin niya' jun wachib'äl achi'el Rupam Ajch'atal.
policy-DisableSetAsDesktopBackground = Tichup runuk'uj k'utsamaj Tiya' achi'el Rupam Ajch'atal kichin ri taq wachib'äl.
policy-DisableSystemAddonUpdate = Tichajïx chi ri okik'amaya'l keruyaka' chuqa' keruk'exa' ri taq rutz'aqat q'inoj.
policy-DisableTelemetry = Tichup Telemetry.
policy-DisplayBookmarksToolbar = Tik'ut ri Rukajtz'ik Samajib'äl achi'el k'o wi.
policy-DisplayMenuBar = Tik'ut ri Rukajtz'ik K'utsamaj achi'el k'o wi.
policy-DontCheckDefaultBrowser = Tichup ri tojtob'äl okik'amaya'l k'o wi pa rutikirisaxik.
# “lock” means that the user won’t be able to change this setting
policy-EnableTrackingProtection = Titzij o tichup ri Kiq'atik Rupam chuqa' ticha' toq niq'at.
# A “locked” extension can’t be disabled or removed by the user. This policy
# takes 3 keys (“Install”, ”Uninstall”, ”Locked”), you can either keep them in
# English or translate them as verbs. See also:
# https://github.com/mozilla/policy-templates/blob/master/README.md#extensions-machine-only
policy-Extensions = Tiyak, tiyuj o keq'at taq k'amal. Ri rucha'ik Tiyak yeruchäp URLs taq ochochib'äl o taq b'ey achi'el etab'äl. Ri Tiyuj chuqa' Tiq'at yeruchäp ID kichin taq k'amal.
policy-FlashPlugin = Niya' q'ij o man niya' ta q'ij nokisäx ri Flash nak'ab'äl.
policy-HardwareAcceleration = We man qitzij ta, tichup ranin ch'akulkem.
# “lock” means that the user won’t be able to change this setting
policy-Homepage = Tiya' chuqa' ütz niq'at ri tikirib'äl ruxaq.
policy-InstallAddonsPermission = Tiya' q'ij chi ke jujun taq ajk'amaya'l ruxaq yekiyäk taq tz'aqat.
policy-NoDefaultBookmarks = Kechup ri kitz'ukik taq yaketal e k'o wi etz'aqatisan rik'in { -brand-short-name }, chuqa' ri na'owinäq taq yaketal (Yalan Etz'eton, K'a ri' Ketz'et taq Yaketal). Ch'utitzijol: ütz re na'ojil re' we nokisäx pa ri nab'ey rusamajixik ruwäch b'i'aj.
policy-OfferToSaveLogins = Tab'ana' ri runuk'ulem richin niya' q'ij chi ri { -brand-short-name } yerusüj yenatäx tikirib'äl taq molojri'ïl chuqa' yakon ewan taq tzij. Yek'ul qitzij chuqa' man qitzij ta taq ajil.
policy-OverrideFirstRunPage = Tiyuj ri nab'ey ruxaq rub'anikil. Tiya' pa säq re na'ojil re' we nawajo' nachüp ri nab'ey ruxaq rub'anik.
policy-OverridePostUpdatePage = Nayüj ri ruxaq "K'ak'a' taq tzijol" chi rij ri k'exoj. Tiya' pa säq re na'ojil re' we nawajo' nachüp re ruxaq chi rij ri k'exoj.
policy-Permissions = Tib'an kinuk'ulem ya'oj taq q'ij kichin elesäy wachib'äl, q'asäy ch'ab'äl, k'ojlib'äl chuqa' taq rutzijol.
policy-PopupBlocking = Tiya' q'ij chi jujun taq ajk'amaya'l ruxaq kekik'utu' elenel taq tzuwäch achi'el e k'o wi.
policy-Proxy = Tib'an runuk'ulem ri proxi.
policy-SanitizeOnShutdown = Keyuj ronojel ri taq rutzij okem pa k'amaya'l toq nichup.
policy-SearchBar = Tib'an runuk'ulem ri k'ojlib'äl k'o wi pa ri rukajtz'ik kanob'äl. Ri okisanel k'a nitikïr nrichinaj.
policy-SearchEngines = Tib'an runuk'ulem ri kanob'äl. Re na'ojil re' xa xe okel pa ri ruwäch Extended Support Release (ESR).
# “format” refers to the format used for the value of this policy. See also:
# https://github.com/mozilla/policy-templates/blob/master/README.md#websitefilter-machine-only
policy-WebsiteFilter = Tiq'at ri tz'etoj rik'in ajk'amaya'l ruxaq. Tatz'eta' ri ruwujil richin nawetamaj ch'aqa' chik rutzijoxik rub'anikil.

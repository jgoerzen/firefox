# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Išbandykite dabar
onboarding-welcome-header = Sveiki, čia „{ -brand-short-name }“
onboarding-start-browsing-button-label = Pradėti naršymą

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Privatusis naršymas
onboarding-private-browsing-text = Naršykite patys. Privatusis naršymas, kartu su turinio blokavimu, blokuoja jūsų veiklą saityne sekančius stebėjimo elementus.
onboarding-screenshots-title = Ekrano nuotraukos
onboarding-screenshots-text = Darykite, įrašykite ir dalinkitės ekrano nuotraukomis – nepalikdami „{ -brand-short-name }“. Užfiksuokite sritį arba visą naršomą tinklalapį. Tada įrašykite į saityną patogiam pasiekimui ir dalinimuisi.
onboarding-addons-title = Priedai
onboarding-addons-text = Priedai leidžia pridėti funkcionalumo į „{ -brand-short-name }“, kad naršyklė dirbtų daugiau jūsų labui. Palyginkite kainas, patikrinkite orus arba išreikškite savo asmenybę su pasirinktu grafiniu apvalkalu.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Naršykite greičiau, protingiau, ar saugiau su tokiais priedais kaip „Ghostery“, kuris leidžia blokuoti erzinančias reklamas.

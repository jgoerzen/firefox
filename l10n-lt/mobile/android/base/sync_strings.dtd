<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Prisijungimas prie „&syncBrand.shortName.label;“'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Norėdami įjungti sinchronizavimą kitame įrenginyje, jame pasirinkite „Konfigūruoti sinchronizavimą“.'>
<!ENTITY sync.subtitle.pair.label 'Norėdami įjungti sinchronizavimą, kitame įrenginyje pasirinkite „Suporuoti įrenginį“.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Įrenginio su savimi neturiu…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Prisijungimai'>
<!ENTITY sync.configure.engines.title.history 'Žurnalą'>
<!ENTITY sync.configure.engines.title.tabs 'Korteles'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '„&formatS1;“ įrenginyje „&formatS2;“'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Adresyno meniu'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Gairės'>
<!ENTITY bookmarks.folder.toolbar.label 'Adresyno juosta'>
<!ENTITY bookmarks.folder.other.label 'Kiti adresai'>
<!ENTITY bookmarks.folder.desktop.label 'Kompiuterio adresynas'>
<!ENTITY bookmarks.folder.mobile.label 'Mobilusis adresynas'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Įsegtos svetainės'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Tęsti naršymą'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Sveiki! Čia „&syncBrand.shortName.label;“'>
<!ENTITY fxaccount_getting_started_description2 'Prisijunkite, jeigu norite sinchronizuoti savo korteles, adresyną, slaptažodžius ir kitką.'>
<!ENTITY fxaccount_getting_started_get_started 'Pradėti'>
<!ENTITY fxaccount_getting_started_old_firefox 'Naudojatės senesne „&syncBrand.shortName.label;“ laida?'>

<!ENTITY fxaccount_status_auth_server 'Paskyros serveris'>
<!ENTITY fxaccount_status_sync_now 'Sinchronizuoti dabar'>
<!ENTITY fxaccount_status_syncing2 'Sinchronizuojama…'>
<!ENTITY fxaccount_status_device_name 'Įrenginio vardas'>
<!ENTITY fxaccount_status_sync_server '„Sync“ serveris'>
<!ENTITY fxaccount_status_needs_verification2 'Jūsų paskyrą būtina patvirtinti. Bakstelėkite, kad būtų pakartotinai išsiųstas patvirtinimo laiškas.'>
<!ENTITY fxaccount_status_needs_credentials 'Prisijungti nepavyko. Bakstelėkite pakartotiniam bandymui.'>
<!ENTITY fxaccount_status_needs_upgrade 'Jeigu norite prisijungti, turite atnaujinti „&brandShortName;“.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '„&syncBrand.shortName.label;“ sukonfigūruota, tačiau duomenys nesinchronizuojami automatiškai. Įjunkite automatinį duomenų sinchronizavimą „Android“ nustatymų skiltyje „Duomenų naudojimas“ („Data Usage“).'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '„&syncBrand.shortName.label;“ sukonfigūruota, tačiau duomenys nesinchronizuojami automatiškai. Įjunkite automatinį duomenų sinchronizavimą „Android“ nustatymų skiltyje „Paskyros“ („Accounts“).'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Bakstelėkite, norėdami prisijungti prie savo naujosios „Firefox“ paskyros.'>
<!ENTITY fxaccount_status_choose_what 'Pasirinkite, ką sinchronizuosite'>
<!ENTITY fxaccount_status_bookmarks 'Adresynas'>
<!ENTITY fxaccount_status_history 'Žurnalas'>
<!ENTITY fxaccount_status_passwords2 'Prisijungimai'>
<!ENTITY fxaccount_status_tabs 'Atviros kortelės'>
<!ENTITY fxaccount_status_additional_settings 'Papildomi nustatymai'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sinchronizuoti tik per belaidį tinklą'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Neleisti „&brandShortName;“ sinchronizuoti per mobilųjį tinklą'>
<!ENTITY fxaccount_status_legal 'Teisinė informacija' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Paslaugos teikimo nuostatai'>
<!ENTITY fxaccount_status_linkprivacy2 'Privatumo pranešimas'>
<!ENTITY fxaccount_remove_account 'Atjungti…'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Atsijungti nuo „Sync“?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Jūsų naršymo duomenys liks šiame įrenginyje, tačiau nebebus sinchronizuojami su jūsų paskyra.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 '„Firefox“ paskyra &formatS; atjungta.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Atsijungti'>

<!ENTITY fxaccount_enable_debug_mode 'Įjungti derinimo režimą'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '„&syncBrand.shortName.label;“ nustatymai'>
<!ENTITY fxaccount_options_configure_title 'Konfigūruoti „&syncBrand.shortName.label;“'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '„&syncBrand.shortName.label;“ nepavyko prisijungti'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Bakstelėkite prisijungimui kaip &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Užbaigti „&syncBrand.shortName.label;“ atnaujinimą?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Bakstelėkite prisijungimui kaip &formatS;'>

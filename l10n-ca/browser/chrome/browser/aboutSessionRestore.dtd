<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY restorepage.tabtitle       "Restauració de la sessió">

<!-- LOCALIZATION NOTE: The title is intended to be apologetic and disarming, expressing dismay
     and regret that we are unable to restore the session for the user -->
<!ENTITY restorepage.errorTitle2    "Tenim problemes per recuperar les vostres pàgines.">
<!ENTITY restorepage.problemDesc2   "Tenim problemes per restaurar l'última sessió de navegació. Trieu «Restaura la sessió» per tornar-ho a provar.">
<!ENTITY restorepage.tryThis2       "Seguiu sense poder restaurar la sessió? A vegades, el problema pot ser una pestanya determinada. Reviseu les pestanyes anteriors, desmarqueu les que no necessiteu i proveu de tornar a restaurar-les.">

<!ENTITY restorepage.hideTabs       "Amaga les pestanyes anteriors">
<!ENTITY restorepage.showTabs       "Mostra les pestanyes anteriors">

<!ENTITY restorepage.tryagainButton2 "Restaura la sessió">
<!ENTITY restorepage.restore.access2 "R">
<!ENTITY restorepage.closeButton2    "Inicia una sessió nova">
<!ENTITY restorepage.close.access2   "n">

<!ENTITY restorepage.restoreHeader  "Restaura">
<!ENTITY restorepage.listHeader     "Finestres i pestanyes">
<!-- LOCALIZATION NOTE: &#37;S will be replaced with a number. -->
<!ENTITY restorepage.windowLabel    "Finestra &#037;S">


<!-- LOCALIZATION NOTE: The following 'welcomeback2' strings are for about:welcomeback,
     not for about:sessionstore -->

<!ENTITY welcomeback2.restoreButton  "Som-hi!">
<!ENTITY welcomeback2.restoreButton.access "S">

<!ENTITY welcomeback2.tabtitle      "Tot correcte">

<!ENTITY welcomeback2.pageTitle     "Tot correcte">
<!ENTITY welcomeback2.pageInfo1     "El &brandShortName; ja està llest.">

<!ENTITY welcomeback2.restoreAll.label  "Restaura totes les finestres i pestanyes">
<!ENTITY welcomeback2.restoreSome.label "Restaura només algunes pestanyes">


<!-- LOCALIZATION NOTE (welcomeback2.beforelink.pageInfo2,
welcomeback2.afterlink.pageInfo2): these two string are used respectively
before and after the the "learn more" link (welcomeback2.link.pageInfo2).
Localizers can use one of them, or both, to better adapt this sentence to
their language.
-->
<!ENTITY welcomeback2.beforelink.pageInfo2  "S'han eliminat els vostres complements i personalitzacions i s'han restaurat els paràmetres del navegador per defecte. Si això no resol el problema, ">
<!ENTITY welcomeback2.afterlink.pageInfo2   "">

<!ENTITY welcomeback2.link.pageInfo2        "obteniu més informació sobre què podeu fer.">


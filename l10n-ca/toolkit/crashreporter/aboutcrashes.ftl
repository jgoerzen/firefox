# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Informes de fallada
clear-all-reports-label = Elimina tots els informes
delete-confirm-title = N'esteu segur?
delete-confirm-description = Se suprimiran tots els informes i és una acció que no pot desfer-se.
crashes-unsubmitted-label = Informes de fallada pendents d'enviar
id-heading = Identificador de l'informe
date-crashed-heading = Data de fallada
crashes-submitted-label = Informes de fallada enviats
date-submitted-heading = Data d'enviament
no-reports-label = No s'ha enviat cap informe de fallada.
no-config-label = No s'ha configurat l'aplicació perquè mostri informes de fallada. Cal definir la preferència <code>breakpad.reportURL</code>.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = સંગઠન નીતિ
# 'Active' is used to describe the policies that are currently active
active-policies-tab = સક્રિય
errors-tab = ભૂલો
documentation-tab = દસ્તાવેજીકરણ
policy-name = નીતિનું નામ
policy-value = નીતિ મૂલ્ય
policy-errors = નીતિ ભૂલો
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = જ્યારે જૂથ નીતિ વાપરી રહ્યા હોય, ત્યારે આ નીતિ ફક્ત કમ્પ્યુટર સ્તર પર સેટ કરી શકાય છે.

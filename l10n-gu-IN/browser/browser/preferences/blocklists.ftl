# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = અવરોધિત સૂચિઓ
    .style = width: 55em
blocklist-desc = તમે તમારી બ્રાઉઝિંગ પ્રવૃત્તિને ટ્રેક કરી શકો છો તે વેબ ઘટકોને અવરોધિત કરવા માટે { -brand-short-name } કયું ઉપયોગ કરશે તે પસંદ કરી શકો છો.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = યાદી
blocklist-button-cancel =
    .label = રદ કરો
    .accesskey = C
blocklist-button-ok =
    .label = પરિવર્તનો સાચવો
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me મૂળભૂત રક્ષણ (ભલામણ કરેલ).
blocklist-item-moz-std-desc = કેટલાક ટ્રેકર તેથી વેબસાઇટ્સ યોગ્ય રીતે કામ પરવાનગી આપે છે.
blocklist-item-moz-full-name = કડક રક્ષણ Disconnect.me.
blocklist-item-moz-full-desc = બ્લોક્સ જાણીતા ટ્રેકર્સ. કેટલીક વેબસાઇટ્સ યોગ્ય રીતે કાર્ય કરી શકશે નહીં.

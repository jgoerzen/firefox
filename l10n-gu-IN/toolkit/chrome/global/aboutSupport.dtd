<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutSupport.pageTitle "મુશ્કેલીનિવારણ જાણકારી">

<!-- LOCALIZATION NOTE (aboutSupport.pageSubtitle): don't change the 'supportLink' id. -->
<!ENTITY aboutSupport.pageSubtitle "  આ પાનું ટૅકનિકલ જાણકારીને સમાવે છે કે જે ઉપયોગી થઇ શકે છે જ્યારે તમે સમસ્યાનો ઉકેલ લાવવાનો પ્રયત્ન કરી રહ્યા હોય. જો તમે &brandShortName; વિશે સામાન્ય પ્રશ્ર્નોનાં જવાબો જોઇ રહ્યા હોય, અમારી <a id='supportLink'>આધાર વેબ સાઇટ</a> ને ચકાસો.">

<!ENTITY aboutSupport.crashes.title "ભંગાણ અહેવાલો">
<!-- LOCALIZATION NOTE (aboutSupport.crashes.id):
This is likely the same like id.heading in crashes.dtd. -->
<!ENTITY aboutSupport.crashes.id "અહેવાલ ઓળખ">
<!ENTITY aboutSupport.crashes.sendDate "જમા થયેલ">
<!ENTITY aboutSupport.crashes.allReports "બધા ભંગાણ અહેવાલો">
<!ENTITY aboutSupport.crashes.noConfig "આ કાર્યક્રમ ભંગાણ અહેવાલો દર્શાવવા માટે રૂપરેખાંકિત થયેલ નથી.">

<!ENTITY aboutSupport.extensionsTitle "એક્સટેન્શનો">
<!ENTITY aboutSupport.extensionName "નામ">
<!ENTITY aboutSupport.extensionEnabled "સક્રિય થયેલ">
<!ENTITY aboutSupport.extensionVersion "આવૃત્તિ">
<!ENTITY aboutSupport.extensionId "ID">

<!ENTITY aboutSupport.securitySoftwareTitle "સુરક્ષા સૉફ્ટવેર">
<!ENTITY aboutSupport.securitySoftwareType "પ્રકાર">
<!ENTITY aboutSupport.securitySoftwareName "નામ">
<!ENTITY aboutSupport.securitySoftwareAntivirus "એન્ટિવાયરસ">
<!ENTITY aboutSupport.securitySoftwareAntiSpyware "એન્ટીસ્વાઇવયર">
<!ENTITY aboutSupport.securitySoftwareFirewall "ફાયરવૉલ">

<!ENTITY aboutSupport.featuresTitle "&brandShortName; વિશેષતા">
<!ENTITY aboutSupport.featureName "નામ">
<!ENTITY aboutSupport.featureVersion "આવૃત્તિ">
<!ENTITY aboutSupport.featureId "ID">

<!ENTITY aboutSupport.experimentsTitle "પરીક્ષણીય લક્ષણો">
<!ENTITY aboutSupport.experimentName "નામ">
<!ENTITY aboutSupport.experimentId "ID">
<!ENTITY aboutSupport.experimentDescription "વર્ણન">
<!ENTITY aboutSupport.experimentActive "સક્રિય">
<!ENTITY aboutSupport.experimentEndDate "અંત તારીખ">
<!ENTITY aboutSupport.experimentHomepage "મુખ્યપાનુ">
<!ENTITY aboutSupport.experimentBranch "શાખા">

<!ENTITY aboutSupport.appBasicsTitle "કાર્યક્રમ મૂળભૂતો">
<!ENTITY aboutSupport.appBasicsName "નામ">
<!ENTITY aboutSupport.appBasicsVersion "આવૃત્તિ">
<!ENTITY aboutSupport.appBasicsBuildID "બિલ્ડ ID">

<!-- LOCALIZATION NOTE (aboutSupport.appBasicsUpdateChannel, aboutSupport.appBasicsUpdateHistory, aboutSupport.appBasicsShowUpdateHistory):
"Update" is a noun here, not a verb. -->
<!ENTITY aboutSupport.appBasicsUpdateChannel "ચેનલ અદ્યતન કરો">
<!ENTITY aboutSupport.appBasicsUpdateHistory "ઇતિહાસ સુધારો">
<!ENTITY aboutSupport.appBasicsShowUpdateHistory "સુધારા ઇતિહાસને બતાવો">

<!ENTITY aboutSupport.appBasicsProfileDir "રૂપરેખા ડિરેક્ટરી">
<!-- LOCALIZATION NOTE (aboutSupport.appBasicsProfileDirWinMac):
This is the Windows- and Mac-specific variant of aboutSupport.appBasicsProfileDir.
Windows/Mac use the term "Folder" instead of "Directory" -->
<!ENTITY aboutSupport.appBasicsProfileDirWinMac "રૂપરેખા ફોલ્ડર">

<!ENTITY aboutSupport.appBasicsEnabledPlugins "સક્રિય થયેલ પ્લગઇનો">
<!ENTITY aboutSupport.appBasicsBuildConfig "બિલ્ડ રૂપરેખાંકન">
<!ENTITY aboutSupport.appBasicsUserAgent "વપરાશકર્તા એજન્ટ">
<!ENTITY aboutSupport.appBasicsOS "OS">
<!ENTITY aboutSupport.appBasicsMemoryUse "મેમરી વપરાશ">
<!ENTITY aboutSupport.appBasicsPerformance "કામગીરી">

<!-- LOCALIZATION NOTE the term "Service Workers" should not be translated. -->
<!ENTITY aboutSupport.appBasicsServiceWorkers "રજીસ્ટર કરેલ Service Workers">

<!ENTITY aboutSupport.appBasicsProfiles "પ્રોફાઇલ્સ">

<!ENTITY aboutSupport.appBasicsMultiProcessSupport "મલ્ટીપ્રોસેસ વિન્ડો">

<!ENTITY aboutSupport.appBasicsProcessCount "વેબ સામગ્રી પ્રક્રિયાઓ">

<!ENTITY aboutSupport.enterprisePolicies "સંગઠન નીતિ">

<!ENTITY aboutSupport.appBasicsKeyGoogle "Google કી">
<!ENTITY aboutSupport.appBasicsKeyMozilla "Mozilla સ્થાન સર્વિસ કી">

<!ENTITY aboutSupport.appBasicsSafeMode "સલામત મોડ">

<!ENTITY aboutSupport.showDir.label "ડિરેક્ટરી ખોલો">
<!-- LOCALIZATION NOTE (aboutSupport.showMac.label): This is the Mac-specific
variant of aboutSupport.showDir.label.  This allows us to use the preferred
"Finder" terminology on Mac. -->
<!ENTITY aboutSupport.showMac.label "શોધકર્તામાં બતાવો">
<!-- LOCALIZATION NOTE (aboutSupport.showWin2.label): This is the Windows-specific
variant of aboutSupport.showDir.label. -->
<!ENTITY aboutSupport.showWin2.label "ફોલ્ડર ખોલો">

<!ENTITY aboutSupport.modifiedKeyPrefsTitle "અગત્યની સુધારા પસંદગીઓ">
<!ENTITY aboutSupport.modifiedPrefsName "નામ">
<!ENTITY aboutSupport.modifiedPrefsValue "કિંમત">

<!-- LOCALIZATION NOTE (aboutSupport.userJSTitle, aboutSupport.userJSDescription): user.js is the name of the preference override file being checked. -->
<!ENTITY aboutSupport.userJSTitle "user.js પસંદગીઓ">
<!ENTITY aboutSupport.userJSDescription "તમારુ રૂપરેખા ફોલ્ડર &lt;a id='prefs-user-js-link'>user.js ફાઇલ&lt;/a> સમાવે છે, કે જે પસંદગીઓને સમાવે છે કે જેઓ  &brandShortName; દ્દારા બનાવેલ હતી નહિ.">

<!ENTITY aboutSupport.lockedKeyPrefsTitle "મહત્વની તાળુ મારેલ પસંદગીઓ">
<!ENTITY aboutSupport.lockedPrefsName "નામ">
<!ENTITY aboutSupport.lockedPrefsValue "કિંમત">

<!ENTITY aboutSupport.graphicsTitle "ગ્રાફિક્સ">

<!ENTITY aboutSupport.placeDatabaseTitle "સ્થાન ડેટાબેઝ">
<!ENTITY aboutSupport.placeDatabaseIntegrity "પ્રામાણિકતા">
<!ENTITY aboutSupport.placeDatabaseVerifyIntegrity "પ્રામાણિકતા ચકાસો">

<!ENTITY aboutSupport.jsTitle "JavaScript">
<!ENTITY aboutSupport.jsIncrementalGC "વધતુ જતુ GC">

<!ENTITY aboutSupport.a11yTitle "સુલભતા">
<!ENTITY aboutSupport.a11yActivated "સક્રિય થયેલ">
<!ENTITY aboutSupport.a11yForceDisabled "સુલભતા અટકાવો">
<!ENTITY aboutSupport.a11yHandlerUsed "સુલભ હેન્ડલર વપરાયેલ">
<!ENTITY aboutSupport.a11yInstantiator "ઇન્સ્ટિટેએટર ઉપલ્બધતા">

<!ENTITY aboutSupport.libraryVersionsTitle "લાઇબ્રેરી આવૃત્તિઓ">

<!ENTITY aboutSupport.installationHistoryTitle "સ્થાપન ઇતિહાસ">
<!ENTITY aboutSupport.updateHistoryTitle "સુધારા ઇતિહાસ">

<!ENTITY aboutSupport.copyTextToClipboard.label "ક્લિપબોર્ડમાં લખાણની નકલ કરો">
<!ENTITY aboutSupport.copyRawDataToClipboard.label "ક્લિપબોર્ડમાં કાચી માહિતીની નકલ કરો">

<!ENTITY aboutSupport.sandboxTitle "સેન્ડબોક્સ">
<!ENTITY aboutSupport.sandboxSyscallLogTitle "રદ કરેલ સિસ્ટમ કૉલ્સ">
<!ENTITY aboutSupport.sandboxSyscallIndex "#">
<!ENTITY aboutSupport.sandboxSyscallAge "સેકંડ પહેલા">
<!ENTITY aboutSupport.sandboxSyscallPID "PID">
<!ENTITY aboutSupport.sandboxSyscallTID "TID">
<!ENTITY aboutSupport.sandboxSyscallProcType "પ્રક્રિયા પ્રકાર">
<!ENTITY aboutSupport.sandboxSyscallNumber "સિસકૉલ">
<!ENTITY aboutSupport.sandboxSyscallArgs "દલીલો">

<!ENTITY aboutSupport.safeModeTitle "સુરક્ષિત મોડ અજમાવો">
<!ENTITY aboutSupport.restartInSafeMode.label "નિષ્ક્રિય થયેલ ઍડ-ઑન સાથે પુન:શરૂ કરો…">

<!ENTITY aboutSupport.graphicsFeaturesTitle "લક્ષણો">
<!ENTITY aboutSupport.graphicsDiagnosticsTitle "નિદાન">
<!ENTITY aboutSupport.graphicsFailureLogTitle "નિષ્ફળતા લોગ">
<!ENTITY aboutSupport.graphicsGPU1Title "GPU #1">
<!ENTITY aboutSupport.graphicsGPU2Title "GPU #2">
<!ENTITY aboutSupport.graphicsDecisionLogTitle "નિર્ણય લોગ">
<!ENTITY aboutSupport.graphicsCrashGuardsTitle "ક્રેશ ગાર્ડની અક્ષમ કરેલ સુવિધાઓ">
<!ENTITY aboutSupport.graphicsWorkaroundsTitle "વર્કરાઉન્ડ્સ">

<!ENTITY aboutSupport.mediaTitle "મીડિયા">
<!ENTITY aboutSupport.mediaOutputDevicesTitle "આઉટપુટ ઉપકરણો">
<!ENTITY aboutSupport.mediaInputDevicesTitle "ઇનપુટ ઉપકરણો">
<!ENTITY aboutSupport.mediaDeviceName "નામ">
<!ENTITY aboutSupport.mediaDeviceGroup "સમૂહ">
<!ENTITY aboutSupport.mediaDeviceVendor "વિક્રેતા">
<!ENTITY aboutSupport.mediaDeviceState "સ્થિતિ">
<!ENTITY aboutSupport.mediaDevicePreferred "પસંદ">
<!ENTITY aboutSupport.mediaDeviceFormat "બંધારણ">
<!ENTITY aboutSupport.mediaDeviceChannels "ચેનલ્સ">
<!ENTITY aboutSupport.mediaDeviceRate "દર">
<!ENTITY aboutSupport.mediaDeviceLatency "લેટન્સી">

<!ENTITY aboutSupport.intlTitle "આંતરરાષ્ટ્રીયકરણ &amp; સ્થાનિકીકરણ">
<!ENTITY aboutSupport.intlAppTitle "એપ્લિકેશનની સેટિંગ્સ">
<!ENTITY aboutSupport.intlLocalesRequested "વિનંતી કરેલ લૉકેલ્સ">
<!ENTITY aboutSupport.intlLocalesAvailable "ઉપલબ્ધ લૉકેલ્સ">
<!ENTITY aboutSupport.intlLocalesSupported "એપ્લિકેશન લૉકેલ્સ">
<!ENTITY aboutSupport.intlLocalesDefault "મૂળભૂત લૉકેલ">
<!ENTITY aboutSupport.intlOSTitle "ઑપરેટિંગ સિસ્ટમ">
<!ENTITY aboutSupport.intlOSPrefsSystemLocales "સિસ્ટમ લૉકેલ્સ">
<!ENTITY aboutSupport.intlRegionalPrefs "ક્ષેત્રીય પસંદગીઓ">

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox સમન્વય">
<!ENTITY syncBrand.shortName.label "સમન્વય">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label; સાથે જોડાવ'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'તમારું નવું ઉપકરણ સક્રિય કરવા માટે, ઉપકરણ પર “&syncBrand.shortName.label; સુયોજીત કરો” પસંદ કરો.'>
<!ENTITY sync.subtitle.pair.label 'સક્રિય કરવા માટે, તમારા અન્ય ઉપકરણમાં “ઉપકરણની જોડી બનાવો” પસંદ કરો.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'ઉપકરણ મારી પાસે નથી…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'લૉગિન'>
<!ENTITY sync.configure.engines.title.history 'ઇતિહાસ'>
<!ENTITY sync.configure.engines.title.tabs 'ટૅબ્સ'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; ચાલુ &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'બુકમાર્કો મેનુ'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'ટેગ'>
<!ENTITY bookmarks.folder.toolbar.label 'બુકમાર્કો સાધનપટ્ટી'>
<!ENTITY bookmarks.folder.other.label 'અન્ય બુકમાર્ક્સ'>
<!ENTITY bookmarks.folder.desktop.label 'ડેસ્કટોપ બુકમાર્કો'>
<!ENTITY bookmarks.folder.mobile.label 'મોબાઇલ બુકમાર્કો'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'પીન થયેલ'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'બ્રાઉઝીંગમાં પાછા'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label; માં સ્વાગત છે'>
<!ENTITY fxaccount_getting_started_description2 'સમન્વયિત કરવા માટે સાઇન ઇન કરો તમારા ટૅબ્સ, બુકમાર્ક્સ, લોગિન &amp; વધુ.'>
<!ENTITY fxaccount_getting_started_get_started 'શરૂઆત કરો'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label; ની જૂની આવૃત્તિ વાપરી રહ્યા છો?'>

<!ENTITY fxaccount_status_auth_server 'એકાઉન્ટ સર્વર'>
<!ENTITY fxaccount_status_sync_now 'હમણાં સમન્વય કરો'>
<!ENTITY fxaccount_status_syncing2 'સમન્વય કરી રહ્યા છે…'>
<!ENTITY fxaccount_status_device_name 'ઉપકરણ નામ'>
<!ENTITY fxaccount_status_sync_server 'સમન્વયન સર્વર'>
<!ENTITY fxaccount_status_needs_verification2 'તમારા ખાતાની ખાતરી કરવાની જરૂર છે. તપાસ ઇમેલ પુનઃમોકલવા માટે ઠપકારો.'>
<!ENTITY fxaccount_status_needs_credentials 'જોડાઇ શકતા નથી. પ્રવેશ કરવા માટે ઠપકારો.'>
<!ENTITY fxaccount_status_needs_upgrade 'પ્રવેશ કરવા માટે તમારે &brandShortName; ને અપગ્રેડ કરવાની જરૂર છે.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; એ સુયોજીત છે, પરંતુ સ્વયં સમન્વયિત થતુ નથી. Android સેટીંગ માહિતી વપરાશમાં “માહિતી સ્વયં-સમન્વય કરો” બદલો.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; સેટ કરેલું છે, પરંતુ આપમેળે સમન્વયિત થતો નથી. Android સેટિંગ્સ &gt; ના મેનૂમાં “સ્વતઃ-સમન્વયન ડેટા” ટૉગલ કરો એકાઉન્ટ્સ.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'તમારા નવા Firefox ખાતામાં સાઇન ઇન કરવા માટે ટેપ કરો.'>
<!ENTITY fxaccount_status_choose_what 'શું સમન્વયિત કરવું તે પસંદ કરો'>
<!ENTITY fxaccount_status_bookmarks 'બુકમાર્ક્સ'>
<!ENTITY fxaccount_status_history 'ઇતિહાસ'>
<!ENTITY fxaccount_status_passwords2 'લૉગિન'>
<!ENTITY fxaccount_status_tabs 'ટૅબ્સ ખોલો'>
<!ENTITY fxaccount_status_additional_settings 'વધારાની સેટિંગ્સ'>
<!ENTITY fxaccount_pref_sync_use_metered2 'ફક્ત Wi-Fi પર સમન્વયિત કરો'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'અટકાવો &brandShortName; સેલ્યુલર અથવા મીટર કરેલ નેટવર્ક પર સમન્વયન કરતા'>
<!ENTITY fxaccount_status_legal 'કાયદાકીય' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'સેવાની શરતો'>
<!ENTITY fxaccount_status_linkprivacy2 'ગોપનીયતા સૂચના'>
<!ENTITY fxaccount_remove_account 'જોડાણ તોડી નાખો&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'સમન્વયનમાંથી જોડાણ તોડી નાખો?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'તમારું બ્રાઉઝિંગ ડેટા આ ઉપકરણ પર રહેશે, પરંતુ તે હવે તમારા એકાઉન્ટ સાથે સમન્વયિત થશે નહીં.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox ખાતું &formatS; જોડાણ તૂટી ગયું.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'જોડાણ તોડી નાખો'>

<!ENTITY fxaccount_enable_debug_mode 'ડીબગ મોડને સક્ષમ કરો'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; વિકલ્પો'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; રૂપરેખાંકિત કરો'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; એ જોડાયેલું નથી'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; તરીકે પ્રવેશ કરવા માટે ઠપકારો'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'સુધારો સમાપ્ત કરો &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'આ તરીકે સાઇન ઇન કરવા માટે ટેપ કરો &formatS;'>

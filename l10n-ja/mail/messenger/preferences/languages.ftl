# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = 上へ移動
    .accesskey = U

languages-customize-movedown =
    .label = 下へ移動
    .accesskey = D

languages-customize-remove =
    .label = 削除
    .accesskey = R

languages-customize-select-language =
    .placeholder = 追加する言語を選択...

languages-customize-add =
    .label = 追加
    .accesskey = A

messenger-languages-window =
    .title = { -brand-short-name } 言語設定
    .style = width: 40em

messenger-languages-description = { -brand-short-name } は最初の言語を既定として表示し、必要ならば優先順位に従って代替言語を表示します。

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = クラッシュレポート
clear-all-reports-label = すべてのレポートを削除
delete-confirm-title = 本当によろしいですか？
delete-confirm-description = すべてのレポートが削除され、復元することはできません。
crashes-unsubmitted-label = 未送信のクラッシュレポート
id-heading = レポート ID
date-crashed-heading = クラッシュ日時
crashes-submitted-label = 送信したクラッシュレポート
date-submitted-heading = 送信日時
no-reports-label = 送信したクラッシュレポートはありません。
no-config-label = このプログラムはクラッシュレポートを表示できるように設定されていません。<code>breakpad.reportURL</code> が設定されている必要があります。

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Мови
    .style = width: 38em
webpage-languages-window =
    .title = Налаштування мови веб-сторінок
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = Деякі веб-сторінки можуть бути доступні більш, ніж однією мовою. Оберіть бажаний порядок мов для відображення таких сторінок
languages-customize-spoof-english =
    .label = Запитувати англійську версію веб-сайтів для покращеної приватності
languages-customize-moveup =
    .label = Вгору
    .accesskey = г
languages-customize-movedown =
    .label = Вниз
    .accesskey = з
languages-customize-remove =
    .label = Вилучити
    .accesskey = и
languages-customize-select-language =
    .placeholder = Додати мову…
languages-customize-add =
    .label = Додати
    .accesskey = о
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = Налаштування мови { -brand-short-name }
    .style = width: 40em
browser-languages-description = { -brand-short-name } типово буде відображати сторінки першою мовою зі списку, а інші мови у вказаному порядку, при необхідності.

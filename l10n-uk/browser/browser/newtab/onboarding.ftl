# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Спробувати зараз
onboarding-welcome-header = Вітаємо в { -brand-short-name }
onboarding-start-browsing-button-label = Почати перегляд

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Приватний перегляд
onboarding-private-browsing-text = Насолоджуйтесь користуванням. Приватний перегляд з блокуванням вмісту захищає від стеження, що переслідує вас в інтернеті.
onboarding-screenshots-title = Знімки екрану
onboarding-screenshots-text = Легко робіть знімки області чи цілої сторінки. Зберігайте в мережі й обмінюйтесь знімками екрану безпосередньо в { -brand-short-name }.
onboarding-addons-title = Додатки
onboarding-addons-text = Додавайте більше функцій до { -brand-short-name }, щоб задовольнити свої потреби. Порівнюйте ціни, перевіряйте погоду, або змінюйте зовнішній вигляд браузера за допомогою тем, щоб зробити його особливим.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Завантажуйте сторінки швидше, кмітливіше та безпечніше, за допомогою таких розширень, як Ghostery, що дозволяють блокувати надокучливу рекламу.

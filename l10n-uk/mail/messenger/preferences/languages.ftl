# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Вгору
    .accesskey = В
languages-customize-movedown =
    .label = Вниз
    .accesskey = з
languages-customize-remove =
    .label = Вилучити
    .accesskey = В
languages-customize-select-language =
    .placeholder = Додати мову…
languages-customize-add =
    .label = Додати
    .accesskey = о
messenger-languages-window =
    .title = Налаштування мови { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } типово буде відображати сторінки першою мовою зі списку, а інші мови у вказаному порядку, при необхідності.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

clear-all-reports-label = Retirar todos os informes
delete-confirm-title = Está seguro?
delete-confirm-description = Esta acción eliminará todos os informes de forma irreversíbel.
id-heading = ID do informe
no-reports-label = Non se enviou ningún informe de erro.
no-config-label = O aplicativo non está configurado para amosar informes de erro. Seleccione a preferencia <code>breakpad.reportURL</code>.

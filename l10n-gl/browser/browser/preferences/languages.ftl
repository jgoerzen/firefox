# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Idiomas
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = É frecuente que as páxinas web se poidan consultar en máis dun idioma. Escolla, en orde de preferencia, os idiomas para visualizar ese tipo de páxinas
languages-customize-spoof-english =
    .label = Solicitar as versións en inglés das páxinas web para mellorar a privacidade
languages-customize-moveup =
    .label = Subir
    .accesskey = S
languages-customize-movedown =
    .label = Baixar
    .accesskey = B
languages-customize-remove =
    .label = Retirar
    .accesskey = R
languages-customize-select-language =
    .placeholder = Seleccione un idioma para engadilo…
languages-customize-add =
    .label = Engadir
    .accesskey = E
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

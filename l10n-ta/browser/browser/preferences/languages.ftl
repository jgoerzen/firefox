# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = மொழிகள்
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = இணைய பக்கங்கள் சில நேரங்களில் ஒரு மொழிக்கு மேல் வழங்கப்படுகின்றன. இந்த இணைய பக்கங்களை முன்னரிமையின் அடிப்படையில் காண்பிக்க மொழிகளை தேர்ந்தெடுக்கவும்
languages-customize-spoof-english =
    .label = மேம்பட்ட தனியுரிமைக்கான வலைப்பக்கங்களின் ஆங்கில பதிப்பைக் கோரவும்
languages-customize-moveup =
    .label = மேலே நகர்த்து
    .accesskey = U
languages-customize-movedown =
    .label = கீழே நகர்த்து
    .accesskey = D
languages-customize-remove =
    .label = நீக்கு
    .accesskey = R
languages-customize-select-language =
    .placeholder = மொழியை தேர்வு செய்து சேர்...
languages-customize-add =
    .label = சேர்
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }

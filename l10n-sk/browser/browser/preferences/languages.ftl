# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Jazyky a kódovanie
    .style = width: 30em
webpage-languages-window =
    .title = Jazyk webových stránok
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = Webové stránky sú niekedy ponúkané vo viac než jednom jazyku. Vyberte jazyky takýchto stránok v poradí podľa svojej priority
languages-customize-spoof-english =
    .label = Za účelom väčšieho súkromia požadovať anglickú verziu webových stránok
languages-customize-moveup =
    .label = Posunúť nahor
    .accesskey = h
languages-customize-movedown =
    .label = Posunúť nadol
    .accesskey = d
languages-customize-remove =
    .label = Odstrániť
    .accesskey = O
languages-customize-select-language =
    .placeholder = Pridať jazyk…
languages-customize-add =
    .label = Pridať
    .accesskey = r
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = Jazyk aplikácie { -brand-short-name }
    .style = width: 40em
browser-languages-description = { -brand-short-name } zobrazí používateľské rozhranie v prvom vybranom jazyku. Ostatné použije podľa potreby a to vo vybranom poradí.

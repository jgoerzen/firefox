# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-BlockAboutConfig = Zablokuje prístup na stránku about:config.
policy-BlockAboutProfiles = Zablokuje prístup na stránku about:profiles.
policy-BlockAboutSupport = Zablokuje prístup na stránku about:support.
policy-DisableAppUpdate = Zabráni aktualizáciám prehliadača.
policy-DisableDeveloperTools = Zablokuje prístup k vývojárskym nástrojom
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Vypne funkciu Firefox Screenshots.
policy-DisableFirefoxStudies = Zabráni aplikácii { -brand-short-name } spúšťať štúdie.
policy-DisableTelemetry = Vypne telemetriu.
policy-FlashPlugin = Povolí alebo zakáže používanie zásuvného modulu Flash.
policy-HardwareAcceleration = Ak je nastavená hodnota false, vypne hardvérové urýchľovanie.
# “lock” means that the user won’t be able to change this setting
policy-Homepage = Nastaví a v prípade potreby uzamkne domovskú stránku.
policy-InstallAddonsPermission = Povolí určitým webovým stránkam inštalovať doplnky.
policy-Permissions = Nastaví povolenia pre kameru, mikrofón, polohu a upozornenia.
policy-PopupBlocking = Povolí určitým webovým stránkam zobrazovať v predvolenom nastavení vyskakovacie okná.
policy-Proxy = Nakonfiguruje nastavenia proxy.

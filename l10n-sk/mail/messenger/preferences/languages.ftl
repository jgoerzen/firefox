# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Posunúť vyššie
    .accesskey = v
languages-customize-movedown =
    .label = Posunúť nižšie
    .accesskey = n
languages-customize-remove =
    .label = Odstrániť
    .accesskey = O
languages-customize-select-language =
    .placeholder = Vyberte si jazyk, ktorý chcete pridať…
languages-customize-add =
    .label = Pridať
    .accesskey = P
messenger-languages-window =
    .title = Jazyk aplikácie { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } zobrazí používateľské rozhranie v prvom vybranom jazyku. Ostatné použije podľa potreby a to vo vybranom poradí.

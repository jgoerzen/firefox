<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY sanitizePrefs2.title          "ইতিহাস অপসারণের সেটিং">
<!-- LOCALIZATION NOTE (sanitizePrefs2.modal.width): width of the Clear History on Shutdown dialog.
     Should be large enough to contain the item* strings on a single line.
     The column width should be set at half of the dialog width. -->
<!ENTITY sanitizePrefs2.modal.width    "34em">
<!ENTITY sanitizePrefs2.column.width   "17em">

<!ENTITY sanitizeDialog2.title         "সাম্প্রতিক ইতিহাস অপসারণ">
<!-- LOCALIZATION NOTE (sanitizeDialog2.width): width of the Clear Recent History dialog -->
<!ENTITY sanitizeDialog2.width         "34em">

<!ENTITY clearDataSettings3.label     "যখন আমি &brandShortName; বন্ধ করব, তখন স্বয়ংক্রিয়ভাবে সব মুছে ফেলা হবে">

<!ENTITY clearDataSettings4.label     "যখন বন্ধ করা হয়, &brandShortName; স্বয়ংক্রিয়ভাবে সব পরিষ্কার করে ফেলে">

<!-- XXX rearrange entities to match physical layout when l10n isn't an issue -->
<!-- LOCALIZATION NOTE (clearTimeDuration.*): "Time range to clear" dropdown.
     See UI mockup at bug 480169 -->
<!ENTITY clearTimeDuration.label          "ইতিহাস অপসারণের সময়ের পরিসর: ">
<!ENTITY clearTimeDuration.accesskey      "T">
<!ENTITY clearTimeDuration.lastHour       "গত ১ ঘন্টা">
<!ENTITY clearTimeDuration.last2Hours     "গত ২ ঘন্টা">
<!ENTITY clearTimeDuration.last4Hours     "গত ৪ ঘন্টা">
<!ENTITY clearTimeDuration.today          "আজ সারাদিন">
<!ENTITY clearTimeDuration.everything     "সব">
<!-- Localization note (clearTimeDuration.suffix) - trailing entity for languages
that require it.  -->
<!ENTITY clearTimeDuration.suffix         "">

<!-- LOCALIZATION NOTE (detailsProgressiveDisclosure.*): Labels and accesskeys
     of the "Details" progressive disclosure button.  See UI mockup at bug
     480169 -->
<!ENTITY detailsProgressiveDisclosure.label     "বিবরণ (e)">
<!ENTITY detailsProgressiveDisclosure.accesskey "e">

<!ENTITY historySection.label         "ইতিহাস">
<!ENTITY dataSection.label            "তথ্য">

<!ENTITY itemHistoryAndDownloads.label     "ব্রাউজিং B এবং ডাউনলোড ইতিহাস">
<!ENTITY itemHistoryAndDownloads.accesskey "B">
<!ENTITY itemFormSearchHistory.label       "ফর্ম ও অনুসন্ধানের ইতিহাস F">
<!ENTITY itemFormSearchHistory.accesskey   "F">
<!ENTITY itemCookies.label                 "কুকি (C)">
<!ENTITY itemCookies.accesskey             "C">
<!ENTITY itemCache.label                   "ক্যাশ (a)">
<!ENTITY itemCache.accesskey               "a">
<!ENTITY itemOfflineApps.label             "অফলাইন ওয়েবসাইটের তথ্য (O)">
<!ENTITY itemOfflineApps.accesskey         "O">
<!ENTITY itemActiveLogins.label            "সক্রিয় লগইন (L)">
<!ENTITY itemActiveLogins.accesskey        "L">
<!ENTITY itemSitePreferences.label         "সাইট সংক্রান্ত পছন্দসমূহ (S)">
<!ENTITY itemSitePreferences.accesskey     "S">

<!-- LOCALIZATION NOTE (sanitizeEverythingUndoWarning): Second warning paragraph
     that appears when "Time range to clear" is set to "Everything".  See UI
     mockup at bug 480169 -->
<!ENTITY sanitizeEverythingUndoWarning     "এই পরিবর্তনটি অপরিবর্তনীয়!">

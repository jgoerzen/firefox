# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = ব্লক তালিকা
    .style = width: 55em
blocklist-desc = আপনার ব্রাউজিং অ্যাক্টিভিটি ট্র্যাক করছে এমন ওয়েব উপাদানের কোন তালিকা { -brand-short-name } ব্লক করবে তা নির্বাচন করতে পারেন।
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = তালিকা
blocklist-button-cancel =
    .label = বাতিল
    .accesskey = C
blocklist-button-ok =
    .label = পরিবর্তন সংরক্ষণ করুন
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me সাধারণ সুরক্ষা (সুপারিশকৃত)।
blocklist-item-moz-std-desc = কিছু ট্র্যাকার অনুমোদন করুন যেন ওয়েবসাইট সঠিকভাবে চলতে পারে।
blocklist-item-moz-full-name = Disconnect.me কঠোর সুরক্ষা।
blocklist-item-moz-full-desc = জানা ট্র্যাকার ব্লক কর। কিছু ওয়েবসাইট হয়তো সঠিকভাবে কাজ করবে না।

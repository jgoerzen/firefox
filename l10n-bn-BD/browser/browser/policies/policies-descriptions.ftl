# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = কাস্টম অ্যাপ আপডেট URL সেট করুন।
policy-Authentication = সমর্থিত সাইটের জন্য অভ্যন্তরীণ ব্যবহারকারী প্রমাণীকরণ কনফিগার করুন।
policy-BlockAboutAddons = অ্যাড-অন পরিচালকের অ্যাক্সেস প্রতিরোধ করুন (about:addons)।
policy-BlockAboutConfig = about:config অ্যাক্সেস প্রতিহত করুন।
policy-BlockAboutProfiles = about:profiles পেজে অ্যাক্সেস প্রতিহত করুন।
policy-BlockAboutSupport = about:support পেজে অ্যাক্সেস প্রতিহত করুন।
policy-Bookmarks = বুকমার্ক টুলবারে, বুকমার্ক মেনুতে বা এগুলোর অভ্যন্তরে নির্দিষ্ট ফোল্ডারে বুকমার্ক তৈরি করুন।
policy-Certificates = অভ্যন্তরীণ সার্টিফিকেট ব্যবহার করা হবে কি না। এই নীতিটি এখন শুধুমাত্র Windows এর জন্য।
policy-Cookies = ওয়েবসাইট কুকি গ্রহণ বা প্রত্যাখ্যান।
policy-DisableAppUpdate = ব্রাউজার আপডেট প্রতিরোধ করুন।
policy-DisableBuiltinPDFViewer = { -brand-short-name } এর ডিফল্ট পিডিএফ ভিউয়ার, PDF.js নিস্ক্রিয় করুন।

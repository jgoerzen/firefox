# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = ক্র্যাশ রিপোর্ট
clear-all-reports-label = সব প্রতিবেদন অপসারণ
delete-confirm-title = আপনি কি নিশ্চিত?
delete-confirm-description = এটি সব প্রতিবেদন মুছে দেবে এবং পরে আর ফেরত পাওয়া যাবে না।
crashes-unsubmitted-label = জমা না দেয়া ক্র্যাশ প্রতিবেদনসমূহ
id-heading = প্রতিবেদন আইডি
date-crashed-heading = ক্র‍্যাশের তারিখ
crashes-submitted-label = জমা দেয়া ক্র্যাশ প্রতিবেদনসমূহ
date-submitted-heading = জমা দেয়ার তারিখ
no-reports-label = কোনো ক্র্যাশ প্রতিবেদন জমা দেয়া হয়নি।
no-config-label = এই অ্যাপ্লিকেশনটি ক্র্যাশ প্রতিবেদন দেখানোর জন্য কনফিগার করা হয়নি। <code>breakpad.reportURL</code> নির্ধারণ করা আবশ্যক।

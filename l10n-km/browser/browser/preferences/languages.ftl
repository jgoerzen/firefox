# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = ភាសា
    .style = width: 30em
languages-close-key =
    .key = w
languages-customize-moveup =
    .label = ​ផ្លាស់ទី​ឡើង​លើ
    .accesskey = ង
languages-customize-movedown =
    .label = ផ្លាស់ទី​ចុះក្រោម
    .accesskey = ម
languages-customize-remove =
    .label = យកចេញ
    .accesskey = ញ
languages-customize-select-language =
    .placeholder = ជ្រើស​ភាសា​ដើម្បី​បន្ថែម...
languages-customize-add =
    .label = បន្ថែម
    .accesskey = ម
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

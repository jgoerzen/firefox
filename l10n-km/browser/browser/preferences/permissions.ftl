# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = ករណី​លើកលែង
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-address = អាសយដ្ឋាន​គេហទំព័រ
    .accesskey = d
permissions-block =
    .label = ទប់ស្កាត់
    .accesskey = ទ
permissions-session =
    .label = អនុញ្ញាត​សម្រាប់​សម័យ
    .accesskey = ត
permissions-allow =
    .label = អនុញ្ញាត
    .accesskey = ត
permissions-site-name =
    .label = គេហទំព័រ
permissions-status =
    .label = ស្ថានភាព
permissions-remove =
    .label = លុប​គេហទំព័រ
    .accesskey = R
permissions-remove-all =
    .label = លុប​គេហទំព័រ​ទាំងអស់
    .accesskey = e
permissions-button-cancel =
    .label = បោះបង់
    .accesskey = C
permissions-button-ok =
    .label = រក្សាទុក​ការ​ផ្លាស់ប្ដូរ
    .accesskey = S
permissions-searchbox =
    .placeholder = ស្វែងរក​គេហទំព័រ
permissions-capabilities-allow =
    .label = អនុញ្ញាត
permissions-capabilities-block =
    .label = ទប់ស្កាត់

## Invalid Hostname Dialog

permissions-invalid-uri-title = បាន​បញ្ចូល​ឈ្មោះ​ម៉ាស៊ីន​ដែល​មិន​ត្រឹមត្រូវ
permissions-invalid-uri-label = សូម​បញ្ចូល​ឈ្មោះ​ម៉ាស៊ីន​ត្រឹមត្រូវ

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = ករណី​​លើកលែង - ការ​ការពារ​ការ​តាមដាន
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = អ្នកបានបិទការការពារការតាមដាននៅលើ​គេហទំព័រ​ទាំងនេះ។

## Exceptions - Cookies


## Exceptions - Pop-ups

permissions-exceptions-popup-desc = អ្នក​អាច​បញ្ជាក់​តំបន់បណ្ដាញ​ណាមួយ​ដែល​អនុញ្ញាត​ឲ្យ​បើក​បង្អួច​លេចឡើង ។ វាយ​អាសយដ្ឋាន​ជាក់លាក់​របស់​តំបន់បណ្ដាញ​ដែល​អ្នក​ចង់​អនុញ្ញាត បន្ទាប់មក ចុច​អនុញ្ញាត ។

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = ការ​លើកលែង - ការ​ចូល​បាន​រក្សាទុក
    .style = { permissions-window.style }

## Exceptions - Add-ons

permissions-exceptions-addons-desc = អ្នក​អាច​បញ្ជាក់​តំបន់បណ្ដាញ​ណាមួយ​ដែល​អនុញ្ញាត​ឲ្យ​ដំឡើង​កម្មវិធី​បន្ថែម ។ វាយ​អាសយដ្ឋាន​ជាក់លាក់​របស់​តំបន់បណ្ដាញ​ដែល​អ្នក​ចង់​អនុញ្ញាត បន្ទាប់មក ចុច​អនុញ្ញាត ។

## Exceptions - Autoplay Media


## Site Permissions - Notifications


## Site Permissions - Location


## Site Permissions - Camera


## Site Permissions - Microphone


# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-ok-button = បន្ថែម​ឥឡូវ
    .accesskey = A

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.


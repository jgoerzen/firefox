# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title=Zgłoszenia awarii
clear-all-reports-label=Usuń wszystkie zgłoszenia
delete-confirm-title=Czy na pewno?
delete-confirm-description=Wszystkie zgłoszenia zostaną usunięte. Tej czynności nie można cofnąć.
crashes-unsubmitted-label=Nieprzesłane zgłoszenia awarii
id-heading=ID zgłoszenia
date-crashed-heading=Data awarii
crashes-submitted-label=Przesłane zgłoszenia awarii
date-submitted-heading=Data zgłoszenia
no-reports-label=Nie przesłano żadnych zgłoszeń awarii.
no-config-label=Ten program nie został właściwie skonfigurowany do wyświetlania zgłoszeń awarii. Konieczne jest prawidłowe ustawienie opcji <code>breakpad.reportURL</code>

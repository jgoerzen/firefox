# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Znajdź następne wystąpienie ostatnio szukanej frazy
findbar-previous =
    .tooltiptext = Znajdź poprzednie wystąpienie ostatnio szukanej frazy
findbar-find-button-close =
    .tooltiptext = Zamknij pasek wyszukiwania
findbar-highlight-all =
    .label = Podświetlanie wszystkich
    .accesskey = w
    .tooltiptext = Podświetl wszystkie wystąpienia szukanej frazy
findbar-case-sensitive =
    .label = Rozróżnianie wielkości liter
    .accesskey = R
    .tooltiptext = Rozróżniaj wielkość liter przy wyszukiwaniu
findbar-entire-word =
    .label = Całe słowa
    .accesskey = C
    .tooltiptext = Wyszukuj tylko całe słowa

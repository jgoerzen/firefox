# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Wyjątki
    .style = width: 39em
permissions-close-key =
    .key = w
permissions-address = Adres witryny:
    .accesskey = s
permissions-block =
    .label = Blokuj
    .accesskey = B
permissions-session =
    .label = Zezwalaj na czas sesji
    .accesskey = c
permissions-allow =
    .label = Zezwalaj
    .accesskey = Z
permissions-site-name =
    .label = Witryna
permissions-status =
    .label = Status
permissions-remove =
    .label = Usuń witrynę
    .accesskey = U
permissions-remove-all =
    .label = Usuń wszystkie witryny
    .accesskey = w
permissions-button-cancel =
    .label = Anuluj
    .accesskey = A
permissions-button-ok =
    .label = Zachowaj
    .accesskey = h
permissions-searchbox =
    .placeholder = Szukaj witryn
permissions-capabilities-allow =
    .label = Zezwalaj
permissions-capabilities-block =
    .label = Blokuj
permissions-capabilities-prompt =
    .label = Zawsze pytaj

permissions-invalid-uri-title = Wprowadzono nieprawidłową nazwę hosta
permissions-invalid-uri-label = Podaj prawidłową nazwę hosta

permissions-exceptions-tracking-protection-window =
    .title = Wyjątki – Ochrona przed śledzeniem
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = Ochrona przed śledzeniem została wyłączona dla następujących witryn.

permissions-exceptions-cookie-window =
    .title = Wyjątki – Ciasteczka i dane stron
    .style = { permissions-window.style }
permissions-exceptions-cookie-desc = Określ zasady akceptacji ciasteczek i danych stron. Podaj dokładny adres witryny, której uprawnienia chcesz zmodyfikować, a następnie naciśnij Zezwalaj, Blokuj lub Zezwalaj na czas sesji.

permissions-exceptions-popup-window =
    .title = Uprawnione witryny – Wyskakujące okna
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = Określ, które witryny mogą otwierać wyskakujące okna. Podaj dokładny adres witryny, której chcesz na to zezwolić, i naciśnij Zezwalaj.

permissions-exceptions-saved-logins-window =
    .title = Wyjątki – zachowywanie danych logowania
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = Dane logowania dla następujących witryn nie będą zachowywane.

permissions-exceptions-addons-window =
    .title = Uprawnione witryny – Instalacja dodatków
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = Określ, które witryny mogą instalować dodatki. Podaj dokładny adres witryny, której chcesz na to zezwolić, i naciśnij Zezwalaj.

permissions-exceptions-autoplay-media-window=
  .title=Uprawnione witryny – Automatyczne odtwarzanie
  .style={ permissions-window.style }
permissions-exceptions-autoplay-media-window2=
  .title=Wyjątki – Automatyczne odtwarzanie
  .style={ permissions-window.style }
permissions-exceptions-autoplay-media-desc=Określ, które witryny mogą automatycznie odtwarzać elementy multimedialne. Podaj dokładny adres witryny, której chcesz na to zezwolić, i naciśnij Zezwalaj.
permissions-exceptions-autoplay-media-desc2=Określ, które witryny będą mogły zawsze (lub nigdy) automatycznie odtwarzać treści z dźwiękiem. Wprowadź adres witryny, której uprawnienia chcesz określić, i wybierz Zezwalaj lub Blokuj.

permissions-site-notification-window =
    .title = Ustawienia – Powiadomienia
    .style = { permissions-window.style }
permissions-site-notification-desc = Następujące strony prosiły o możliwość wyświetlania powiadomień. Określ, które witryny mogą je wyświetlać. Można także zablokować nowe prośby.
permissions-site-notification-disable-label =
    .label = Blokowanie nowych próśb o możliwość wyświetlania powiadomień.
permissions-site-notification-disable-desc = Uniemożliwi to witrynom spoza listy powyżej proszenie o możliwość wysyłania powiadomień. Zablokowanie powiadomień może spowodować, że niektóre funkcje witryny nie będą działać.

permissions-site-location-window =
    .title = Ustawienia – Informacje o położeniu
    .style = { permissions-window.style }
permissions-site-location-desc = Następujące strony prosiły o możliwość uzyskiwania informacji o położeniu. Określ, które witryny mogą je uzyskiwać. Można także zablokować nowe prośby.
permissions-site-location-disable-label =
    .label = Blokowanie nowych próśb o możliwość uzyskiwania informacji o położeniu
permissions-site-location-disable-desc = Uniemożliwi to witrynom spoza listy powyżej proszenie o możliwość uzyskiwania informacji o położeniu. Zablokowanie uzyskiwania informacji o położeniu może spowodować, że niektóre funkcje witryny nie będą działać.

permissions-site-camera-window =
    .title = Ustawienia – Kamera
    .style = { permissions-window.style }
permissions-site-camera-desc = Następujące strony prosiły o dostęp do kamery. Określ, które witryny mogą uzyskiwać do niej dostęp. Można także zablokować nowe prośby.
permissions-site-camera-disable-label =
    .label = Blokowanie nowych próśb o dostęp do kamery
permissions-site-camera-disable-desc = Uniemożliwi to witrynom spoza listy powyżej proszenie o dostęp do kamery. Zablokowanie dostępu do kamery może spowodować, że niektóre funkcje witryny nie będą działać.

permissions-site-microphone-window =
    .title = Ustawienia – Mikrofon
    .style = { permissions-window.style }
permissions-site-microphone-desc = Następujące strony prosiły o dostęp do mikrofonu. Określ, które witryny mogą uzyskiwać do niego dostęp. Można także zablokować nowe prośby.
permissions-site-microphone-disable-label =
    .label = Blokowanie nowych próśb o dostęp do mikrofonu
permissions-site-microphone-disable-desc = Uniemożliwi to witrynom spoza listy powyżej proszenie o dostęp do mikrofonu. Zablokowanie dostępu do mikrofonu może spowodować, że niektóre funkcje witryny nie będą działać.

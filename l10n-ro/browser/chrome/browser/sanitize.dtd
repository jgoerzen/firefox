<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY sanitizePrefs2.title          "Setări pentru ștergerea istoricului">
<!-- LOCALIZATION NOTE (sanitizePrefs2.modal.width): width of the Clear History on Shutdown dialog.
     Should be large enough to contain the item* strings on a single line.
     The column width should be set at half of the dialog width. -->
<!ENTITY sanitizePrefs2.modal.width    "34em">
<!ENTITY sanitizePrefs2.column.width   "17em">

<!ENTITY sanitizeDialog2.title         "Șterge istoricul recent">
<!-- LOCALIZATION NOTE (sanitizeDialog2.width): width of the Clear Recent History dialog -->
<!ENTITY sanitizeDialog2.width         "34em">

<!ENTITY clearDataSettings3.label     "Când închid &brandShortName;, ar trebui să șteargă automat totul">

<!ENTITY clearDataSettings4.label     "Când este închis, &brandShortName; ar trebui să șteargă automat totul">

<!-- XXX rearrange entities to match physical layout when l10n isn't an issue -->
<!-- LOCALIZATION NOTE (clearTimeDuration.*): "Time range to clear" dropdown.
     See UI mockup at bug 480169 -->
<!ENTITY clearTimeDuration.label          "Perioadă de șters: ">
<!ENTITY clearTimeDuration.accesskey      "T">
<!ENTITY clearTimeDuration.lastHour       "Ultima oră">
<!ENTITY clearTimeDuration.last2Hours     "Ultimele două ore">
<!ENTITY clearTimeDuration.last4Hours     "Ultimele patru ore">
<!ENTITY clearTimeDuration.today          "Azi">
<!ENTITY clearTimeDuration.everything     "Tot">
<!-- Localization note (clearTimeDuration.suffix) - trailing entity for languages
that require it.  -->
<!ENTITY clearTimeDuration.suffix         "">

<!-- LOCALIZATION NOTE (detailsProgressiveDisclosure.*): Labels and accesskeys
     of the "Details" progressive disclosure button.  See UI mockup at bug
     480169 -->
<!ENTITY detailsProgressiveDisclosure.label     "Detalii">
<!ENTITY detailsProgressiveDisclosure.accesskey "e">

<!ENTITY historySection.label         "Istoric">
<!ENTITY dataSection.label            "Date">

<!ENTITY itemHistoryAndDownloads.label     "Istoricul navigării și al descărcărilor">
<!ENTITY itemHistoryAndDownloads.accesskey "B">
<!ENTITY itemFormSearchHistory.label       "Istoricul formularelor și al căutărilor">
<!ENTITY itemFormSearchHistory.accesskey   "F">
<!ENTITY itemCookies.label                 "Cookie-urile">
<!ENTITY itemCookies.accesskey             "C">
<!ENTITY itemCache.label                   "Cache-ul">
<!ENTITY itemCache.accesskey               "a">
<!ENTITY itemOfflineApps.label             "Datele offline ale site-urilor web">
<!ENTITY itemOfflineApps.accesskey         "O">
<!ENTITY itemActiveLogins.label            "Autentificările active">
<!ENTITY itemActiveLogins.accesskey        "L">
<!ENTITY itemSitePreferences.label         "Preferințele pentru site-uri">
<!ENTITY itemSitePreferences.accesskey     "S">

<!-- LOCALIZATION NOTE (sanitizeEverythingUndoWarning): Second warning paragraph
     that appears when "Time range to clear" is set to "Everything".  See UI
     mockup at bug 480169 -->
<!ENTITY sanitizeEverythingUndoWarning     "Această acțiune nu este reversibilă.">

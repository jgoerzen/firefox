# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Limbi
    .style = width: 32em
webpage-languages-window =
    .title = Setări privind limba pe paginile web
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = Paginile web sunt oferite uneori în mai multe limbi. Selectează limbile pentru afișarea acestor pagini web, în ordinea preferințelor
languages-customize-spoof-english =
    .label = Solicită versiunile în engleză ale paginiloe web pentru o confidențialitate sporită
languages-customize-moveup =
    .label = Mută în sus
    .accesskey = u
languages-customize-movedown =
    .label = Mută în jos
    .accesskey = D
languages-customize-remove =
    .label = Elimină
    .accesskey = R
languages-customize-select-language =
    .placeholder = Selectează o limbă pentru adăugare…
languages-customize-add =
    .label = Adaugă
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = Setări de limbă { -brand-short-name }
    .style = width: 40em
browser-languages-description = { -brand-short-name } va afișa prima limbă ca cea implicită și va afișa limbi alternative în ordinea în care apar, dacă e nevoie.

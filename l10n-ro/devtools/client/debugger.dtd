<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : FILE This file contains the Debugger strings -->
<!-- LOCALIZATION NOTE : FILE Do not translate commandkey -->

<!-- LOCALIZATION NOTE : FILE The correct localization of this file might be to
  - keep it in English, or another language commonly spoken among web developers.
  - You want to make that choice consistent across the developer tools.
  - A good criteria is the language in which you'd find the best
  - documentation on web development on the web. -->

<!-- LOCALIZATION NOTE (debuggerUI.closeButton.tooltip): This is the tooltip for
  -  the button that closes the debugger UI. -->
<!ENTITY debuggerUI.closeButton.tooltip "Închide">

<!-- LOCALIZATION NOTE (debuggerUI.panesButton.tooltip): This is the tooltip for
  -  the button that toggles the panes visible or hidden in the debugger UI. -->
<!ENTITY debuggerUI.panesButton.tooltip "Comută panourile">

<!-- LOCALIZATION NOTE (debuggerUI.blackBoxMessage.label): This is the message
  - displayed to users when they select a black boxed source from the sources
  - list in the debugger. -->
<!ENTITY debuggerUI.blackBoxMessage.label "Sursa este de tip black box: break-point-urile sunt deactivate și execuția pas cu pas sare peste aceasta.">

<!-- LOCALIZATION NOTE (debuggerUI.blackBoxMessage.unBlackBoxButton): This is
  - the text displayed in the button to stop black boxing the currently selected
  - source. -->
<!ENTITY debuggerUI.blackBoxMessage.unBlackBoxButton "Nu mai aplica black box pe această sursă">

<!-- LOCALIZATION NOTE (debuggerUI.optsButton.tooltip): This is the tooltip for
  -  the button that opens up an options context menu for the debugger UI. -->
<!ENTITY debuggerUI.optsButton.tooltip  "Opțiuni de depanare">

<!-- LOCALIZATION NOTE (debuggerUI.sources.blackBoxTooltip): This is the tooltip
  -  for the button that black boxes the selected source. -->
<!ENTITY debuggerUI.sources.blackBoxTooltip "Comută black boxing">

<!-- LOCALIZATION NOTE (debuggerUI.sources.prettyPrint): This is the tooltip for the
  -  button that pretty prints the selected source. -->
<!ENTITY debuggerUI.sources.prettyPrint "Reformatează sursa">

<!-- LOCALIZATION NOTE (debuggerUI.autoPrettyPrint): This is the label for the
  -  checkbox that toggles auto pretty print. -->
<!ENTITY debuggerUI.autoPrettyPrint     "Reformatează automat sursele minimizate">
<!ENTITY debuggerUI.autoPrettyPrint.accesskey "P">

<!-- LOCALIZATION NOTE (debuggerUI.sources.toggleBreakpoints): This is the tooltip for the
  -  button that toggles all breakpoints for all sources. -->
<!ENTITY debuggerUI.sources.toggleBreakpoints "Activează/dezactivează toate break-point-urile">

<!-- LOCALIZATION NOTE (debuggerUI.clearButton): This is the label for
  -  the button that clears the collected tracing data in the tracing tab. -->
<!ENTITY debuggerUI.clearButton "Curăță">

<!-- LOCALIZATION NOTE (debuggerUI.clearButton.tooltip): This is the tooltip for
  -  the button that clears the collected tracing data in the tracing tab. -->
<!ENTITY debuggerUI.clearButton.tooltip "Șterge trace-urile colectate">

<!-- LOCALIZATION NOTE (debuggerUI.pauseExceptions): This is the label for the
  -  checkbox that toggles pausing on exceptions. -->
<!ENTITY debuggerUI.pauseExceptions           "Pune pauză la excepții">
<!ENTITY debuggerUI.pauseExceptions.accesskey "E">

<!-- LOCALIZATION NOTE (debuggerUI.ignoreCaughtExceptions): This is the label for the
  -  checkbox that toggles ignoring caught exceptions. -->
<!ENTITY debuggerUI.ignoreCaughtExceptions           "Ignoră excepțiile capturate">
<!ENTITY debuggerUI.ignoreCaughtExceptions.accesskey "C">

<!-- LOCALIZATION NOTE (debuggerUI.showPanesOnInit): This is the label for the
  -  checkbox that toggles visibility of panes when opening the debugger. -->
<!ENTITY debuggerUI.showPanesOnInit           "Afișează panourile la pornire">
<!ENTITY debuggerUI.showPanesOnInit.accesskey "S">

<!-- LOCALIZATION NOTE (debuggerUI.showVarsFilter): This is the label for the
  -  checkbox that toggles visibility of a designated variables filter box. -->
<!ENTITY debuggerUI.showVarsFilter           "Afișează caseta de filtrare a variabilelor">
<!ENTITY debuggerUI.showVarsFilter.accesskey "V">

<!-- LOCALIZATION NOTE (debuggerUI.showOnlyEnum): This is the label for the
  -  checkbox that toggles visibility of hidden (non-enumerable) variables and
  -  properties in stack views. The "enumerable" flag is a state of a property
  -  defined in JavaScript. When in doubt, leave untranslated. -->
<!ENTITY debuggerUI.showOnlyEnum           "Afișează numai proprietățile enumerabile">
<!ENTITY debuggerUI.showOnlyEnum.accesskey "P">

<!-- LOCALIZATION NOTE (debuggerUI.showOriginalSource): This is the label for
  -  the checkbox that toggles the display of original or sourcemap-derived
  -  sources. -->
<!ENTITY debuggerUI.showOriginalSource           "Afișează sursele originale">
<!ENTITY debuggerUI.showOriginalSource.accesskey "O">

<!-- LOCALIZATION NOTE (debuggerUI.autoBlackBox): This is the label for
  -  the checkbox that toggles whether sources that we suspect are minified are
  -  automatically black boxed or not. -->
<!ENTITY debuggerUI.autoBlackBox           "Introdu automat sursele minimizate în black box">
<!ENTITY debuggerUI.autoBlackBox.accesskey "B">

<!-- LOCALIZATION NOTE (debuggerUI.searchPanelOperators): This is the text that
  -  appears in the filter panel popup as a header for the operators part. -->
<!ENTITY debuggerUI.searchPanelOperators    "Operatori:">

<!-- LOCALIZATION NOTE (debuggerUI.searchFile): This is the text that appears
  -  in the source editor's context menu for the scripts search operation. -->
<!ENTITY debuggerUI.searchFile           "Filtrează scripturile">
<!ENTITY debuggerUI.searchFile.key       "P">
<!ENTITY debuggerUI.searchFile.altkey    "O">
<!ENTITY debuggerUI.searchFile.accesskey "P">

<!-- LOCALIZATION NOTE (debuggerUI.searchGlobal): This is the text that appears
  -  in the source editor's context menu for the global search operation. -->
<!ENTITY debuggerUI.searchGlobal           "Caută în toate fișierele">
<!ENTITY debuggerUI.searchGlobal.key       "F">
<!ENTITY debuggerUI.searchGlobal.accesskey "F">

<!-- LOCALIZATION NOTE (debuggerUI.searchFunction): This is the text that appears
  -  in the source editor's context menu for the function search operation. -->
<!ENTITY debuggerUI.searchFunction           "Caută definiția funcției">
<!ENTITY debuggerUI.searchFunction.key       "D">
<!ENTITY debuggerUI.searchFunction.accesskey "D">

<!-- LOCALIZATION NOTE (debuggerUI.searchToken): This is the text that appears
  -  in the source editor's context menu for the token search operation. -->
<!ENTITY debuggerUI.searchToken           "Găsește">
<!ENTITY debuggerUI.searchToken.key       "F">
<!ENTITY debuggerUI.searchToken.accesskey "F">

<!-- LOCALIZATION NOTE (debuggerUI.searchGoToLine): This is the text that appears
  -  in the source editor's context menu for the line search operation. -->
<!ENTITY debuggerUI.searchGoToLine           "Mergi la rândul…">
<!ENTITY debuggerUI.searchGoToLine.key       "L">
<!ENTITY debuggerUI.searchGoToLine.accesskey "L">

<!-- LOCALIZATION NOTE (debuggerUI.searchVariable): This is the text that appears
  -  in the source editor's context menu for the variables search operation. -->
<!ENTITY debuggerUI.searchVariable           "Filtrează variabilele">
<!ENTITY debuggerUI.searchVariable.key       "V">
<!ENTITY debuggerUI.searchVariable.accesskey "V">

<!-- LOCALIZATION NOTE (debuggerUI.focusVariables): This is the text that appears
  -  in the source editor's context menu for the variables focus operation. -->
<!ENTITY debuggerUI.focusVariables           "Focalizează pe arborele de variabile">
<!ENTITY debuggerUI.focusVariables.key       "V">
<!ENTITY debuggerUI.focusVariables.accesskey "V">

<!-- LOCALIZATION NOTE (debuggerUI.condBreakPanelTitle): This is the text that
  -  appears in the conditional breakpoint panel popup as a description. -->
<!ENTITY debuggerUI.condBreakPanelTitle "Break-point-ul va întrerupe execuția numai dacă expresia următoare este adevărată">

<!-- LOCALIZATION NOTE (debuggerUI.seMenuBreak): This is the text that
  -  appears in the source editor context menu for adding a breakpoint. -->
<!ENTITY debuggerUI.seMenuBreak     "Adaugă break point">
<!ENTITY debuggerUI.seMenuBreak.key "B">

<!-- LOCALIZATION NOTE (debuggerUI.seMenuCondBreak): This is the text that
  -  appears in the source editor context menu for adding a conditional
  -  breakpoint. -->
<!ENTITY debuggerUI.seMenuCondBreak     "Adaugă break point condițional">
<!ENTITY debuggerUI.seMenuCondBreak.key "B">

<!-- LOCALIZATION NOTE (debuggerUI.seEditMenuCondBreak): This is the text that
  -  appears in the source editor context menu for editing a breakpoint. -->
<!ENTITY debuggerUI.seEditMenuCondBreak     "Editează break point-ul condițional">
<!ENTITY debuggerUI.seEditMenuCondBreak.key "B">

<!-- LOCALIZATION NOTE (debuggerUI.tabs.*): This is the text that
  -  appears in the debugger's side pane tabs. -->
<!ENTITY debuggerUI.tabs.workers        "Workeri">
<!ENTITY debuggerUI.tabs.sources        "Surse">
<!ENTITY debuggerUI.tabs.traces         "Traces">
<!ENTITY debuggerUI.tabs.callstack      "Stivă de apeluri">
<!ENTITY debuggerUI.tabs.variables      "Variabile">
<!ENTITY debuggerUI.tabs.events         "Evenimente">

<!-- LOCALIZATION NOTE (debuggerUI.seMenuAddWatch): This is the text that
  -  appears in the source editor context menu for adding an expression. -->
<!ENTITY debuggerUI.seMenuAddWatch      "Selecție pentru expresie urmărită">
<!ENTITY debuggerUI.seMenuAddWatch.key  "E">

<!-- LOCALIZATION NOTE (debuggerUI.addWatch): This is the text that
  -  appears in the watch expressions context menu for adding an expression. -->
<!ENTITY debuggerUI.addWatch            "Adaugă expresie de urmărit">
<!ENTITY debuggerUI.addWatch.accesskey  "E">

<!-- LOCALIZATION NOTE (debuggerUI.removeAllWatch): This is the text that
  -  appears in the watch expressions context menu for removing all expressions. -->
<!ENTITY debuggerUI.removeAllWatch           "Elimină toate expresiile urmărite">
<!ENTITY debuggerUI.removeAllWatch.key       "E">
<!ENTITY debuggerUI.removeAllWatch.accesskey "E">

<!-- LOCALIZATION NOTE (debuggerUI.stepping): These are the keycodes that
  -  control the stepping commands in the debugger (continue, step over,
  -  step in and step out). -->
<!ENTITY debuggerUI.stepping.resume1    "VK_F8">
<!ENTITY debuggerUI.stepping.stepOver1  "VK_F10">
<!ENTITY debuggerUI.stepping.stepIn1    "VK_F11">
<!ENTITY debuggerUI.stepping.stepOut1   "VK_F11">

<!-- LOCALIZATION NOTE (debuggerUI.context.newTab):  This is the label
  -  for the Open in New Tab menu item displayed in the context menu of the
  -  debugger sources side menu. This should be the same as
  -  netmonitorUI.context.newTab  -->
<!ENTITY debuggerUI.context.newTab           "Deschide într-o filă nouă">
<!ENTITY debuggerUI.context.newTab.accesskey "O">

<!-- LOCALIZATION NOTE (debuggerUI.context.copyUrl): This is the label displayed
  -  on the context menu that copies the selected request's url. This should be
  -  the same as netmonitorUI.context.copyUrl -->
<!ENTITY debuggerUI.context.copyUrl           "Copiază URL-ul">
<!ENTITY debuggerUI.context.copyUrl.accesskey "C">
<!ENTITY debuggerUI.context.copyUrl.key "C">

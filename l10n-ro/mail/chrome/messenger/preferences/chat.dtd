<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  generalTab.label            "General">

<!ENTITY  startupAction.label         "Când &brandShortName; pornește:">
<!ENTITY  startupAction.accesskey     "p">
<!ENTITY  startupOffline.label        "Păstrează-mi conturile de chat deconectate">
<!ENTITY  startupConnectAuto.label    "Conectează-mă automat la conturile de chat">

<!-- LOCALIZATION NOTE: reportIdleAfter.label is displayed first, then
there's a field where the user can enter a number, and itemTime is
displayed at the end of the line. The translations of the
reportIdleAfter.label and idleTime parts don't have to mean the exact
same thing as in English; please try instead to translate the whole
sentence. -->
<!ENTITY  reportIdleAfter.label         "Permite contactelor să știe când sunt inactiv după">
<!ENTITY  reportIdleAfter.accesskey     "P">
<!ENTITY  idleTime                      "minute de inactivitate">

<!ENTITY  andSetStatusToAway.label      "și setează-mi starea ca Plecat cu acest mesaj de stare:">
<!ENTITY  andSetStatusToAway.accesskey  "s">

<!ENTITY  sendTyping.label              "Trimite notificări de tastare în conversații">
<!ENTITY  sendTyping.accesskey          "t">

<!ENTITY  chatNotifications.label             "Când mesajele direcționate spre dumneavoastră ajung:">
<!ENTITY  desktopChatNotifications.label      "Arată o notificare:">
<!ENTITY  desktopChatNotifications.accesskey  "c">
<!ENTITY  completeNotification.label          "cu numele expeditorului și previzualizarea mesajului">
<!ENTITY  buddyInfoOnly.label                 "cu numele expeditorului doar">
<!ENTITY  dummyNotification.label             "fără nicio informație">
<!ENTITY  getAttention.label                  "Pâlpâie elementul de pe bara de activități">
<!ENTITY  getAttention.accesskey              "I">
<!ENTITY  getAttentionMac.label               "Animează pictograma de andocare">
<!ENTITY  getAttentionMac.accesskey           "o">
<!ENTITY  chatSound.accesskey                 "d">
<!ENTITY  chatSound.label                     "Redă un sunet">
<!ENTITY  play.label                          "Redă">
<!ENTITY  play.accesskey                      "R">
<!ENTITY  systemSound.label                   "Sunetul de sistem implicit pentru mesaj nou">
<!ENTITY  systemSound.accesskey               "S">
<!ENTITY  customsound.label                   "Utilizează următorul fișier de sunet">
<!ENTITY  customsound.accesskey               "U">
<!ENTITY  browse.label                        "Răsfoiește…">
<!ENTITY  browse.accesskey                    "f">

<!ENTITY messageStyleTab.title             "Stilurile mesajului">
<!ENTITY messageStylePreview.label         "Previzualizare:">
<!ENTITY messageStyleTheme.label           "Temă:">
<!ENTITY messageStyleTheme.accesskey       "T">
<!ENTITY messageStyleThunderbirdTheme.label "Thunderbird">
<!ENTITY messageStyleBubblesTheme.label    "Bule">
<!ENTITY messageStyleDarkTheme.label       "Întunecată">
<!ENTITY messageStylePaperSheetsTheme.label "Foi de hârtie">
<!ENTITY messageStyleSimpleTheme.label     "Simplă">
<!ENTITY messageStyleDefaultTheme.label    "Implicită">
<!ENTITY messageStyleVariant.label         "Variantă:">
<!ENTITY messageStyleVariant.accesskey     "V">
<!ENTITY messageStyleShowHeader.label      "Arată antetul">
<!ENTITY messageStyleShowHeader.accesskey  "a">
<!ENTITY messageStyleNoPreview.title       "Previzualizarea nu este disponibilă">
<!ENTITY messageStyleNoPreview.description "Această temă nu este validă sau nu este disponibilă momentan (supliment dezactivat, mod-de-siguranță, …).">

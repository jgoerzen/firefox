# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Găsește apariția următoare a textului
findbar-previous =
    .tooltiptext = Găsește apariția anterioară a textului
findbar-find-button-close =
    .tooltiptext = Închide bara de căutare
findbar-highlight-all =
    .label = Evidențiază toate
    .accesskey = a
    .tooltiptext = Evidențiază toate aparițiile textului
findbar-case-sensitive =
    .label = Potrivește literele mari și mici
    .accesskey = c
    .tooltiptext = Caută cu potrivirea literelor mari și mici
findbar-entire-word =
    .label = Cuvinte întregi
    .accesskey = W
    .tooltiptext = Caută numai cuvinte întregi

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

onboarding-button-label-try-now = Prova subito
onboarding-welcome-header = Benvenuto in { -brand-short-name }
onboarding-start-browsing-button-label = Inizia a navigare
onboarding-private-browsing-title = Navigazione anonima
onboarding-private-browsing-text = Naviga al riparo da occhi indiscreti. La navigazione anonima con blocco contenuti mette uno stop a tutti gli elementi traccianti che cercano di pedinarti sul Web.
onboarding-screenshots-title = Screenshots
onboarding-screenshots-text = Cattura, salva e condividi schermate senza uscire da { -brand-short-name }. Cattura una sezione della pagina, o la pagina intera, mentre navighi. Poi salvala sul web per poterla visualizzare e condividere ancora più facilmente.
onboarding-addons-title = Componenti aggiuntivi
onboarding-addons-text = Aggiungi nuove caratteristiche in grado di rendere { -brand-short-name } ancora più adatto alle tue esigenze. Confronta prezzi, controlla il meteo o esprimi la tua personalità con un tema su misura.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Naviga in modo più veloce, intelligente e sicuro grazie a estensioni come Ghostery, in grado di bloccare tutte quelle fastidiose pubblicità.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Imposta URL personalizzato per aggiornamento applicazione.
policy-Authentication = Configura autenticazione integrata per i siti in cui è supportata.
policy-BlockAboutAddons = Blocca accesso al gestore componenti aggiuntivi (about:addons).
policy-BlockAboutConfig = Blocca accesso alla pagina about:config.
policy-BlockAboutProfiles = Blocca accesso alla pagina about:profiles.
policy-BlockAboutSupport = Blocca accesso alla pagina about:support.
policy-Bookmarks = Crea segnalibri nella barra dei segnalibri, nel menu segnalibri o in una specifica cartella in questi elementi.
policy-Certificates = Imposta se utilizzare o meno i certificati predefiniti (built-in). Al momento questo criterio è disponibile solo per Windows.
policy-Cookies = Consenti o nega ai siti web di impostare cookie.
policy-DisableAppUpdate = Blocca l’aggiornamento del browser.
policy-DisableBuiltinPDFViewer = Disattiva PDF.js, il lettore PDF integrato in { -brand-short-name }.
policy-DisableDeveloperTools = Blocca accesso agli strumenti di sviluppo.
policy-DisableFeedbackCommands = Disattiva i comandi per inviare feedback dal menu Aiuto (“Invia feedback…” e “Segnala un sito ingannevole…”).
policy-DisableFirefoxAccounts = Disattiva i servizi basati sugli { -fxaccount-brand-name[lowercase] }, incluso Sync.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Disattiva Firefox Screenshots.
policy-DisableFirefoxStudies = Impedisci a { -brand-short-name } di condurre studi.
policy-DisableForgetButton = Impedisci accesso al pulsante “Dimentica”.
policy-DisableFormHistory = Non conservare la cronologia delle ricerche e dei moduli.
policy-DisableMasterPasswordCreation = Se impostato a “true” non è possibile impostare una password principale.
policy-DisablePocket = Disattiva la possibilità di salvare pagine web in Pocket.
policy-DisablePrivateBrowsing = Disattiva la modalità navigazione anonima.
policy-DisableProfileImport = Disattiva il menu per importare dati da un altro browser.
policy-DisableProfileRefresh = Disattiva il pulsante “Ripristina { -brand-short-name }” nella pagina about:support.
policy-DisableSafeMode = Disattiva la possibilità di riavviare in modalità provvisoria. Nota: l’utilizzo del pulsante Maiusc per avviare in modalità provvisoria può essere disattivato solo nei criteri di gruppo.
policy-DisableSecurityBypass = Impedisci all’utente di ignorare alcuni avvisi di sicurezza.
policy-DisableSetDesktopBackground = Disattiva il comando per impostare un’immagine come sfondo del desktop.
policy-DisableSetAsDesktopBackground = Disattiva il comando “Imposta come sfondo del desktop…” per le immagini.
policy-DisableSystemAddonUpdate = Impedisci al browser di installare e aggiornare componenti aggiuntivi di sistema.
policy-DisableTelemetry = Disattiva telemetria.
policy-DisplayBookmarksToolbar = Visualizza la barra dei segnalibri per impostazione predefinita.
policy-DisplayMenuBar = Visualizza la barra dei menu per impostazione predefinita.
policy-DontCheckDefaultBrowser = Disattiva il controllo del browser predefinito all’avvio.
policy-EnableTrackingProtection = Attiva o disattiva il blocco contenuti ed eventualmente impedisci modifiche all’opzione.
policy-Extensions = Installa, disinstalla o blocca estensioni. L’opzione per installare richiede URL o percorsi come parametri. L’opzione per disinstallare o bloccare richiede gli ID delle estensioni.
policy-FlashPlugin = Consenti o nega l’utilizzo del plugin Flash.
policy-HardwareAcceleration = Se “false”, disattiva l’accelerazione hardware.
policy-Homepage = Imposta la pagina iniziale ed eventualmente impedisci modifiche all’opzione.
policy-InstallAddonsPermission = Consenti a determinati siti web di installare componenti aggiuntivi.
policy-NoDefaultBookmarks = Disattiva la creazione dei segnalibri predefiniti di { -brand-short-name } e dei segnalibri intelligenti (“Più visitati”, “Etichette recenti”). Nota: questo criterio ha effetto solo se utilizzato prima della prima apertura del profilo.
policy-OfferToSaveLogins = Gestisci la richiesta in { -brand-short-name } di salvare credenziali di accesso. Entrambi i valori “true” e “false” sono validi.
policy-OverrideFirstRunPage = Sostituisci la pagina visualizzata alla prima esecuzione. Impostare questo criterio con un valore vuoto per disattivarne la visualizzazione.
policy-OverridePostUpdatePage = Sostituisci la pagina “Novità” visualizzata dopo un aggiornamento. Impostare questo criterio con un valore vuoto per disattivarne la visualizzazione.
policy-Permissions = Configura i permessi per fotocamera, microfono, posizione e notifiche.
policy-PopupBlocking = Consenti ad alcuni siti web di visualizzare finestre popup per impostazione predefinita.
policy-Proxy = Configura le impostazioni dei proxy.
policy-SanitizeOnShutdown = Elimina tutti i dati di navigazione alla chiusura.
policy-SearchBar = Imposta l’indirizzo predefinito nella barra di ricerca. L’utente potra comunque modificarlo.
policy-SearchEngines = Configura le impostazioni relative ai motori di ricerca. Questo criterio è disponibile solo nella versione Extended Support Release (ESR).
policy-SecurityDevices = Installa moduli PKCS #11.
policy-WebsiteFilter = Impedisci l’accesso a determinati siti web. Consulta la documentazione per ulteriori dettagli sul formato da utilizzare.

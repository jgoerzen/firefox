# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Web Console
# command line which is available from the Web Developer sub-menu
# -> 'Web Console'.
# These messages are displayed when an attempt is made to validate a
# page or a cache manifest using AppCacheUtils.jsm

# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (noManifest): the specified page has no cache manifest.
noManifest=Le pagina specificate non ha manifesto.

# LOCALIZATION NOTE (notUTF8): the associated cache manifest has a character
# encoding that is not UTF-8. Parameters: %S is the current encoding.
notUTF8=Le manifesto ha un codification del characteres de %S. Le manifestos debe haber le codification del characteres utf-8.

# LOCALIZATION NOTE (badMimeType): the associated cache manifest has a
# mimetype that is not text/cache-manifest. Parameters: %S is the current
# mimetype.
badMimeType=Le manifesto ha un type MIME %S. Le type MIME del manifestos debe esser texto/cache-manifest.

# LOCALIZATION NOTE (duplicateURI): the associated cache manifest references
# the same URI from multiple locations. Parameters: %1$S is the URI, %2$S is a
# list of references to this URI.
duplicateURI=Le URI %1$S se refere a plure positiones. Isto non es permittite: %2$S.

# LOCALIZATION NOTE (networkBlocksURI, fallbackBlocksURI): the associated
# cache manifest references the same URI in the NETWORK (or FALLBACK) section
# as it does in other sections. Parameters: %1$S is the line number, %2$S is
# the resource name, %3$S is the line number, %4$S is the resource name, %5$S
# is the section name.
networkBlocksURI=Le linea %1$S (%2$S) del section NETWORK impedi de mitter in cache le linea %3$S (%4$S) in le section %5$S.
fallbackBlocksURI=Le linea %1$S (%2$S) del section FALLBACK impedi de mitter in cache le linea %3$S (%4$S) in le section %5$S.

# LOCALIZATION NOTE (fileChangedButNotManifest): the associated cache manifest
# references a URI that has a file modified after the cache manifest.
# Parameters: %1$S is the resource name, %2$S is the cache manifest, %3$S is
# the line number.
fileChangedButNotManifest=Le file %1$S ha essite modificate post %2$S. A minus que le texto del file manifesto ha cambiate, le version in cache essera usate in su loco al linea %3$S.

# LOCALIZATION NOTE (cacheControlNoStore): the specified page has a header
# preventing caching or storing information. Parameters: %1$S is the resource
# name, %2$S is the line number.
cacheControlNoStore=%1$S ha su controlo de cache definite pro non stockar. Isto impedira que le cache del application stocka le file al linea %2$S.

# LOCALIZATION NOTE (notAvailable): the specified resource is not available.
# Parameters: %1$S is the resource name, %2$S is the line number.
notAvailable=%1$S puncta a un ressource que non es disponibile, al linea %2$S.

# LOCALIZATION NOTE (invalidURI): it's used when an invalid URI is passed to
# the appcache.
invalidURI=Le URI passate a AppCacheUtils es invalide.

# LOCALIZATION NOTE (noResults): it's used when a search returns no results.
noResults=Tu recerca non retornava ulle resultato.

# LOCALIZATION NOTE (cacheDisabled): it's used when the cache is disabled and
# an attempt is made to view offline data.
cacheDisabled=Le cache de tu disco es inactive. Per favor defini browser.cache.disk.enable a true in about:config e retenta.

# LOCALIZATION NOTE (firstLineMustBeCacheManifest): the associated cache
# manifest has a first line that is not "CACHE MANIFEST". Parameters: %S is
# the line number.
firstLineMustBeCacheManifest=Le prime linea del manifesto debe ser “CACHE MANIFEST” al linea %S.

# LOCALIZATION NOTE (cacheManifestOnlyFirstLine2): the associated cache
# manifest has "CACHE MANIFEST" on a line other than the first line.
# Parameters: %S is the line number where "CACHE MANIFEST" appears.
cacheManifestOnlyFirstLine2=“CACHE MANIFEST” es valide solmente al prime linea, ma illo ha essite trovate al linea %S.

# LOCALIZATION NOTE (asteriskInWrongSection2): the associated cache manifest
# has an asterisk (*) in a section other than the NETWORK section. Parameters:
# %1$S is the section name, %2$S is the line number.
asteriskInWrongSection2=Asterisco (*) mal usate in le section %1$S al linea %2$S. Si un linea in le section NETWORK contine solo un singule character asterisco, alora ulle URI non listate in le manifesto essera tractate como si le URI esseva listate in le section NETWORK. Alteremente tal URIs essera tractate como indisponibile. Altere usus del character * es prohibite.

# LOCALIZATION NOTE (escapeSpaces1): the associated cache manifest has a space
# in a URI. Spaces must be replaced with %20. Parameters: %S is the line
# number where this error occurs.
# %% will be displayed as a single % character (% is commonly used to define
# format specifiers, so it needs to be escaped).
escapeSpaces1=Le spatios in le URIs necessita ser replaciate con %%20 al linea %S.

# LOCALIZATION NOTE (slashDotDotSlashBad): the associated cache manifest has a
# URI containing /../, which is invalid. Parameters: %S is the line number
# where this error occurs.
slashDotDotSlashBad=/../ non es un prefixo valide de URI al linea %S.

# LOCALIZATION NOTE (tooManyDotDotSlashes): the associated cache manifest has
# a URI containing too many ../ operators. Too many of these operators mean
# that the file would be below the root of the site, which is not possible.
# Parameters: %S is the line number where this error occurs.
tooManyDotDotSlashes=Troppo de operatores puncto puncto barra (../) al linea %S.

# LOCALIZATION NOTE (fallbackUseSpaces): the associated cache manifest has a
# FALLBACK section containing more or less than the standard two URIs
# separated by a single space. Parameters: %S is the line number where this
# error occurs.
fallbackUseSpaces=Solo duo URIs separate per spatios es permittite in le section FALLBACK al linea %S.

# LOCALIZATION NOTE (fallbackAsterisk2): the associated cache manifest has a
# FALLBACK section that attempts to use an asterisk (*) as a wildcard. In this
# section the URI is simply a path prefix. Parameters: %S is the line number
# where this error occurs.
fallbackAsterisk2=Asterisco (*) incorrectemente usate in le section FALLBACK al linea %S. Le URIs in le section FALLBACK debe solmente corresponder a un preffixo de URI de requesta.

# LOCALIZATION NOTE (settingsBadValue): the associated cache manifest has a
# SETTINGS section containing something other than the valid "prefer-online"
# or "fast". Parameters: %S is the line number where this error occurs.
settingsBadValue=Le session SETTINGS (parametros) pote continer un sol valor, “prefer-online” (preferer in linea) o “fast” (rapide) in le linea %S.

# LOCALIZATION NOTE (invalidSectionName): the associated cache manifest
# contains an invalid section name. Parameters: %1$S is the section name, %2$S
# is the line number.
invalidSectionName=Le nomine de section (%1$S) al linea %2$S non es valide.

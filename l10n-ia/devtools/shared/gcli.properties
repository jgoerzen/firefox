# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Web Console
# command line which is available from the Web Developer sub-menu
# -> 'Web Console'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# For each command there are in general two strings. As an example consider
# the 'pref' command.
# commandDesc (e.g. prefDesc for the command 'pref'): this string contains a
# very short description of the command. It's designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
# commandManual (e.g. prefManual for the command 'pref'): this string will
# contain a fuller description of the command. It's diplayed when the user
# asks for help about a specific command (e.g. 'help pref').

# LOCALIZATION NOTE: This message is used to describe any command or command
# parameter when no description has been provided.
canonDescNone=(Nulle description)

# LOCALIZATION NOTE: The default name for a group of parameters.
canonDefaultGroupName=Optiones

# LOCALIZATION NOTE (canonProxyDesc, canonProxyManual): These commands are
# used to execute commands on a remote system (using a proxy). Parameters: %S
# is the name of the remote system.
canonProxyDesc=Exequer un commando sur %S
canonProxyManual=Un serie de commandos que es exequite per un systema remote. Le systema remote es attingite per: %S

# LOCALIZATION NOTE: This error message is displayed when we try to add a new
# command (using a proxy) where one already exists with the same name.
canonProxyExists=Il ha jam un commando denominate ‘%S’

# LOCALIZATION NOTE: This message describes the '{' command, which allows
# entry of JavaScript like traditional developer tool command lines.
cliEvalJavascript=Entrar directemente in JavaScript

# LOCALIZATION NOTE: This message is displayed when the command line has more
# arguments than the current command can understand.
cliUnusedArg=Troppo de argumentos

# LOCALIZATION NOTE: The title of the dialog which displays the options that
# are available to the current command.
cliOptions=Optiones disponibile

# LOCALIZATION NOTE: The error message when the user types a command that
# isn't registered
cliUnknownCommand2=Commando non valide: ‘%1$S’.

# LOCALIZATION NOTE: A parameter should have a value, but doesn't
cliIncompleteParam=Valor requirite pro ‘%1$S’.

# LOCALIZATION NOTE: Error message given when a file argument points to a file
# that does not exist, but should (e.g. for use with File->Open) %1$S is a
# filename
fileErrNotExists=‘%1$S’ non existe

# LOCALIZATION NOTE: Error message given when a file argument points to a file
# that exists, but should not (e.g. for use with File->Save As) %1$S is a
# filename
fileErrExists=‘%1$S’ jam existe

# LOCALIZATION NOTE: Error message given when a file argument points to a
# non-file, when a file is needed. %1$S is a filename
fileErrIsNotFile=‘%1$S’ non es un file

# LOCALIZATION NOTE: Error message given when a file argument points to a
# non-directory, when a directory is needed (e.g. for use with 'cd') %1$S is a
# filename
fileErrIsNotDirectory=‘%1$S’ non es un directorio

# LOCALIZATION NOTE: Error message given when a file argument does not match
# the specified regular expression %1$S is a filename %2$S is a regular
# expression
fileErrDoesntMatch=‘%1$S’ non concorda con ‘%2$S’

# LOCALIZATION NOTE: When the menu has displayed all the matches that it
# should (i.e. about 10 items) then we display this to alert the user that
# more matches are available.
fieldMenuMore=Plus de correspondentias, continua scribente

# LOCALIZATION NOTE: The command line provides completion for JavaScript
# commands, however there are times when the scope of what we're completing
# against can't be used. This error message is displayed when this happens.
jstypeParseScope=Ambito perdite

# LOCALIZATION NOTE (jstypeParseMissing, jstypeBeginSyntax,
# jstypeBeginUnterm): These error messages are displayed when the command line
# is doing JavaScript completion and encounters errors.
jstypeParseMissing=Impossibile trovar le proprietate ‘%S’
jstypeBeginSyntax=Error de syntaxe
jstypeBeginUnterm=Catena literal non terminate

# LOCALIZATION NOTE: This message is displayed if the system for providing
# JavaScript completions encounters and error it displays this.
jstypeParseError=Error

# LOCALIZATION NOTE (typesNumberNan, typesNumberNotInt2, typesDateNan): These
# error messages are displayed when the command line is passed a variable
# which has the wrong format and can't be converted. Parameters: %S is the
# passed variable.
typesNumberNan=Impossibile converter “%S” a un numero.
typesNumberNotInt2=Impossibile converter “%S” a un numero integre.
typesDateNan=Impossibile converter “%S” a un data.

# LOCALIZATION NOTE (typesNumberMax, typesNumberMin, typesDateMax,
# typesDateMin): These error messages are displayed when the command line is
# passed a variable which has a value out of range (number or date).
# Parameters: %1$S is the passed variable, %2$S is the limit value.
typesNumberMax=%1$S es major del maximo consentite: %2$S.
typesNumberMin=%1$S es minor del minimo consentite: %2$S.
typesDateMax=%1$S es plu tarde del maxime tempore consentite: %2$S.
typesDateMin=%1$S es precedente le minimo consentite: %2$S.

# LOCALIZATION NOTE: This error message is displayed when the command line is
# passed an option with a limited number of correct values, but the passed
# value is not one of them.
typesSelectionNomatch=Impossibile usar ‘%S’.

# LOCALIZATION NOTE: This error message is displayed when the command line is
# expecting a CSS query string, however the passed string is not valid.
nodeParseSyntax=Error de syntaxe in le requesta CSS

# LOCALIZATION NOTE (nodeParseMultiple, nodeParseNone): These error messages
# are displayed when the command line is expecting a CSS string that matches a
# single node, but more nodes (or none) match.
nodeParseMultiple=Troppo de correspondentias (%S)
nodeParseNone=Nulle correspondentia

# LOCALIZATION NOTE (helpDesc, helpManual, helpSearchDesc, helpSearchManual3):
# These strings describe the "help" command, used to display a description of
# a command (e.g. "help pref"), and its parameter 'search'.
helpDesc=Obtener auxilio sur le commandos disponibile
helpManual=Provide le adjuta sur un commando specific (si il es providite un catena a recercar e un correspondentia exacte es trovate) o sur le commandos disponibile (si il non es providite un catena a recercar, o si il non es trovate un correspondentia exacte).
helpSearchDesc=Linea de characteres pro le recerca
helpSearchManual3=catena de characteres a usar pro reducer le numero de commandos monstrate. Expressiones regular non es admittite.

# LOCALIZATION NOTE: These strings are displayed in the help page for a
# command in the console.
helpManSynopsis=Synopsis

# LOCALIZATION NOTE: This message is displayed in the help page if the command
# has no parameters.
helpManNone=Nihil

# LOCALIZATION NOTE: This message is displayed in response to the 'help'
# command when used without a filter, just above the list of known commands.
helpListAll=Commandos disponibile:

# LOCALIZATION NOTE (helpListPrefix, helpListNone): These messages are
# displayed in response to the 'help <search>' command (i.e. with a search
# string), just above the list of matching commands. Parameters: %S is the
# search string.
helpListPrefix=Commandos comenciante per ‘%S’:
helpListNone=Nulle commandos es comenciante per ‘%S’:

# LOCALIZATION NOTE (helpManRequired, helpManOptional, helpManDefault): When
# the 'help x' command wants to show the manual for the 'x' command, it needs
# to be able to describe the parameters as either required or optional, or if
# they have a default value.
helpManRequired=necessari
helpManOptional=optional
helpManDefault=optional, predefinite=%S

# LOCALIZATION NOTE: This forms part of the output from the 'help' command.
# 'GCLI' is a project name and should be left untranslated.
helpIntro=GCLI es un experimentation pro crear un linea de commando facile a usar pro le disveloppatores del web.

# LOCALIZATION NOTE: Text shown as part of the output of the 'help' command
# when the command in question has sub-commands, before a list of the matching
# sub-commands.
subCommands=Sub-commandos

# LOCALIZATION NOTE: This error message is displayed when the command line is
# cannot find a match for the parse types.
commandParseError=Error a interpretar le linea de commando

# LOCALIZATION NOTE (contextDesc, contextManual, contextPrefixDesc): These
# strings are used to describe the 'context' command and its 'prefix'
# parameter. See localization comment for 'connect' for an explanation about
# 'prefix'.
contextDesc=Concentrar se su un gruppo de commandos
contextManual=Predefinir un prefixo pro le commandos futur. Per exemplo ‘context git’ te permitterea scriber ‘commit’ in loco de ‘git commit’.
contextPrefixDesc=Le prefixo del commando

# LOCALIZATION NOTE: This message message displayed during the processing of
# the 'context' command, when the found command is not a parent command.
contextNotParentError=Impossibile usar ‘%S’ como prefixo de perque illo non es un commando parente.

# LOCALIZATION NOTE (contextReply, contextEmptyReply): These messages are
# displayed during the processing of the 'context' command, to indicate
# success or that there is no command prefix.
contextReply=%S in uso como prefixo de commando
contextEmptyReply=Prefixo de commando non definite

# LOCALIZATION NOTE (connectDesc, connectManual, connectPrefixDesc,
# connectMethodDesc, connectUrlDesc, connectDupReply): These strings describe
# the 'connect' command and all its available parameters. A 'prefix' is an
# alias for the remote server (think of it as a "connection name"), and it
# allows to identify a specific server when connected to multiple remote
# servers.
connectDesc=Commandos de proxy pro le servitor
connectManual=Connecter se al servitor, creante le version local del commandos sur le servitor. Le commandos remote ha un prefixo pro los distinguer del commandos local (ma vide le commando de contexto pro superar isto)
connectPrefixDesc=Prefixo parente pro le commandos importate
connectMethodDesc=Le methodo de connexion
connectUrlDesc=Le URL pro le connexion
connectDupReply=Un connexion denominate %S existe jam.

# LOCALIZATION NOTE: The output of the 'connect' command, telling the user
# what it has done. Parameters: %S is the prefix command. See localization
# comment for 'connect' for an explanation about 'prefix'.
connectReply=Addite %S commandos.

# LOCALIZATION NOTE (disconnectDesc2, disconnectManual2,
# disconnectPrefixDesc): These strings describe the 'disconnect' command and
# all its available parameters. See localization comment for 'connect' for an
# explanation about 'prefix'.
disconnectDesc2=Disconnecter se ab le servitor
disconnectManual2=Disconnecter se del servitor actual pro le execution remote de commandos
disconnectPrefixDesc=Prefixo parente pro le commandos importate

# LOCALIZATION NOTE: This is the output of the 'disconnect' command,
# explaining the user what has been done. Parameters: %S is the number of
# commands removed.
disconnectReply=Removite %S commandos.

# LOCALIZATION NOTE: These strings describe the 'clear' command
clearDesc=Vacuar le area de egresso

# LOCALIZATION NOTE (prefDesc, prefManual, prefListDesc, prefListManual,
# prefListSearchDesc, prefListSearchManual, prefShowDesc, prefShowManual,
# prefShowSettingDesc, prefShowSettingManual): These strings describe the
# 'pref' command and all its available sub-commands and parameters.
prefDesc=Commandos pro controlar le configurationes
prefManual=Commandos a monstrar e alterar le preferentias pro GCLI e le ambiente al contorno
prefListDesc=Monstrar le parametros disponibile
prefListManual=Monstra un lista de preferentias, optionalmente filtrate per le parametro ‘search’
prefListSearchDesc=Filtrar le lista del parametros monstrate
prefListSearchManual=Cerca le catena de characteres date in le lista de preferentias disponibile
prefShowDesc=Monstrar valor predefinite
prefShowManual=Monstra le valor de un date preferentia
prefShowSettingDesc=Predefinition a monstrar
prefShowSettingManual=Le nomine del predefinition a monstrar

# LOCALIZATION NOTE: This message is used to show the preference name and the
# associated preference value. Parameters: %1$S is the preference name, %2$S
# is the preference value.
prefShowSettingValue=%1$S: %2$S

# LOCALIZATION NOTE (prefSetDesc, prefSetManual, prefSetSettingDesc,
# prefSetSettingManual, prefSetValueDesc, prefSetValueManual): These strings
# describe the 'pref set' command and all its parameters.
prefSetDesc=Alterar un definition
prefSetManual=Alterar le preferentias definite per le ambiente
prefSetSettingDesc=Parametro a alterar
prefSetSettingManual=Le nomine del parametro a alterar.
prefSetValueDesc=Nove valor del parametro
prefSetValueManual=Le nove valor del predefinition specificate

# LOCALIZATION NOTE (prefResetDesc, prefResetManual, prefResetSettingDesc,
# prefResetSettingManual): These strings describe the 'pref reset' command and
# all its parameters.
prefResetDesc=Remontar un parametro
prefResetManual=Remonta un parametro al valor predefinite per le systema
prefResetSettingDesc=Parametro a remontar
prefResetSettingManual=Le nomine del parametro a remontar al valor predefinite per le systema

# LOCALIZATION NOTE: This string is displayed in the output from the 'pref
# list' command as a label to an input element that allows the user to filter
# the results.
prefOutputFilter=Filtro

# LOCALIZATION NOTE (prefOutputName, prefOutputValue): These strings are
# displayed in the output from the 'pref list' command as table headings.
prefOutputName=Namine
prefOutputValue=Valor

# LOCALIZATION NOTE (introTextOpening3, introTextCommands, introTextKeys2,
# introTextF1Escape, introTextGo): These strings are displayed when the user
# first opens the developer toolbar to explain the command line, and is shown
# each time it is opened until the user clicks the 'Got it!' button.
introTextOpening3=GCLI es un experimentation pro crear un linea de commando facile a usar pro le disveloppatores del web.
introTextCommands=Pro vider un lista de commandos scriber
introTextKeys2=o pro monstrar/occultar informationes re le commandos pulsa
introTextF1Escape=F1/Escape
introTextGo=Io lo comprendeva!

# LOCALIZATION NOTE: This is a short description of the 'hideIntro' setting.
hideIntroDesc=Monstrar le message initial de benvenie

# LOCALIZATION NOTE: This is a description of the 'eagerHelper' setting. It's
# displayed when the user asks for help on the settings. eagerHelper allows
# users to select between showing no tooltips, permanent tooltips, and only
# important tooltips.
eagerHelperDesc=Frequentia del bullas de information

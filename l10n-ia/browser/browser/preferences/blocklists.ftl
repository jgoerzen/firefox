# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Listas de blocage
    .style = width: 55em
blocklist-desc = Tu pote eliger le lista que { -brand-short-name } usara pro blocar le elementos de web que pote traciar tu activitate de navigation.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Lista
blocklist-button-cancel =
    .label = Cancellar
    .accesskey = C
blocklist-button-ok =
    .label = Salvar le cambios
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me basic protection (Recommendate).
blocklist-item-moz-std-desc = Permitter alcun traciatores a fin que le sitos web functiona correctemente.
blocklist-item-moz-full-name = Protection stricte de Disconnect.me.
blocklist-item-moz-full-desc = Blocar le traciatores cognoscite. Alcun sitos web potera non functionar correctemente.

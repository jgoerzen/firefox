<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutSupport.pageTitle "Informationes de diagnostico">

<!-- LOCALIZATION NOTE (aboutSupport.pageSubtitle): don't change the 'supportLink' id. -->
<!ENTITY aboutSupport.pageSubtitle "
  Iste pagina contine informationes technic que pote servir quando tu
  tenta de resolver un problema. Si tu cerca responsas a questiones commun 
  re &brandShortName;, controla nostre <a id='supportLink'>sito web de assistentia</a>.">

<!ENTITY aboutSupport.crashes.title "Reportos de collapso">
<!-- LOCALIZATION NOTE (aboutSupport.crashes.id):
This is likely the same like id.heading in crashes.dtd. -->
<!ENTITY aboutSupport.crashes.id "ID de reporto">
<!ENTITY aboutSupport.crashes.sendDate "Submittite">
<!ENTITY aboutSupport.crashes.allReports "Tote le reportos de collapso">
<!ENTITY aboutSupport.crashes.noConfig "Iste application non ha essite configurate pro monstrar le reportos de collapso.">

<!ENTITY aboutSupport.extensionsTitle "Extensiones">
<!ENTITY aboutSupport.extensionName "Nomine">
<!ENTITY aboutSupport.extensionEnabled "Activate">
<!ENTITY aboutSupport.extensionVersion "Version">
<!ENTITY aboutSupport.extensionId "ID">

<!ENTITY aboutSupport.securitySoftwareTitle "Software de securitate">
<!ENTITY aboutSupport.securitySoftwareType "Typo">
<!ENTITY aboutSupport.securitySoftwareName "Nomine">
<!ENTITY aboutSupport.securitySoftwareAntivirus "Antivirus">
<!ENTITY aboutSupport.securitySoftwareAntiSpyware "Antispyware">
<!ENTITY aboutSupport.securitySoftwareFirewall "Parafoco">

<!ENTITY aboutSupport.featuresTitle "Functionalitates de &brandShortName;">
<!ENTITY aboutSupport.featureName "Nomine">
<!ENTITY aboutSupport.featureVersion "Version">
<!ENTITY aboutSupport.featureId "ID">

<!ENTITY aboutSupport.experimentsTitle "Functionalitates experimental">
<!ENTITY aboutSupport.experimentName "Nomine">
<!ENTITY aboutSupport.experimentId "ID">
<!ENTITY aboutSupport.experimentDescription "Description">
<!ENTITY aboutSupport.experimentActive "Active">
<!ENTITY aboutSupport.experimentEndDate "Data de fin">
<!ENTITY aboutSupport.experimentHomepage "Pagina initial">
<!ENTITY aboutSupport.experimentBranch "Branca">

<!ENTITY aboutSupport.appBasicsTitle "Informationes basic del application">
<!ENTITY aboutSupport.appBasicsName "Nomine">
<!ENTITY aboutSupport.appBasicsVersion "Version">
<!ENTITY aboutSupport.appBasicsBuildID "ID de compilation">

<!-- LOCALIZATION NOTE (aboutSupport.appBasicsUpdateChannel, aboutSupport.appBasicsUpdateHistory, aboutSupport.appBasicsShowUpdateHistory):
"Update" is a noun here, not a verb. -->
<!ENTITY aboutSupport.appBasicsUpdateChannel "Canal de actualisation">
<!ENTITY aboutSupport.appBasicsUpdateHistory "Chronologia de actualisationes">
<!ENTITY aboutSupport.appBasicsShowUpdateHistory "Monstrar le chronologia del actualisationes">

<!ENTITY aboutSupport.appBasicsProfileDir "Directorio del profilo">
<!-- LOCALIZATION NOTE (aboutSupport.appBasicsProfileDirWinMac):
This is the Windows- and Mac-specific variant of aboutSupport.appBasicsProfileDir.
Windows/Mac use the term "Folder" instead of "Directory" -->
<!ENTITY aboutSupport.appBasicsProfileDirWinMac "Dossier del profilo">

<!ENTITY aboutSupport.appBasicsEnabledPlugins "Plugins activate">
<!ENTITY aboutSupport.appBasicsBuildConfig "Configuration de compilation">
<!ENTITY aboutSupport.appBasicsUserAgent "Agente del usator">
<!ENTITY aboutSupport.appBasicsOS "OS">
<!ENTITY aboutSupport.appBasicsMemoryUse "Uso del memoria">
<!ENTITY aboutSupport.appBasicsPerformance "Rendimento">

<!-- LOCALIZATION NOTE the term "Service Workers" should not be translated. -->
<!ENTITY aboutSupport.appBasicsServiceWorkers "Service Workers registrate">

<!ENTITY aboutSupport.appBasicsProfiles "Profilos">

<!ENTITY aboutSupport.appBasicsMultiProcessSupport "Fenestras in multiprocesso">

<!ENTITY aboutSupport.appBasicsProcessCount "Processos del contento del web">

<!ENTITY aboutSupport.enterprisePolicies "Politicas de interprisa">

<!ENTITY aboutSupport.appBasicsKeyGoogle "Clave de Google">
<!ENTITY aboutSupport.appBasicsKeyMozilla "Clave del servicio de localisation de Mozilla">

<!ENTITY aboutSupport.appBasicsSafeMode "Modo secur">

<!ENTITY aboutSupport.showDir.label "Aperir le directorio">
<!-- LOCALIZATION NOTE (aboutSupport.showMac.label): This is the Mac-specific
variant of aboutSupport.showDir.label.  This allows us to use the preferred
"Finder" terminology on Mac. -->
<!ENTITY aboutSupport.showMac.label "Monstrar in Finder">
<!-- LOCALIZATION NOTE (aboutSupport.showWin2.label): This is the Windows-specific
variant of aboutSupport.showDir.label. -->
<!ENTITY aboutSupport.showWin2.label "Aperir le dossier">

<!ENTITY aboutSupport.modifiedKeyPrefsTitle "Preferentias importante modificate">
<!ENTITY aboutSupport.modifiedPrefsName "Nomine">
<!ENTITY aboutSupport.modifiedPrefsValue "Valor">

<!-- LOCALIZATION NOTE (aboutSupport.userJSTitle, aboutSupport.userJSDescription): user.js is the name of the preference override file being checked. -->
<!ENTITY aboutSupport.userJSTitle "Preferentias de user.js">
<!ENTITY aboutSupport.userJSDescription "Le dossier de tu profilo contine un <a id='prefs-user-js-link'>file user.js</a>, que include preferentias que non esseva create per &brandShortName;.">

<!ENTITY aboutSupport.lockedKeyPrefsTitle "Preferentias importante blocate">
<!ENTITY aboutSupport.lockedPrefsName "Nomine">
<!ENTITY aboutSupport.lockedPrefsValue "Valor">

<!ENTITY aboutSupport.graphicsTitle "Graphicos">

<!ENTITY aboutSupport.placeDatabaseTitle "Base de datos de locos">
<!ENTITY aboutSupport.placeDatabaseIntegrity "Integritate">
<!ENTITY aboutSupport.placeDatabaseVerifyIntegrity "Verificar le integritate">

<!ENTITY aboutSupport.jsTitle "JavaScript">
<!ENTITY aboutSupport.jsIncrementalGC "Collecta de immunditias incremental">

<!ENTITY aboutSupport.a11yTitle "Accessibilitate">
<!ENTITY aboutSupport.a11yActivated "Activate">
<!ENTITY aboutSupport.a11yForceDisabled "Impedir le accessibilitate">
<!ENTITY aboutSupport.a11yHandlerUsed "Es usate le gestor accessibile">
<!ENTITY aboutSupport.a11yInstantiator "Generator de accessibilitate">

<!ENTITY aboutSupport.libraryVersionsTitle "Versiones de bibliotheca">

<!ENTITY aboutSupport.installationHistoryTitle "Chronologia del installation">
<!ENTITY aboutSupport.updateHistoryTitle "Chronologia de actualisationes">

<!ENTITY aboutSupport.copyTextToClipboard.label "Copiar le texto in le planchetta">
<!ENTITY aboutSupport.copyRawDataToClipboard.label "Copiar le datos brute in le planchetta">

<!ENTITY aboutSupport.sandboxTitle "Cassa de arena">
<!ENTITY aboutSupport.sandboxSyscallLogTitle "Appellos de systema rejectate">
<!ENTITY aboutSupport.sandboxSyscallIndex "#">
<!ENTITY aboutSupport.sandboxSyscallAge "Secundas retro">
<!ENTITY aboutSupport.sandboxSyscallPID "PID">
<!ENTITY aboutSupport.sandboxSyscallTID "TID">
<!ENTITY aboutSupport.sandboxSyscallProcType "Typo de processo">
<!ENTITY aboutSupport.sandboxSyscallNumber "Syscall">
<!ENTITY aboutSupport.sandboxSyscallArgs "Argumentos">

<!ENTITY aboutSupport.safeModeTitle "Probar le modo secur">
<!ENTITY aboutSupport.restartInSafeMode.label "Reinitiar con le additivos inactive…">

<!ENTITY aboutSupport.graphicsFeaturesTitle "Functionalitates">
<!ENTITY aboutSupport.graphicsDiagnosticsTitle "Diagnostico">
<!ENTITY aboutSupport.graphicsFailureLogTitle "Registro de falta">
<!ENTITY aboutSupport.graphicsGPU1Title "GPU #1">
<!ENTITY aboutSupport.graphicsGPU2Title "GPU #2">
<!ENTITY aboutSupport.graphicsDecisionLogTitle "Registro de decision">
<!ENTITY aboutSupport.graphicsCrashGuardsTitle "Functionalitates inactive de protection contra collapso">
<!ENTITY aboutSupport.graphicsWorkaroundsTitle "Solutiones alternative">

<!ENTITY aboutSupport.mediaTitle "Media">
<!ENTITY aboutSupport.mediaOutputDevicesTitle "Apparatos de exita">
<!ENTITY aboutSupport.mediaInputDevicesTitle "Apparatos de entrata">
<!ENTITY aboutSupport.mediaDeviceName "Nomine">
<!ENTITY aboutSupport.mediaDeviceGroup "Gruppo">
<!ENTITY aboutSupport.mediaDeviceVendor "Venditor">
<!ENTITY aboutSupport.mediaDeviceState "Stato">
<!ENTITY aboutSupport.mediaDevicePreferred "Preferite">
<!ENTITY aboutSupport.mediaDeviceFormat "Formato">
<!ENTITY aboutSupport.mediaDeviceChannels "Canales">
<!ENTITY aboutSupport.mediaDeviceRate "Rata">
<!ENTITY aboutSupport.mediaDeviceLatency "Latentia">

<!ENTITY aboutSupport.intlTitle "Internationalisation &amp; Localisation">
<!ENTITY aboutSupport.intlAppTitle "Parametros del application">
<!ENTITY aboutSupport.intlLocalesRequested "Localisationes requirite">
<!ENTITY aboutSupport.intlLocalesAvailable "Localisationes disponibile">
<!ENTITY aboutSupport.intlLocalesSupported "Linguas del application">
<!ENTITY aboutSupport.intlLocalesDefault "Localisation predefinite">
<!ENTITY aboutSupport.intlOSTitle "Systema operative">
<!ENTITY aboutSupport.intlOSPrefsSystemLocales "Localisationes del systema">
<!ENTITY aboutSupport.intlRegionalPrefs "Preferentias regional">

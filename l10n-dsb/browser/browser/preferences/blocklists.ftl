# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Blokěrowańske lisćiny
    .style = width: 55em
blocklist-desc = Móžośo wubraś, kótaru lisćinu { -brand-short-name } ma wužywaś, aby webelementy blokěrował, kótarež by mógli wašu pśeglědowańsku aktiwitu slědowaś.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Lisćina
blocklist-button-cancel =
    .label = Pśetergnuś
    .accesskey = P
blocklist-button-ok =
    .label = Změny składowaś
    .accesskey = s
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Zakładny šćit Disconnect.me (Doporucony).
blocklist-item-moz-std-desc = Dowólujo někotare slědowarje tak až websydła pórědnje funkcioněruju.
blocklist-item-moz-full-name = Striktny šćit Disconnect.me.
blocklist-item-moz-full-desc = Blokěrujo znate pśeslědowaki. Někotare sedła snaź pórědnje njefunkcioněruju.

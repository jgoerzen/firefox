# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Exceptions
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-block =
    .label = Block
    .accesskey = B
permissions-session =
    .label = Allow for Session
    .accesskey = S
permissions-allow =
    .label = Allow
    .accesskey = A
permissions-status =
    .label = Status
permissions-button-cancel =
    .label = Cancel
    .accesskey = C
permissions-button-ok =
    .label = Save Changes
    .accesskey = S
permissions-capabilities-allow =
    .label = Allow
permissions-capabilities-block =
    .label = Block

## Invalid Hostname Dialog

permissions-invalid-uri-title = Invalid Hostname Entered
permissions-invalid-uri-label = Please enter a valid hostname

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = Exceptions - Tracking Protection
    .style = { permissions-window.style }

## Exceptions - Cookies


## Exceptions - Pop-ups

permissions-exceptions-popup-desc = You can specify which websites are allowed to open pop-up windows. Type the exact address of the site you want to allow and then click Allow.

## Exceptions - Saved Logins


## Exceptions - Add-ons

permissions-exceptions-addons-desc = You can specify which websites are allowed to install add-ons. Type the exact address of the site you want to allow and then click Allow.

## Site Permissions - Notifications


## Site Permissions - Location


## Site Permissions - Camera


## Site Permissions - Microphone


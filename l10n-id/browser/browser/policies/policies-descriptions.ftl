# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Atur URL pemutakhiran aplikasi khusus
policy-Authentication = Atur autentikasi terintegrasi untuk situs lain yang mendukung.
policy-BlockAboutAddons = Blokir akses ke Pengelola Pengaya (about:addons).
policy-BlockAboutConfig = Blokir akses ke laman about:config.
policy-BlockAboutProfiles = Blokir akses ke laman about:profiles.
policy-BlockAboutSupport = Blokir akses ke laman about:support.
policy-Bookmarks = Buat markah pada bilah alat Markah, menu Markah, atau folder tertentu yang ada di dalamnya.
policy-Cookies = Izinkan atau tolak situs untuk menyetel kuki.
policy-DisableAppUpdate = Cegah peramban untuk memperbarui.
policy-DisableBuiltinPDFViewer = Nonaktifkan PDF.js, penampil PDF bawaan di { -brand-short-name }.
policy-DisableDeveloperTools = Blokir akses ke alat pengembang.
policy-DisableFeedbackCommands = Nonaktifkan perintah untuk mengirim umpan balik dari menu Bantuan (Kirim Saran dan Laporkan Situs Tipuan).
policy-DisableFirefoxAccounts = Nonaktifkan layanan berbasis { -fxaccount-brand-name }, termasuk Sync.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Nonaktifkan fitur Firefox Screenshots.
policy-DisableFirefoxStudies = Cegah { -brand-short-name } menjalankan kajian.
policy-DisableForgetButton = Cegah akses ke tombol Lupakan.
policy-DisableFormHistory = Jangan ingat riwayat pencarian dan formulir.
policy-DisableMasterPasswordCreation = Jika ya, sandi utama tidak bisa dibuat.
policy-DisablePocket = Nonaktifkan fitur untuk menyimpan laman web ke Pocket.
policy-DisablePrivateBrowsing = Nonaktifkan Penjelajahan Pribadi.
policy-DisableProfileImport = Nonaktifkan perintah menu untuk mengimpor data dari peramban lainnya.
policy-DisableTelemetry = Nonaktifkan Telemetry.
policy-DisplayMenuBar = Tampilkan Bilah Menu secara otomatis.
policy-DontCheckDefaultBrowser = Nonaktifkan pemeriksaan untuk peramban bawaan saat memulai.
policy-FlashPlugin = Izinkan atau tolak penggunaan plugin Flash.
policy-InstallAddonsPermission = Izinkan situs tertentu untuk memasang pengaya.
policy-Permissions = Atur izin untuk kamera, mikrofon, lokasi, dan notifikasi.
policy-PopupBlocking = Izinkan situs tertentu untuk menampilkan pop-up secara otomatis.
policy-Proxy = Atur setelan proxy.
policy-SearchBar = Setel lokasi bawaan untuk bilah pencarian. Pengguna masih diizinkan untuk mengubahsuainya.

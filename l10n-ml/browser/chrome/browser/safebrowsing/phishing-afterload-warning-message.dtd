<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "മടങ്ങിപ്പോവുക">
<!ENTITY safeb.palm.seedetails.label "വിശദാംശങ്ങൾ കാണുക">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "ഇത് വഞ്ചനാപരമായ സൈറ്റല്ല…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "<a id='advisory_provider'/> നൽകിയ ഉപദേശങ്ങൾ.">


<!ENTITY safeb.blocked.malwarePage.title2 "ഈ വെബ്സൈറ്റ് സന്ദർശിക്കുന്നത് നിങ്ങളുടെ കമ്പ്യൂട്ടറിനെ കേടാക്കാനിടയുണ്ട്">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "നിങ്ങളുടെ കമ്പ്യൂട്ടറിൽ സ്വകാര്യ വിവരങ്ങൾ മോഷ്ടിക്കാനോ ഇല്ലാതാക്കാനോ ഇടയുള്ള ക്ഷുദ്ര സോഫ്റ്റ്‍വെയറുകള്‍ ഇൻസ്റ്റാൾ ചെയ്യാൻ ശ്രമിച്ചേക്കാം എന്നതിനാൽ &brandShortName; ഈ പേജിനെ തടഞ്ഞു.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->



<!ENTITY safeb.blocked.malwarePage.learnMore "വൈറസ്, മാൽവെയര്‍ തുടങ്ങിയ ദോഷകരമായ വെബ്സൈറ്റ് ഉള്ളടക്കങ്ങളെ കുറിച്ച് കൂടൂതൽ അറിയാനും അവയിൽ നിന്നും നിങ്ങളൂടെ കമ്പ്യൂട്ടറിനെ എങ്ങനെ സംരക്ഷിക്കാമെന്ന് അറിയാനും <a id='learn_more_link'>StopBadware.org</a> സന്ദര്‍ശിക്കുക. &brandShortName; ന്റെ ഫിഷിംഗ്, മാല്‍വെയര്‍ സംരക്ഷണത്തെക്കുറിച്ച് കൂടുതല്‍ അറിയാന്‍ <a id='firefox_support'>support.mozilla.org</a> സന്ദര്‍ശിക്കുക.">


<!ENTITY safeb.blocked.unwantedPage.title2 "മുന്നോട്ടുള്ള സൈറ്റിൽ ദോഷകരമായ പ്രോഗ്രാമുകൾ അടങ്ങിയിരിക്കാം">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; നിങ്ങളുടെ ബ്രൌസിംഗിനെ ബാധിക്കുന്ന പ്രോഗ്രാമുകൾ ഇൻസ്റ്റാള്‍ ചെയ്യുാന്‍ നോക്കി കബളിപ്പിക്കാൻ ശ്രമിക്കാനിടയുള്ളത് കൊണ്ടാണ് ഈ പേജ് തടഞ്ഞത് (ഉദാഹരണത്തിന്, നിങ്ങളുടെ ഹോംപേജ് മാറ്റുന്നതിലൂടെ അല്ലെങ്കിൽ നിങ്ങൾ സന്ദർശിക്കുന്ന സൈറ്റുകളിൽ അധിക പരസ്യങ്ങൾ കാണിക്കുന്നതിലൂടെ).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->


<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> ദോഷകരമായ സോഫ്റ്റവെയർ അടിങ്ങിയിരിക്കുന്നതായി <a id='error_desc_link'>റിപ്പോർട്ട് ചെയ്യപ്പെട്ടിരിക്കുന്നു</a>.">



<!ENTITY safeb.blocked.phishingPage.title3 "വഞ്ചിപ്പിക്കാവുന്ന സൈറ്റ്">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; ഈ പേജ് തടഞ്ഞു കാരണം ഇത് സോഫ്റ്റ്‍വെയറുകൾ ഇൻസ്റ്റാൾ ചെയ്യുന്നത് പോലെയോ അല്ലെങ്കിൽ പാസ്വേഡുകൾ അല്ലെങ്കിൽ ക്രെഡിറ്റ് കാർഡുകൾ പോലുള്ള വ്യക്തിഗത വിവരങ്ങൾ വെളിപ്പെടുത്തുന്നതോ പോലുള്ള അപകടകരമായ കാര്യങ്ങൾ ചെയ്യുന്നതിലൂടെ നിങ്ങളെ കബളിപ്പിച്ചേക്കാം.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->




<!ENTITY safeb.blocked.harmfulPage.title "മുന്നിലുള്ള സൈറ്റിൽ മാല്‍വെയർ അടങ്ങിയിരിക്കാം">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->



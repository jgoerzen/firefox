# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-search-textbox =
    .placeholder = വെബ്‍സൈറ്റുകള്‍ തിരയുക
    .accesskey = S
site-data-column-host =
    .label = സൈറ്റ്‌
site-data-column-cookies =
    .label = കുക്കികള്‍
site-data-column-storage =
    .label = സംഭരണം
site-data-column-last-used =
    .label = അവസാനം ഉപയോഗിച്ചത്
site-data-remove-selected =
    .label = തിരഞ്ഞെടുത്തവ നീക്കം ചെയ്യുക
    .accesskey = r
site-data-button-cancel =
    .label = റദ്ദാക്കുക
    .accesskey = C
site-data-button-save =
    .label = മാറ്റങ്ങള്‍ സൂക്ഷിക്കുക
    .accesskey = a
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = എല്ലാം നീക്കം ചെയ്യുക
    .accesskey = e
site-data-remove-shown =
    .label = കാണിച്ചെല്ലാം നീക്കംചെയ്യുക
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
site-data-removing-dialog =
    .title = { site-data-removing-header }
    .buttonlabelaccept = നീക്കംചെയ്യുക

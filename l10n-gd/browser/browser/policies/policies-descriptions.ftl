# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Suidhich URL gnàthaichte airson ùrachadh aplacaidean.
policy-Authentication = Rèitich dearbhadh amalaichte airson làraichean-lìn a chuireas taic ris.
policy-BlockAboutAddons = Bac inntrigeadh do mhanaidsear nan tuilleadan (about:addons).
policy-BlockAboutConfig = Bac inntrigeadh dhan duilleag about:config.
policy-BlockAboutProfiles = Bac inntrigeadh dhan duilleag about:profiles.
policy-BlockAboutSupport = Bac inntrigeadh dhan duilleag about:support.
policy-Bookmarks = Cruthaich comharran-lìn air bàr-inneal nan comharran-lìn, ann an clàr-taice nan comharran-lìn no ann am pasgan sònraichte ’nam broinn.
policy-Certificates = An dèid teisteanasan built-in a chleachdadh gus nach dèid. ’S ann air Windows a-mhàin a gheibhear am poileasaidh seo aig an àm seo.
policy-Cookies = Thoir cead do làraichean-lìn briosgaidean a shuidheachadh no bac iad.
policy-DisableAppUpdate = Bac ùrachadh a’ bhrabhsair.
policy-DisableBuiltinPDFViewer = Cuir PDF.js à comas, an sealladair PDF a tha am broinn { -brand-short-name } o thùs.
policy-DisableDeveloperTools = Bac inntrigeadh dha innealan an luchd-leasachaidh.
policy-DisableFeedbackCommands = Cuir à comas àitheantan a chuireadh beachdan o chlàr-taice na cobharach (“Cuir beachdan” agus “Dèan aithris air làrach amharasach”).
policy-DisableFirefoxAccounts = Cuir seirbheisean stèidhichte air { -fxaccount-brand-name } à comas, a’ gabhail a-staigh an t-sioncronachaidh.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Cuir gleus nan glacaidhean-sgrìn aig Firefox à comas.
policy-DisableFirefoxStudies = Na leig le { -brand-short-name } obair-rannsachaidh a ruith.
policy-DisableForgetButton = Na ceadaich cothrom air a’ phutan “Dìochuimhnich”.
policy-DisableFormHistory = Na cuimhnich eachdraidh nan lorg is nam foirmean.
policy-DisableMasterPasswordCreation = Ma thagh thu true, cha ghabh maighstir facail-fhaire a chruthachadh.
policy-DisablePocket = Cuir à comas an gleus a shàbhaileas duilleagan-lìn ann am Pocket.
policy-DisablePrivateBrowsing = Cuir am brabhsadh prìobhaideach à comas.

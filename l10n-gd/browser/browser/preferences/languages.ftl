# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Cànain
    .style = width: 35em
languages-close-key =
    .key = w
languages-description = Gheibhear làraichean-lìn ann an iomadh cànan uaireannan. Tagh na cànain a bu toigh leat fhaicinn air duilleagan-lìn, ann an òrdugh a-rèir do thoil fhèin
languages-customize-spoof-english =
    .label = Iarr tionndaidhean Beurla de làraichean-lìn airson prìobhaideachd a bharrachd
languages-customize-moveup =
    .label = Gluais suas
    .accesskey = u
languages-customize-movedown =
    .label = Gluais sìos
    .accesskey = G
languages-customize-remove =
    .label = Thoir air falbh
    .accesskey = r
languages-customize-select-language =
    .placeholder = Tagh cànan gus a chur ris…
languages-customize-add =
    .label = Cuir ris
    .accesskey = u
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

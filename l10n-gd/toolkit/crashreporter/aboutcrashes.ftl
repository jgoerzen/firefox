# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Aithisgean tuislidh
clear-all-reports-label = Thoir air falbh gach aithisg
delete-confirm-title = A bheil thu cinnteach?
delete-confirm-description = Sguabaidh seo às a h-uile aithisg is cha ghabh an aiseag.
crashes-unsubmitted-label = Aithisgean air tuislidhean nach deach an cur
id-heading = ID na h-aithisge
date-crashed-heading = Ceann-là an tuislidh
crashes-submitted-label = Aithisgean air tuislidhean coimpiutair a chaidh a chur
date-submitted-heading = Ceann-là a chaidh a chur
no-reports-label = Cha deach aithisg air tuisleadh a chur.
no-config-label = Cha deach an aplacaid seo a rèiteachadh gus aithisgean tuislidh a shealltainn. Feumaidh tu an roghainn <code>breakpad.reportURL</code> a shuidheachadh.

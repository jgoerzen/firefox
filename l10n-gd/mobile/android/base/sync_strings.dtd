<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Sioncronachadh Firefox">
<!ENTITY syncBrand.shortName.label "Sioncronachadh">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Dèan ceangal ri &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Gus an t-uidheam ùr agad a ghnìomhachadh, tagh “Suidhich &syncBrand.shortName.label;” air an uidheam.'>
<!ENTITY sync.subtitle.pair.label 'Gus a ghnìomhachadh, tagh “Paidhrich uidheam” air an dàrna uidheam agad.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Chan eil an t-uidheam agam an-seo…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Clàraidhean a-steach'>
<!ENTITY sync.configure.engines.title.history 'Eachdraidh'>
<!ENTITY sync.configure.engines.title.tabs 'Tabaichean'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; air &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Clàr-taice nan comharra-lìn'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Tagaichean'>
<!ENTITY bookmarks.folder.toolbar.label 'Bàr nan comharra-lìn'>
<!ENTITY bookmarks.folder.other.label 'Comharran-lìn eile'>
<!ENTITY bookmarks.folder.desktop.label 'Comharran-lìn an desktop'>
<!ENTITY bookmarks.folder.mobile.label 'Comharran-lìn an fhòn-làimhe'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Prìnichte'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Till gun bhrabhsadh'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Fàilte gu &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Clàraich a-steach gus na tabaichean, comharran-lìn, clàraidhean a-steach ⁊ mòran a bharrachd a shioncronachadh.'>
<!ENTITY fxaccount_getting_started_get_started 'Dèan toiseach tòiseachaidh'>
<!ENTITY fxaccount_getting_started_old_firefox 'A bheil thu a\&apos; cleachdadh seann-tionndadh de &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Frithealaiche a\&apos; chunntais'>
<!ENTITY fxaccount_status_sync_now 'Sioncronaich an-dràsta'>
<!ENTITY fxaccount_status_syncing2 '’Ga shioncronachadh…'>
<!ENTITY fxaccount_status_device_name 'Ainm an uidheim'>
<!ENTITY fxaccount_status_sync_server 'Frithealaiche an t-sioncronachaidh'>
<!ENTITY fxaccount_status_needs_verification2 'Feumaidh tu an cunntas agad a dhearbhadh. Thoir gnogag gus post-d dearbhaidh ath-chur.'>
<!ENTITY fxaccount_status_needs_credentials 'Cha ghabh ceangal a dhèanamh. Thoir gnogag gus clàradh a-steach.'>
<!ENTITY fxaccount_status_needs_upgrade 'Feumaidh tu &brandShortName; àrdachadh mus urrainn dhut clàradh a-steach.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled 'Chaidh &syncBrand.shortName.label; a shuidheachadh ach cha dèan e sioncronachadh gu fèin-obrachail. Toglaich “Auto-sync data” ann an Android Settings &gt; Data Usage.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 'Chaidh &syncBrand.shortName.label; a shuidheachadh ach cha dèan e sioncronachadh gu fèin-obrachail. Toglaich “Auto-sync data” ann an Android Settings &gt; Accounts.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Thoir gnogag airson clàradh a-steach dhan chunntas Firefox ùr agad.'>
<!ENTITY fxaccount_status_choose_what 'Tagh na tha thu airson sioncronachadh'>
<!ENTITY fxaccount_status_bookmarks 'Comharran-lìn'>
<!ENTITY fxaccount_status_history 'Eachdraidh'>
<!ENTITY fxaccount_status_passwords2 'Clàraidhean a-steach'>
<!ENTITY fxaccount_status_tabs 'Tabaichean fosgailte'>
<!ENTITY fxaccount_status_additional_settings 'Roghainnean a bharrachd'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sioncronaich air WiFi a-mhàin'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Na leig le &brandShortName; sioncronachadh air lìonra mobile no meadaraichte'>
<!ENTITY fxaccount_status_legal 'Nòtaichean laghail' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Teirmichean na seirbheise'>
<!ENTITY fxaccount_status_linkprivacy2 'An t-sanas prìobhaideachd'>
<!ENTITY fxaccount_remove_account 'Dì-cheangail&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'A bheil thu airson dì-cheangal on t-sioncronachadh?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Bidh an dàta brabhsaidh air an uidheam seo fhathast ach cha dèid a shioncronachadh leis a’ chunntas agad.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Chaidh an cunntas Firefox &formatS; a dhì-cheangal.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Dì-cheangail'>

<!ENTITY fxaccount_enable_debug_mode 'Cuir am modh dì-bhugachaidh an comas'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Roghainnean &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Rèitich &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 'Chan eil &syncBrand.shortName.label; ceangailte'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Thoirt gnogag gus clàradh a-steach mar &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'A bheil thu airson àrdachadh &syncBrand.shortName.label; a choileanadh?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Thoirt gnogag gus clàradh a-steach mar &formatS;'>

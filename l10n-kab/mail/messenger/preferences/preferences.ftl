# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Fren tutlayt yettwaseqdacen i uskan n wumuɣen, iznan, akked yilɣa seg { -brand-short-name }.
manage-messenger-languages-button =
    .label = Fren wayeḍ...
    .accesskey = F
confirm-messenger-language-change-description = Ales tanekra n { -brand-short-name } akken ad ddun ibeddilen-a
confirm-messenger-language-change-button = Snes sakin ales tanekra

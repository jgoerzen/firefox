# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Tibdarin n usewḥel
    .style = width: 55em
blocklist-desc = Tzemreḍ ad tferneḍ anta tabdart ara yesseqdec { -brand-short-name } akken ad isewḥel iferdisen Web i izemren ad sfuɣlen armud-inek n tunigin.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Tabdart
blocklist-button-cancel =
    .label = Sefsex
    .accesskey = S
blocklist-button-ok =
    .label = Sekles asnifel
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Ammesten azadur Disconnect.me (ihul).
blocklist-item-moz-std-desc = Sireg kra n imesfuɣal akken ismal web ad ddun akken iwata.
blocklist-item-moz-full-name = Ammesten ufrin Disconnect.me.
blocklist-item-moz-full-desc = Ad isewḥel imsfuɣalen irusinen. Kra n yismal zemren ur tteddun ara akken iwata.

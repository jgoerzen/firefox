<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox синхрондау">
<!ENTITY syncBrand.shortName.label "Синхрондау">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label; үшін байланысты орнату'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Жаңа құрылғыны баптау үшін ол құрылғыда «&syncBrand.shortName.label; баптау» таңдаңыз.'>
<!ENTITY sync.subtitle.pair.label 'Белсендіру үшін өзіңіздің басқа құрылғыңызда «Құрылғымен байланысу» таңдаңыз.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Ол құрылғы қазір менің қолымда емес…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Логиндер'>
<!ENTITY sync.configure.engines.title.history 'Тарихы'>
<!ENTITY sync.configure.engines.title.tabs 'Беттер'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1;, қайда: &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Бетбелгілер мәзірі'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Белгілер'>
<!ENTITY bookmarks.folder.toolbar.label 'Бетбелгілер панелі'>
<!ENTITY bookmarks.folder.other.label 'Басқа бетбелгілер'>
<!ENTITY bookmarks.folder.desktop.label 'Жұмыс үстел бетбелгілері'>
<!ENTITY bookmarks.folder.mobile.label 'Мобильді бетбелгілер'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Бекітілген'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Шолуға оралу'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label; ішіне қош келдіңіз'>
<!ENTITY fxaccount_getting_started_description2 'Беттер, бетбелгілер және логиндерді синхрондау үшін кіріңіз.'>
<!ENTITY fxaccount_getting_started_get_started 'Бастау'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label; ескі нұсқасын қолданудасыз ба?'>

<!ENTITY fxaccount_status_auth_server 'Тіркелгі сервері'>
<!ENTITY fxaccount_status_sync_now 'Қазір синхрондау'>
<!ENTITY fxaccount_status_syncing2 'Синхрондау…'>
<!ENTITY fxaccount_status_device_name 'Құрылғы аты'>
<!ENTITY fxaccount_status_sync_server 'Sync сервері'>
<!ENTITY fxaccount_status_needs_verification2 'Тіркелгіңізді растау керек. Растау эл. хатын қайта жіберу үшін шертіңіз.'>
<!ENTITY fxaccount_status_needs_credentials 'Байланысу мүмкін емес. Кіру үшін шертіңіз.'>
<!ENTITY fxaccount_status_needs_upgrade 'Кіру үшін сізге &brandShortName; жаңарту керек.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; бапталған, бірақ, автоматты түрде синхрондалмайды. Android баптаулары &gt; Деректерді қолдану ішінде \&quot;Деректерді автосинхрондауды\&quot; іске қосыңыз.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; бапталған, бірақ, автоматты түрде синхрондалмайды. Android баптаулары &gt; Тіркелгілер ішінде \&quot;Деректерді автосинхрондауды\&quot; іске қосыңыз.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Сіздің жаңа Firefox тіркелгісіне кіру үшін шертіңіз.'>
<!ENTITY fxaccount_status_choose_what 'Нені синхрондауды таңдаңыз'>
<!ENTITY fxaccount_status_bookmarks 'Бетбелгілер'>
<!ENTITY fxaccount_status_history 'Тарихы'>
<!ENTITY fxaccount_status_passwords2 'Логиндер'>
<!ENTITY fxaccount_status_tabs 'Ашық беттер'>
<!ENTITY fxaccount_status_additional_settings 'Қосымша баптаулар'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Тек Wi-Fi арқылы синхрондау'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 '&brandShortName; үшін ұялы немесе өлшенетін желі арқылы синхрондауға тыйым салу'>
<!ENTITY fxaccount_status_legal 'Құқықтық ақпарат' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Қолдану шарттары'>
<!ENTITY fxaccount_status_linkprivacy2 'Жекелік саясаты'>
<!ENTITY fxaccount_remove_account 'Байланысты үзу&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Синхрондаумен байланысты үзу керек пе?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Сіздің шолу деректеріңіз осы құрылғыда қала береді, бірақ, енді тіркелгіңізбен синхрондалмайтын болады.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 '&formatS; Firefox тіркелгісі ажыратылды.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Байланысты үзу'>

<!ENTITY fxaccount_enable_debug_mode 'Жөндеу режимін іске қосу'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; опциялары'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; баптау'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; желіге байланыспаған'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; ретінде кіру үшін шертіңіз'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label; жаңартуды аяқтау керек пе?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; ретінде кіру үшін шертіңіз'>

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Блоктізімдер
    .style = width: 55em
blocklist-desc = { -brand-short-name } сіздің шолу белсенділігіңізді бақылай алатын веб элементтерді блоктау үшін қай тізімді қолдану керектігін таңдай аласыз.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Тізім
blocklist-button-cancel =
    .label = Бас тарту
    .accesskey = а
blocklist-button-ok =
    .label = Өзгерістерді сақтау
    .accesskey = с
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me қарапайым қорғанысы (ұсынылады).
blocklist-item-moz-std-desc = Веб сайттар дұрыс жұмыс істейтіндей кейбір бақылаушыларды рұқсат етеді.
blocklist-item-moz-full-name = Disconnect.me қатаң қорғанысы.
blocklist-item-moz-full-desc = Белгілі трекерлерді блоктайды. Кейбір вебсайттар дұрыс жасамауы мүмкін.

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Жалпы">
<!ENTITY dataChoicesTab.label    "Деректер таңдауы">
<!ENTITY itemUpdate.label        "Жаңартулар">
<!ENTITY itemNetworking.label    "Желі және дискілік орын">
<!ENTITY itemCertificates.label  "Сертификаттар">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Глобалды іздеуді және индекстеуді іске қосу">
<!ENTITY enableGlodaSearch.accesskey   "е">
<!ENTITY dateTimeFormatting.label      "Күн және уақытты пішімдеу">
<!ENTITY languageSelector.label        "Тіл">
<!ENTITY allowHWAccel.label            "Қолжетерлік болса құрылғылық үдетуді қолдану">
<!ENTITY allowHWAccel.accesskey        "р">
<!ENTITY storeType.label               "Жаңа тіркелгілер үшін хабарламаларды сақтау түрі:">
<!ENTITY storeType.accesskey           "т">
<!ENTITY mboxStore2.label              "Бума үшін файл (mbox)">
<!ENTITY maildirStore.label            "Хабарлама үшін файл (maildir)">

<!ENTITY scrolling.label               "Айналдыру">
<!ENTITY useAutoScroll.label           "Автоматты айналдыруды қолдану">
<!ENTITY useAutoScroll.accesskey       "в">
<!ENTITY useSmoothScrolling.label      "Байсалды айналдыруды қолдану">
<!ENTITY useSmoothScrolling.accesskey  "й">

<!ENTITY systemIntegration.label       "Жүйелік интеграция">
<!ENTITY alwaysCheckDefault.label      "Әр қосылған кезде &brandShortName; жүйедегі негізгі пошта клиенті екенін тексеру">
<!ENTITY alwaysCheckDefault.accesskey  "A">
<!ENTITY searchIntegration.label       "&searchIntegration.engineName; үшін хабарламалардан іздеуді рұқсат ету">
<!ENTITY searchIntegration.accesskey   "з">
<!ENTITY checkDefaultsNow.label        "Қазір тексеру…">
<!ENTITY checkDefaultsNow.accesskey    "з">
<!ENTITY configEditDesc.label          "Кеңейтілген баптаулар">
<!ENTITY configEdit.label              "Баптаулар түзеткіші…">
<!ENTITY configEdit.accesskey          "Б">
<!ENTITY returnReceiptsInfo.label      "&brandShortName; алу есептемелерін қалай өңдейтінін таңдаңыз">
<!ENTITY showReturnReceipts.label      "Алу есептемелері…">
<!ENTITY showReturnReceipts.accesskey  "р">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Телеметрия">
<!ENTITY telemetryDesc.label             "Эл. пошта клиентіңіздің өнімділік, қолдану, құрылғылар және баптаулар мәліметін &vendorShortName; үшін жібереді, және бізге &brandShortName; жақсартуға көмектеседі">
<!ENTITY enableTelemetry.label           "Телеметрияны іске қосу">
<!ENTITY enableTelemetry.accesskey       "Т">
<!ENTITY telemetryLearnMore.label        "Көбірек білу">

<!ENTITY crashReporterSection.label      "Құлаулар хабаршысы">
<!ENTITY crashReporterDesc.label         "&brandShortName; эл. пошта клиентіңіздің тұрақтылық және қауіпсіздігін арттыру үшін құлау жөніндегі хабарламаларды &vendorShortName; адресіне жібереді">
<!ENTITY enableCrashReporter.label       "Құлаулар хабарлаушысын іске қосу">
<!ENTITY enableCrashReporter.accesskey   "с">
<!ENTITY crashReporterLearnMore.label    "Көбірек білу">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "&brandShortName; жаңартулары">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Нұсқасы ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAuto.label                "Жаңартуларды автоматты түрде орнату (ұсынылады: қауіпсіздікті арттырады)">
<!ENTITY updateAuto.accesskey            "а">
<!ENTITY updateCheck.label               "Жаңартуларды тексеру, бірақ орнату уақытын өзім тандаймын">
<!ENTITY updateCheck.accesskey           "м">
<!ENTITY updateManual.label              "Жаңартуларды тексермеу (ұсынылмайды: қауіпсіздік төмендейді)">
<!ENTITY updateManual.accesskey          "р">
<!ENTITY updateHistory.label             "Жаңартулар тарихын көрсету">
<!ENTITY updateHistory.accesskey         "р">

<!ENTITY useService.label                "Жаңартуларды орнату үшін фон қызметін қолдану">
<!ENTITY useService.accesskey            "ф">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Баптаулар…">
<!ENTITY showSettings.accesskey        "а">
<!ENTITY proxiesConfigure.label        "&brandShortName; Интернетке байланысу параметрлерін баптау">
<!ENTITY connectionsInfo.caption       "Байланыс">
<!ENTITY offlineInfo.caption           "Желіде емес">
<!ENTITY offlineInfo.label             "Желіден тыс баптаулары">
<!ENTITY showOffline.label             "Желіден тыс…">
<!ENTITY showOffline.accesskey         "ы">

<!ENTITY Diskspace                       "Дискідегі орын">
<!ENTITY offlineCompactFolders.label     "Барлық бумаларды ықшамдау, егер ол сақтаса">
<!ENTITY offlineCompactFolders.accesskey "ы">
<!ENTITY offlineCompactFoldersMB.label   "МБ жалпы">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Дейін қолдану">
<!ENTITY useCacheBefore.accesskey        "о">
<!ENTITY useCacheAfter.label             "МБ орын кэш үшін">
<!ENTITY overrideSmartCacheSize.label    "Кэшті автобасқаруды елемеу">
<!ENTITY overrideSmartCacheSize.accesskey "м">
<!ENTITY clearCacheNow.label             "Қазір өшіру">
<!ENTITY clearCacheNow.accesskey         "з">

<!-- Certificates -->
<!ENTITY certSelection.description       "Егерде сервер менің жеке сертификатымды сұраса:">
<!ENTITY certs.auto                      "Мені сұрамай-ақ жіберу">
<!ENTITY certs.auto.accesskey            "с">
<!ENTITY certs.ask                       "Әрбір ретте менен сұрау">
<!ENTITY certs.ask.accesskey             "а">
<!ENTITY enableOCSP.label                "OCSP жауап беруші серверлерін сертификаттардың ағымдағы дұрыстығы жөнінде сұрау">
<!ENTITY enableOCSP.accesskey            "с">

<!ENTITY manageCertificates.label "Сертификаттарды басқару">
<!ENTITY manageCertificates.accesskey "б">
<!ENTITY viewSecurityDevices.label "Қорғаныс жабдықтары">
<!ENTITY viewSecurityDevices.accesskey "ж">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Жоғары жылжыту
    .accesskey = ы
languages-customize-movedown =
    .label = Төмен жылжыту
    .accesskey = е
languages-customize-remove =
    .label = Өшіру
    .accesskey = р
languages-customize-select-language =
    .placeholder = Қосылатын тілді таңдау…
languages-customize-add =
    .label = Қосу
    .accesskey = о
messenger-languages-window =
    .title = { -brand-short-name } тіл баптаулары
    .style = width: 40em
messenger-languages-description = { -brand-short-name } бірінші тілді сіздің негізгі тілі ретінде көрсетеді, және қосымша тілдерді керек болған кезде олардың пайда болу ретімен көрсететін болады.

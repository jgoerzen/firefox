# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Pilih bahasa yang digunakan untuk memaparkan menu, mesej dan notifikasi { -brand-short-name }.
manage-messenger-languages-button =
    .label = Tetapkan Alternatif...
    .accesskey = T
confirm-messenger-language-change-description = Mula semula { -brand-short-name } untuk melaksanakan perubahan ini
confirm-messenger-language-change-button = Terap dan Mula semula

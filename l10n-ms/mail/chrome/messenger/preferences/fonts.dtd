<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  fontsAndEncodingsDialog.title           "Fon &amp; Pengekodan">

<!ENTITY  language.label                          "Fon untuk:">
<!ENTITY  language.accesskey                      "t">

<!ENTITY  size.label                              "Saiz:">
<!ENTITY  sizeProportional.accesskey              "z">
<!ENTITY  sizeMonospace.accesskey                 "i">

<!ENTITY  proportional.label                      "Berkadaran:">
<!ENTITY  proportional.accesskey                  "B">

<!ENTITY  serif.label                             "Serif:">
<!ENTITY  serif.accesskey                         "S">
<!ENTITY  sans-serif.label                        "Sans-serif:">
<!ENTITY  sans-serif.accesskey                    "n">
<!ENTITY  monospace.label                         "Monospace:">
<!ENTITY  monospace.accesskey                     "M">

<!-- LOCALIZATION NOTE (font.langGroup.latin) :
     Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language. -->
<!ENTITY  font.langGroup.latin                    "Latin">
<!ENTITY  font.langGroup.japanese                 "Japanese">
<!ENTITY  font.langGroup.trad-chinese             "Traditional Chinese (Taiwan)">
<!ENTITY  font.langGroup.simpl-chinese            "Simplified Chinese">
<!ENTITY  font.langGroup.trad-chinese-hk          "Traditional Chinese (Hong Kong)">
<!ENTITY  font.langGroup.korean                   "Korean">
<!ENTITY  font.langGroup.cyrillic                 "Cyrillic">
<!ENTITY  font.langGroup.el                       "Greek">
<!ENTITY  font.langGroup.other                    "Sistem Penulisan Lain">
<!ENTITY  font.langGroup.thai                     "Thai">
<!ENTITY  font.langGroup.hebrew                   "Hebrew">
<!ENTITY  font.langGroup.arabic                   "Arabic">
<!ENTITY  font.langGroup.devanagari               "Devanagari">
<!ENTITY  font.langGroup.tamil                    "Tamil">
<!ENTITY  font.langGroup.armenian                 "Armenian">
<!ENTITY  font.langGroup.bengali                  "Bengali">
<!ENTITY  font.langGroup.canadian                 "Unified Canadian Syllabary">
<!ENTITY  font.langGroup.ethiopic                 "Ethiopic">
<!ENTITY  font.langGroup.georgian                 "Georgian">
<!ENTITY  font.langGroup.gujarati                 "Gujarati">
<!ENTITY  font.langGroup.gurmukhi                 "Gurmukhi">
<!ENTITY  font.langGroup.khmer                    "Khmer">
<!ENTITY  font.langGroup.malayalam                "Malayalam">
<!ENTITY  font.langGroup.math                     "Mathematics">
<!ENTITY  font.langGroup.odia                     "Odia">
<!ENTITY  font.langGroup.telugu                   "Telugu">
<!ENTITY  font.langGroup.kannada                  "Kannada">
<!ENTITY  font.langGroup.sinhala                  "Sinhala">
<!ENTITY  font.langGroup.tibetan                  "Tibetan">
<!-- Minimum font size -->
<!ENTITY minSize.label                            "Saiz fon minimum:">
<!ENTITY minSize.accesskey                        "z">
<!ENTITY minSize.none                             "Tiada">

<!-- default font type -->
<!ENTITY  useDefaultFontSerif.label               "Serif">
<!ENTITY  useDefaultFontSansSerif.label           "Sans Serif">

<!-- fonts in message -->
<!ENTITY  fontControl.label                       "Kawalan Fon">
<!ENTITY  useFixedWidthForPlainText.label         "Guna fon lebar tetap untuk mesej teks biasa">
<!ENTITY  fixedWidth.accesskey                    "t">
<!ENTITY  useDocumentFonts.label                  "Izinkan mesej menggunakan fon lain">
<!ENTITY  useDocumentFonts.accesskey              "o">

<!-- Language settings -->
<!ENTITY sendDefaultCharset.label         "Mel Keluar:">
<!ENTITY sendDefaultCharset.accesskey     "u">
<!ENTITY languagesTitle2.label            "Pengekodan Teks">
<!ENTITY composingDescription2.label      "Tetapkan pengekodan teks piawai untuk menghantar dan menerima mel">

<!ENTITY viewDefaultCharsetList.label     "Mel Masuk:">
<!ENTITY viewDefaultCharsetList.accesskey  "M">
<!ENTITY replyInDefaultCharset3.label     "Jika boleh, guna pengekodan teks piawai apabila membalas">
<!ENTITY replyInDefaultCharset3.accesskey "h">

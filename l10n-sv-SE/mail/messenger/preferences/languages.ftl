# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Flytta upp
    .accesskey = u
languages-customize-movedown =
    .label = Flytta ner
    .accesskey = n
languages-customize-remove =
    .label = Ta bort
    .accesskey = T
languages-customize-select-language =
    .placeholder = Välj ett språk att lägga till…
languages-customize-add =
    .label = Lägg till
    .accesskey = L
messenger-languages-window =
    .title = { -brand-short-name } Språkinställningar
    .style = width: 40em
messenger-languages-description = { -brand-short-name } visar första språket som standard och kommer att visa alternativa språk om det behövs i den ordning de tillkommer.

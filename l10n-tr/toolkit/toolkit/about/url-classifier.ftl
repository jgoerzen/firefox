# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

url-classifier-title = URL sınıflandırıcısı bilgileri
url-classifier-provider-title = Sağlayıcı
url-classifier-provider = Sağlayıcı
url-classifier-provider-last-update-time = Son güncelleme saati
url-classifier-provider-next-update-time = Sonraki güncelleme saati
url-classifier-provider-back-off-time = Geri çekme zamanı
url-classifier-provider-last-update-status = Son güncelleme durumu
url-classifier-provider-update-btn = Güncelle
url-classifier-cache-title = Önbellek
url-classifier-cache-refresh-btn = Tazele
url-classifier-cache-clear-btn = Temizle
url-classifier-cache-table-name = Tablo adı
url-classifier-cache-ncache-entries = Negatif önbellek girdisi sayısı
url-classifier-cache-pcache-entries = Pozitif önbellek girdisi sayısı
url-classifier-cache-show-entries = Girdileri göster
url-classifier-cache-entries = Önbellek girdileri
url-classifier-cache-prefix = Ön ek
url-classifier-cache-ncache-expiry = Negatif önbellek son kullanma
url-classifier-cache-fullhash = Tam hash
url-classifier-cache-pcache-expiry = Pozitif önbellek son kullanma
url-classifier-debug-title = Hata ayıklama
url-classifier-debug-module-btn = Log modüllerini ayarla
url-classifier-debug-file-btn = Log doyasını ayarla
url-classifier-debug-js-log-chk = JS logunu ayarla
url-classifier-debug-sb-modules = Safe Browsing log modülleri
url-classifier-debug-modules = Geçerli log modülleri
url-classifier-debug-sbjs-modules = Safe Browsing JS logu
url-classifier-debug-file = Geçerli log dosyası
url-classifier-trigger-update = Güncellemeyi tetikle
url-classifier-not-available = Yok
url-classifier-disable-sbjs-log = Safe Browsing JS logunu kapat
url-classifier-enable-sbjs-log = Safe Browsing JS logunu etkinleştir
url-classifier-enabled = Etkin
url-classifier-disabled = Devre dışı
url-classifier-updating = güncelleniyor
url-classifier-cannot-update = güncellenemedi
url-classifier-success = başarılı
url-classifier-update-error = güncelleme hatası ({ $error })
url-classifier-download-error = indirme hatası ({ $error })

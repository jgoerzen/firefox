# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Kokeile nyt
onboarding-welcome-header = Tervetuloa { -brand-short-name }iin
onboarding-start-browsing-button-label = Aloita selaaminen

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Yksityinen selaus
onboarding-private-browsing-text = Selaa itseksesi. Yksityinen selaus varustettuna sisällön estotoiminnolla estää verkkoseuraimet, jotka seuraavat liikkeitäsi verkossa.
onboarding-screenshots-title = Kuvakaappaukset
onboarding-screenshots-text = Ota, tallenna ja jaa kuvakaappauksia - poistumatta { -brand-short-name }ista. Ota kaappaus sivun tietystä alueesta tai koko sivusta. Tallenna kuvakaappaus verkkoon, jolloin se on vaivatta käytettävissä ja jaettavissa.
onboarding-addons-title = Lisäosat
onboarding-addons-text = Lisää ominaisuuksia, joiden myötä { -brand-short-name } soveltuu tarpeisiisi entistä paremmin. Vertaa hintoja, tarkista sääennuste tai ilmaise itseäsi mukautetun teeman avulla.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Selaa nopeammin, älykkäämmin tai turvallisemmin laajennusten avulla; esimerkkinä Ghostery, joka mahdollistaa ärsyttävien mainosten estämisen.

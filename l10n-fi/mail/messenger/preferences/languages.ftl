# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Siirry ylös
    .accesskey = y
languages-customize-movedown =
    .label = Siirry alas
    .accesskey = a
languages-customize-remove =
    .label = Poista
    .accesskey = P
languages-customize-select-language =
    .placeholder = Valitse lisättävä kieli...
languages-customize-add =
    .label = Lisää
    .accesskey = L
messenger-languages-window =
    .title = { -brand-short-name }-kieliasetukset
    .style = width: 40em
messenger-languages-description = { -brand-short-name } näyttää ensimmäisen kielen oletuskielenä ja tarvittaessa muut kielet siinä järjestyksessä kuin ne ilmenevät.

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Yhdistä &syncBrand.shortName.label;-palveluun'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Aktivoi uusi laitteesi valitsemalla laitteessa “Määritä &syncBrand.shortName.label;”-palvelun asetukset.'>
<!ENTITY sync.subtitle.pair.label 'Aktivoi valitsemalla “Liitä laite” -valinta toisella laitteellasi.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Minulla ei ole laitetta vieressäni…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Kirjautumistiedot'>
<!ENTITY sync.configure.engines.title.history 'Sivuhistoria'>
<!ENTITY sync.configure.engines.title.tabs 'Välilehdet'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; laitteella &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Kirjanmerkkivalikko'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Avainsanat'>
<!ENTITY bookmarks.folder.toolbar.label 'Kirjanmerkkipalkki'>
<!ENTITY bookmarks.folder.other.label 'Muut kirjanmerkit'>
<!ENTITY bookmarks.folder.desktop.label 'Työpöydän kirjanmerkit'>
<!ENTITY bookmarks.folder.mobile.label 'Kannettavan laitteen kirjanmerkit'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Kiinnitetyt'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Palaa selaamisen pariin'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Tervetuloa &syncBrand.shortName.label;-palveluun'>
<!ENTITY fxaccount_getting_started_description2 'Kirjaudu sisään synkronoidaksesi välilehdet, kirjanmerkit ja kirjautumistiedot.'>
<!ENTITY fxaccount_getting_started_get_started 'Aloitetaan'>
<!ENTITY fxaccount_getting_started_old_firefox 'Käytätkö vanhempaa versiota &syncBrand.shortName.label;-palvelusta?'>

<!ENTITY fxaccount_status_auth_server 'Tilin palvelin'>
<!ENTITY fxaccount_status_sync_now 'Synkronoi heti'>
<!ENTITY fxaccount_status_syncing2 'Synkronoidaan…'>
<!ENTITY fxaccount_status_device_name 'Laitteen nimi'>
<!ENTITY fxaccount_status_sync_server 'Synkronointipalvelin'>
<!ENTITY fxaccount_status_needs_verification2 'Tilisi täytyy vahvistaa. Kosketa lähettääksesi vahvistussähköpostin.'>
<!ENTITY fxaccount_status_needs_credentials 'Ei voida muodostaa yhteyttä. Kosketa kirjautuaksesi sisään.'>
<!ENTITY fxaccount_status_needs_upgrade '&brandShortName; täytyy päivittää, jotta voit kirjautua sisään.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label;-palvelu on määritelty, mutta ei synkronoi tietoja automaattisesti. Aseta päälle “Synkronoi tiedot automaattisesti” Androidin asetuksista kohdasta Tietojen käyttö.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label;-palvelu on määritelty, mutta ei synkronoi tietoja automaattisesti. Aseta päälle “Synkronoi tiedot automaattisesti” valikosta Android-asetukset &gt; Tilit.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Kosketa kirjautuaksesi uudelle Firefox-tilillesi.'>
<!ENTITY fxaccount_status_choose_what 'Valitse, mitä synkronoidaan'>
<!ENTITY fxaccount_status_bookmarks 'Kirjanmerkit'>
<!ENTITY fxaccount_status_history 'Sivuhistoria'>
<!ENTITY fxaccount_status_passwords2 'Kirjautumistiedot'>
<!ENTITY fxaccount_status_tabs 'Avoimet välilehdet'>
<!ENTITY fxaccount_status_additional_settings 'Lisäasetukset'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Synkronoi vain Wi-Fi-yhteydellä'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Estä &brandShortName; synkronoimasta käytettäessä mobiili- tai käytön mukaan laskutettavaa verkkoa'>
<!ENTITY fxaccount_status_legal 'Juridiset asiat' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Käyttöehdot'>
<!ENTITY fxaccount_status_linkprivacy2 'Tietosuojakäytäntö'>
<!ENTITY fxaccount_remove_account 'Poista&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Katkaistaanko yhteys Sync-palveluun?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Selaustietosi säilyvät tällä laitteella, mutta niitä ei enää synkronoida tilisi kanssa.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Yhteys Firefox-tiliin &formatS; katkaistu.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Katkaise yhteys'>

<!ENTITY fxaccount_enable_debug_mode 'Ota debug-tila käyttöön'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label;-palvelun asetukset'>
<!ENTITY fxaccount_options_configure_title 'Määritä &syncBrand.shortName.label;-palvelu'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label;-palveluun ei ole yhteyttä'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Kosketa kirjautuaksesi tilille &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Viimeistelläänkö &syncBrand.shortName.label;-palvelun päivitys?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Kirjaudu tilille &formatS;'>

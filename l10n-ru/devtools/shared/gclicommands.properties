# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside Web Console commands.
# The Web Console command line is available from the Web Developer sub-menu
# -> 'Web Console'.
#
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (helpDesc) A very short string used to describe the
# function of the help command.
helpDesc=Справка по доступным командам

# LOCALIZATION NOTE (helpAvailable) Used in the output of the help command to
# explain the contents of the command help table.

# LOCALIZATION NOTE (notAvailableInE10S) Used in the output of any command that
# is not compatible with multiprocess mode (E10S).

# LOCALIZATION NOTE (consoleDesc) A very short string used to describe the
# function of the console command.
consoleDesc=Команды управления консолью

# LOCALIZATION NOTE (consoleManual) A longer description describing the
# set of commands that control the console.
consoleManual=Фильтрация, очистка и закрытие веб-консоли

# LOCALIZATION NOTE (consoleclearDesc) A very short string used to describe the
# function of the 'console clear' command.
consoleclearDesc=Очистить консоль

# LOCALIZATION NOTE (screenshotDesc) A very short description of the
# 'screenshot' command. See screenshotManual for a fuller description of what
# it does. This string is designed to be shown in a menu alongside the
# command name, which is why it should be as short as possible.
screenshotDesc=Сделать скриншот страницы

# LOCALIZATION NOTE (screenshotManual) A fuller description of the 'screenshot'
# command, displayed when the user asks for help on what it does.
screenshotManual=Сохранить скриншот всего видимого окна в формате PNG (опционально после задержки)

# LOCALIZATION NOTE (screenshotFilenameDesc) A very short string to describe
# the 'filename' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotFilenameDesc=Имя файла для сохранения

# LOCALIZATION NOTE (screenshotFilenameManual) A fuller description of the
# 'filename' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotFilenameManual=Имя файла (должно иметь расширение «.png»), в который мы запишем скриншот.

# LOCALIZATION NOTE (screenshotClipboardDesc) A very short string to describe
# the 'clipboard' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotClipboardDesc=Скопировать скриншот в буфер обмена? (true/false)

# LOCALIZATION NOTE (screenshotClipboardManual) A fuller description of the
# 'clipboard' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotClipboardManual=True, если вы хотите скопировать скриншот, а не сохранить его в файл.

# LOCALIZATION NOTE (screenshotGroupOptions) A label for the optional options of
# the screenshot command.
screenshotGroupOptions=Настройки

# LOCALIZATION NOTE (screenshotDelayDesc) A very short string to describe
# the 'delay' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotDelayDesc=Задержка (секунды)

# LOCALIZATION NOTE (screenshotDelayManual) A fuller description of the
# 'delay' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotDelayManual=Время задержки (в секундах) перед тем, как будет сделан скриншот

# LOCALIZATION NOTE (screenshotDPRDesc) A very short string to describe
# the 'dpr' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotDPRDesc=Пиксельное соотношение устройства

# LOCALIZATION NOTE (screenshotDPRManual) A fuller description of the
# 'dpr' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotDPRManual=Пиксельное соотношение устройства, используемое при снятии скриншота

# LOCALIZATION NOTE (screenshotFullPageDesc) A very short string to describe
# the 'fullpage' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotFullPageDesc=Всю веб-страницу? (true/false)

# LOCALIZATION NOTE (screenshotFullPageManual) A fuller description of the
# 'fullpage' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotFullPageManual=True, если скриншот также должен включать в себя части веб-страницы, находящиеся за пределами видимой области.

# LOCALIZATION NOTE (screenshotFileDesc) A very short string to describe
# the 'file' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotFileDesc=Сохранить в файл? (true/false)

# LOCALIZATION NOTE (screenshotFileManual) A fuller description of the
# 'file' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotFileManual=True, если скриншот должен быть сохранён в файл, даже если включены другие настройки (например буфер обмена).

# LOCALIZATION NOTE (screenshotGeneratedFilename) The auto generated filename
# when no file name is provided. The first argument (%1$S) is the date string
# in yyyy-mm-dd format and the second argument (%2$S) is the time string
# in HH.MM.SS format. Please don't add the extension here.
screenshotGeneratedFilename=Скриншот сделанный %1$S в %2$S

# LOCALIZATION NOTE (screenshotErrorSavingToFile) Text displayed to user upon
# encountering error while saving the screenshot to the file specified.
screenshotErrorSavingToFile=Ошибка сохранения в

# LOCALIZATION NOTE (screenshotSavedToFile) Text displayed to user when the
# screenshot is successfully saved to the file specified.
screenshotSavedToFile=Сохранён в

# LOCALIZATION NOTE (screenshotErrorCopying) Text displayed to user upon
# encountering error while copying the screenshot to clipboard.
screenshotErrorCopying=Ошибка при копировании в буфер обмена.

# LOCALIZATION NOTE (screenshotCopied) Text displayed to user when the
# screenshot is successfully copied to the clipboard.
screenshotCopied=Скопирован в буфер обмена.

# LOCALIZATION NOTE (screenshotTooltipPage) Text displayed as tooltip for screenshot button in devtools ToolBox.
screenshotTooltipPage=Сделать скриншот всей страницы

# LOCALIZATION NOTE (screenshotImgurDesc) A very short string to describe
# the 'imgur' parameter to the 'screenshot' command, which is displayed in
# a dialog when the user is using this command.
screenshotImgurDesc=Выгрузить на imgur.com

# LOCALIZATION NOTE (screenshotImgurManual) A fuller description of the
# 'imgur' parameter to the 'screenshot' command, displayed when the user
# asks for help on what it does.
screenshotImgurManual=Используйте, если вы хотите выгрузить на imgur.com вместо сохранения на диск

# LOCALIZATION NOTE (screenshotImgurError) Text displayed to user upon
# encountering error while uploading the screenshot to imgur.com.
screenshotImgurError=Не удалось связаться с imgur API

# LOCALIZATION NOTE (screenshotImgurUploaded) Text displayed to user when the
# screenshot is successfully sent to Imgur but the program is waiting on a response.
# The argument (%1$S) is a new image URL at Imgur.
screenshotImgurUploaded=Выгружено на %1$S

# LOCALIZATION NOTE (inspectDesc) A very short description of the 'inspect'
# command. See inspectManual for a fuller description of what it does. This
# string is designed to be shown in a menu alongside the command name, which
# is why it should be as short as possible.
inspectDesc=Исследовать узел

# LOCALIZATION NOTE (inspectManual) A fuller description of the 'inspect'
# command, displayed when the user asks for help on what it does.
inspectManual=Исследовать размеры и свойства элемента, используя CSS-селектор для открытия инструмента подсветки DOM

# LOCALIZATION NOTE (inspectNodeDesc) A very short string to describe the
# 'node' parameter to the 'inspect' command, which is displayed in a dialog
# when the user is using this command.
inspectNodeDesc=CSS-селектор

# LOCALIZATION NOTE (inspectNodeManual) A fuller description of the 'node'
# parameter to the 'inspect' command, displayed when the user asks for help
# on what it does.
inspectNodeManual=CSS-селектор, используемый с document.querySelector, который идентифицирует одиночный элемент

# LOCALIZATION NOTE (eyedropperDesc) A very short description of the 'eyedropper'
# command. See eyedropperManual for a fuller description of what it does. This
# string is designed to be shown in a menu alongside the command name, which
# is why it should be as short as possible.
eyedropperDesc=Захватить цвет со страницы

# LOCALIZATION NOTE (eyedropperManual) A fuller description of the 'eyedropper'
# command, displayed when the user asks for help on what it does.
eyedropperManual=Открывает панель, увеличивающую область страницы для исследования пикселей и копирования кодов цвета

# LOCALIZATION NOTE (debuggerClosed) Used in the output of several commands
# to explain that the debugger must be opened first.
debuggerClosed=Перед использованием этой команды необходимо открыть отладчик

# LOCALIZATION NOTE (debuggerStopped) Used in the output of several commands
# to explain that the debugger must be opened first before setting breakpoints.
debuggerStopped=Перед установкой точек останова необходимо открыть отладчик

# LOCALIZATION NOTE (breakDesc) A very short string used to describe the
# function of the break command.
breakDesc=Управление точками останова

# LOCALIZATION NOTE (breakManual) A longer description describing the
# set of commands that control breakpoints.
breakManual=Команды отображения, добавления и удаления точек останова

# LOCALIZATION NOTE (breaklistDesc) A very short string used to describe the
# function of the 'break list' command.
breaklistDesc=Показать известные точки останова

# LOCALIZATION NOTE (breaklistNone) Used in the output of the 'break list'
# command to explain that the list is empty.
breaklistNone=Ни одной точки останова не установлено

# LOCALIZATION NOTE (breaklistOutRemove) A title used in the output from the
# 'break list' command on a button which can be used to remove breakpoints
breaklistOutRemove=Удалить

# LOCALIZATION NOTE (breakaddAdded) Used in the output of the 'break add'
# command to explain that a breakpoint was added.
breakaddAdded=Добавленная точка останова

# LOCALIZATION NOTE (breakaddFailed) Used in the output of the 'break add'
# command to explain that a breakpoint could not be added.
breakaddFailed=Не удалось установить точку останова: %S

# LOCALIZATION NOTE (breakaddDesc) A very short string used to describe the
# function of the 'break add' command.
breakaddDesc=Добавить точку останова

# LOCALIZATION NOTE (breakaddManual) A longer description describing the
# set of commands that are responsible for adding breakpoints.
breakaddManual=Поддерживаемые типы точек останова: line

# LOCALIZATION NOTE (breakaddlineDesc) A very short string used to describe the
# function of the 'break add line' command.
breakaddlineDesc=Добавить строку точки останова

# LOCALIZATION NOTE (breakaddlineFileDesc) A very short string used to describe
# the function of the file parameter in the 'break add line' command.
breakaddlineFileDesc=URI файла JS

# LOCALIZATION NOTE (breakaddlineLineDesc) A very short string used to describe
# the function of the line parameter in the 'break add line' command.
breakaddlineLineDesc=Номер строки

# LOCALIZATION NOTE (breakdelDesc) A very short string used to describe the
# function of the 'break del' command.
breakdelDesc=Удалить точку останова

# LOCALIZATION NOTE (breakdelBreakidDesc) A very short string used to describe
# the function of the index parameter in the 'break del' command.
breakdelBreakidDesc=Индекс точки останова

# LOCALIZATION NOTE (breakdelRemoved) Used in the output of the 'break del'
# command to explain that a breakpoint was removed.
breakdelRemoved=Точка останова удалена

# LOCALIZATION NOTE (dbgDesc) A very short string used to describe the
# function of the dbg command.
dbgDesc=Управление отладчиком

# LOCALIZATION NOTE (dbgManual) A longer description describing the
# set of commands that control the debugger.
dbgManual=Команды для прерывания или возобновления главного потока, входа, выхода или перешагивания через строки кода

# LOCALIZATION NOTE (dbgOpen) A very short string used to describe the function
# of the dbg open command.
dbgOpen=Открыть отладчик

# LOCALIZATION NOTE (dbgClose) A very short string used to describe the function
# of the dbg close command.
dbgClose=Закрыть отладчик

# LOCALIZATION NOTE (dbgInterrupt) A very short string used to describe the
# function of the dbg interrupt command.
dbgInterrupt=Приостановить главный поток

# LOCALIZATION NOTE (dbgContinue) A very short string used to describe the
# function of the dbg continue command.
dbgContinue=Возобновить главный поток, и продолжать выполнение после точки останова до следующей точки останова или конца сценария.

# LOCALIZATION NOTE (dbgStepDesc) A very short string used to describe the
# function of the dbg step command.
dbgStepDesc=Управление шагом

# LOCALIZATION NOTE (dbgStepManual) A longer description describing the
# set of commands that control stepping.
dbgStepManual=Команды для входа, выхода или перешагивания через строки кода

# LOCALIZATION NOTE (dbgStepOverDesc) A very short string used to describe the
# function of the dbg step over command.
dbgStepOverDesc=Выполнить текущий оператор, а затем остановиться на следующем операторе. Если текущий оператор является вызовом функции, то отладчик выполняет эту функцию целиком и останавливается на следующем операторе после вызова функции

# LOCALIZATION NOTE (dbgStepInDesc) A very short string used to describe the
# function of the dbg step in command.
dbgStepInDesc=Выполнить текущий оператор, а затем остановиться на следующем операторе. Если текущий оператор является вызовом функции, то отладчик заходит в эту функцию, в противном случае он останавливается на следующем операторе

# LOCALIZATION NOTE (dbgStepOutDesc) A very short string used to describe the
# function of the dbg step out command.
dbgStepOutDesc=Выйти из текущей функции и перейти на уровень выше, если функция является вложенной. При нахождении в основном теле сценарий выполняется до конца или до следующей точки останова. Пропущенные операторы выполняются, но не проходятся

# LOCALIZATION NOTE (dbgListSourcesDesc) A very short string used to describe the
# function of the dbg list command.
dbgListSourcesDesc=Показать URLы исходника загруженного в отладчик

# LOCALIZATION NOTE (dbgBlackBoxDesc) A very short string used to describe the
# function of the 'dbg blackbox' command.
dbgBlackBoxDesc=Поместить исходники в отладчике в чёрный ящик

# LOCALIZATION NOTE (dbgBlackBoxSourceDesc) A very short string used to describe the
# 'source' parameter to the 'dbg blackbox' command.
dbgBlackBoxSourceDesc=Специфичный исходник для помещения в чёрный ящик

# LOCALIZATION NOTE (dbgBlackBoxGlobDesc) A very short string used to describe the
# 'glob' parameter to the 'dbg blackbox' command.
dbgBlackBoxGlobDesc=Поместить в чёрный ящик все исходники подходящие под эту маску (например: «*.min.js»)

# LOCALIZATION NOTE (dbgBlackBoxInvertDesc) A very short string used to describe the
# 'invert' parameter to the 'dbg blackbox' command.
dbgBlackBoxInvertDesc=Обратить соответствие, чтобы мы могли поместить в черный ящик каждый исходник, не являющийся указанным исходником или не подходящий под указанный глобальный шаблон.

# LOCALIZATION NOTE (dbgBlackBoxEmptyDesc) A very short string used to let the
# user know that no sources were black boxed.
dbgBlackBoxEmptyDesc=(В чёрном ящике исходников нет)

# LOCALIZATION NOTE (dbgBlackBoxNonEmptyDesc) A very short string used to let the
# user know which sources were black boxed.
dbgBlackBoxNonEmptyDesc=Следующие исходники помещены в чёрный ящик:

# LOCALIZATION NOTE (dbgBlackBoxErrorDesc) A very short string used to let the
# user know there was an error black boxing a source (whose url follows this
# text).
dbgBlackBoxErrorDesc=Ошибка помещения в чёрный ящик:

# LOCALIZATION NOTE (dbgUnBlackBoxDesc) A very short string used to describe the
# function of the 'dbg unblackbox' command.
dbgUnBlackBoxDesc=Вынуть исходники в отладчике из чёрного ящика

# LOCALIZATION NOTE (dbgUnBlackBoxSourceDesc) A very short string used to describe the
# 'source' parameter to the 'dbg unblackbox' command.
dbgUnBlackBoxSourceDesc=Специфичный исходник для выемки из чёрного ящика

# LOCALIZATION NOTE (dbgUnBlackBoxGlobDesc) A very short string used to describe the
# 'glob' parameter to the 'dbg blackbox' command.
dbgUnBlackBoxGlobDesc=Вынуть из чёрного ящика все исходники подходящие под эту маску (например: «*.min.js»)

# LOCALIZATION NOTE (dbgUnBlackBoxEmptyDesc) A very short string used to let the
# user know that we did not stop black boxing any sources.
dbgUnBlackBoxEmptyDesc=(Исходники из чёрного ящика не вынуты)

# LOCALIZATION NOTE (dbgUnBlackBoxNonEmptyDesc) A very short string used to let the
# user know which sources we stopped black boxing.
dbgUnBlackBoxNonEmptyDesc=Вынул из чёрного ящика следующие исходники:

# LOCALIZATION NOTE (dbgUnBlackBoxErrorDesc) A very short string used to let the
# user know there was an error black boxing a source (whose url follows this
# text).
dbgUnBlackBoxErrorDesc=Ошибка выемки из чёрного ящика:

# LOCALIZATION NOTE (dbgUnBlackBoxInvertDesc) A very short string used to describe the
# 'invert' parameter to the 'dbg unblackbox' command.
dbgUnBlackBoxInvertDesc=Обратить соответствие, чтобы мы могли вынуть из черного ящика каждый исходник, не являющийся указанным исходником или не подходящий под указанный глобальный шаблон.

# LOCALIZATION NOTE (consolecloseDesc) A very short description of the
# 'console close' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
consolecloseDesc=Закрыть консоль

# LOCALIZATION NOTE (consoleopenDesc) A very short description of the
# 'console open' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
consoleopenDesc=Открыть консоль

# LOCALIZATION NOTE (editDesc) A very short description of the 'edit'
# command. See editManual2 for a fuller description of what it does. This
# string is designed to be shown in a menu alongside the command name, which
# is why it should be as short as possible.
editDesc=Правка ресурса страницы

# LOCALIZATION NOTE (editManual2) A fuller description of the 'edit' command,
# displayed when the user asks for help on what it does.
editManual2=Правка одного из ресурсов, являющегося частью этой страницы

# LOCALIZATION NOTE (editResourceDesc) A very short string to describe the
# 'resource' parameter to the 'edit' command, which is displayed in a dialog
# when the user is using this command.
editResourceDesc=URL для правки

# LOCALIZATION NOTE (editLineToJumpToDesc) A very short string to describe the
# 'line' parameter to the 'edit' command, which is displayed in a dialog
# when the user is using this command.
editLineToJumpToDesc=Строка для перехода

# LOCALIZATION NOTE (resizePageArgWidthDesc) A very short string to describe the
# 'width' parameter to the 'resizepage' command, which is displayed in a dialog
# when the user is using this command.
resizePageArgWidthDesc=Ширина в пикселях

# LOCALIZATION NOTE (resizePageArgHeightDesc) A very short string to describe the
# 'height' parameter to the 'resizepage' command, which is displayed in a dialog
# when the user is using this command.
resizePageArgHeightDesc=Высота в пикселях

# LOCALIZATION NOTE (resizeModeOnDesc) A very short string to describe the
# 'resizeon ' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeOnDesc=Войти в режим адаптивного дизайна

# LOCALIZATION NOTE (resizeModeOffDesc) A very short string to describe the
# 'resize off' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeOffDesc=Выйти из режима адаптивного дизайна

# LOCALIZATION NOTE (resizeModeToggleDesc) A very short string to describe the
# 'resize toggle' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeToggleDesc=Переключить режим адаптивного дизайна

# LOCALIZATION NOTE (resizeModeToggleTooltip2) A string displayed as the
# tooltip of button in devtools toolbox which toggles Responsive Design Mode.
# Keyboard shortcut will be shown inside brackets.
resizeModeToggleTooltip2=Режим адаптивного дизайна (%S)

# LOCALIZATION NOTE (resizeModeToDesc) A very short string to describe the
# 'resize to' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeToDesc=Изменить размер страницы

# LOCALIZATION NOTE (resizeModeDesc) A very short string to describe the
# 'resize' command. This string is designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
resizeModeDesc=Управление режимом адаптивного дизайна

# LOCALIZATION NOTE (resizeModeManual2) A fuller description of the 'resize'
# command, displayed when the user asks for help on what it does.
# The argument (%1$S) is the browser name.
resizeModeManual2=Адаптивные веб-сайты реагируют на свое окружение, поэтому они хорошо выглядят на мобильном экране, на киноэкране и на всех промежуточных по размеру экранах. Режим адаптивного дизайна позволяет вам с лёгкостью протестировать различные размеры страниц в %1$S без изменения размера всего браузера.

# LOCALIZATION NOTE (scratchpadOpenTooltip) A string displayed as the
# tooltip of button in devtools toolbox which opens Scratchpad.
scratchpadOpenTooltip=Простой редактор JavaScript

# LOCALIZATION NOTE (paintflashingDesc) A very short string used to describe the
# function of the "paintflashing" command
paintflashingDesc=Подсвечивать прорисованную область

# LOCALIZATION NOTE (paintflashingOnDesc) A very short string used to describe the
# function of the "paintflashing on" command.
paintflashingOnDesc=Включить paint flashing

# LOCALIZATION NOTE (paintflashingOffDesc) A very short string used to describe the
# function of the "paintflashing off" command.
paintflashingOffDesc=Отключить paint flashing

# LOCALIZATION NOTE (paintflashingChromeDesc) A very short string used to describe the
# function of the "paintflashing on/off chrome" command.
paintflashingChromeDesc=фреймы chrome

# LOCALIZATION NOTE (paintflashingManual) A longer description describing the
# set of commands that control paint flashing.
paintflashingManual=Рисовать перерисованные области разными цветами

# LOCALIZATION NOTE (paintflashingTooltip) A string displayed as the
# tooltip of button in devtools toolbox which toggles paint flashing.
paintflashingTooltip=Подсветить прорисованную область

# LOCALIZATION NOTE (paintflashingToggleDesc) A very short string used to describe the
# function of the "paintflashing toggle" command.
paintflashingToggleDesc=Переключить paint flashing

# LOCALIZATION NOTE (splitconsoleTooltip2) A string displayed as the
# tooltip of button in devtools toolbox which toggles the split webconsole.
# Keyboard shortcut will be shown inside brackets.
splitconsoleTooltip2=Показать/скрыть консоль внизу (%S)

# LOCALIZATION NOTE (rulersDesc) A very short description of the
# 'rulers' command. See rulersManual for a fuller description of what
# it does. This string is designed to be shown in a menu alongside the
# command name, which is why it should be as short as possible.
rulersDesc=Показать/скрыть линейки на этой странице

# LOCALIZATION NOTE (rulersManual) A fuller description of the 'rulers'
# command, displayed when the user asks for help on what it does.
rulersManual=Показать/скрыть горизонтальные и вертикальные линейки на текущей странице

# LOCALIZATION NOTE (rulersTooltip) A string displayed as the
# tooltip of button in devtools toolbox which toggles the rulers.
rulersTooltip=Показать/скрыть линейки на этой странице

# LOCALIZATION NOTE (measureDesc) A very short description of the
# 'measure' command. See measureManual for a fuller description of what
# it does. This string is designed to be shown in a menu alongside the
# command name, which is why it should be as short as possible.
measureDesc=Измерить часть страницы

# LOCALIZATION NOTE (measureManual) A fuller description of the 'measure'
# command, displayed when the user asks for help on what it does.
measureManual=Активирует измерительный инструмент для измерения произвольной области страницы

# LOCALIZATION NOTE (measureTooltip) A string displayed as the
# tooltip of button in devtools toolbox which toggles the measuring tool.
measureTooltip=Измерить часть страницы

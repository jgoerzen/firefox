<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Connectar cun &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Per activar tes nov apparat tscherna “Endrizzar &syncBrand.shortName.label;” sin l\&apos;apparat.'>
<!ENTITY sync.subtitle.pair.label 'Per activar tscherna “Associar in apparat” sin l\&apos;auter apparat.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Jau n\&apos;hai betg qua l\&apos;apparat…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Infurmaziuns d\&apos;annunzia'>
<!ENTITY sync.configure.engines.title.history 'Cronologia'>
<!ENTITY sync.configure.engines.title.tabs 'Tabs'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; sin &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menu da segnapaginas'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etichettas'>
<!ENTITY bookmarks.folder.toolbar.label 'Trav da simbols dals segnapaginas'>
<!ENTITY bookmarks.folder.other.label 'Auters segnapaginas'>
<!ENTITY bookmarks.folder.desktop.label 'Segnapaginas dal immobil'>
<!ENTITY bookmarks.folder.mobile.label 'Segnapaginas dal mobil'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Fixads'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Turnar a navigar'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Bainvegni tar &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'T\&apos;annunzia per sincronisar tabs, segnapaginas, infurmaziuns d\&apos;annunzia e dapli.'>
<!ENTITY fxaccount_getting_started_get_started 'Cumenzar'>
<!ENTITY fxaccount_getting_started_old_firefox 'Utiliseschas ti ina versiun pli veglia da &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Server dal conto'>
<!ENTITY fxaccount_status_sync_now 'Sincronisar ussa'>
<!ENTITY fxaccount_status_syncing2 'Sincronisar…'>
<!ENTITY fxaccount_status_device_name 'Num da l\&apos;apparat'>
<!ENTITY fxaccount_status_sync_server 'Server per sincronisar'>
<!ENTITY fxaccount_status_needs_verification2 'Tes conto sto vegnir verifitgà. Tutga per trametter danovamain l\&apos;e-mail da verificaziun.'>
<!ENTITY fxaccount_status_needs_credentials 'Impussibel da connectar. Tutgar per s\&apos;annunziar.'>
<!ENTITY fxaccount_status_needs_upgrade 'Ti stos actualisar &brandShortName; per s\&apos;annunziar.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; è endrizzà ma betg configurà per sincronisar automaticamain. Activescha “Sincronisar automaticamain las datas” en las preferenzas dad Android sut Utilisaziun da datas.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; è endrizzà ma betg configurà per sincronisar automaticamain. Activescha “Sincronisar automaticamain las datas” en il menu da las preferenzas dad Android sut Contos.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Smatga per t\&apos;annunziar cun tes nov conto da Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Tscherna tge che duai vegnir sincronisà'>
<!ENTITY fxaccount_status_bookmarks 'Segnapaginas'>
<!ENTITY fxaccount_status_history 'Cronologia'>
<!ENTITY fxaccount_status_passwords2 'Infurmaziuns d\&apos;annunzia'>
<!ENTITY fxaccount_status_tabs 'Tabs averts'>
<!ENTITY fxaccount_status_additional_settings 'Parameters supplementars'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sincronisar mo via WLAN'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Impedir la sincronisaziun da &brandShortName; en la rait mobila'>
<!ENTITY fxaccount_status_legal 'Indicaziuns giuridicas' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Cundiziuns d\&apos;utilisaziun'>
<!ENTITY fxaccount_status_linkprivacy2 'Infurmaziuns davart la protecziun da datas'>
<!ENTITY fxaccount_remove_account 'Deconnectar&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Deconnectar da Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Las datas da navigaziun restan sin quest apparat, ma ellas na vegnan betg pli sincronisadas cun tes conto.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Deconnectà il conto da Firefox &formatS;.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Deconnectar'>

<!ENTITY fxaccount_enable_debug_mode 'Activar il modus per debugar'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Opziuns da &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Configurar &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; n\&apos;è betg connectà'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tutgar per s\&apos;annunziar sco &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Terminar l\&apos;actualisaziun da &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Tutgar per s\&apos;annunziar sco &formatS;'>

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Tscherna la lingua da menus, messadis ed avis da { -brand-short-name }.
manage-messenger-languages-button =
    .label = Tscherner las alternativas…
    .accesskey = l
confirm-messenger-language-change-description = Reaviar { -brand-short-name } per applitgar questas midadas
confirm-messenger-language-change-button = Applitgar e reaviar

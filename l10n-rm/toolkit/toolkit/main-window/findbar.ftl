# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Tschertgar la proxima posiziun da l'expressiun
findbar-previous =
    .tooltiptext = Tschertgar la posiziun precedenta da l'expressiun
findbar-find-button-close =
    .tooltiptext = Serrar la trav da tschertgar
findbar-highlight-all =
    .label = Relevar tut
    .accesskey = t
    .tooltiptext = Relevar il term dapertut nua ch'el cumpara
findbar-case-sensitive =
    .label = Resguardar maiusclas/minusclas
    .accesskey = r
    .tooltiptext = Tschertgar cun resguardar maiusclas/minusclas
findbar-entire-word =
    .label = Pleds entirs
    .accesskey = P
    .tooltiptext = Be tschertgar entirs pleds

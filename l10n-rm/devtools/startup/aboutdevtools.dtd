<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Davart ils utensils per sviluppaders">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Activar ils utensils per sviluppaders da Firefox">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Activar ils utensils per sviluppaders da Firefox per utilisar «Inspectar l'element»">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Examinar e modifitgar HTML e CSS cun l'inspectur dals utensils per sviluppaders.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Sviluppar e debugar WebExtensions, web workers, service workers e dapli cun ils utensils per sviluppaders da Firefox.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Ti has activà ina scursanida da tasta dals utensils per sviluppaders. Sche quai è capità per sbagl, pos ti serrar quest tab.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Perfecziunescha HTML, CSS e JavaScript da tia website cun utensils sco l'inspectur u il debugadi.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Ils utensils per sviluppaders da Firefox èn deactivads tenor standard per che ti possias controllar meglier tes navigatur.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Vegnir a savair dapli davart ils utensils per sviluppaders">

<!ENTITY  aboutDevtools.enable.enableButton "Activar ils utensils per sviluppaders">
<!ENTITY  aboutDevtools.enable.closeButton2 "Serrar quest tab">

<!ENTITY  aboutDevtools.welcome.title "Bainvegni tar ils utensils per sviluppaders da Firefox!">

<!ENTITY  aboutDevtools.newsletter.title "Mozilla Developer Newsletter">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Retschaiva novitads per sviluppaders, trics e resursas directamain en tia chascha da posta entrada dad e-mail.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-mail">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Jau sun d'accord che Mozilla tracta mias datas tenor questas <a class='external' href='https://www.mozilla.org/privacy/'>reglas da la protecziun da datas</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Abunar">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Grazia!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Sche ti n'has anc mai abunà in newsletter da Mozilla, stos ti confermar l'abunament. Controllescha per plaschair sche tia posta entrada u tes filter da posta nungiavischada cuntegnia in e-mail da nus.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Tschertgas ti dapli che mo ils utensils per sviluppaders? Emprova il navigatur da Firefox creà aposta per sviluppaders e process da lavur moderns.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Ulteriuras infurmaziuns">


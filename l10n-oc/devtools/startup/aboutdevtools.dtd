<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "A prepaus de las aisinas de desvolopament">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Activar las aisinas de desvolopament de Firefox">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Activatz las aisinas de desvolopament per utilizar Examinar l’element">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Examinatz e modificatz lo contengut HTML e CSS amb l’inspector de las aisinas de desvolopament.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Desvolopatz e desbugatz de WebExtensions, web workers, servicis workers e mai amb las aisinas de desvolopament de Firfox.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Avètz activat un acorchi cap a las aisinas de desvolopament. Se’s una error, podètz tampar aqueste onglet.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Examinatz, modificatz e desbugatz lo contengut HTML, CSS e JavaScript gràcias a las aisinas coma l’inspector e lo desbugador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Perfeccionatz l’HTML, CSS e JavaScript de vòstre site amb d’aisinas coma l’inspector e lo desbugador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Las aisinas de desvolopament de Firefox son desactivadas per defaut per dire de vos permetre de contrarotlar mai vòstre navegador.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Ne saber mai sus las aisinas pels devolopaires">

<!ENTITY  aboutDevtools.enable.enableButton "Activar las aisinas pels desvolopaires">
<!ENTITY  aboutDevtools.enable.closeButton "Tampar aquesta pagina">

<!ENTITY  aboutDevtools.enable.closeButton2 "Tampar aqueste onglet">

<!ENTITY  aboutDevtools.welcome.title "Benvenguda a las aisinas de desvolopament de Firefox !">

<!ENTITY  aboutDevtools.newsletter.title "Letra de ligason de Mozilla pels desvolopaires">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Recebètz de novetats, d’astúcias e de resorgas pels desvolopaires, dirèctament per corrièl.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "Corrièl">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Accepti que Mozila utilize mas informacion segon sa <a class='external' href='https://www.mozilla.org/privacy/'>politica de confidencialitat</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Se marcar">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Mercés plan !">
<!ENTITY  aboutDevtools.newsletter.thanks.message "S’avètz pas ja confirmat un abonament a una letra de ligason de Mozilla, benlèu que o vos cal far. Verificatz vòstra bóstia de recepcion o corrièls pas desirables e cercatz un messatge nòstre.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Cercatz mai que d’aisinas de desvolopament ? Descobrissètz lo navegador Firefox fach especialament pels desvolopaires e los flux laborals del monde modèrn.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Ne saber mai">


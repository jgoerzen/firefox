# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Lengas
    .style = width: 30em
webpage-languages-window =
    .title = Paramètres de lenga de la pagina
    .style = width: 40em
languages-close-key =
    .key = w
languages-description = De còps las paginas web son dins mai d'una lenga. Causissètz las lengas d'afichatge d'aquestas paginas web, per òrdre de preferéncia
languages-customize-spoof-english =
    .label = Demandar las versions en anglés de las paginas web per mai de confidencialitat
languages-customize-moveup =
    .label = Montar
    .accesskey = M
languages-customize-movedown =
    .label = Davalar
    .accesskey = D
languages-customize-remove =
    .label = Suprimir
    .accesskey = S
languages-customize-select-language =
    .placeholder = Seleccionatz una lenga mai…
languages-customize-add =
    .label = Apondre
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
browser-languages-window =
    .title = Paramètres de lenga de { -brand-short-name }
    .style = width: 40em

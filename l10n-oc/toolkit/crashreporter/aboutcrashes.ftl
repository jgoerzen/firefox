# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Rapòrts de plantatge
clear-all-reports-label = Suprimir totes los rapòrts
delete-confirm-title = Sètz segur ?
delete-confirm-description = Aquò suprimirà totes los rapòrts e serà impossible de l'annular.
crashes-unsubmitted-label = Rapòrts de plantatge en espèra de mandadís
id-heading = Identificant del rapòrt
date-crashed-heading = Data del plantatge
crashes-submitted-label = Rapòrts de plantatge mandats
date-submitted-heading = Data de somission
no-reports-label = Cap de rapòrt de plantatge es pas estat mandat.
no-config-label = Aquesta aplicacion es pas configurada per afichar los rapòrts de plantatge. Vos cal definir la variabla <code>breakpad.reportURL</code>
